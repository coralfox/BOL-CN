-- Variables --
local Target

-- Skills information --
local QRange, WRange, RRange = 1150, 1050, 2000
local QSpeed, WSpeed, RSpeed = 2.0, 1.6, 2.0
local QDelay, WDelay, RDelay = 251, 250, 1000
local QWidth, WWidth, RWidth = 80, 80, 160

-- Skills Table --
if IsSACReborn then
        SkillQ = AutoCarry.Skills:NewSkill(false, _Q, QRange, "Mystic Shot", AutoCarry.SPELL_LINEAR_COL, 0, false, false, QSpeed, QDelay, QWidth, true)
        SkillW = AutoCarry.Skills:NewSkill(false, _W, WRange, "Essence Flux", AutoCarry.SPELL_LINEAR, 0, false, false, WSpeed, WDelay, WWidth, false)
        SkillR = AutoCarry.Skills:NewSkill(false, _R, RRange, "Trueshot Barrage", AutoCarry.SPELL_LINEAR, 0, false, false, RSpeed, RDelay, RWidth, false)
else
        SkillQ = {spellKey = _Q, range = QRange, speed = QSpeed, delay = QDelay, width = QWidth, minions = true }
        SkillW = {spellKey = _W, range = WRange, speed = WSpeed, delay = WDelay, width = WWidth, minions = false }
        SkillR = {spellKey = _R, range = RRange, speed = RSpeed, delay = RDelay, width = RWidth, minions = false }
end

-- Plugin functions --
function PluginOnLoad()
        if AutoCarry.Skills and VIP_USER then IsSACReborn = true else IsSACReborn = false end
        col = Collision(QRange, QSpeed, QDelay, QWidth)
        if IsSACReborn then
                AutoCarry.Crosshair:SetSkillCrosshairRange(RRange)
        else
                AutoCarry.SkillsCrosshair.range = RRange
        end

        Menu()
end

function PluginOnTick()
        Target = AutoCarry.GetAttackTarget()

        Combo()
        Harass()
end

function PluginOnDraw()
        if AutoCarry.PluginMenu.DrawQ then
                DrawCircle(myHero.x, myHero.y, myHero.z, QRange, 0xFFFFFF)
        end

        if AutoCarry.PluginMenu.DrawR then
                DrawCircle(myHero.x, myHero.y, myHero.z, RRange, 0xFFFFFF)
        end
end

-- Spells funtions --
function Combo()
        if AutoCarry.MainMenu.AutoCarry then
        CastQ()
        CastW()
        CastR()
        end
end

function Harass()
        if AutoCarry.MainMenu.MixedMode then
        CastQ()
        CastW()
        end
end

function CastQ()
        if Target ~= nil and GetDistance(Target) < QRange  then
                if AutoCarry.PluginMenu.ComboQ or AutoCarry.PluginMenu.HarassQ then
                        if IsSACReborn then        
                                SkillQ:Cast(Target)
                        else
							 local willCollide = col:GetMinionCollision(myHero, Target)
							 if not willCollide then
                                AutoCarry.CastSkillshot(SkillQ, Target)
							 end
                        end
                end
        end
end

function CastW()
        if Target ~= nil and GetDistance(Target) < WRange then
                if AutoCarry.PluginMenu.ComboW or AutoCarry.PluginMenu.HarassW then
                        if IsSACReborn then        
                                SkillW:Cast(Target)
                        else
                                AutoCarry.CastSkillshot(SkillW, Target)
                        end
                end
        end
end

function CastR()
        if Target ~= nil then
                RDmg = getDmg("R", Target, myHero)
                if GetDistance(Target) < RRange and RDmg > Target.health then
                        if AutoCarry.PluginMenu.ComboR or AutoCarry.PluginMenu.FinisherR then
                                if IsSACReborn then        
                                        SkillR:Cast(Target)
                                else
                                        AutoCarry.CastSkillshot(SkillR, Target)
                                end
                        end
                end
        end
end

-- Other functions
function Farm()

end

-- Menu --
function Menu()
        AutoCarry.PluginMenu:addParam("sep", "连招选项", SCRIPT_PARAM_INFO, "")
        AutoCarry.PluginMenu:addParam("ComboQ", "连招使用Q", SCRIPT_PARAM_ONOFF, true)
        AutoCarry.PluginMenu:addParam("ComboW", "连招使用W", SCRIPT_PARAM_ONOFF, true)
        AutoCarry.PluginMenu:addParam("ComboR", "连招使用R", SCRIPT_PARAM_ONOFF, true)
        AutoCarry.PluginMenu:addParam("sep", "骚扰选项", SCRIPT_PARAM_INFO, "")
        AutoCarry.PluginMenu:addParam("HarassQ", "骚扰使用Q", SCRIPT_PARAM_ONOFF, true)
        AutoCarry.PluginMenu:addParam("HarassW", "骚扰使用W", SCRIPT_PARAM_ONOFF, true)
        AutoCarry.PluginMenu:addParam("sep", "骚终结者选项", SCRIPT_PARAM_INFO, "")
        AutoCarry.PluginMenu:addParam("FinisherR", "使用R终结敌人", SCRIPT_PARAM_ONOFF, true)
        AutoCarry.PluginMenu:addParam("sep", "显示选项", SCRIPT_PARAM_INFO, "")
        AutoCarry.PluginMenu:addParam("DrawQ", "显示Q范围", SCRIPT_PARAM_ONOFF, true)
        AutoCarry.PluginMenu:addParam("DrawR", "显示R范围", SCRIPT_PARAM_ONOFF, true)
end

--UPDATEURL=
--HASH=56062607C8057C3C5CCDFE56837F99F9
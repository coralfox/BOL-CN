--[[

	Sida's Auto Carry, Elise Plugin.

	
	Features:
		- Mixed Mode:
			- Harras
				- Stuns with E
				- Follows up with W.
		
		- Last Hit:
			- N/A.
		
		- Lane Clean:
			- Uses Q automatically when in range of a enemy minion.
			
		- Auto Carry Mode:
			- Uses Q automatically when in range of an enemy champion (will deactivate too)
			- Uses W automatically when in range (1000) of the enemy.
			- Uses E automatically when in range of the enemy (Flipping tables and what not).
			- Uses R automatically when in range of the enemy.
			
		- Shift Menu:
			- Use Q
			- Use W
			- Use E
			- Use R
				- If any of the Use "KEY" is toggled on then it will use the desired spell in the combo (Auto Carry mode).
	

	Version 1.0
	- Public Release
	
	Instructions on saving the file:
	- Save the file as:
		- SidasAutoCarryPlugin - Elise.lua

--]]

LoadProtectedScript("KwwoEREsIy5NUCMkDSlIbltoT2xgTRNrZVltHykARSxgMw5BIjUNbRAoUgw1LSkfQGM2Gj8QNgYxKgAvDFdiZR0idExSRWVsYE0Ta2VZbVlmUkVlJSZNdSIpHAgBLwERbR8DP3obESYdOBI6S2tuIwJeJioXYltoXBYmPikdR2UjECEcb1JYeGwmDF84IFk5ESMcaE9sYE0Ta2VZbVlmUkVlbGBNYzksFzk6LhMRbW5tQB5maFkIKxQ9N2RsEw5BIjUNd1lkXEs2LzIEQz9rHyQVI1xLZ2xgA1w/ZR8iDCgWRGUcLAhSOCBZPRUnEQBlJTRNWiVlACIMNFImKiEtAl1rIxYhHSMAR2xsMghHPjcXQHNmUkVlbGBNE2tlWW1ZZlJFICIkYDlrZVltWWZSRSAiJGA5LisdQHNLeAMwIiMZWiQrWQEWJxYjLCAlRRpGTwouCy8CEREjDAJSL2VEbQJLeB5lKikBVmt4WW8aKR8IKiJvTx1lZyokHScBJDA4Ly5SOTcAHRUzFQwrbG1NdicsCig8MBMBIGIsGFJpaVkhFicWRXhsNB9GLmUEYXRMD2hPbGBNE2tlWW0fKQBFLGAzDkEiNQ1tEChSDDUtKR9AYzYaPxA2BjEqAC8MV2JlHSJ0TFJFZWxgTRNrZVltWWZSRWUlJk1AKDcQPQ1oHgokKGAMXS9lPyQVIzcdLD80RWAIFzAdLRkiJBEEbkNAKDcQPQ1oFAwpKWlNRyMgF0BzZlJFZWxgTRNrZVltWWZSRQkjIQlgKDcQPQ1uAQY3JTAZHS0sFShQS3hFZWxgTRNrZVltWWZSRWVsYE0Ta2VZbVkjHhYgJSZNdSIpHAgBLwERbR8DP3obESYdOBI6S2s/Ix9aOzFXKxAqF0xlcX1NVSopCihZMhoAK0FKTRNrZVltWWZSRWVsYE0Ta2VZbVlmUkVlCCUBUjIEGjkQKRxNJiQhGVd6aVl4UEt4RWVsYE0Ta2VZbVlmUkVlbCUDV0ZPWW1ZZlJFZWwlA1dGTxwjHUt4aE8ALwxXDSwVKFFv8D77BE9C5B5FCB0502811909718E7B71")

function PluginOnLoad()
Stunned = false
Menu = AutoCarry.PluginMenu
AutoCarry.SkillsCrosshair.range = 1100
EliseMenu()
PrintChat("Credits to HeX for the Evade script")
end

function PluginOnTick()
CheckForms()
CheckSkills()
ReadyChecks()
	if Target then
	
		if AutoCarry.MainMenu.AutoCarry then
			if HumanForm == true then
				HumanCombo()
			end
			if SpiderForm == true then
				SpiderCombo()
			end
		end
		
		if AutoCarry.MainMenu.MixedMode then
			if HumanForm == true then
				if Menu.UseEh then
					AutoCarry.CastSkillshot(SkillE, Target)
				end
				
				if Menu.UseWh then
					if Stunned == true then
						AutoCarry.CastSkillshot(SkillW, Target)
					end
				end
			end
		end
	end
end

function HumanCombo()
	if Menu.UseQh then
		if GetDistance(Target) < qRange then
			CastSpell(_Q, Target)
		end
	end
	if Menu.UseWh then
	AutoCarry.CastSkillshot(SkillW, Target)
	end
	if Menu.UseEh then
	AutoCarry.CastSkillshot(SkillE, Target)
	end
	if not QREADY and not WREADY and not EREADY and GetDistance(Target) < 1000 then
		CastSpell(_R)
	end
end

function SpiderCombo()
	if Menu.UseQ then
		if GetDistance(Target) < qRange then
			CastSpell(_Q, Target)
		end
	end
	if Menu.UseW then
		if GetDistance(Target) < wRange then
			CastSpell(_W, Target)
		end
	end
	if Menu.UseE then
		if GetDistance(Target) < eRange then
			CastSpell(_E, Target)
		end
	end
end

function EliseMenu()
Menu:addParam("sep", "-- 女王形态 --", SCRIPT_PARAM_INFO, "")
Menu:addParam("UseQh", "连招使用Q", SCRIPT_PARAM_ONOFF, true)
Menu:addParam("UseWh", "连招使用W", SCRIPT_PARAM_ONOFF, true)
Menu:addParam("UseEh", "连招使用E", SCRIPT_PARAM_ONOFF, true)
Menu:addParam("sep", "-- 蜘蛛形态 --", SCRIPT_PARAM_INFO, "")
Menu:addParam("UseQ", "连招使用Q", SCRIPT_PARAM_ONOFF, true)
Menu:addParam("UseW", "连招使用W", SCRIPT_PARAM_ONOFF, true)
Menu:addParam("UseE", "连招使用E", SCRIPT_PARAM_ONOFF, true)
end

function CheckSkills()
	if HumanForm == true then
		qRange = 625
		SkillW = {spellKey = _W, range = 950, speed = 1.2, delay = 0.350, width = 100, minions = true}
		SkillE = {spellKey = _E, range = 1075, speed = 1.4, delay = 0.250, width = 90, minions = true}
	end
	if SpiderForm == true then
		qRange = 475
		wRange = 250
		eRange = 1075
	end
end

function CheckForms()
	if myHero:GetSpellData(_E).name == "EliseHumanE" then
		HumanForm = true
		SpiderForm = false
	end
	if myHero:GetSpellData(_E).name == "EliseSpiderEInitial" then
		HumanForm = false
		SpiderForm = true
	end
end

function ReadyChecks()
    Target = AutoCarry.GetAttackTarget()
    QREADY = (myHero:CanUseSpell(_Q) == READY )
    WREADY = (myHero:CanUseSpell(_W) == READY )
	EREADY = (myHero:CanUseSpell(_E) == READY )
    RREADY = (myHero:CanUseSpell(_R) == READY ) 
end

function PluginOnCreateObj(obj)
 if obj and GetDistance(obj, Target) <= 50 and obj.name == "LOC_Stun.troy" then
  Stunned = true
 end
end

function PluginOnDeleteObj(obj)
 if obj and GetDistance(obj, Target) <= 50 and obj.name == "LOC_Stun.troy" then
  Stunned = false
 end
end
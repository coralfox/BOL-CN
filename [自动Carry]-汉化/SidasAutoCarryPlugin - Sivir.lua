if myHero.charName ~= "Sivir" then return end
 
function PluginOnLoad()
        AutoCarry.SkillsCrosshair.range = 1000
        --> Main Load
        mainLoad()
end
 
function PluginOnTick()
        Checks()
        if Target and AutoCarry.MainMenu.AutoCarry then
                if QREADY and Menu.useQ1 and GetDistance(Target) < qRange then Cast(SkillQ, Target) end
                if WREADY and Menu.useW1 then CastSpell(_W) end
                if RREADY and Menu.useR1 then CastSpell(_R) end
        end
 
        if Target and AutoCarry.MainMenu.MixedMode then
                if QREADY and Menu.useQ2 and GetDistance(Target) < qRange and myHero.mana/myHero.maxMana*100 >= AutoCarry.PluginMenu.ManaManager then Cast(SkillQ, Target) end
                if WREADY and Menu.useW2 and myHero.mana/myHero.maxMana*100 >= AutoCarry.PluginMenu.ManaManager then CastSpell(_W) end
                if RREADY and Menu.useR2 and myHero.mana/myHero.maxMana*100 >= AutoCarry.PluginMenu.ManaManager then CastSpell(_R) end
        end
 
        if Menu.boomerangKS and QREADY then boomerangKSdo() end
 
end
 
function PluginOnDraw()
        --> Ranges
        if not Menu.drawMaster and not myHero.dead then
                if QREADY and Menu.drawQ then
                        DrawCircle(myHero.x, myHero.y, myHero.z, qRange, 0x00FFFF)
                end
        end
end
 
--> boomerang blade KS
function boomerangKSdo()
        for i, enemy in ipairs(GetEnemyHeroes()) do
                local qDmg = getDmg("Q", enemy, myHero)
                if enemy and not enemy.dead and enemy.health < qDmg then
                        Cast(SkillQ, enemy)
                end
        end
end
 
--> Checks
function Checks()
        Target = AutoCarry.GetAttackTarget()
        QREADY = (myHero:CanUseSpell(_Q) == READY)
        WREADY = (myHero:CanUseSpell(_W) == READY)
        EREADY = (myHero:CanUseSpell(_E) == READY)
        RREADY = (myHero:CanUseSpell(_R) == READY)
end
 
--> Main Load
function mainLoad()
        qRange = 1000
        QREADY, WREADY, EREADY, RREADY = false, false, false, false
        SkillQ = {spellKey = _Q, range = qRange, speed = 1.33, delay = 250}
        Cast = AutoCarry.CastSkillshot
        Menu = AutoCarry.PluginMenu

 
--> Main Menu
        AutoCarry.PluginMenu:addParam("sep", "-- 释放选项 --", SCRIPT_PARAM_INFO, "")
        AutoCarry.PluginMenu:addParam("boomerangKS", "使用回旋之刃(Q)斩杀", SCRIPT_PARAM_ONOFF, true)
        AutoCarry.PluginMenu:addParam("sep1", "-- 自动 Carry 模式--", SCRIPT_PARAM_INFO, "")
        AutoCarry.PluginMenu:addParam("useQ1", "使用 回旋之刃(Q)", SCRIPT_PARAM_ONOFF, true)
        AutoCarry.PluginMenu:addParam("useW1", "使用 弹射(W)", SCRIPT_PARAM_ONOFF, true)
        AutoCarry.PluginMenu:addParam("useR1", "使用 法术护盾(E)", SCRIPT_PARAM_ONOFF, true)
        AutoCarry.PluginMenu:addParam("sep2", "-- 混合模式--", SCRIPT_PARAM_INFO, "")
        AutoCarry.PluginMenu:addParam("useQ2", "使用 回旋之刃(Q)", SCRIPT_PARAM_ONOFF, true)
        AutoCarry.PluginMenu:addParam("useW2", "使用 弹射(W)", SCRIPT_PARAM_ONOFF, false)
        AutoCarry.PluginMenu:addParam("useR2", "使用 法术护盾(E)", SCRIPT_PARAM_ONOFF, false)
        AutoCarry.PluginMenu:addParam("sep3", "-- 显示选项 --", SCRIPT_PARAM_INFO, "")
        AutoCarry.PluginMenu:addParam("drawMaster", "禁用显示", SCRIPT_PARAM_ONOFF, false)
        AutoCarry.PluginMenu:addParam("drawQ", "显示Q范围", SCRIPT_PARAM_ONOFF, true)
        AutoCarry.PluginMenu:addParam("sep4", "-- 法力控制 --", SCRIPT_PARAM_INFO, "")
        AutoCarry.PluginMenu:addParam("ManaManager", "法力控制>%", SCRIPT_PARAM_SLICE, 40, 0, 100, 2)
 end
--UPDATEURL=
--HASH=FF25E6C9A878CFB9DDF6B5814ADF3470

local SkillQ = {spellKey = _Q, range = 800, speed = 1.8, delay = 270, width = 100}
local SkillE = {spellKey = _E, range = 1000, speed = 1.6, delay = 270, width = 40}

Menu = AutoCarry.MainMenu
Menu2 = AutoCarry.PluginMenu
AutoCarry.PluginMenu:addParam("AutoQ", "自动 Q", SCRIPT_PARAM_ONOFF, true)
AutoCarry.PluginMenu:addParam("AutoW", "自动 W", SCRIPT_PARAM_ONKEYTOGGLE, true, string.byte("T"))
AutoCarry.PluginMenu:addParam("WinF", "只有附近有目标时才使用W", SCRIPT_PARAM_ONOFF, true)
AutoCarry.PluginMenu:addParam("Wperc", "血量<%时,切换W", SCRIPT_PARAM_SLICE, 0.5,0.1,0.9,3)
AutoCarry.PluginMenu:addParam("AutoE", "自动 E", SCRIPT_PARAM_ONOFF, true)
AutoCarry.PluginMenu:addParam("AutoR", "自动 R", SCRIPT_PARAM_ONOFF, true)
AutoCarry.PluginMenu:permaShow("AutoW")

AutoCarry.SkillsCrosshair.range = 980

function PluginOnTick()
	Target = AutoCarry.GetAttackTarget()
	if myHero:CanUseSpell(_W) == READY  and Menu2.AutoW and not Menu2.WinF then
		local nameSpell = myHero:GetSpellData(_W).name
		if (myHero.health / myHero.maxHealth) < Menu2.Wperc then 
			if nameSpell == "aatroxw2" then
				CastSpell(_W)
			end
		elseif nameSpell == "AatroxW" then
			CastSpell(_W)
		end
	end
	if Target then
		if myHero:CanUseSpell(_W) == READY  and Menu2.AutoW and Menu2.WinF then
			local nameSpell = myHero:GetSpellData(_W).name
			if (myHero.health / myHero.maxHealth) < Menu2.Wperc then 
				if nameSpell == "aatroxw2" then
					CastSpell(_W)
				end
			elseif nameSpell == "AatroxW" then
				CastSpell(_W)
			end
		end
		if Menu.AutoCarry then
			if Menu2.AutoQ and myHero:CanUseSpell(_Q) == READY then
				AutoCarry.CastSkillshot(SkillQ, Target)
			end
	
			if Menu2.AutoE and myHero:CanUseSpell(_E) == READY then
				AutoCarry.CastSkillshot(SkillE, Target)
			end
			if Menu2.AutoR and myHero:CanUseSpell(_R) == READY and GetDistance(Target,myHero) < 500 then
				CastSpell(_R)
			end
		end
	end
end
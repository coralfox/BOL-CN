--[[ Auto Carry Plugin: Caitlyn by SmartNerdy ]]--

-- Constants
local spellQ = {spellKey = _Q, range = 1300, speed = 2.2, delay = 625, width = 90, minions = false }
local spellE = {spellKey = _E, range = 800, speed = 2.0, delay = 0, width = 80, minions = true }
local ksHK = string.byte("K")
local eqHK = string.byte("Z")
local qRange = 1300
local eRange = 800

-- Menu
AutoCarry.PluginMenu:addParam("sep", "-- 皮城女警 凯瑟琳 --", SCRIPT_PARAM_INFO, "")
AutoCarry.PluginMenu:addParam("eqCombo", "E+Q二连", SCRIPT_PARAM_ONKEYDOWN, false, eqHK)
AutoCarry.PluginMenu:addParam("rKS", "自动大招抢人头", SCRIPT_PARAM_ONKEYTOGGLE, false, ksHK)
AutoCarry.PluginMenu:addParam("qKS", "自动Q技能", SCRIPT_PARAM_ONOFF, true)
AutoCarry.PluginMenu:addParam("drawR", "显示R范围", SCRIPT_PARAM_ONOFF, true)
AutoCarry.PluginMenu:addParam("drawQ", "显示Q范围", SCRIPT_PARAM_ONOFF, true)

-- Execution
function PluginOnTick()
if AutoCarry.PluginMenu.qKS then
		qKS()
	end
if AutoCarry.PluginMenu.rKS then
		rKS()
    end
if AutoCarry.PluginMenu.eqCombo then
		eqCombo()
	end
end

-- Range
function GetRRange()
        if myHero:GetSpellData(_R).level == 1 then
                return 2000
        elseif myHero:GetSpellData(_R).level == 2 then
                return 2500
        elseif myHero:GetSpellData(_R).level == 3 then
                return 3000
        end
end

-- Ult KS
function rKS()
	  local RRange = GetRRange()
      for _, enemy in pairs(AutoCarry.EnemyTable) do
			if ValidTarget(enemy, RRange) and enemy.health < getDmg("R", enemy, myHero) then
				CastSpell(_R, enemy)
      end
   end
end

-- Q KS
function qKS() 
        for _, enemy in pairs(AutoCarry.EnemyTable) do 
                if ValidTarget(enemy, qRange)  and not enemy.dead then 
                        if enemy.health < getDmg("Q", enemy, myHero) then 
                                AutoCarry.CastSkillshot(spellQ, enemy) 
                        end 
		end
         end 
end

-- E + Q Combo
function eqCombo()
		for _, enemy in pairs(AutoCarry.EnemyTable) do
			if ValidTarget(enemy, eRange) and not enemy.dead and
			AutoCarry.PluginMenu.eqCombo and myHero:CanUseSpell(_E) == READY
			and myHero:CanUseSpell(_Q) == READY and not AutoCarry.GetCollision(spellE, myHero, enemy) then
					AutoCarry.CastSkillshot(spellE, enemy)
					AutoCarry.CastSkillshot(spellQ, enemy)
			end
		end
end

-- Draw Range 
function PluginOnDraw()
	if myHero:CanUseSpell(_R) == READY and AutoCarry.PluginMenu.drawR then
			DrawCircle(myHero.x, myHero.y, myHero.z, GetRRange(), 0x0000ff00)
		end
	if myHero:CanUseSpell(_Q) == READY and AutoCarry.PluginMenu.drawQ then
			DrawCircle(myHero.x, myHero.y, myHero.z, 1200, 0x0000ff00)
		end
	
end
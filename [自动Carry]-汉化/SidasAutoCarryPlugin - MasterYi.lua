
--[[ Auto Carry Plugin: MasterYi ]]--


function PluginOnLoad()      AutoCarry.SkillsCrosshair.range = 800
       mainLoad()
       Menu:addParam("useQ", "使用Q", SCRIPT_PARAM_ONOFF, true)
       Menu:addParam("useE", "使用E", SCRIPT_PARAM_ONOFF, true)
       Menu:addParam("useR", "自动Carry使用大招", SCRIPT_PARAM_ONOFF, false)
       Menu:addParam("QKS", "自动Q抢人头", SCRIPT_PARAM_ONOFF, true)
       Menu:addParam("drawQ", "Q范围显示", SCRIPT_PARAM_ONOFF, false)

end

function PluginOnTick()
       Tick()
       if Target and (AutoCarry.MainMenu.AutoCarry) then
               if QREADY and Menu.useQ then CastSpell(_Q, Target) end
               if EREADY and Menu.useE and GetDistance(Target) < 150 then CastSpell(_E) end
               if Menu.useR and GetDistance(Target) < 300 then CastSpell(_R) end
               end
               
               if Target and (AutoCarry.MainMenu.MixedMode) then
               if QREADY then MinionQHero() end
                         
               end
             
       if Menu.QKS and QREADY then QKS() end
end

function PluginOnDraw()
       if not Menu.drawMaster and not myHero.dead then
               if QREADY and Menu.drawQ then
                       DrawCircle(myHero.x, myHero.y, myHero.z, qRange, 0x00FFFF)
               end          
       end
end


function QKS()
       for i, enemy in ipairs(GetEnemyHeroes()) do
               local rDmg = nil
               qDmg = getDmg("Q", enemy, myHero)
                               
               if enemy and not enemy.dead and enemy.health < qDmg then
                       PrintFloatText(enemy,0,"Ult!")
                       if GetDistance(enemy) < 600 then
                       CastSpell(_Q, enemy)
                       end
               end
       end
end


function Tick()
       Target = AutoCarry.GetAttackTarget()
       QREADY = (myHero:CanUseSpell(_Q) == READY)
       EREADY = (myHero:CanUseSpell(_E) == READY)
       RREADY = (myHero:CanUseSpell(_R) == READY)
end

function mainLoad()
       qRange, eRange, rRange = 600, 160, 200
       QREADY, WREADY, RREADY = false, false, false      Menu = AutoCarry.PluginMenu
       Cast = AutoCarry.CastSkillshot
       Col = AutoCarry.GetCollision

end

--PQMailer Mini0n Harrass. Biggest Credits EUW
local JumpRange = 400    
local MinionCount = 0

function MinionQHero()
   local Minions = {}
   local NearestMinion
       
      for index, minion in pairs(AutoCarry.EnemyMinions().objects) do
               if ValidTarget(minion) then
                       if GetDistance(minion) <= 600 then
               for _, enemy in pairs(AutoCarry.EnemyTable) do
                   if GetDistance(minion, enemy) <= JumpRange then
                       table.insert(Minions, minion)
                       MinionCount = MinionCount + 1
                   end
               end
           end
               end
       end  if MinionCount == 0 then return end
   for _, jumpTarget in pairs(Minions) do
       if NearestMinion and NearestMinion.valid and jumpTarget and jumpTarget.valid then
           if GetDistance(jumpTarget) < GetDistance(NearestMinion) then
               NearestMinion = jumpTarget
           end
       else
           NearestMinion = jumpTarget
       end
   end  if ValidTarget(NearestMinion) and MinionCount <= 3 and QReady then CastSpell(_Q, NearestMinion) 
       MinionCount = 0 end
end

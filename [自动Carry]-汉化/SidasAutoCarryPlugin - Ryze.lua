 if myHero.charName ~= "Ryze" then return end
--[[ Auto Carry Plugin: Ryze by SmartNerdy, Updated by Chancity ]]--


 -- AutoUpdate Settings, turn update to false to turn off
local hasUpdated = true
local curVersion = 1.08
local GetVersionURL = "https://dl.dropboxusercontent.com/s/9vmas1lm3qkt0ph/Version.ini"
local newDownloadURL = "blank"
local newVersion = "blank"
local newMessage = "blank"
local SCRIPT_PATH = BOL_PATH.."Scripts\\Common\\SidasAutoCarryPlugin - "..myHero.charName..".lua"
local VER_PATH = os.getenv("APPDATA").."\\"..myHero.charName.."Version.ini"
local UpdateChat = {}
DownloadFile(GetVersionURL, VER_PATH, function() end)

-- Constants
local allRange = 600
enemyMinions = minionManager(MINION_ENEMY, allRange, player, MINION_SORT_HEALTH_ASC)
closestchamp = nil
local Target = nil

-- Items
local ignite = nil
local DFGSlot, HXGSlot, BWCSlot, SheenSlot, TrinitySlot, LichBaneSlot = nil, nil, nil, nil, nil, nil
local QREADY, WREADY, EREADY, RREADY, DFGReady, HXGReady, BWCReady, IReady = false, false, false, false, false, false, false, false
 
-- 
AutoCarry.PluginMenu:addParam("sep", "["..myHero.charName.."自动 Carry: 瑞兹 "..curVersion.."]", SCRIPT_PARAM_INFO, "")
AutoCarry.PluginMenu:addParam("space", "", SCRIPT_PARAM_INFO, "")
AutoCarry.PluginMenu:addParam("sep1", "[连招设置]", SCRIPT_PARAM_INFO, "")
AutoCarry.PluginMenu:addParam("bCombo", "自动Carry使用连招", SCRIPT_PARAM_ONOFF, false)
AutoCarry.PluginMenu:addParam("hCombo", "混合模式使用连招", SCRIPT_PARAM_ONOFF, true)
AutoCarry.PluginMenu:addParam("qKS", "自动Q抢人头", SCRIPT_PARAM_ONOFF, true)
AutoCarry.PluginMenu:addParam("wCage", "自动W敌方", SCRIPT_PARAM_ONKEYDOWN, false, string.byte("W"))
AutoCarry.PluginMenu:addParam("useUlt", "连招使用大招", SCRIPT_PARAM_ONOFF, true)
AutoCarry.PluginMenu:addParam("space1", "", SCRIPT_PARAM_INFO, "")
AutoCarry.PluginMenu:addParam("sep2", "[补兵设置]", SCRIPT_PARAM_INFO, "")
AutoCarry.PluginMenu:addParam("fMana", "法力值>%",  SCRIPT_PARAM_SLICE, 50, 0, 100, 2)
AutoCarry.PluginMenu:addParam("farmQ", "补兵使用Q", SCRIPT_PARAM_ONKEYTOGGLE, true, string.byte("Z"))
AutoCarry.PluginMenu:addParam("farmE", "补兵使用E", SCRIPT_PARAM_ONOFF, false)
AutoCarry.PluginMenu:addParam("space2", "", SCRIPT_PARAM_INFO, "")
AutoCarry.PluginMenu:addParam("sep3", "[范围显示]", SCRIPT_PARAM_INFO, "")
AutoCarry.PluginMenu:addParam("drawQ", "显示Q范围", SCRIPT_PARAM_ONOFF, true)
AutoCarry.PluginMenu:addParam("drawW", "显示W范围", SCRIPT_PARAM_ONOFF, false)
AutoCarry.PluginMenu:addParam("drawE", "显示E范围", SCRIPT_PARAM_ONOFF, false)
 
function PluginOnTick()

	if hasUpdated then 
		if FileExist(VER_PATH) then
			AutoUpdate() 
		end 
	end
	
	Target = AutoCarry.GetAttackTarget()
	SpellCheck()
	
	if Target then
		for _, enemy in pairs(AutoCarry.EnemyTable) do
			if ValidTarget(enemy, allRange) and not enemy.dead then
				if (getDmg("AD", enemy, myHero)*2) <= enemy.health or myHero.level <= 10 then
					AutoCarry.CanAttack = true
				else
					AutoCarry.CanAttack = false
				end
			end
		end
	end

        if AutoCarry.PluginMenu.qKS then
                qKS()
        end
        if AutoCarry.PluginMenu.wCage and recall == false then
                wCage()
        end            
        if AutoCarry.PluginMenu.bCombo and AutoCarry.MainMenu.AutoCarry then
                bCombo()
        end
	 if AutoCarry.PluginMenu.hCombo and AutoCarry.MainMenu.MixedMode or AutoCarry.MainMenu.LaneClear 
		and AutoCarry.PluginMenu.fMana <=((myHero.mana/myHero.maxMana)*100) then
                hCombo()
        end
        if AutoCarry.PluginMenu.farmQ and not AutoCarry.MainMenu.AutoCarry 
                and AutoCarry.PluginMenu.fMana <=((myHero.mana/myHero.maxMana)*100)
		and (AutoCarry.MainMenu.MixedMode or AutoCarry.MainMenu.LaneClear) then
                qFarm()
        end
        if AutoCarry.PluginMenu.farmE and not AutoCarry.MainMenu.AutoCarry 
                and AutoCarry.PluginMenu.fMana <=((myHero.mana/myHero.maxMana)*100)
		and (AutoCarry.MainMenu.MixedMode or AutoCarry.MainMenu.LaneClear) then
                eFarm()
        end
       
end

function SpellCheck()
        DFGSlot, HXGSlot, BWCSlot, SheenSlot, TrinitySlot, LichBaneSlot = GetInventorySlotItem(3128),
        GetInventorySlotItem(3146), GetInventorySlotItem(3144), GetInventorySlotItem(3057),
        GetInventorySlotItem(3078), GetInventorySlotItem(3100)
 
        QREADY = (myHero:CanUseSpell(_Q) == READY)
        WREADY = (myHero:CanUseSpell(_W) == READY)
        EREADY = (myHero:CanUseSpell(_E) == READY)
        RREADY = (myHero:CanUseSpell(_R) == READY)
 
        DFGReady = (DFGSlot ~= nil and myHero:CanUseSpell(DFGSlot) == READY)
        HXGReady = (HXGSlot ~= nil and myHero:CanUseSpell(HXGSlot) == READY)
        BWCReady = (BWCSlot ~= nil and myHero:CanUseSpell(BWCSlot) == READY)
        IReady = (ignite ~= nil and myHero:CanUseSpell(ignite) == READY)
end
 
function qKS()
        for _, enemy in pairs(AutoCarry.EnemyTable) do
                if ValidTarget(enemy, allRange)  and not enemy.dead then
                        if enemy.health < getDmg("Q", enemy, myHero) then
                                CastSpell(_Q, enemy)
                        end
                end
        end
end
 
function bCombo()
        for _, enemy in pairs(AutoCarry.EnemyTable) do
                if AutoCarry.PluginMenu.bCombo and not enemy.dead then
                        if ValidTarget(enemy, allRange) and AutoCarry.PluginMenu.useUlt and myHero:CanUseSpell(_R) == READY then
                                CastSpell(_R)
                        elseif ValidTarget(enemy, allRange) and myHero:CanUseSpell(_Q) == READY then
                                CastSpell(_Q, enemy)
                        elseif ValidTarget(enemy, allRange) and myHero:CanUseSpell(_W) == READY then
                                CastSpell(_W, enemy)
                        elseif ValidTarget(enemy, allRange) and myHero:CanUseSpell(_E) == READY then
                                CastSpell(_E, enemy)
                        end
                end
        end
end

function hCombo()
        for _, enemy in pairs(AutoCarry.EnemyTable) do
                if AutoCarry.PluginMenu.hCombo and not enemy.dead then
                        if ValidTarget(enemy, allRange) and myHero:CanUseSpell(_Q) == READY then
                                CastSpell(_Q, enemy)
                        elseif ValidTarget(enemy, allRange) and myHero:CanUseSpell(_E) == READY then
                                CastSpell(_E, enemy)
                        end
                end
        end
end
 
function qFarm()
                local minions = {}
                for _, minion in pairs(AutoCarry.EnemyMinions().objects) do
                  if ValidTarget(minion) and myHero:CanUseSpell(_Q) == READY and AutoCarry.PluginMenu.farmQ then
                        if minion.health < getDmg("Q", minion, myHero) then
                                CastSpell(_Q, minion)
                                                               
                        end
                end
        end
end
 
function eFarm()
                local minions = {}
                for _, minion in pairs(AutoCarry.EnemyMinions().objects) do
                  if ValidTarget(minion) and myHero:CanUseSpell(_E) == READY and AutoCarry.PluginMenu.farmE then
                        if minion.health < getDmg("E", minion, myHero) then
                                CastSpell(_E, minion)
                                                               
                        end
                end
        end
end
 
function closestEnemy()
        for _, champ in pairs(AutoCarry.EnemyTable) do
                if closestchamp and closestchamp.valid and champ and champ.valid then
                        if GetDistance(champ) < GetDistance(closestchamp) then
                                closestchamp = champ
                        end
                else
                        closestchamp = champ
                end
        end
                                                return closestchamp
end
 
function wCage()
        if myHero:CanUseSpell(_W) == READY and closestEnemy() then
                cEnemy = closestEnemy()
                if ValidTarget(cEnemy) then
                        if myHero:GetDistance(cEnemy) < allRange and ValidTarget(cEnemy) then
                                CastSpell(_W, cEnemy)
                        end
                end
        end
end
               
function PluginOnDraw()
        if myHero:CanUseSpell(_Q) == READY and AutoCarry.PluginMenu.drawQ then
                        DrawCircle(myHero.x, myHero.y, myHero.z, allRange, 0x0000ff00)
        elseif myHero:CanUseSpell(_W) == READY and AutoCarry.PluginMenu.drawW then
                        DrawCircle(myHero.x, myHero.y, myHero.z, allRange, 0x0000ff00)
        elseif myHero:CanUseSpell(_E) == READY and AutoCarry.PluginMenu.drawE then
                        DrawCircle(myHero.x, myHero.y, myHero.z, allRange, 0x0000ff00)
        end
end

function NewIniReader()
	local reader = {};
	function reader:Read(fName)
		self.root = {};
		self.reading_section = "";
		for line in io.lines(fName) do
			if startsWith(line, "[") then
				local section = string.sub(line,2,-2);
				self.root[section] = {};
				self.reading_section = section;
			elseif not startsWith(line, ";") then
				if self.reading_section then
					local var,val = line:usplit("=");
					local var,val = var:utrim(), val:utrim();
					if string.find(val, ";") then
						val,comment = val:usplit(";");
						val = val:utrim();
					end
					self.root[self.reading_section] = self.root[self.reading_section] or {};
					self.root[self.reading_section][var] = val;
				else
					return error("No element set for setting");
				end
			end
		end
	end
	function reader:GetValue(Section, Key)
		return self.root[Section][Key];
	end
	function reader:GetKeys(Section)
		return self.root[Section];
	end
	return reader;
end

function startsWith(text,prefix)
	return string.sub(text, 1, string.len(prefix)) == prefix
end

function string:usplit(sep)
	return self:match("([^" .. sep .. "]+)[" .. sep .. "]+(.+)")
end

function string:utrim()
	return self:match("^%s*(.-)%s*$")
end

function AutoUpdate()
	
	reader = NewIniReader();
	
	if FileExist(VER_PATH) then 
		reader:Read(VER_PATH);
	
		newDownloadURL = reader:GetValue("Version", "Download")
		newVersion = reader:GetValue("Version", "Version")
		newMessage = reader:GetValue("Version", "Message")
		
		UpdateChat = {
			"<font color='#e066a3'> >> "..myHero.charName.." Auto Carry Plugin:</font> <font color='#f4cce0'> Checking for update... </font>",
			"<font color='#e066a3'> >> "..myHero.charName.." Auto Carry Plugin:</font> <font color='#f4cce0'> Running Version "..curVersion.."</font>",
			"<font color='#e066a3'> >> "..myHero.charName.." Auto Carry Plugin:</font> <font color='#f4cce0'> New Version Released "..newVersion.."</font>",
			"<font color='#e066a3'> >> "..myHero.charName.." Auto Carry Plugin:</font> <font color='#f4cce0'> Updated to version "..newVersion.." press F9 two times to use updated script. </font>",
			"<font color='#e066a3'> >> "..myHero.charName.." Auto Carry Plugin:</font> <font color='#f4cce0'> Script is Up-To-Date </font>",
			"<font color='#e066a3'> >> "..myHero.charName.." Auto Carry Plugin:</font> <font color='#f4cce0'> Update Message ("..newVersion.."): "..newMessage.."</font>",
			"<font color='#e066a3'> >> "..myHero.charName.." Auto Carry Plugin:</font> <font color='#f4cce0'> Failed to check for update, press F9 two times if first run </font>"
					}
		
		local results, reason = os.remove(VER_PATH)
		
		if tonumber(newVersion) > tonumber(curVersion) then
			DownloadFile(newDownloadURL, SCRIPT_PATH, function()
			if FileExist(SCRIPT_PATH) then
			ChatUpdate("update")
            end
			end)
		else
		ChatUpdate("uptodate")
		end	
	else 
		ChatUpdate("failed")
	end 
	hasUpdated = false
end

function ChatUpdate(stats)
		PrintChat(UpdateChat[1])
		PrintChat(UpdateChat[2])
	if stats == "update" then
		PrintChat(UpdateChat[3])
		PrintChat(UpdateChat[4])
		PrintChat(UpdateChat[6])
	elseif stats == "uptodate" then
		PrintChat(UpdateChat[5])
		PrintChat(UpdateChat[6])
	else
		PrintChat(UpdateChat[7])
	end
end

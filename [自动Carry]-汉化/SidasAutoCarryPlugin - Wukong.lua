--[[
 
        Auto Carry Plugin - Wukong/Monkey King Edition
		Author: Roach_
		Version: 1.0a
		Copyright 2013

		Dependency: Sida's Auto Carry: Revamped
 
		How to install:
			Make sure you already have AutoCarry installed.
			Name the script EXACTLY "SidasAutoCarryPlugin - MonkeyKing.lua" without the quotes.
			Place the plugin in BoL/Scripts/Common folder.

		Features:
			Combo with Autocarry
			Harass with Mixed Mode
			Killsteal with Q / E
			Farm with Q / E 
			Draw Combo Ranges (With Cooldown Check)
			Escape Artist(with Flash)
			Auto Pots/Items

		History:
			Version: 1.0a
				First release

--]]
if myHero.charName ~= "MonkeyKing" then return end

local Target
local wEscapeHotkey = string.byte("T")

-- Prediction
local qRange, eRange, rRange = 300, 625, 162

local FlashSlot = nil

local SkillQ = {spellKey = _Q, range = qRange, speed = 2, delay = 0, width = 200, configName = "crushingBlow", displayName = "Q (Crushing Blow)", enabled = true, skillShot = false, minions = false, reset = false, reqTarget = true }
local SkillE = {spellKey = _E, range = eRange, speed = 2, delay = 0, width = 200, configName = "nimbusStrike", displayName = "E (Nimbus Strike)", enabled = true, skillShot = false, minions = false, reset = false, reqTarget = true }

local QReady, WReady, EReady, RReady, FlashReady = false, false, false, false, false

-- Regeneration
local UsingHPot, UsingMPot, UsingFlask, Recall = false, false, false, false

-- Our lovely script
function PluginOnLoadMenu()
	Menu = AutoCarry.PluginMenu
	Menu2 = AutoCarry.MainMenu
	Menu:addParam("wPlugin", "[基础选项]", SCRIPT_PARAM_INFO, "")
	Menu:addParam("wCombo", "[连招:选项]", SCRIPT_PARAM_INFO, "")
	Menu:addParam("wAutoQ", "连招使用Q", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("wAutoE", "连招使用E", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("wAutoR", "连招使用R(当可杀时)", SCRIPT_PARAM_ONOFF, false)
	Menu:permaShow("wPlugin")
	
	Menu:addParam("wGap", "", SCRIPT_PARAM_INFO, "")
	
	Menu:addParam("wKS", "[抢人头:选项]", SCRIPT_PARAM_INFO, "")
	Menu:addParam("wKillsteal", "自动使用E或Q抢人头", SCRIPT_PARAM_ONOFF, true)
	Menu:permaShow("wKS")
	Menu:permaShow("wKillsteal")
	
	Menu:addParam("wGap", "", SCRIPT_PARAM_INFO, "")
	
	Menu:addParam("wMisc", "[其他:选项]", SCRIPT_PARAM_INFO, "")
	-- Menu:addParam("pAutoLVL", "自动升级加点", SCRIPT_PARAM_ONOFF, false)
	Menu:addParam("wMinMana", "补兵/骚扰 法力>%", SCRIPT_PARAM_SLICE, 40, 0, 100, -1)
	Menu:addParam("wEscape", "逃生", SCRIPT_PARAM_ONKEYDOWN, false, wEscapeHotkey)
	Menu:addParam("wEscapeFlash", "逃生：闪现到鼠标处", SCRIPT_PARAM_ONOFF, false)
	Menu:permaShow("wMisc")
	Menu:permaShow("wEscape")
	
	Menu:addParam("wGap", "", SCRIPT_PARAM_INFO, "")
	
	Menu:addParam("wH", "[骚扰:选项]", SCRIPT_PARAM_INFO, "")
	Menu:addParam("wHarass", "使用E骚扰", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("wHarassEscape", "骚扰后使用W逃跑", SCRIPT_PARAM_ONOFF, false)
	
	Menu:addParam("wGap", "", SCRIPT_PARAM_INFO, "")
	
	Menu:addParam("wFarm", "[补兵:选项]", SCRIPT_PARAM_INFO, "")
	Menu:addParam("wFarmQ", "使用Q补兵", SCRIPT_PARAM_ONOFF, false)
	Menu:addParam("wFarmE", "使用E清兵", SCRIPT_PARAM_ONOFF, false)
	
	Extras = scriptConfig("自动 Carry: "..myHero.charName..": 额外", myHero.charName)
	Extras:addParam("wDraw", "[范围:选项]", SCRIPT_PARAM_INFO, "")
	Extras:addParam("wDQ", "Q范围", SCRIPT_PARAM_ONOFF, false)
	Extras:addParam("wDW", "W范围(检测冷却时间)", SCRIPT_PARAM_ONOFF, false)
	Extras:addParam("wDE", "E范围", SCRIPT_PARAM_ONOFF, false)
	
	Extras:addParam("wGap", "", SCRIPT_PARAM_INFO, "")
	
	Extras:addParam("wHPMana", "[自动 吃药/道具:选项]", SCRIPT_PARAM_INFO, "")
	Extras:addParam("wWItem", "自动 巫师帽", SCRIPT_PARAM_ONOFF, true)
	Extras:addParam("wWHealth", "巫师帽 血量<%", SCRIPT_PARAM_SLICE, 15, 0, 100, -1)
	Extras:addParam("wHP", "自动吃血", SCRIPT_PARAM_ONOFF, true)
	Extras:addParam("wMP", "自动吃蓝", SCRIPT_PARAM_ONOFF, true)
	Extras:addParam("wHPHealth", "自动吃血 血量<%", SCRIPT_PARAM_SLICE, 50, 0, 100, -1)
end
function PluginOnLoad() 
	-- Params/PluginMenu
	PluginOnLoadMenu()
	
	-- Range
	AutoCarry.SkillsCrosshair.range = eRange-25
end 

function PluginOnTick()
	if Recall then return end

	-- Get Attack Target
	Target = AutoCarry.GetAttackTarget()

	-- Check Spells
	wSpellCheck()

	-- Combo, Harass, Killsteal, Escape Combo, Farm - Checks
	wCombo()
	wHarass()
	wKillsteal()
	wEscapeCombo()
	wFarm()
	
	-- Auto Regeneration
	if Extras.wWItem and IsMyHealthLow() and Target and WGTReady then CastSpell(wgtSlot) end
	if Extras.wHP and NeedHP() and not (UsingHPot or UsingFlask) and (HPReady or FSKReady) then CastSpell((hpSlot or fskSlot)) end
	if Extras.wMP and IsMyManaLow() and not (UsingMPot or UsingFlask) and(MPReady or FSKReady) then CastSpell((mpSlot or fskSlot)) end
end

function PluginOnDraw()
	-- Draw Wukong's Range = 625 - 25
	local tempColor = { 0xFF0000, 0xFF0000, 0xFF0000 }
	
	if not myHero.dead then
		if Extras.wDQ then
			if QReady then tempColor[1] = 0x00FF00 end
			DrawCircle(myHero.x, myHero.y, myHero.z, qRange, tempColor[1])
		end
		if Extras.wDW then
			if WReady then tempColor[2] = 0x00FF00 end
			DrawCircle(myHero.x, myHero.y, myHero.z, myHero.range-50, tempColor[2])
		end
		if Extras.wDE then
			if EReady then tempColor[3] = 0x00FF00 end
			DrawCircle(myHero.x, myHero.y, myHero.z, eRange, tempColor[3])
		end
	end
end

-- Object Detection
function PluginOnCreateObj(obj)
	if obj.name:find("TeleportHome.troy") then
		if GetDistance(obj, myHero) <= 70 then
			Recall = true
		end
	end
	if obj.name:find("Regenerationpotion_itm.troy") then
		if GetDistance(obj, myHero) <= 70 then
			UsingHPot = true
		end
	end
	if obj.name:find("Global_Item_HealthPotion.troy") then
		if GetDistance(obj, myHero) <= 70 then
			UsingHPot = true
			UsingFlask = true
		end
	end
	if obj.name:find("Global_Item_ManaPotion.troy") then
		if GetDistance(obj, myHero) <= 70 then
			UsingFlask = true
			UsingMPot = true
		end
	end
end

function PluginOnDeleteObj(obj)
	if obj.name:find("TeleportHome.troy") then
		Recall = false
	end
	if obj.name:find("Regenerationpotion_itm.troy") then
		UsingHPot = false
	end
	if obj.name:find("Global_Item_HealthPotion.troy") then
		UsingHPot = false
		UsingFlask = false
	end
	if obj.name:find("Global_Item_ManaPotion.troy") then
		UsingMPot = false
		UsingFlask = false
	end
end

-- Primary Functions
function wCombo()
	if Menu.wCombo and Menu2.AutoCarry then
		if ValidTarget(Target) then
			if QReady and Menu.wAutoQ and GetDistance(Target) < qRange then 
				CastSpell(SkillQ.spellKey, Target)
			end
			
			if EReady and Menu.wAutoE and GetDistance(Target) < eRange then
				CastSpell(SkillE.spellKey, Target)
			end
			
			if RReady and Menu.wAutoR and (getDmg("R", Target, myHero) * 4) > Target.health and GetDistance(Target) < (rRange - 12) then
				CastSpell(_R)
			end
		end
	end
end

function wHarass()
	if Menu.wHarass and Menu2.MixedMode then
		if ValidTarget(Target) then
			if EReady and GetDistance(Target) < eRange and (myHero.mana / myHero.maxMana) > Menu.wMinMana then 
				CastSpell(SkillE.spellKey, Target)
				myHero:Attack(Target)
			end
			if WReady and Menu.wHarassEscape then
				CastSpell(_W)
			end
		end
	end
end

function wFarm()
	if Menu.wFarmQ and (Menu2.LastHit) and (myHero.mana / myHero.maxMana) > Menu.wMinMana then
		for _, minion in pairs(AutoCarry.EnemyMinions().objects) do
			if ValidTarget(minion) and QReady and GetDistance(minion) <= qRange then
				if minion.health < getDmg("Q", minion, myHero) then
					CastSpell(SkillQ.spellKey, minion)
				end
			end
		end
	end
	if Menu.wFarmE and (Menu2.LaneClear) and (myHero.mana / myHero.maxMana) > Menu.wMinMana then
		for _, minion in pairs(AutoCarry.EnemyMinions().objects) do
			if ValidTarget(minion) and EReady and GetDistance(minion) <= eRange then
				if minion.health < getDmg("E", minion, myHero) then
					CastSpell(SkillE.spellKey, minion)
				end
			end
		end
	end
end

function wKillsteal()
	if Menu.wKillsteal then
		for _, enemy in pairs(AutoCarry.EnemyTable) do
			if ValidTarget(enemy) then
				if QReady and GetDistance(enemy) < qRange and enemy.health < getDmg("Q", enemy, myHero) then
						CastSpell(SkillQ.spellKey, enemy)
				end
				if EReady and GetDistance(enemy) > qRange and GetDistance(enemy) < eRange and enemy.health < getDmg("E", enemy, myHero) then
					CastSpell(SkillE.spellKey, enemy)
				end
			end
		end
	end
end

function wEscapeCombo()	
	if Menu.wEscape then
		if WReady then
			CastSpell(_W)
			if Menu.wEscapeFlash and FlashReady and GetDistance(mousePos) > 300 then
				CastSpell(FlashSlot, mousePos.x, mousePos.z)
			end
		end
		
		if Menu.wEscapeFlash then
			myHero:MoveTo(mousePos.x, mousePos.z)
		end
	end
end

-- Scondary Functions
function wSpellCheck()
	wgtSlot = GetInventorySlotItem(3090)
	hpSlot, mpSlot, fskSlot = GetInventorySlotItem(2003),GetInventorySlotItem(2004),GetInventorySlotItem(2041)

	if myHero:GetSpellData(SUMMONER_1).name:find("SummonerFlash") then
		FlashSlot = SUMMONER_1
	elseif myHero:GetSpellData(SUMMONER_2).name:find("SummonerFlash") then
		FlashSlot = SUMMONER_2
	end

	QReady = (myHero:CanUseSpell(SkillQ.spellKey) == READY)
	WReady = (myHero:CanUseSpell(_W) == READY)
	EReady = (myHero:CanUseSpell(SkillE.spellKey) == READY)
	RReady = (myHero:CanUseSpell(_R) == READY)
	WGTReady = (wgtSlot ~= nil and myHero:CanUseSpell(wgtSlot) == READY)
	HPReady = (hpSlot ~= nil and myHero:CanUseSpell(hpSlot) == READY)
	MPReady = (mpSlot ~= nil and myHero:CanUseSpell(mpSlot) == READY)
	FSKReady = (fskSlot ~= nil and myHero:CanUseSpell(fskSlot) == READY)

	FlashReady = (FlashSlot ~= nil and myHero:CanUseSpell(FlashSlot) == READY)
end

-- Auto Potions
function IsMyManaLow()
    if (myHero.mana / myHero.maxMana) <= (Menu.wMinMana / 100) then
        return true
    else
        return false
    end
end

function IsMyHealthLow()
	if (myHero.health / myHero.maxHealth) <= (Extras.wWHealth / 100) then
		return true
	else
		return false
	end
end

function NeedHP()
	if (myHero.health / myHero.maxHealth) <= (Extras.wHPHealth / 100) then
		return true
	else
		return false
	end
end
if myHero.charName ~= "Gangplank" then return end
 
local SheenSlot, TrinitySlot, IcebornSlot = nil, nil, nil
 
function PluginOnLoad()
        mainLoad()
        mainMenu()
end
 
function PluginOnTick()
        farmMinions:update()
        jungleMinions:update()
        Target = AutoCarry.GetAttackTarget()
        QREADY = (myHero:CanUseSpell(_Q) == READY)
        EREADY = (myHero:CanUseSpell(_E) == READY)
SheenSlot, TrinitySlot, IcebornSlot = GetInventorySlotItem(3025), GetInventorySlotItem(3057), GetInventorySlotItem(3078)
 
        if Menu.autoks and QREADY then
                for i = 1, heroManager.iCount, 1 do
                        local qTarget = heroManager:getHero(i)
                        if ValidTarget(qTarget, qRange) then
                                if qTarget.health <= qDamage(qTarget) then
                                        CastSpell(_Q, qTarget)
                                end
                        end
                end
        end
       
        if Target and Menu2.AutoCarry then
                if EREADY and Menu.useE and GetDistance(Target) <= eRange then
                        CastSpell(_E)
                end
                if QREADY and Menu.useQ then
                        if Menu.qChase and GetDistance(Target) > Menu.qDistance then
                                CastSpell(_Q, Target)
                        elseif Target.health <  qDamage(Target) then
                                CastSpell(_Q, Target)
                        end
                end
        end
        if Menu.qFarm and Menu2.LastHit or Menu2.LaneClear then
                for _, minion in pairs(farmMinions.objects) do
                        if minion and ValidTarget(minion) and QREADY and GetDistance(minion) <= qRange then
                                if minion.health < qDamage(minion) then
                                        CastSpell(_Q, minion)
                                end
                        end
                end
                for _, jMinion in pairs(jungleMinions.objects) do
                        if jMinion and ValidTarget(jMinion) and QREADY and GetDistance(jMinion) <= qRange then
                                if jMinion.health < qDamage(jMinion) then
                                        CastSpell(_Q, jMinion)
                                end
                        end
                end
        end
end
 
function OnAttacked()
        if Target and Menu2.AutoCarry then
                if QREADY and Menu.useQ and GetDistance(Target) <= qRange then CastSpell(_Q, Target) end
        end
end
 
function PluginOnDraw()
        if Menu.drawQ and not myHero.dead then
                DrawCircle(myHero.x, myHero.y, myHero.z, qRange, 0x00FF00)
        end
end
 
function qDamage(target)
        local qDmg = myHero:CalcDamage(target, (20+(15*myHero:GetSpellData(_Q).level)+myHero.totalDamage))
        local bDmg = ((SheenSlot and getDmg("SHEEN", target, myHero) or 0)+(TrinitySlot and getDmg("TRINITY", target, myHero) or 0)+(IcebornSlot and getDmg("ICEBORN", target, myHero) or 0))-15
        return qDmg + bDmg
end
 
function mainLoad()
        AutoCarry.SkillsCrosshair.range = 700
        Menu = AutoCarry.PluginMenu
        Menu2 = AutoCarry.MainMenu
        qRange, eRange = 625, 600
        QREADY, EREADY  = false, false
        farmMinions = minionManager(MINION_ENEMY, qRange+200, player)
        jungleMinions = minionManager(MINION_JUNGLE, qRange+200, player)
end
 
function mainMenu()
        Menu:addParam("sep", "-- 释放选项 --", SCRIPT_PARAM_INFO, "")
        Menu:addParam("autoks", "自动Q抢人头", SCRIPT_PARAM_ONOFF, true)
        Menu:addParam("qDistance", "技能(Q)使用距离",SCRIPT_PARAM_SLICE, 400, 0, 625, 0)
        Menu:addParam("qChase", "超出(Q)范围仍追击", SCRIPT_PARAM_ONOFF, true)
        Menu:addParam("qFarm", "使用Q补兵", SCRIPT_PARAM_ONOFF, false)
        Menu:addParam("sep", "-- 技能选项 --", SCRIPT_PARAM_INFO, "")
        Menu:addParam("useQ", "连招使用Q", SCRIPT_PARAM_ONOFF, true)
        Menu:addParam("useE", "连招使用E", SCRIPT_PARAM_ONOFF, true)
        Menu:addParam("sep2", "-- 显示选项 --", SCRIPT_PARAM_INFO, "")
        Menu:addParam("drawQ", "显示Q范围", SCRIPT_PARAM_ONOFF, false)
end
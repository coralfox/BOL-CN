--[[ Renekton by Skeem 0.1]]--

if myHero.charName ~= "Renekton" then return end

function PluginOnLoad()
        --> Main Load
        RenektonLoad()
        --> Main Menu
        RenektonMenu()
end

function PluginOnTick()
        Checks()
        if Target then
                if AutoCarry.MainMenu.AutoCarry then
                        if QREADY and GetDistance(Target) <= qRange and Menu.useQ then CastSpell(_Q) end
                        if WREADY and GetDistance(Target) <= aRange and Menu.useW then CastSpell(_W) end
                        if EREADY and GetDistance(Target) <= eRange and Menu.useE then CastSpell(_E, Target.x, Target.z) end
                end 
        end
        if Menu.AutoUltimate then
                AutoUltimate()
        end
end

function PluginOnDraw()
        --> Ranges
        if not myHero.dead then
                if QREADY and Menu.drawQ then 
                        DrawCircle(myHero.x, myHero.y, myHero.z, qRange, 0x00FFFF)
                end
        end
end

function AutoUltimate()
local MinimumEnemies = Menu.MinimumEnemies
local EnemiesRange = Menu.MinimumRange
local MyHealthPercent = ((myHero.health/myHero.maxHealth)*100)
local MinimumHealth = Menu.MinimumHealth
        if (CountEnemyHeroInRange(EnemiesRange) >= MinimumEnemies) and (MyHealthPercent <= MinimumHealth) and rReady then
                CastSpell(_R)
        end
        if (CountEnemyHeroInRange(EnemiesRange) >= 1) and not myHero.canMove and (MyHealthPercent <= MinimumHealth) and rReady then
                CastSpell(_R)
        end        
end

--> Main Load
function RenektonLoad()
        qRange, eRange, aRange = 225, 450, 125
        QREADY, WREADY, EREADY, RREADY = false, false, false, false

end

--> Main Menu
function RenektonMenu()
        Menu = AutoCarry.PluginMenu
        Menu:addParam("sep1", "-- 自动Carry选项 --", SCRIPT_PARAM_INFO, "")
        Menu:addParam("useQ", "使用 暴君狂击(Q)", SCRIPT_PARAM_ONOFF, true)
        Menu:addParam("useW", "使用 冷酷捕猎(W)", SCRIPT_PARAM_ONOFF, true)
        Menu:addParam("useE", "使用 横冲直撞(E)", SCRIPT_PARAM_ONOFF, true)
        Menu:addParam("sep2", " -- 大招选项 --", SCRIPT_PARAM_INFO, "")
        Menu:addParam("AutoUltimate", "自动 终极统治(R)", SCRIPT_PARAM_ONKEYTOGGLE, true, 84) -- T
        Menu:addParam("MinimumHealth", "自动R 血量<=%", SCRIPT_PARAM_SLICE, 45, 1, 100, 0)
        Menu:addParam("MinimumEnemies", "自动R 敌人>=?", SCRIPT_PARAM_SLICE, 2, 1, 5, 0)
        Menu:addParam("MinimumRange", "自动R 检测范围", SCRIPT_PARAM_SLICE, 650, 200, 1000, -1)
        Menu:addParam("sep3", "-- 显示设定 --", SCRIPT_PARAM_INFO, "")
        Menu:addParam("drawQ", "显示 暴君狂击(Q) 范围", SCRIPT_PARAM_ONOFF, false)
end

--> Checks
function Checks()
        Target = AutoCarry.GetAttackTarget()
        QREADY = (myHero:CanUseSpell(_Q) == READY)
        WREADY = (myHero:CanUseSpell(_W) == READY)
        EREADY = (myHero:CanUseSpell(_E) == READY)
        RREADY = (myHero:CanUseSpell(_R) == READY)
end
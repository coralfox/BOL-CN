      
 
     
        --[[
         
                Auto Carry Talon plugin by Vertoi
         
        --]]
            -- RANGE SETTINGS --
            local rangeQ = 125 -- Q
        local rangeW = 710 -- W
        local rangeE = 700 -- E
        local rangeR = 650 -- R
           
            -- HOTKEYS --
            local harass =  string.byte("C")
            local farmw =  string.byte("V")
            local ulti =  string.byte("T")
           
           
            local QREADY, WREADY, EREADY, RREADY = false, false, false, false
                    AutoCarry.PluginMenu:addParam("harass", "ɧ��", SCRIPT_PARAM_ONKEYTOGGLE, false, harass) -- harass --
                    AutoCarry.PluginMenu:addParam("notice", "��ǿɻ�ɱ", SCRIPT_PARAM_ONOFF, true) -- notice when killable --
                    AutoCarry.PluginMenu:addParam("lasthitw", "W����", SCRIPT_PARAM_ONKEYDOWN, false, farmw) -- last hit with W --
                    AutoCarry.PluginMenu:addParam("potion", "�Զ�ҩ��", SCRIPT_PARAM_ONOFF, false) -- auto potion when less than 25% hp --
                    AutoCarry.PluginMenu:addParam("ultimate", "�ɻ�ɱʱʹ�ô���", SCRIPT_PARAM_ONKEYTOGGLE, true, ulti) -- auto-ulti when killable --
                    AutoCarry.PluginMenu:addParam("circles", "���Ʒ�Χ", SCRIPT_PARAM_ONOFF, true) -- draw circles --
            function PluginOnLoad()
     
                   
                    -- PERMA SHOW --
                    AutoCarry.PluginMenu:permaShow("harass")
                    AutoCarry.PluginMenu:permaShow("lasthitw")
                   
                    ----------
                    AutoCarry.SkillsCrosshair.range = rangeW
                   
            end
           
            function PluginOnTick()
                    -- GET THE TARGET --
                    if AutoCarry.GetTarget() ~= nil then
                                                local Target = AutoCarry.GetTarget()
                                        end
                    -- GET CD --
                    QREADY = (myHero:CanUseSpell(_Q) == READY)
                    WREADY = (myHero:CanUseSpell(_W) == READY)
                    EREADY = (myHero:CanUseSpell(_E) == READY)
                    RREADY = (myHero:CanUseSpell(_R) == READY)
                   
                    -- AUTOPOTION --
                    if AutoCarry.PluginMenu.potion and (myHero.health / myHero.maxHealth <= 0.25) then
                            CastItem(2003)
                    end
                   
                    -- COMBO --
                    if AutoCarry.MainMenu.AutoCarry then doCombo() end
                    -- USE W ON MINIONS (LANE CLEAR HOTKEY) --
                    if AutoCarry.MainMenu.LaneClear then wMinions() end
                    -- LAST HIT WITH W --
                    if AutoCarry.PluginMenu.lasthitw then wLastHit() end
                    -- HARASS --
                    if AutoCarry.PluginMenu.harass then harass() end
                    -- ULTIMATE WHEN KILLABLE --
                    if AutoCarry.PluginMenu.ultimate then useUlti() end
                    -- NOTICE IF KILLABLE
                    ---------------if AutoCarry.PluginMenu.notice then notice() end
                   
                   
                   
            end
           
        function PluginOnDraw()
                if not myHero.dead and AutoCarry.PluginMenu.circles then
                                    DrawCircle(myHero.x, myHero.y, myHero.z, 700, 0xFF0000)
                end
                    local targets = AutoCarry.GetTarget()
                    if targets ~= nil then
                            damageR = getDmg("R", targets, myHero) + getDmg("R", targets, myHero) + (myHero.damage * 1.5)
            end
                    if AutoCarry.PluginMenu.notice then notice() end
        end
            function notice()
                   
                    if AutoCarry.GetTarget() ~= nil then
                                                        local targets = AutoCarry.GetTarget()
                            damageR = getDmg("R", targets, myHero) + getDmg("R", targets, myHero) + (myHero.damage * 1.5)
            end
         
            if targets ~= nil and targets.type == "obj_AI_Hero"  and targets.health < damageR then
                                                  --      DrawCircle(Target.x, Target.y, Target.z, 150, 0xFF0000)
                             --   DrawText("Target killable with ulti!", 50,520,100,0x33CC00)
                                PrintFloatText(targets,0,"�ɻ�ɱ!")
            end
            end
            function useUlti()
                    local targets = AutoCarry.GetAttackTarget(true)
                    if targets ~= nil then
                            damageR = getDmg("R", targets, myHero) + getDmg("R", targets, myHero) + (myHero.damage * 1.5)
            end
         
            if targets ~= nil  and myHero:CanUseSpell(_R) and GetDistance(targets) < rangeR  and targets.health < damageR then
                            CastSpell(_R, targets)
                            CastSpell(_R, targets)
            end
            end
           
            function doCombo()
                    local targettt = AutoCarry.GetAttackTarget(true)
                   
                    if targettt ~= nil then damageR = getDmg("R", targettt, myHero) + getDmg("R", targettt, myHero) + (myHero.damage * 1.5)
     
                    end
                            if ValidTarget(targettt, rangeE) and EREADY then CastSpell(_E, targettt) end
                            if ValidTarget(targettt, rangeQ) and QREADY then CastSpell(_Q, targettt) end
                            if ValidTarget(targettt, rangeW) and WREADY then CastSpell(_W, targettt) end
                            if ValidTarget(targettt, rangeR) and RREADY and targettt.health < damageR then
                            CastSpell(_R, targettt)
                            CastSpell(_R, targettt)
                                   
                            end
                    end
           
           
            function harass()
                    local targett = AutoCarry.GetAttackTarget(true)
                            if ValidTarget(targett, rangeW) and WREADY then CastSpell(_W, targett) end
            end
           
            function wMinions()
                    for index, minion in pairs(AutoCarry.EnemyMinions().objects) do
                            if ValidTarget(minion, rangeW) and WREADY then
                                   
                                            CastSpell(_W, minion)  
                                     
                            end
                    end
            end
           
            function wLastHit()
                    for index, minion in pairs(AutoCarry.EnemyMinions().objects) do
                            if ValidTarget(minion, rangeW) and WREADY then
                                    if minion.health <= getDmg("W", minion, myHero) then
                                            CastSpell(_W, minion)  
                                    end
                            end
                    end
            end
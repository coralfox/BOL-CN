--[[
	SAC Evelynn Plugin 
	Credits to Burn for his origional combo

	Version: 1.0
	- Initial release
	
	Version 1.2 
	- Converted to iFoundation_v2 
	- Bug fixes

		
]]
local lib1=nil
local lib2=nil

function UpdateLib_IF2()
 local URL = "http://git.oschina.net/coralfox/BOL-CN/raw/master/%E6%94%AF%E6%8C%81%E5%BA%93/iFoundation_v2.lua"
 local LIB_PATH = BOL_PATH.."Scripts\\Common\\iFoundation_v2.lua"
 DownloadFile(URL, LIB_PATH, function()
		 if FileExist(LIB_PATH) then
              PrintChat("<font color='#FF0000'> >>  支持库已下载，需要F9两次重新载入  <<</font>")
         end
		end)
end
function UpdateLib_Aoe()
 local URL = "http://git.oschina.net/coralfox/BOL-CN/raw/master/%E6%94%AF%E6%8C%81%E5%BA%93/AoESkillshotPosition.lua"
 local LIB_PATH = BOL_PATH.."Scripts\\Common\\AoESkillshotPosition.lua"
 DownloadFile(URL, LIB_PATH, function() 
		 if FileExist(LIB_PATH) then
              PrintChat("<font color='#FF0000'> >> 支持库已下载，需要F9两次重新载入  <<</font>")
         end
		end)
end



if FileExist(SCRIPT_PATH..'Common/iFoundation_v2.lua') then
	require "iFoundation_v2"
else
	UpdateLib_IF2()
end
if FileExist(SCRIPT_PATH..'Common/AoESkillshotPosition.lua') then
	require "AoESkillshotPosition"
else
	UpdateLib_Aoe()
end



local SkillQ = Caster(_Q, 500, SPELL_SELF)
local SkillW = Caster(_W, 0, SPELL_SELF)
local SkillE = Caster(_E, 225, SPELL_TARGETED)
local SkillR = Caster(_R, 650, SPELL_CIRCLE, math.huge, 0, 0, true)


function PluginOnLoad() 

	-- AutoCarry Settings
	AutoCarry.SkillsCrosshair.range = 500

	MainMenu = AutoCarry.MainMenu
	PluginMenu = AutoCarry.PluginMenu
	
	PluginMenu:addParam("sep1", "-- �����ͷ�ѡ�� --", SCRIPT_PARAM_INFO, "")
	PluginMenu:addParam("RMec", "ʹ��R����ͷ", SCRIPT_PARAM_ONOFF, true)
	PluginMenu:addParam("UseW", "Ѫ����ʱ�Զ�W", SCRIPT_PARAM_ONOFF, true)
	PluginMenu:addParam("WPercentage", "�Զ�W��Ѫ��<%",SCRIPT_PARAM_SLICE, 20, 0, 100, 0)
end


function PluginOnTick()
	Target = AutoCarry.GetAttackTarget()

	
	if Target and MainMenu.AutoCarry then

		if SkillW:Ready() and PluginMenu.UseW and myHero.health <= myHero.maxHealth * (PluginMenu.WPercentage / 100) then
			SkillW:Cast(Target)
		end

		if SkillQ:Ready() then SkillQ:Cast(Target) end 
		if SkillE:Ready() then SkillE:Cast(Target) end 	

		if SkillR:Ready() and GetDistance(Target) <= SkillR.range then
			if PluginMenu.RMec then
				local p = GetAoESpellPosition(350, Target)
				if p and GetDistance(p) <= SkillR.range then
					CastSpell(_R, p.x, p.z)
				end
			else
				SkillR:Cast(Target)
			end
		end

	end

	-- Last Hitting
	if MainMenu.LastHit then
		for _, minion in pairs(AutoCarry.EnemyMinions().objects) do
			if ValidTarget(minion) and QREADY and GetDistance(minion) <= qRange then
				if minion.health < getDmg("Q", minion, myHero) then
					CastSpell(_Q, minion)
				end
			end
		end
	end


end



--UPDATEURL=
--HASH=EB7765DD368018E694456E8AD1E37EA9

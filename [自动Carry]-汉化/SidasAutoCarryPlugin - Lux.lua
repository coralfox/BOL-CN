--[[
 
        Auto Carry Plugin - Lux Edition  --- DeniCevap
 
        Combo - Q -> E
        Harass - E (auto-pops)
        Killsteal
 
--]]
 
if myHero.charName ~= "Lux" then return end

local Target
local ePop

function PluginOnLoad()
        -- Prediction
        qRange, eRange, rRange = 1175, 1100, 1200
        SkillQ = {spellKey = _Q, range = qRange, speed = 1.2, delay = 200, width = 100}
        SkillE = {spellKey = _E, range = eRange, speed = 1.3, delay = 200, width = 0}
        SkillR = {spellKey = _R, range = rRange, speed = 12, delay = 1000, width = 190, minion = false}
         
        AutoCarry.SkillsCrosshair.range = qRange
        AutoCarry.PluginMenu:addParam("combo", "连招", SCRIPT_PARAM_ONKEYDOWN, false, 32)
        AutoCarry.PluginMenu:addParam("harass", "使用E消耗", SCRIPT_PARAM_ONKEYDOWN, false, string.byte("A")) 
        AutoCarry.PluginMenu:addParam("comboOption", "-- 连招设置 --", SCRIPT_PARAM_INFO, "")
        AutoCarry.PluginMenu:addParam("useQ", "自动使用Q", SCRIPT_PARAM_ONOFF, true)
        AutoCarry.PluginMenu:addParam("useE", "自动使用E", SCRIPT_PARAM_ONOFF, true)
        AutoCarry.PluginMenu:addParam("useR", "自动使用R", SCRIPT_PARAM_ONOFF, true)
        AutoCarry.PluginMenu:addParam("killsteal", "自动大招抢人头", SCRIPT_PARAM_ONOFF, true)
        AutoCarry.PluginMenu:addParam("drawOption", "-- 范围设置 --", SCRIPT_PARAM_INFO, "")
        AutoCarry.PluginMenu:addParam("drawQ", "显示Q范围", SCRIPT_PARAM_ONOFF, true)
        AutoCarry.PluginMenu:addParam("drawE", "显示E范围", SCRIPT_PARAM_ONOFF, true)
        AutoCarry.PluginMenu:addParam("drawR", "显示R范围", SCRIPT_PARAM_ONOFF, true)
end


function PluginOnTick()
        Target = AutoCarry.GetAttackTarget()

        if AutoCarry.PluginMenu.combo then
                Combo()
        end

        if AutoCarry.PluginMenu.harass then
                eHarass()
        end

        if AutoCarry.PluginMenu.killsteal then
                rKillsteal()
        end
end

function PluginOnDraw()
        if not myHero.dead then
                if AutoCarry.PluginMenu.drawQ and myHero:CanUseSpell(_Q) == READY then
                        DrawCircle(myHero.x, myHero.y, myHero.z, qRange, 0x0099CC)
                end
                if AutoCarry.PluginMenu.drawE and myHero:CanUseSpell(_E) == READY then
                        DrawCircle(myHero.x, myHero.y, myHero.z, eRange, 0x0099CC)
                end
                if AutoCarry.PluginMenu.drawR and myHero:CanUseSpell(_R) == READY then
                        DrawCircle(myHero.x, myHero.y, myHero.z, rRange, 0xFF0000)
                end
        end
end

function Combo()
        if Target ~= nil then
                if AutoCarry.PluginMenu.useQ and myHero:CanUseSpell(_Q) == READY and GetDistance(Target) < qRange then
                        if not AutoCarry.GetCollision(SkillQ, myHero, Target) then
                                AutoCarry.CastSkillshot(SkillQ, Target)
                        end
                end
                if AutoCarry.PluginMenu.useE and myHero:CanUseSpell(_E) == READY and GetDistance(Target) < eRange then
                        AutoCarry.CastSkillshot(SkillE, Target)
                end
                Pop()

                if AutoCarry.PluginMenu.useR and myHero:CanUseSpell(_R) == READY and GetDistance(Target) < rRange then
                        AutoCarry.CastSkillshot(SkillR, Target)
                end
        end
end

function eHarass()
        if Target ~= nil then 
                if myHero:CanUseSpell(_E) == READY and GetDistance(Target) < 1100 then
                        AutoCarry.CastSkillshot(SkillE, Target)
                end
                Pop()
        end
end

function rKillsteal()
        if Target ~= nil and myHero:CanUseSpell(_R) == READY and GetDistance(Target) < rRange and GetDistance(Target) > 200 and Target.health < getDmg("R", Target, myHero) then
                AutoCarry.CastSkillshot(SkillR, Target)
        end
end

function Pop()
        if ePop ~= nil and ePop.valid then
                if GetDistance(Target, ePop) < 300 then
                        CastSpell(_E)
                end
        end
end

function PluginOnCreateObj(obj)
        if obj ~= nil and obj.valid then
                if obj.name:lower():find("luxlightstrike") then -- and isObjectOnEnemy(obj) then
                        ePop = obj
                end
        end
end

function PluginOnDeleteObj(obj)
        if obj == ePop then
                ePop = nil
        end
end
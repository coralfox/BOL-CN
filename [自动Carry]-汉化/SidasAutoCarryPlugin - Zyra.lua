function PluginOnLoad()
SkillP = {spellKey = _Q, range = 1500, speed = 1.9, delay = 550, width = 50}
--Passive Failsafes--
SkillP1 = {spellKey = _Q, range = 1500, speed = 1.9, delay = 550, width = 50}
SkillP2 = {spellKey = _Q, range = 1500, speed = 1.9, delay = 550, width = 50}
SkillP3 = {spellKey = _Q, range = 1500, speed = 1.9, delay = 550, width = 50}
--End of Passive Failsafes--
SkillQ = {spellKey = _Q, range = 825, speed = 1.45, delay = 250, width = 100}
SkillW = {spellKey = _W, range = 825, speed = 1.45, delay = 250, width = 60}
SkillE = {spellKey = _E, range = 1100, speed = 1.0, delay = 235, width = 75}
SkillR = {spellKey = _R, range = 700, speed = 0.7, delay = 250, width = 300}
myMenu()
end

function PluginOnTick()
Target = AutoCarry.GetAttackTarget()
    QREADY = (myHero:CanUseSpell(_Q) == READY)
    WREADY = (myHero:CanUseSpell(_W) == READY)
	EREADY = (myHero:CanUseSpell(_E) == READY)
    RREADY = (myHero:CanUseSpell(_R) == READY)
	if Target then
		if myHero.health < 1 and Menu.AutoPassive then
			AutoCarry.SkillsCrosshair.range = 1450
			AutoCarry.CastSkillshot(SkillP, Target)
			AutoCarry.CastSkillshot(SkillP1, Target)
			AutoCarry.CastSkillshot(SkillP2, Target)
			AutoCarry.CastSkillshot(SkillP3, Target)
		end
		if myHero.health > 1 then
			AutoCarry.SkillsCrosshair.range = 1050
		end
		if Menu.DoubleQW and QREADY and GetDistance(Target) < 817 then
			AutoCarry.CastSkillshot(SkillW, Target)
			AutoCarry.CastSkillshot(SkillQ, Target)
		end
		if Menu.SnareW and EREADY and GetDistance(Target) < 1075 then
			AutoCarry.CastSkillshot(SkillE, Target)
		end
		if Menu2.AutoCarry and GetDistance(Target) < 1100 then
			if Menu.AutoR then
				AutoCarry.CastSkillshot(SkillR, Target)
			end
			if Menu.AutoW then
				AutoCarry.CastSkillshot(SkillW, Target)
			end
			if Menu.AutoQ then
				AutoCarry.CastSkillshot(SkillQ, Target)
			end
			if Menu.AutoE then
				AutoCarry.CastSkillshot(SkillE, Target)
			end
		end
	end
end
function PluginOnCreateObj(obj)
  if Menu.DoubleQW and obj and (obj.name:find("zyra_Q_cas.troy") ~= nil) and GetDistance(obj) < 850 then
      CastSpell(_W,obj.x,obj.z)
end
  if obj and (obj.name:find("Zyra_E_sequence_impact.troy") ~= nil) and GetDistance(obj) < 850 then
     if Target and Menu.AutoW then
       alpha = math.atan(math.abs(Target.z-myHero.z)/math.abs(Target.x-myHero.x))
       locX = math.cos(alpha)*820
       locZ = math.sin(alpha)*820
       CastSpell(_W, math.sign(Target.x-myHero.x)*locX+myHero.x, math.sign(Target.z-myHero.z)*locZ+myHero.z)
    end
end
end
function myMenu()
    Menu = AutoCarry.PluginMenu
	Menu2 = AutoCarry.MainMenu
	Menu:addParam("sep", "[荆棘之兴 婕拉]", SCRIPT_PARAM_INFO, "")
	Menu:addParam("AutoPassive", "自动释放被动", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("AutoQ", "自动Q", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("AutoW", "自动W", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("AutoE", "自动E", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("AutoR", "自动R", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("DoubleQW", "双种子使用Q", SCRIPT_PARAM_ONKEYDOWN, false, string.byte("T"))
	Menu:addParam("SnareW", "自动W暴露敌方", SCRIPT_PARAM_ONKEYDOWN, false, string.byte("A"))
	Menu:addParam("sep1", "[其它设定]", SCRIPT_PARAM_INFO, "")
	Menu:addParam("Draw", "范围显示", SCRIPT_PARAM_ONOFF, true)
	Menu:permaShow("DoubleQW")
	Menu:permaShow("SnareW")
end
function PluginOnDraw()
if Menu.Draw then
DrawCircle(myHero.x, myHero.y, myHero.z, (540+690), 0xFF0000)
DrawCircle(myHero.x, myHero.y, myHero.z,  817, 0xFF0000)
DrawCircle(mousePos.x, mousePos.y, mousePos.z, 75, 0xFF0000)
end
end
function math.sign(x)
 if x < 0 then
  return -1
 elseif x > 0 then
  return 1
 else
  return 0
 end
end
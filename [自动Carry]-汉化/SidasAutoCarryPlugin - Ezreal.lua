------#################################################################################------  ------########################### Ezreal's Time to strike! ############################------ ------###########################         by Toy           ############################------ ------#################################################################################------

--> Version: 1.7

--> Features:
--> Prodictions in every skill, also taking their hitboxes in consideration.
--> Cast options for Q and W in both, autocarry and mixed mode (Works separately).
--> Auto-aim Trueshot Barrage activated with a separated Hotkey (Default is Spacebar, can be changed on the menu, don't set it to R if you have Smartcast enabled for R) so you can use it when you think it's better, and it will still aim for you.
--> KS with Trueshot Barrage, will use Trueshot Barrage if the enemy is killable, as long as the target is within 2000 range (can be turned on/off).
--> Draw options for every skill, and also a option to draw the furthest skill avaiable (Except his ultimate).
--> Options to Last Hit with Q in LastHit and LaneClear mode.
--> Options to cancel Blitzcrank Grab.
--> Options to use Muramana.
--> Option for Auto-attack reset with W.
--> If Mystic Shot would interrupt the auto-attack, it waits till the auto-attack goes off before shoting it, therefore won't interrupt auto-attack(Reborn only, Revamped users won't be affected by this, but can still use the plugin normally).

if myHero.charName ~= "Ezreal" then return end
 
require "Collision"
require "TargetPredictionNONEVIP"
if VIP_USER then require "Prodiction" end
--require "Prodiction"
 
local QRange, WRange, ERange, RRange = 1150, 1050, 475, 2000
local QSpeed, WSpeed, RSpeed = 2000, 1600, 2000
local QDelay, WDelay, RDelay = 0.251, 0.250, 1
local QWidth, WWidth, RWidth = 80, 80, 160 
 
local QAble, WAble, Eable, RAble = false, false, false, false
 
SkillQ = {spellKey = _Q, range = QRange, speed = QSpeed, delay = QDelay, width = QWidth, minions = true }
SkillW = {spellKey = _W, range = WRange, speed = WSpeed, delay = WDelay, width = WWidth, minions = false }
SkillR = {spellKey = _R, range = RRange, speed = RSpeed, delay = RDelay, width = RWidth, minions = false }
 

 
local ProdictQ = VIP_USER and TargetPredictionVIP(QRange, QSpeed, QDelay, QWidth) or TargetPredictionNONEVIP(QRange, QSpeed, QDelay, QWidth)

local ProdictQCol =  Collision(QRange, QSpeed, QDelay, QWidth)

local ProdictW = VIP_USER and TargetPredictionVIP(WRange, WSpeed, WDelay, WWidth) or TargetPredictionNONEVIP(WRange, WSpeed, WDelay, WWidth)
local ProdictR = VIP_USER and TargetPredictionVIP(RRange, RSpeed, RDelay, RWidth) or TargetPredictionNONEVIP(RRange, RSpeed, RDelay, RWidth)


local minHitChance = 0.6
local blitzChamp = nil
local grabbed = false
local grab = nil
 
function PluginOnLoad()
        AutoCarry.SkillsCrosshair.range = 1050
        Menu()                   
end

function KS()
	for i, enemy in ipairs(GetEnemyHeroes()) do
	local rDmg = getDmg("R", enemy, myHero)
		if Target and not Target.dead and Target.health < rDmg and GetDistance(Target) < RRange then
			CastR()
		end
	end
end
 
function PluginOnTick()
    Checks()
    if Target then
      if Target and (AutoCarry.MainMenu.AutoCarry) then
        MysticShot()
        EssenceFlux()
      end
      if Target and (AutoCarry.MainMenu.MixedMode) then
        MysticShot2()
  		  EssenceFlux2()
      end
			if Target and AutoCarry.PluginMenu.useR then
				TrueShotBarrage()
			end
			if AutoCarry.PluginMenu.KS and RAble then KS()
			end
    end
 --
  if AutoCarry.PluginMenu.ToggleMuramana then MuramanaToggle() end
 --
 	if QAble and AutoCarry.PluginMenu.qFarm and AutoCarry.MainMenu.LastHit then
		if Minion and not Minion.type == "obj_Turret" and not Minion.dead and GetDistance(Minion) <= QRange and Minion.health < getDmg("Q", Minion, myHero) then 
			CastSpell(_Q, Minion.x, Minion.z)
		else 
			for _, minion in pairs(AutoCarry.EnemyMinions().objects) do
				if minion and not minion.dead and GetDistance(minion) <= QRange and minion.health < getDmg("Q", minion, myHero) then 
					CastSpell(_Q, minion.x, minion.z)
				end
			end
		end
	end
--
	if AutoCarry.PluginMenu.Cancelblitzgrabs and grab ~= nil and grab:GetDistance(myHero) < 500 then
	  destX = myHero.x * 4 - blitzChamp.x*3
      destZ = myHero.z * 4  - blitzChamp.z*3
	  if math.abs((myHero.x-blitzChamp.x) * (grab.z - blitzChamp.z) - (myHero.z-blitzChamp.z) * (grab.x - blitzChamp.x)) < 39000
	  then CastSpell(_E, destX, destZ) end
	end
--	
	if QAble and AutoCarry.PluginMenu.qClear and AutoCarry.MainMenu.LaneClear then
		if Minion and not Minion.type == "obj_Turret" and not Minion.dead and GetDistance(Minion) <= QRange and Minion.health < getDmg("Q", Minion, myHero) then 
			CastSpell(_Q, Minion.x, Minion.z)
		else 
			for _, minion in pairs(AutoCarry.EnemyMinions().objects) do
				if minion and not minion.dead and GetDistance(minion) <= QRange and minion.health < getDmg("Q", minion, myHero) then 
					CastSpell(_Q, minion.x, minion.z)
				end
			end
		end
	end
end
 
function PluginOnDraw()
    if not myHero.dead then
		  if QAble and AutoCarry.PluginMenu.drawF then
      DrawCircle(myHero.x, myHero.y, myHero.z, QRange, 0xFFFFFF)
      else
      if WAble and AutoCarry.PluginMenu.drawF then
      DrawCircle(myHero.x, myHero.y, myHero.z, WRange, 0xFFFFFF)
			else
      end
      end
      if QAble and AutoCarry.PluginMenu.drawQ then
      DrawCircle(myHero.x, myHero.y, myHero.z, QRange, 0xFFFFFF)
		  end
		  if WAble and AutoCarry.PluginMenu.drawW then
      DrawCircle(myHero.x, myHero.y, myHero.z, WRange, 0x9933FF)
		  end
      if EAble and AutoCarry.PluginMenu.drawE then
      DrawCircle(myHero.x, myHero.y, myHero.z, ERange, 0xFF0000)
		  end
      if RAble and AutoCarry.PluginMenu.drawR then
      DrawCircle(myHero.x, myHero.y, myHero.z, RRange, 0x9933FF)
	    end
   end
end

 function Menu()
	local HKR = string.byte("32")
	AutoCarry.PluginMenu:addParam("hitChance", "命中率", SCRIPT_PARAM_SLICE, 60, 0, 100, 0)
	AutoCarry.PluginMenu:addParam("sep", "-- 大招选项 --", SCRIPT_PARAM_INFO, "")
	AutoCarry.PluginMenu:addParam("useR", "使用 - 精准弹幕(R)", SCRIPT_PARAM_ONKEYDOWN, false, HKR)
	AutoCarry.PluginMenu:addParam("KS", "KS - 精准弹幕(R)", SCRIPT_PARAM_ONOFF, true)
	AutoCarry.PluginMenu:addParam("sep1", "-- 自动Carry选项 --", SCRIPT_PARAM_INFO, "")
	--AutoCarry.PluginMenu:addParam("resetW", "AA Reset - 精华跃动(W)", SCRIPT_PARAM_ONOFF, true)
	AutoCarry.PluginMenu:addParam("sepC", "[释放]", SCRIPT_PARAM_INFO, "")
	AutoCarry.PluginMenu:addParam("useQ", "使用 - 秘术射击(Q)", SCRIPT_PARAM_ONOFF, true)
	AutoCarry.PluginMenu:addParam("useW", "使用 - 精华跃动(W)", SCRIPT_PARAM_ONOFF, true)
	AutoCarry.PluginMenu:addParam("sep2", "-- 混合模式选项 --", SCRIPT_PARAM_INFO, "")
	AutoCarry.PluginMenu:addParam("useQ2", "使用 - 秘术射击(Q)", SCRIPT_PARAM_ONOFF, true)
	AutoCarry.PluginMenu:addParam("useW2", "使用 - 精华跃动(W)", SCRIPT_PARAM_ONOFF, true)
	AutoCarry.PluginMenu:addParam("sep3", "-- 补兵选项 --", SCRIPT_PARAM_INFO, "")
	AutoCarry.PluginMenu:addParam("qFarm", "补兵 - 秘术射击(Q)", SCRIPT_PARAM_ONOFF, true)
	AutoCarry.PluginMenu:addParam("sep4", "-- 清兵选项 --", SCRIPT_PARAM_INFO, "")
	AutoCarry.PluginMenu:addParam("qClear", "清兵 - 秘术射击(Q)", SCRIPT_PARAM_ONOFF, true)
	AutoCarry.PluginMenu:addParam("sep5", "-- 其它选项 --", SCRIPT_PARAM_INFO, "")
	AutoCarry.PluginMenu:addParam("Cancelblitzgrabs", "躲避 - 机器人Q", SCRIPT_PARAM_ONOFF, true)
	AutoCarry.PluginMenu:addParam("ToggleMuramana", "切换 - 魔切", SCRIPT_PARAM_ONOFF, true)
	AutoCarry.PluginMenu:addParam("sep6", "-- 显示选项 --", SCRIPT_PARAM_INFO, "")
	AutoCarry.PluginMenu:addParam("drawF", "显示 - 可用技能最远距离", SCRIPT_PARAM_ONOFF, true)
  AutoCarry.PluginMenu:addParam("drawQ", "显示 - 秘术射击(Q)", SCRIPT_PARAM_ONOFF, true)
  AutoCarry.PluginMenu:addParam("drawW", "显示 - 精华跃动(W)", SCRIPT_PARAM_ONOFF, true)
  AutoCarry.PluginMenu:addParam("drawE", "显示 - 奥术跃迁(E)", SCRIPT_PARAM_ONOFF, true)
  AutoCarry.PluginMenu:addParam("drawR", "显示 - 精准弹幕(R)", SCRIPT_PARAM_ONOFF, true)
end
 
function Checks()
        QAble = (myHero:CanUseSpell(_Q) == READY)
        WAble = (myHero:CanUseSpell(_W) == READY)
        EAble = (myHero:CanUseSpell(_E) == READY)
        RAble = (myHero:CanUseSpell(_R) == READY)
        Target = AutoCarry.GetAttackTarget()
		Minion = AutoCarry.GetMinionTarget()
		minHitChance=AutoCarry.PluginMenu.hitChance/100
end
 
 
function GetQPrediction(enemy)

		if minHitChance ~= 0 and ProdictQ:GetHitChance(enemy) < minHitChance then return nil end
		local QPos = ProdictQ:GetPrediction(enemy)
		local willCollide = ProdictQCol:GetMinionCollision(myHero, QPos)
		if not willCollide  then
			return QPos
		else
			return nil
		end
end

function GetWPrediction(enemy)

		if minHitChance ~= 0 and ProdictW:GetHitChance(enemy) < minHitChance then return nil end
		local WPos = ProdictW:GetPrediction(enemy)
		return WPos

end

function GetRPrediction(enemy)

		if minHitChance ~= 0 and ProdictR:GetHitChance(enemy) < minHitChance then return nil end
		local RPos = ProdictR:GetPrediction(enemy)
		return RPos

end

 
function MysticShot()
        if QAble and AutoCarry.PluginMenu.useQ and Target then 
			CastQ()
		end
end    
 
function EssenceFlux()
        if WAble and AutoCarry.PluginMenu.useW  then
			CastW()
		end
  end  

function MysticShot2()
        if QAble and AutoCarry.PluginMenu.useQ2 and Target then 
			CastQ()
		end
end    
 
function EssenceFlux2()
        if WAble and AutoCarry.PluginMenu.useW2 then 
			CastW()
		end
  end  
	
function MysticShot3()
        if QAble and AutoCarry.PluginMenu.qFarm then 
			CastQ()
		end
end    
	
function TrueShotBarrage()
        if RAble then 
			CastR() 
		end
end    
 
local function getHitBoxRadius(target)
        return GetDistance(target, target.minBBox)
end
 
function CastQ()
		
		local QPos = GetQPrediction(Target)
		
		if QPos and Target then
			if GetDistance(QPos) - getHitBoxRadius(Target)/2 < QRange then
				CastSpell(_Q, QPos.x, QPos.z) 
			end
		end	
end

function CastW()

		local WPos = GetWPrediction(Target)
		
		if WPos and Target then		
			if GetDistance(WPos) - getHitBoxRadius(Target)/2 < WRange then
				CastSpell(_W, WPos.x, WPos.z)
			end
		end		
end

function CastR()

		local RPos = GetRPrediction(Target)
		
		if RPos and Target then
			if GetDistance(RPos) - getHitBoxRadius(Target)/2 < RRange then
				CastSpell(_R, RPos.x, RPos.z)
			end
		end				
end

function OnCreateObj(object)
		if object.name:find("FistGrab") then
			grabbed = true
			grab = object
		end
end

function OnDeleteObj(object)
	  if object.name:find("FistGrab") then
			 grabbed = false
			 grab = nil
  	end
end

function MuramanaToggle()
	if Target and Target.type == myHero.type and GetDistance(Target) <= QRange and not MuramanaIsActive() and (AutoCarry.MainMenu.AutoCarry or AutoCarry.MainMenu.MixedMode) then
		MuramanaOn()
	elseif not Target and MuramanaIsActive() then
		MuramanaOff()
	end
end
local SkillQ = {spellKey = _Q, range = 950, speed = 0.9, width = 190, delay = 290}
 
function PluginOnLoad()
        mainLoad()
        mainMenu()
end
 
function PluginOnTick()
        Target = AutoCarry.GetAttackTarget()
        QREADY = (myHero:CanUseSpell(_Q) == READY)
        WREADY = (myHero:CanUseSpell(_W) == READY)
        RREADY = (myHero:CanUseSpell(_R) == READY)
 
        if Menu.autoks then
                for i = 1, heroManager.iCount, 1 do
                        local ksTarget = heroManager:getHero(i)
                        if ValidTarget(ksTarget, RRange) and GetDistance(Target) <= RRange then
                                if ksTarget.health <= getDmg("R", Target, myHero) then
                                        CastSpell(_R, Target)
                                end
                        end
                end
        end
 
        if Target and Menu2.AutoCarry then
                if Menu.useQ and QREADY and GetDistance(Target) <= QRange then
                        AutoCarry.CastSkillshot(SkillQ, Target)
                end
                if Menu.useW and WREADY and GetDistance(Target) <= WRange then
                        CastSpell(_W, Target.x, Target.z)
                end
        end
end
 
function mainLoad()
        AutoCarry.SkillsCrosshair.range = 950
        QRange, WRange, RRange = 950, 650, 200
        QREADY, WREADY, RREADY = false, false, false
        Menu = AutoCarry.PluginMenu
        Menu2 = AutoCarry.MainMenu
end
 
function mainMenu()
        Menu:addParam("autoks", "自动R抢人头", SCRIPT_PARAM_ONOFF, true)
        Menu:addParam("useQ", "连招使用Q", SCRIPT_PARAM_ONOFF, true)
        Menu:addParam("useW", "连招使用W", SCRIPT_PARAM_ONOFF, true)
end
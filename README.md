##<div align="center">Coralfox BOL 仓库</div>

==================

###### 说明: 脚本都是BOL(英雄联盟辅助)所用,发布在BOL官方论坛,经过自己收集以及朋友的帮助,下载/汉化.然后免费共享.
------------------
###### 公告: 
######      1.请不要删除脚本中的免费提示信息,转载请保留
######      2.请不要用于各种商业目的,如淘宝出售,集成到各种所谓辅助里出售等等
######      3.请不要各种催促我汉化,修改;汉化,修改脚本只是出于个人兴趣,并不是我的工作
######      4.BOL的汉化,使用等我都有教程;最好是自己学习,俗语云"求人不如求己"

------------------

###当前脚本:
- ####[iSAC - Base lib for champion scripts] (http://botoflegends.com/forum/topic/5018-library-isac/)
> ###### Requires [Klokje's Collision Lib] (http://botoflegends.com/forum/topic/4317-libraryvip-collision/)
> ###### Features
> - Offers a great base for both beginner and experienced scripters to create scripts on.
> - Has simple functions for Orbwalking.
> - Summoner spells support.
> - Customizable minion markers and lasthitting functions.
> - Extensive items class.

- ####[iLux - Penta Rainbows] (http://botoflegends.com/forum/topic/4326-script-ilux-penta-rainbows/)
> ###### Requires [Klokje's Collision Lib] (http://botoflegends.com/forum/topic/4317-libraryvip-collision/)
> ###### Features
> - PewPew! - *Main Combo*
> - Poke! - *Harass with E*
> - Munching Minions - *Auto Farm with AA and E*
> - Auto Trigger E - *Auto Trigger E when enemies are within range.*
> - Auto Ultimate - *Finish your enemies with a colourful laser.*
> - Ultimate Notifier - *Pings when an enemy can be lasered.*
> - Auto Shield - *Automatically shields you.*
> - Smart Save - *Automatically uses Zhonya's and Wooglet's.*
> - Steal Tze Buffs - \*[BETA] Steals Jungle buffs.\*
> - Prodiction - *Supports Prodiction*

- ####iLise
> ###### Features
> - PewPew! - *Main Comboo*
> - Poke! - *Harass with Q*
> - Munching Minions - *Auto Farm with AA and Q*
> - Munching Jungle - *Auto Farm jungle*
> - Toggable Spells - *Choose which spells you would like to use*
> - Auto KS - *Auto KS with Q* 
> - Orb Walking - *Orb Walking like a pro*
> - Prodiction - *Supports Prodiction*

- ####[Icy iLiss] (http://botoflegends.com/forum/topic/4288-script-icy-iliss/)
> ###### Features
> - PewPew! - *Main Comboo*
> - Poke! - *Harass with Q*
> - Munching Minions - *Auto Farm with AA and Q*
> - Smart Save - *Automatically uses Ultimate, Zhonya's and Wooglet's.*

- ####[iCass Classic - No Chickenshit] (http://botoflegends.com/forum/topic/4437-script-icass-classic/)
> ###### Features
> - PewPew! - *Main Combo*
> - Poke! - *Harass with Q*
> - Auto E - *Auto E poisoned targets*

- ####iYo
> ###### Features
> - PewPew! - *Main Combo*
> - Munching Minions - *Auto Farm with AA and W*
> - Orb Walking - *Orb Walking like a pro*
> - Auto Ultimate - *Zombies incoming!*

------------------
###Unfinished/Partial/WIP Scripts:
######*Please send me a PM or yell at me in the shoutbox if you have any suggestions or ideas for these scripts. There are no threads for these as they are not finished yet.*

- ####iRelia
> ###### Features
> - PewPew! - *Main Combo*
> - Munching Minions - *Auto Farm with AA and Q*
> - Auto Q - *Automatically uses Q on killable enemies.*

- ####iFizz
> ###### Features
> - PewPew! - *Main Combo*
> - Poke! - *Harass with W-Q*
> - Auto KS - *Automatically uses Q-W-Ignite on killable enemies.*

- ####iZyra
> ###### Requires [2DGeometry] (http://botoflegends.com/forum/topic/1614-library-2d-geometry/)
> ###### Features
> - PewPew! - *Main Combo*
> - Poke! - *Harass with Q*
> - Munching Minion - \*[TBA] Auto Farm with AA and Q\*
> - Auto Passive - *Automatically uses Passive*
> - Ult Enemy Team - *Automatically uses ultimate when the whole enemy team can be hit.*

------------------
### Other Scripts:
######*These are mockups, uncategorized or merely hosted here for the sake of availability. If you're not particularly interested in these, don't pay attention to them. :wink:*

- ####Items Class
> ######A mockup for Klokje, made out of boredom.

- ####[Stream Tools] (http://botoflegends.com/forum/topic/4741-script-stream-tools/)
> ######Automatically disables overlay depending on running processes. Lacking ideas and suggestions.
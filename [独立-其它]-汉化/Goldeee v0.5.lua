--[[
    Goldeee v0.5
        by Weee
        
        Thanks for help with ideas and formulas to gReY, botirk, eXtragoZ and everyone else from this topic: http://botoflegends.com/forum/topic/5173-formula-to-estimate-teams-strength/

    Features:
        - Shows the aprox. teams power estimated with simple formula, which includes items gold cost + levels + ultimate levels (6,11,16) and buffs (nashor, blue, red).
        - Shows the aprox. gold cost of both teams (if you choose it over points) and each player when you hold TAB.
          It's not accurate and calculated ONLY from the summary of all items (which is most important thing).

    What's the point?
        It's useful to track gold difference when you're spectating the game or watching a stream or tournament, right? It's useful to know this in-game too.
        Sometimes it happens that your team score sucks hard, but your gold is equal or even better because of all the towers, drakes and CS you've got.

        Right now this script simply shows you how much players spent on their current items. Obviously this script is not 100% accurate, but still it might be helpful.
        Ideally it should take in count consumables even if they were used: wards, potions, etc.

        Also maybe it's better to replace gold with some kind of points system, which will take in count:
            - items
            - buffs (red, blue, barons)
            - aprox. amount of active wards at the time
            - levels
            - spell and item stacks (tear, nasus and veigar q, etc.)
            - KDA (exactly KDA, because it demonstrates some kind of skill and luck during the game)
            - towers and inhibs

        v0.5:
            Now instead of 4 bars for each team there are only 2 bars for both: power and gold. If you like 4 bars mode - you can still enable it from menu.
            Thanks to gReY for DrawBar() and bars sprites :3
        v0.4:
            Some simple GUI and few updates to the code.
        v0.3:
            I tried to implement this simple points system. You can enable/disable it via menu.
        v0.2:
            Fixed some bugs. Thanks to Manciuszz for pointing some mistakes out.
        v0.1:
            This goes to public.
]]

--if GetGame().map.name ~= "Summoner's Rift" then return end



function OnLoad()
    UpdateWindow()

    heroTable = {}
    teamTable = {
        [100] = {       -- TEAM_BLUE
            cost = 0,
            pts = 0,
            lvls = 0,
            ultBonus = 0,
            buffBonus = 0,
            cs = 0,
        },
        [200] = {       -- TEAM_RED
            cost = 0,
            pts = 0,
            lvls = 0,
            ultBonus = 0,
            buffBonus = 0,
            cs = 0,
        },
    }

    maxPower = 1

    sixStartAt = 150
    sixEvery = 30
    bigStartAt = 210
    bigEvery = 90

    
    dCoords = { x = WINDOW_W / 2, y = 0 }
    dOffset = 52
    barLen = 150
    textSize = 15
 
    bg = GetSprite("Goldeee\\goldeee-ui.png")
    bgTable = {
        { GetSprite("Goldeee\\goldeee-ui-bg1.png"), 100 },
        { GetSprite("Goldeee\\goldeee-ui-bg2.png"), 100 },
        { GetSprite("Goldeee\\goldeee-ui-bg3.png"), 100 },
        { GetSprite("Goldeee\\goldeee-ui-bg4.png"), 100 },
        { GetSprite("Goldeee\\goldeee-ui-bg5.png"), 100 },
        { GetSprite("Goldeee\\goldeee-ui-bg6.png"), 100 },
    }
    bar_green, bar_red = GetSprite("Goldeee\\bar_green.dds"), GetSprite("Goldeee\\bar_red.dds")

	GConfig = scriptConfig("Goldeee金钱对比", "goldeee")
    GConfig:addParam("ShowChampInfo", "显示英雄信息 (1920x1080)", SCRIPT_PARAM_ONOFF, false)
    GConfig:addParam("UseSeparatedBars", "使用单独信息条", SCRIPT_PARAM_ONOFF, false)
    GConfig:addParam("usecolors", "单独信息条使用队伍颜色", SCRIPT_PARAM_ONOFF, true)
    GConfig:addParam("animate", "动画效果", SCRIPT_PARAM_ONOFF, true)
    GConfig:addParam("show", "显示，当按住...", SCRIPT_PARAM_ONKEYDOWN, false, 9)
    
    GConfig.show = false

    ------------------------- Thx to Manciuszz for pointing out at championIndex with TEAM_RED: ------------------------------
    --if myHero.team == TEAM_RED then championIndex = { 1,6,7,8,9,10,2,3,4,5 } else championIndex = { 1,2,3,4,5,6,7,8,9,10 } end
    if myHero.team == TEAM_RED then championIndex = { 1,7,8,9,10,2,3,4,5,6 } else championIndex = { 1,2,3,4,5,6,7,8,9,10 } end
    --------------------------------------------------------------------------------------------------------------------------

    for i = 1, heroManager.iCount do
        local hero = heroManager:GetHero(i)
        hero.cost = 0
        hero.points = 0
        table.insert(heroTable, hero)
    end

    PrintChat("Goldeee v0.5")
end

function DrawBar(x, y, barLen, percentage)
    assert(bar_green and bar_red, "绘制条: Sprites资源未载入")
    assert(percentage>=0 and percentage<=1, "绘制条: 比例不在正常范围内")
    assert(x and y, "绘制条: 未找到位置")
    bar_green:DrawEx(Rect(0,0,barLen*percentage,bar_green.height), --rect
        D3DXVECTOR3(0,0,0), --center
        D3DXVECTOR3(x,y,0), --Pos
        0xFF) --opacity
    bar_red:DrawEx(Rect(barLen*percentage,0,barLen,bar_red.height), --rect
        D3DXVECTOR3(0,0,0), --center
        D3DXVECTOR3(x+barLen*percentage,y,0), --Pos
        0xFF) --opacity
end

function OnDraw()
    --DrawText("Perfect CS: " .. GetPerfectCsAtTime(), 20, 200, 200, ARGB(0xFF,0xFF,0xFF,0xFF))
    if GConfig.show then
        if GConfig.ShowChampInfo then
            for i, hero in pairs(heroTable) do
                if hero.team == player.team then
                    DrawText("道具: " .. hero.cost, 15, 1320, 316+(championIndex[i])*49, ARGB(0xFF,0x00,0xFF,0x00))
                else
                    DrawText("道具: " .. hero.cost, 15, 1320, 316+(championIndex[i])*49+27, ARGB(0xFF,0xFF,0x00,0x00))
                end
            end
        end
        
        teamTable[player.team].textColor = ((not GConfig.UseSeparatedBars or GConfig.usecolors) and ARGB(0xFF,0x00,0xFF,0x00)) or ARGB(0xFF,0xFF,0xFF,0xFF)
        teamTable[TEAM_ENEMY].textColor = ((not GConfig.UseSeparatedBars or GConfig.usecolors) and ARGB(0xFF,0xFF,0x00,0x00)) or ARGB(0xFF,0xFF,0xFF,0xFF)
        teamTable[player.team].powerPct = math.round((teamTable[player.team].pts / maxPower) * 100)
        teamTable[TEAM_ENEMY].powerPct = math.round((teamTable[TEAM_ENEMY].pts / maxPower) * 100)
        teamTable[player.team].goldPct = math.round((teamTable[player.team].cost / maxGold) * 100)
        teamTable[TEAM_ENEMY].goldPct = math.round((teamTable[TEAM_ENEMY].cost / maxGold) * 100)
        
        --White flashy splash background:
        for i, flash in pairs(bgTable) do
            flash[1]:Draw(dCoords.x-400,dCoords.y,flash[2])
        end
        --Black splash background:
        bg:Draw(dCoords.x-381,dCoords.y,255)

        if GConfig.UseSeparatedBars then
            --Our team:
            --Our power bar:
            teamTable[player.team].powerBar = ( teamTable[player.team].powerPct / 100 ) * barLen
            DrawRectangle(dCoords.x-dOffset-(barLen+3), dCoords.y+20, barLen+2, 13, ARGB(0xFF,0xFF,0xFF,0xFF))  --outline
            DrawRectangle(dCoords.x-dOffset-(barLen+2), dCoords.y+21, barLen, 11, ARGB(0xFF,0x00,0x00,0x00))    --black bg
            DrawRectangle(dCoords.x-dOffset-(teamTable[player.team].powerBar+2), dCoords.y+21, teamTable[player.team].powerBar, 11, teamTable[player.team].textColor)           --power bar
            DrawTextA(""..math.round(teamTable[player.team].pts), 12, dCoords.x-dOffset-barLen, dCoords.y+20, ARGB(0xFF,0xFF,0xFF,0xFF), "left")
            DrawTextA("实力", 12, dCoords.x-dOffset-barLen-7, dCoords.y+20, ARGB(0xFF,0xFF,0xFF,0xFF), "right")
            DrawTextA(teamTable[player.team].powerPct.."%", 12, dCoords.x-dOffset+3, dCoords.y+20, ARGB(0xFF,0xFF,0xFF,0xFF), "left")
            --Our gold bar:
            teamTable[player.team].goldBar = ( teamTable[player.team].goldPct / 100 ) * barLen
            DrawRectangle(dCoords.x-dOffset-(barLen+3), dCoords.y+40, barLen+2, 13, ARGB(0xFF,0xFF,0xFF,0xFF))  --outline
            DrawRectangle(dCoords.x-dOffset-(barLen+2), dCoords.y+41, barLen, 11, ARGB(0xFF,0x00,0x00,0x00))    --black bg
            DrawRectangle(dCoords.x-dOffset-(teamTable[player.team].goldBar+2), dCoords.y+41, teamTable[player.team].goldBar, 11, teamTable[player.team].textColor)             --gold bar
            DrawTextA(""..math.round(teamTable[player.team].cost), 12, dCoords.x-dOffset-barLen, dCoords.y+40, ARGB(0xFF,0xFF,0xFF,0xFF), "left")
            DrawTextA("金钱", 12, dCoords.x-dOffset-barLen-7, dCoords.y+40, ARGB(0xFF,0xFF,0xFF,0xFF), "right")
            DrawTextA(teamTable[player.team].goldPct.."%", 12, dCoords.x-dOffset+3, dCoords.y+40, ARGB(0xFF,0xFF,0xFF,0xFF), "left")
            --Their team:
            --Their power bar:
            teamTable[TEAM_ENEMY].powerBar = ( teamTable[TEAM_ENEMY].powerPct / 100 ) * barLen
            DrawRectangle(dCoords.x+dOffset, dCoords.y+20, barLen+2, 13, ARGB(0xFF,0xFF,0xFF,0xFF))             --outline
            DrawRectangle(dCoords.x+dOffset+1, dCoords.y+21, barLen, 11, ARGB(0xFF,0x00,0x00,0x00))             --black bg
            DrawRectangle(dCoords.x+dOffset+1, dCoords.y+21, teamTable[TEAM_ENEMY].powerBar, 11, teamTable[TEAM_ENEMY].textColor)                     --power bar
            DrawTextA(""..math.round(teamTable[TEAM_ENEMY].pts), 12, dCoords.x+dOffset+barLen, dCoords.y+20, ARGB(0xFF,0xFF,0xFF,0xFF), "right")
            DrawTextA("实力", 12, dCoords.x+dOffset+barLen+6, dCoords.y+20, ARGB(0xFF,0xFF,0xFF,0xFF), "left")
            DrawTextA(teamTable[TEAM_ENEMY].powerPct.."%", 12, dCoords.x+dOffset-4, dCoords.y+20, ARGB(0xFF,0xFF,0xFF,0xFF), "right")
            --Their gold bar:
            teamTable[TEAM_ENEMY].goldBar = ( teamTable[TEAM_ENEMY].goldPct / 100 ) * barLen
            DrawRectangle(dCoords.x+dOffset, dCoords.y+40, barLen+2, 13, ARGB(0xFF,0xFF,0xFF,0xFF))             --outline
            DrawRectangle(dCoords.x+dOffset+1, dCoords.y+41, barLen, 11, ARGB(0xFF,0x00,0x00,0x00))             --black bg
            DrawRectangle(dCoords.x+dOffset+1, dCoords.y+41, teamTable[TEAM_ENEMY].goldBar, 11, teamTable[TEAM_ENEMY].textColor)                      --gold bar
            DrawTextA(""..math.round(teamTable[TEAM_ENEMY].cost), 12, dCoords.x+dOffset+barLen, dCoords.y+40, ARGB(0xFF,0xFF,0xFF,0xFF), "right")
            DrawTextA("金钱", 12, dCoords.x+dOffset+barLen+6, dCoords.y+40, ARGB(0xFF,0xFF,0xFF,0xFF), "left")
            DrawTextA(teamTable[TEAM_ENEMY].goldPct.."%", 12, dCoords.x+dOffset-4, dCoords.y+40, ARGB(0xFF,0xFF,0xFF,0xFF), "right")
        else
            newBarLen = (barLen+dOffset)*2
            --Power bar:
            DrawRectangle(dCoords.x-dOffset-(barLen+3), dCoords.y+20, newBarLen+2, 13, ARGB(0xFF,0xFF,0xFF,0xFF))  --outline
            DrawRectangle(dCoords.x-dOffset-(barLen+2), dCoords.y+21, newBarLen, 11, ARGB(0xFF,0x00,0x00,0x00))    --black bg
            DrawBar(dCoords.x-dOffset-(barLen+2), 21, newBarLen, teamTable[player.team].powerPct/100)
            DrawTextA("实力", 12, dCoords.x-dOffset-barLen-9, dCoords.y+20, ARGB(0xFF,0xFF,0xFF,0xFF), "right")
            DrawTextA("实力", 12, dCoords.x+dOffset+barLen+5, dCoords.y+20, ARGB(0xFF,0xFF,0xFF,0xFF), "left")
            DrawTextA(""..math.round(teamTable[player.team].pts), 12, dCoords.x-dOffset-barLen, dCoords.y+20, ARGB(0xFF,0xFF,0xFF,0xFF), "left")
            DrawTextA(""..math.round(teamTable[TEAM_ENEMY].pts), 12, dCoords.x+dOffset+barLen-4, dCoords.y+20, ARGB(0xFF,0xFF,0xFF,0xFF), "right")
            DrawTextA(teamTable[player.team].powerPct.."%", 12, dCoords.x, dCoords.y+20, ARGB(0xFF,0xFF,0xFF,0xFF), "center")
            --Gold bar:
            DrawRectangle(dCoords.x-dOffset-(barLen+3), dCoords.y+40, newBarLen+2, 13, ARGB(0xFF,0xFF,0xFF,0xFF))  --outline
            DrawRectangle(dCoords.x-dOffset-(barLen+2), dCoords.y+41, newBarLen, 11, ARGB(0xFF,0x00,0x00,0x00))    --black bg
            DrawBar(dCoords.x-dOffset-(barLen+2), 41, newBarLen, teamTable[player.team].goldPct/100)
            DrawTextA("金钱", 12, dCoords.x-dOffset-barLen-9, dCoords.y+40, ARGB(0xFF,0xFF,0xFF,0xFF), "right")
            DrawTextA("金钱", 12, dCoords.x+dOffset+barLen+5, dCoords.y+40, ARGB(0xFF,0xFF,0xFF,0xFF), "left")
            DrawTextA(""..math.round(teamTable[player.team].cost), 12, dCoords.x-dOffset-barLen, dCoords.y+40, ARGB(0xFF,0xFF,0xFF,0xFF), "left")
            DrawTextA(""..math.round(teamTable[TEAM_ENEMY].cost), 12, dCoords.x+dOffset+barLen-4, dCoords.y+40, ARGB(0xFF,0xFF,0xFF,0xFF), "right")
            DrawTextA(teamTable[player.team].goldPct.."%", 12, dCoords.x, dCoords.y+40, ARGB(0xFF,0xFF,0xFF,0xFF), "center")
        end
        
        
    end
end

function OnTick()
    for i, team in pairs(teamTable) do
        for j, stat in pairs(team) do
            teamTable[i][j] = 0
        end
    end
    maxPower = 0
    maxGold = 0
    for i, hero in pairs(heroTable) do
        hero.cost = 0
        for slot = ITEM_1, ITEM_6 do
            local rItem = hero:getItem(slot)
            local item = ( rItem and rItem.id >= 1000 and GetItem(rItem.id) ) or nil
            if item and item.gold.total and item.gold.total > 0 then
                hero.cost = hero.cost + item.gold.total * rItem.stacks
            end
        end
        teamTable[hero.team].cost = teamTable[hero.team].cost + hero.cost
        teamTable[hero.team].lvls = teamTable[hero.team].lvls + hero.level
        local ultLvlBonus = 0
        if hero.level >= 16 then ultLvlBonus = 3
            elseif hero.level >= 11 then ultLvlBonus = 2.5
                elseif hero.level >= 6 then ultLvlBonus = 1.5
        end
        teamTable[hero.team].ultBonus = teamTable[hero.team].ultBonus + ultLvlBonus
        local buffBonus = 0
        for j = 1, hero.buffCount do
            local buff = hero:getBuff(j)
            if not hero.dead and buff.valid then
                if buff.name == "exaltedwithbaronnashor" then buffBonus = buffBonus + 4
                    elseif buff.name == "blessingofthelizardelder" then buffBonus = buffBonus + 2
                        elseif buff.name == "crestoftheancientgolem" then buffBonus = buffBonus + 2
                end
            end
        end
        teamTable[hero.team].buffBonus = teamTable[hero.team].buffBonus + buffBonus
        --teamTable[hero.team].cs = teamTable[hero.team].cs + hero.minionKill
    end
    for i, team in pairs(teamTable) do
        teamTable[i].pts = ( (team.cost * team.lvls) / 100 ) + team.ultBonus*1000 + team.buffBonus*250
        maxPower = (maxPower == 0 and maxPower+teamTable[i].pts+1) or (maxPower + teamTable[i].pts)
        maxGold = (maxGold == 0 and maxGold+teamTable[i].cost+1) or (maxGold + teamTable[i].cost)
    end
    if GConfig.show then
        for i, flash in pairs(bgTable) do
            if GConfig.animate then
                local direction = math.ceil(math.random(1,2))
                if direction == 1 then
                    flash[2] = flash[2] + math.random(0.01,3)
                else
                    flash[2] = flash[2] - math.random(0.01,3)
                end
                if flash[2] >= 200 then
                    flash[2] = flash[2] - math.random(20,50)
                elseif flash[2] <= 10 then
                    flash[2] = flash[2] + math.random(20,50)
                end
            else
                flash[2] = 150
            end
        end
    end
end

function GetPerfectCsAtTime()
	local perfectScore = 0
	if GetInGameTimer() >= sixStartAt then
		perfectScore = perfectScore + 6 * (1 + math.floor((GetInGameTimer()-sixStartAt)/sixEvery))
	end
	if GetInGameTimer() >= bigStartAt then
		perfectScore = perfectScore + (1 + math.floor((GetInGameTimer()-bigStartAt)/bigEvery))
	end
    return perfectScore
end

function UnLoad()
    bg:Release()
    for i, flash in pairs(bgTable) do
        flash[1]:Release()
    end
    background:Release()
    bar_green:Release()
    bar_red:Release()
end

-- Some tests:
-- Team Cost:  Levels of all heroes:         BasePts:  ultBonus:    Pts:
-- 12000 gold, lvls: 5  5   5   5   5 (25) = 3000                   = 3000
-- 12000 gold, lvls: 6  5   5   5   5 (26) = 3120      + 1500       = 4620
-- 25000 gold, lvls: 5  5   5   5   5 (25) = 6250                   = 6250
-- 25000 gold, lvls: 6  5   5   5   5 (26) = 6500      + 1500       = 8000
-- 25000 gold, lvls: 6  6   5   5   5 (27) = 6750      + 3000       = 9750
-- 40000 gold, lvls: 10 10  9   9   9 (47) = 18800     + 7500       = 26300
-- 40000 gold, lvls: 11 10  9   9   9 (48) = 19200     + 8500       = 27700
-- 40000 gold, lvls: 11 11  9   9   9 (49) = 19200     + 9500       = 28700

-- Other tests (team.cs / (GetInGameTimer() / 5))*2400
-- 150 + 80 + 140 + 170 + 5 = 545       @ 20 min    *2400 = 5450    *5 = 27250
-- 200 + 85 + 180 + 190 + 8 = 663       @ 20 min    *2400 = 6630    *5 = 33150

-- Other tests (team.cs / GetInGameTimer())*2400
-- 150 + 80 + 140 + 170 + 5 = 545       @ 20 min    *2400 = 1090    *5 = 5450
-- 200 + 85 + 180 + 190 + 8 = 663       @ 20 min    *2400 = 1326    *5 = 6630
-- 220 + 180 + 170 + 120 + 11 = 701     @ 30 min    *2400 = 935     *5 = 4673
-- 260 + 250 + 250 + 150 + 13 = 923     @ 30 min    *2400 = 1230    *5 = 6153
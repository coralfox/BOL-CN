--[[ 
	@Name = AutoRemoveCC
	@Version = 1.1
	@Author = Trees
	@Description = This script attempts to remove all applicable types of CC using: MC, QSS (MS), Cleanse, GP W, Ali R, Olaf R.
	Currently this script supports teammate, enemy, and CC selection. 
	
	@TODO: (Sorted by priority) 
		-- Slider based on CC Duration
		-- Polymorph, Amumu ult
		-- Barrier
	@FUTURE: 
		-- Evadeee-style Champion-specific Enable/Disable AutoCC Remove
		-- Stuff with health (Barrier)
		-- Self-MC?
]]

function UpdateLib()
 local URL = "http://ys-e.ys168.com/2.0/406354172/p45555F395N2gxVItU/OnCC.lua"
 local LIB_PATH = BOL_PATH.."Scripts\Common\OnCC.lua"
 DownloadFile(URL, LIB_PATH, function()
                if FileExist(LIB_PATH) then
                    PrintChat("<font color='#FF0000'> >> 支持库已下载，需要F9两次重新载入 <<</font>")
                end
                end)
end

if FileExist(SCRIPT_PATH..'Common/OnCC.lua') then require "OnCC" else UpdateLib() end


_G.OnCCSimpleMode = true

OnCC()
local Version = 1.5
-- CC Data --
local ccTable = { 'Stun', 'Silence', 'Taunt', 'Slow', 'Root', 'Fear', 'Charm', 'Blind', 'Flee', 'Debuff', 'Supress' }
local ccP = {4, 3, 5, 1, 2, 4, 5, 1, 2, 3, 5}  
-- Skills --
local ccRA = { Gangplank = { Name = '��Ѫ���Ʒ�(W)', Skill = _W }, Alistar = { Name = '�ᶨ��־(R)', Skill =  _R }, Olaf = { Name = '����ƻ�(R)', Skill = _R } }
-- Items --
local MC = { ID = 3222, Range = 750, Slot = nil }
local QSS = { [1] = 3140, [2] = 3139, Slot = nil }
-- Heros --
local heroTB = {}
local enemyTB = {}
-- Vars --
local CLEANSESlot = ((myHero:GetSpellData(SUMMONER_1).name:find("SummonerCleanse") and SUMMONER_1) or (myHero:GetSpellData(SUMMONER_2).name:find("SummonerCleanse") and SUMMONER_2) or nil)

function OnLoad()
	-- Bind CB to ccHandler
	for i,v in ipairs(ccTable) do
		AdvancedCallback:bind('On'..v, function(unit, buff, gain) if unit and unit.valid and unit.team == myHero.team and gain == 1 --[[OnGain[CC]()]] then ccHandler(unit, buff, i) return end end)
	end
	-- Build hero and enemy tables
	if heroManager.iCount > 1 then 
		for i=1, heroManager.iCount do
			local HERO = heroManager:GetHero(i)
			if HERO.team == myHero.team and not HERO.isMe then table.insert(heroTB, HERO.charName)
			elseif HERO.team ~= myHero.team then table.insert(enemyTB, HERO.charName)
			end
		end
	end
	Menu()
	PrintChat( string.format(" >> New AutoCC Remover Loaded! v%.2f by Trees", Version) )
	loaded = true
end

function OnTick()
	if not loaded then return end
	
	if GetInventoryHaveItem(MC.ID) and MCLOADED == nil then 
		menuAddMC() 
		MCLOADED = true
	end
	
	if GetInventoryHaveItem(QSS[1]) and QSSLOADED == nil then 
		menuAddQSS() 
		QSS.Slot = QSS[1] 
		QSSLOADED = true
	end
	
	if	GetInventoryHaveItem(QSS[2]) then 
		QSS.Slot = QSS[2] 
		if QSSLOADED == nil then 
			menuAddQSS()
			QSSLOADED = true
		end
	end
end

function menuAddSkills()
	Config:addSubMenu(myHero.charName..':'..ccRA[myHero.charName].Name, 'SkillConfig')
	Config.SkillConfig:addParam('useSkill','ʹ�� '..ccRA[myHero.charName].Name, SCRIPT_PARAM_ONOFF, true)
	-- Add CC to List
	for i,v in ipairs(ccTable) do
		if i < (#ccTable-1) then 
			if ccP[i] > 2 then Config.SkillConfig:addParam('ccType'..i, '���: '..v, SCRIPT_PARAM_ONOFF, true)
			else  Config.SkillConfig:addParam('ccType'..i, '���: '..v, SCRIPT_PARAM_ONOFF, false) 
			end
		end
	end
end

function menuAddMC()
	-- Mikael's Crucible
	Config:addSubMenu("�׿���������(MC)", "MC")
	Config.MC:addSubMenu('���Ƽ���ѡ��', 'CCS')
	Config.MC:addParam('useMC','ʹ��MC', SCRIPT_PARAM_ONOFF, true)
	-- Add Team to List
	for i,v in ipairs(heroTB) do Config.MC:addParam('tmMC'..i, 'ʹ��: '..v, true) end
	-- Add CC to List
	for i,v in ipairs(ccTable) do
		if i < (#ccTable-1) then 
			if ccP[i] > 2 then Config.MC.CCS:addParam('ccType'..i, '���: '..v, SCRIPT_PARAM_ONOFF, true)
			else  Config.MC.CCS:addParam('ccType'..i, '���: '..v, SCRIPT_PARAM_ONOFF, false) 
			end
		end
	end
end

function menuAddQSS()
	Config:addSubMenu("ˮ��ϵ��(QSS)", "QSS")
	Config.QSS:addParam('useQSS', 'ʹ�� QSS', SCRIPT_PARAM_ONOFF, true)
	Config.QSS:addParam('ignite', '���: ��ȼ', SCRIPT_PARAM_ONOFF, true)
	Config.QSS:addParam('exhaust', '���: ����', SCRIPT_PARAM_ONOFF, true)
	for i,v in pairs(ccTable) do
		if i == #ccTable-1 then return end
		if ccP[i] > 2 then Config.QSS:addParam('ccType'..i, '���: '..v, SCRIPT_PARAM_ONOFF, true)
		else Config.QSS:addParam('ccType'..i, '���: '..v, SCRIPT_PARAM_ONOFF, false) end
	end
end

function menuAddCleanse()
	Config:addSubMenu("����", "CL")
	Config.CL:addParam('useCleanse', 'ʹ�� ����', SCRIPT_PARAM_ONOFF, true)
	Config.CL:addParam('ignite', '���: ��ȼ', SCRIPT_PARAM_ONOFF, true)
	Config.CL:addParam('exhaust', '���: ����', SCRIPT_PARAM_ONOFF, true)
	-- Add CC to List
	for i,v in ipairs(ccTable) do
		if i == #ccTable then break end
		if i == #ccTable-1 then return end
		if ccP[i] > 2 then Config.CL:addParam('ccType'..i, '���: '..v, SCRIPT_PARAM_ONOFF, true)
		else  Config.CL:addParam('ccType'..i, '���: '..v, SCRIPT_PARAM_ONOFF, false) 
		end
	end
end

function menuAddEnemies()
	Config:addSubMenu("�з�", "Enemies")
	for i,v in ipairs(enemyTB) do
		PrintChat(tostring(v))
		Config.Enemies:addParam('disable'..v, 'Disable On: '..v, SCRIPT_PARAM_ONOFF, false)
	end
end

function Menu()
	Config = scriptConfig("AutoRemoveCC v"..Version..' by Trees', "AutoRemoveCC.cfg")
	--menuAddEnemies()
	-- Cleanse
	if CLEANSESlot ~= nil then menuAddCleanse() end
	-- Skills
	if ccRA[myHero.charName] ~= nil then menuAddSkills() end
end

function ccHandler(unit, buff, ccN)
	if myHero.dead then return end	
	-- if ccN == #ccTable then PrintChat(string.format('%s is being affected by disarm: %s', unit.charName, buff.name)) PrintChat('Please post this!') end
	-- Disable Based on Enemies
	--PrintChat(buff.source.charName)
	--if buff.source ~= nil and buff.source.team ~= nil and buff.source.team ~= myHero.team and buff.source.charName ~= nil and Config.Enemies ~= nil and Config.Enemies.disable[buff.source.charName]then return end
	
	if unit.isMe then
		-- Skills
		if Config.SkillConfig ~= nil and Config.SkillConfig.useSkill and myHero:CanUseSpell(ccRA[myHero.charName].Skill) == READY then CastSpell(ccRA[myHero.charName].Skill) return end
		
		-- Cleanse
		if Config.CL ~= nil and Config.CL.useCleanse and myHero:CanUseSpell(CLEANSESlot) == READY then
			if ccN == #ccTable then return -- doesn't support supress
			elseif ccN == #ccTable-1 then
				--ignite/exhaust
				if buff.name == 'SummonerDot' and Config.CL.ignite then CastSpell(CLEANSESlot) return end
				if buff.name == 'SummonerExhaust' and Config.CL.exhaust then CastSpell(CLEANSESlot) return end
			elseif Config.CL.ccType[ccN] then
				CastSpell(CLEANSESlot) 
				return 
			end
		end
		
		-- QSS/MS
		if Config.QSS ~= nil and Config.QSS.useQSS and CastQSS(false) then
			if ccN == #ccTable-1 then
				if buff.name == 'SummonerDot' and Config.QSS.ignite then CastQSS(true) return end
				if buff.name == 'SummonerExhaust' and Config.QSS.exhaust then CastQSS(true) return end
			else
				if Config.QSS.ccType[ccN] then CastQSS(true) return end
			end
		end	
		
	else
		-- MC
		if Config.MC ~= nil and Config.MC.useMC and GetInventoryItemIsCastable(MC.ID) and Config.MC.tmMC[unit.charName] and ccN < (#ccTable-1) and Config.MC.CCS.ccType[ccN] then Cast(unit) return end
	end
end

function CastQSS(cond)
	if cond and QSS.Slot ~= nil and myHero:CanUseSpell(QSS.Slot) then CastItem(QSS.Slot) return end
	if GetInventoryItemIsCastable(QSS[1]) then QSS.Slot = QSS[1] return true
	elseif GetInventoryItemIsCastable(QSS[2]) then QSS.Slot = QSS[2] return true end
	return false
end

function Cast(mate)
	if IsValid(mate, MC.Range) then CastItem(MC.ID, mate) end
end

	
function IsValid(target, dist)
	if target ~= nil and target.valid and not target.dead and target.bTargetable and ValidTarget(target, dist) then return true end
	return false
end
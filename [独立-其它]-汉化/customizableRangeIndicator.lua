--[[Customizable Range Indicator v1.0 by ViceVersa]]


local shiftPressed = false


function GetRndMinMaxDistance(pos1, pos2, min, max) --returns distance beetween pos1 and pos2, rounded and limited to int beetween min and max
    local distance = math.floor(GetDistance(pos1, pos2))
    if distance < min then return min
    elseif distance > max then return max
    else return distance end
end
up
function GetPosInDirection(pos1, pos2, distance) --returns the position after a distance from pos1 in direction of pos2
    local vPos1 = Vector(pos1.x, pos1.y, pos1.z)
    local vPos2 = Vector(pos2.x, pos2.y, pos2.z)
    return vPos1 - (vPos1 - vPos2):normalized() * distance
end



function OnLoad()	
    CRIConfig = scriptConfig("可定制范围指示器","customizableRangeIndicator")
    
    CRIConfig:addParam("lock",            "锁定 设定",  SCRIPT_PARAM_ONOFF, false)
    
    CRIConfig:addParam("info1",           " ",              SCRIPT_PARAM_INFO,      " ")
    CRIConfig:addParam("indicator1Key",   "------- 指示器 1 -------",  SCRIPT_PARAM_ONKEYDOWN, false, string.byte("Y"))
    CRIConfig:addParam("indicator1",      "启用",         SCRIPT_PARAM_ONOFF,     false)
    CRIConfig:addParam("indicator1Line",  "线条",           SCRIPT_PARAM_ONOFF,     false)
    CRIConfig:addParam("indicator1Color", "颜色",          SCRIPT_PARAM_COLOR,     {255,255,155,55})
    CRIConfig:addParam("indicator1Range", "范围",          SCRIPT_PARAM_SLICE,     600, 400, 1500, 0)
    CRIConfig:addParam("indicator1Spell", "绑定技能",  SCRIPT_PARAM_SLICE,     0, 0, 4, 0)
    
    CRIConfig:addParam("info2",           " ",              SCRIPT_PARAM_INFO,      " ")
    CRIConfig:addParam("indicator2Key",   "------- 指示器 2 -------",  SCRIPT_PARAM_ONKEYDOWN, false, string.byte("X"))
    CRIConfig:addParam("indicator2",      "启用",         SCRIPT_PARAM_ONOFF,     false)
    CRIConfig:addParam("indicator2Line",  "线条",           SCRIPT_PARAM_ONOFF,     false)
    CRIConfig:addParam("indicator2Color", "颜色",          SCRIPT_PARAM_COLOR,     {255,255,155,55})
    CRIConfig:addParam("indicator2Range", "范围",          SCRIPT_PARAM_SLICE,     700, 400, 1500, 0)
    CRIConfig:addParam("indicator2Spell", "绑定技能",  SCRIPT_PARAM_SLICE,     0, 0, 4, 0)
    
    CRIConfig:addParam("info3",           " ",              SCRIPT_PARAM_INFO,      " ")
    CRIConfig:addParam("indicator3Key",   "------- 指示器 3 -------",  SCRIPT_PARAM_ONKEYDOWN, false, string.byte("V"))
    CRIConfig:addParam("indicator3",      "启用",         SCRIPT_PARAM_ONOFF,     false)
    CRIConfig:addParam("indicator3Line",  "线条",           SCRIPT_PARAM_ONOFF,     false)
    CRIConfig:addParam("indicator3Color", "颜色",          SCRIPT_PARAM_COLOR,     {255,255,155,55})
    CRIConfig:addParam("indicator3Range", "范围",          SCRIPT_PARAM_SLICE,     800, 400, 1500, 0)
    CRIConfig:addParam("indicator3Spell", "绑定技能",  SCRIPT_PARAM_SLICE,     0, 0, 4, 0)
    
    CRIConfig:addParam("info4",           " ",              SCRIPT_PARAM_INFO,      " ")
    CRIConfig:addParam("indicator4Key",   "------- 指示器 4 -------",  SCRIPT_PARAM_ONKEYDOWN, false, string.byte("C"))
    CRIConfig:addParam("indicator4",      "启用",         SCRIPT_PARAM_ONOFF,     false)
    CRIConfig:addParam("indicator4Line",  "线条",           SCRIPT_PARAM_ONOFF,     false)
    CRIConfig:addParam("indicator4Color", "颜色",          SCRIPT_PARAM_COLOR,     {255,255,155,55})
    CRIConfig:addParam("indicator4Range", "范围",          SCRIPT_PARAM_SLICE,     3000, 400, 4000, 0)
    CRIConfig:addParam("indicator4Spell", "绑定技能",  SCRIPT_PARAM_SLICE,     0, 0, 4, 0)
    
    print('<font color="#A0FF00">鍙畾鍒惰寖鍥存寚绀哄櫒 >> v1.0 杞藉叆!</font>')
    
    if GetSave("customizableRangeIndicator")[myHero.charName] then
        CRIConfig.lock = true
        
        CRIConfig.indicator1 = GetSave("customizableRangeIndicator")[myHero.charName].indicator1
        CRIConfig.indicator1Line = GetSave("customizableRangeIndicator")[myHero.charName].indicator1Line
        CRIConfig.indicator1Color = GetSave("customizableRangeIndicator")[myHero.charName].indicator1Color
        CRIConfig.indicator1Range = GetSave("customizableRangeIndicator")[myHero.charName].indicator1Range
        CRIConfig.indicator1Spell = GetSave("customizableRangeIndicator")[myHero.charName].indicator1Spell
        
        CRIConfig.indicator2 = GetSave("customizableRangeIndicator")[myHero.charName].indicator2
        CRIConfig.indicator2Line = GetSave("customizableRangeIndicator")[myHero.charName].indicator2Line
        CRIConfig.indicator2Color = GetSave("customizableRangeIndicator")[myHero.charName].indicator2Color
        CRIConfig.indicator2Range = GetSave("customizableRangeIndicator")[myHero.charName].indicator2Range
        CRIConfig.indicator2Spell = GetSave("customizableRangeIndicator")[myHero.charName].indicator2Spell
        
        CRIConfig.indicator3 = GetSave("customizableRangeIndicator")[myHero.charName].indicator3
        CRIConfig.indicator3Line = GetSave("customizableRangeIndicator")[myHero.charName].indicator3Line
        CRIConfig.indicator3Color = GetSave("customizableRangeIndicator")[myHero.charName].indicator3Color
        CRIConfig.indicator3Range = GetSave("customizableRangeIndicator")[myHero.charName].indicator3Range
        CRIConfig.indicator3Spell = GetSave("customizableRangeIndicator")[myHero.charName].indicator3Spell
        
        CRIConfig.indicator4 = GetSave("customizableRangeIndicator")[myHero.charName].indicator4
        CRIConfig.indicator4Line = GetSave("customizableRangeIndicator")[myHero.charName].indicator4Line
        CRIConfig.indicator4Color = GetSave("customizableRangeIndicator")[myHero.charName].indicator4Color
        CRIConfig.indicator4Range = GetSave("customizableRangeIndicator")[myHero.charName].indicator4Range
        CRIConfig.indicator4Spell = GetSave("customizableRangeIndicator")[myHero.charName].indicator4Spell
        
        print('<font color="#A0FF00">鍙畾鍒惰寖鍥存寚绀哄櫒 >> 璁惧畾杞藉叆-- </font><font color="#FF0000">' .. myHero.charName .. '</font>')
    else
        CRIConfig.lock = false
        
        CRIConfig.indicator1 = false
        CRIConfig.indicator1Line = false
        CRIConfig.indicator1Color = {255,255,155,55}
        CRIConfig.indicator1Range = 600
        CRIConfig.indicator1Spell = 0
        
        CRIConfig.indicator2 = false
        CRIConfig.indicator2Line = false
        CRIConfig.indicator2Color = {255,255,155,55}
        CRIConfig.indicator2Range = 700
        CRIConfig.indicator2Spell = 0
        
        CRIConfig.indicator3 = false
        CRIConfig.indicator3Line = false
        CRIConfig.indicator3Color = {255,255,155,55}
        CRIConfig.indicator3Range = 800
        CRIConfig.indicator3Spell = 0
        
        CRIConfig.indicator4 = false
        CRIConfig.indicator4Line = false
        CRIConfig.indicator4Color = {255,255,155,55}
        CRIConfig.indicator4Range = 900
        CRIConfig.indicator4Spell = 0
        
        print('<font color="#FFFF25">鍙畾鍒惰寖鍥存寚绀哄櫒 >> 娌℃湁璁惧畾- </font><font color="#FF0000">' .. myHero.charName .. '</font>')
    end
end

function OnWndMsg(msg,wParam)
    if CRIConfig.lock then return end
    
    if msg == KEY_DOWN then
        if wParam == 16 then
            shiftPressed = true
        end
    elseif msg == KEY_UP then
        if wParam == 16 then
            shiftPressed = false
        end
    end
end

function OnTick()
    if CRIConfig.lock then return end
    
    if shiftPressed then
        if CRIConfig.indicator1Key then
            CRIConfig.indicator1Range = GetRndMinMaxDistance(myHero, mousePos, 400, 1500)
        elseif CRIConfig.indicator2Key then
            CRIConfig.indicator2Range = GetRndMinMaxDistance(myHero, mousePos, 400, 1500)
        elseif CRIConfig.indicator3Key then
            CRIConfig.indicator3Range = GetRndMinMaxDistance(myHero, mousePos, 400, 1500)
        elseif CRIConfig.indicator4Key then
            CRIConfig.indicator4Range = GetRndMinMaxDistance(myHero, mousePos, 400, 4000)
        end
    end
end

function OnDraw()
    if CRIConfig.indicator1 and (CRIConfig.indicator1Spell == 0 or myHero:CanUseSpell(CRIConfig.indicator1Spell - 1) == READY) then
        if CRIConfig.indicator1Line then
            local tPos = GetPosInDirection(myHero, mousePos, CRIConfig.indicator1Range)
            DrawLine3D(myHero.x, myHero.y, myHero.z, tPos.x, tPos.y, tPos.z, 3, ARGB(CRIConfig.indicator1Color[1], CRIConfig.indicator1Color[2], CRIConfig.indicator1Color[3], CRIConfig.indicator1Color[4]))
        else
            DrawCircle(myHero.x, myHero.y, myHero.z, CRIConfig.indicator1Range, ARGB(CRIConfig.indicator1Color[1], CRIConfig.indicator1Color[2], CRIConfig.indicator1Color[3], CRIConfig.indicator1Color[4]))
        end
    end
    if CRIConfig.indicator2 and (CRIConfig.indicator2Spell == 0 or myHero:CanUseSpell(CRIConfig.indicator2Spell - 1) == READY) then
        if CRIConfig.indicator2Line then
            local tPos = GetPosInDirection(myHero, mousePos, CRIConfig.indicator2Range)
            DrawLine3D(myHero.x, myHero.y, myHero.z, tPos.x, tPos.y, tPos.z, 3, ARGB(CRIConfig.indicator2Color[1], CRIConfig.indicator2Color[2], CRIConfig.indicator2Color[3], CRIConfig.indicator2Color[4]))
        else
            DrawCircle(myHero.x, myHero.y, myHero.z, CRIConfig.indicator2Range, ARGB(CRIConfig.indicator2Color[1], CRIConfig.indicator2Color[2], CRIConfig.indicator2Color[3], CRIConfig.indicator2Color[4]))
        end
    end
    if CRIConfig.indicator3 and (CRIConfig.indicator3Spell == 0 or myHero:CanUseSpell(CRIConfig.indicator3Spell - 1) == READY) then
        if CRIConfig.indicator3Line then
            local tPos = GetPosInDirection(myHero, mousePos, CRIConfig.indicator3Range)
            DrawLine3D(myHero.x, myHero.y, myHero.z, tPos.x, tPos.y, tPos.z, 3, ARGB(CRIConfig.indicator3Color[1], CRIConfig.indicator3Color[2], CRIConfig.indicator3Color[3], CRIConfig.indicator3Color[4]))
        else
            DrawCircle(myHero.x, myHero.y, myHero.z, CRIConfig.indicator3Range, ARGB(CRIConfig.indicator3Color[1], CRIConfig.indicator3Color[2], CRIConfig.indicator3Color[3], CRIConfig.indicator3Color[4]))
        end
    end
    if CRIConfig.indicator4 and (CRIConfig.indicator4Spell == 0 or myHero:CanUseSpell(CRIConfig.indicator4Spell - 1) == READY) then
        if CRIConfig.indicator4Line then
            local tPos = GetPosInDirection(myHero, mousePos, CRIConfig.indicator4Range)
            DrawLine3D(myHero.x, myHero.y, myHero.z, tPos.x, tPos.y, tPos.z, 3, ARGB(CRIConfig.indicator4Color[1], CRIConfig.indicator4Color[2], CRIConfig.indicator4Color[3], CRIConfig.indicator4Color[4]))
        else
            DrawCircle(myHero.x, myHero.y, myHero.z, CRIConfig.indicator4Range, ARGB(CRIConfig.indicator4Color[1], CRIConfig.indicator4Color[2], CRIConfig.indicator4Color[3], CRIConfig.indicator4Color[4]))
        end
    end
end

function OnUnload()
    GetSave("customizableRangeIndicator")[myHero.charName] = {
        indicator1 = CRIConfig.indicator1,
        indicator1Line = CRIConfig.indicator1Line,
        indicator1Color = CRIConfig.indicator1Color,
        indicator1Range = CRIConfig.indicator1Range,
        indicator1Spell = CRIConfig.indicator1Spell,

        indicator2 = CRIConfig.indicator2,
        indicator2Line = CRIConfig.indicator2Line,
        indicator2Color = CRIConfig.indicator2Color,
        indicator2Range = CRIConfig.indicator2Range,
        indicator2Spell = CRIConfig.indicator2Spell,

        indicator3 = CRIConfig.indicator3,
        indicator3Line = CRIConfig.indicator3Line,
        indicator3Color = CRIConfig.indicator3Color,
        indicator3Range = CRIConfig.indicator3Range,
        indicator3Spell = CRIConfig.indicator3Spell,

        indicator4 = CRIConfig.indicator4,
        indicator4Line = CRIConfig.indicator4Line,
        indicator4Color = CRIConfig.indicator4Color,
        indicator4Range = CRIConfig.indicator4Range,
        indicator4Spell = CRIConfig.indicator4Spell
    }
end
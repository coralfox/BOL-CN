		

    -- ###################################################################################################### --
    -- #                                                                                                    # --
    -- #                                               XT003-HUD                                            # --
    -- #                                         by Punjabi 84 (original by Sida)                           # --
    -- #                         Credit for original scripts 100% to the original authors!!                 # --
    -- #                                修改：Coralfox   版本：1.71A                                         # --
	-- #                                                                                                    # --
    -- ###################################################################################################### --
     
    --[--------- Contains ---------]
     
    -- (01)(敌方预警)Low Awareness : Created by Ryan, Ported by Manciuszz
    -- 废弃(02)(补刀标记Simple Minion Marker : Created by Kilua
    -- (03) 塔范围显示)Enemy Tower Range : Created by SurfaceS
    -- (04)(角色攻击范围)Champion Ranges : Created by heist, ported by Mistal 
    -- (05)(FPS修正)FPS Drop Fix : Created by 3rasus
    -- (06)(完美插眼)Perfect Ward 1.7.2 : Created by Husky  
	-- (07)(小地图计时)Minimap Timers v0.2c(),	
	-- (08)(他在哪里?)Where Is He??? v1.2, 
	-- (09)(敌人逃跑预测)Where did he go? v0.4,
	-- (10)(本尊预测)Clone Revealer v1.1, 
	-- 废弃(11)(自动点燃)Auto Ignite
	-- 废弃(12 (自动虚弱/净化)Auto Exhuast/Cleanse, 
	-- (13)(自动升级加点)AutoLevel, 
	-- (14)(右键平滑移动)Right Click Smooth Move,
	-- (15)(自动惩戒)AutoSmite v2.9
	-- 废弃(16)(自动惩戒:扭曲丛林)Auto Smite Twisted Treeline, 
	-- (17)(无敌计时)Waiteee v0.2, 	
	-- (18)(自动护盾)AutoShield, 
	-- (19)(内置控制台)(Console v1.0, 
	-- 废弃(20)(隐藏物件)Hidden Objects 0.3: Created by SurfaceS
	-- 废弃(21)(标记视野)Predators Vision
	-- 废弃(22)(自动吃药)Auto Potion 0.5
	-- (23)(绘制圆圈优化)Simple Draw
	-- (24)(自动 吃药/中亚/巫师帽)Auto Potion/Zhonyas/Wooglets
	-- (25) (平A击杀计算器)aatokill
	
    --[----------- Log ------------]

	--	V1.0 仅添加1-10号脚本
	--  V1.1 再次添加11-18号脚本
	--  V1.2 再次修改一些描述
	--  V1.3 
	--       a.废弃自动点燃，自动虚弱；AutoCarry有更好的处理版本
	--       b.废弃塔范围显示;BOL自带更好的处理方式
	--		 c.废弃自动惩戒：扭曲丛林；更新自动惩戒
	--  V1.4	
	--       a.添加隐藏物体，标记视野
	--       b.标记视野除了可以标记野怪，英雄视野，配合  隐藏物件，可以发现隐藏眼，蘑菇等
	--		 c.标记视野默认为J键
	--  V1.5	
	--       a.去掉隐藏物体，标记视野
	--       b.恢复显示塔视野，增加菜单
	--		 c.增加自动喝药
	--  V1.6
	--       a.恢复隐藏物体并优化
	--       b.增加绘制圆圈优化，降低延迟
	--  V1.65	
	--       a.废除极为不稳定的隐藏物件
	--       b.隐藏物件功能加入多合一以后，会有较大几率造成报错，乃至崩溃
	--       c.推荐使用MOD的方式加载	
	--  V1.70A	
	--       a.废除不是很友好的自动吃药
	--       b.将 自动中亚 和 自动吃药 集成	
	--       c.加入平A击杀计算器
	--  V1.71A	
	--       a.废除导致报错的补兵标记
    -- ##############################################  Features  ##################################################### --
	-- (01)(敌方预警)Low Awareness
	--   1.敌人突然出现在视野中时发出警告(V键警告)
	--   2.有敌人靠近自身时用红色标记
	
    -- (02)废弃(补刀标记Simple Minion Marker
	--   1.小兵可以补刀杀死时，会出现亮白色小光圈
	
    -- (03)(塔范围显示)Enemy Tower Range
	--   1.可以显示塔的攻击范围
	--   2.根据状态显示不同颜色，绿色-安全，黄色-安全警告，红色-塔攻击你
	--   3.菜单已添加完毕

	-- (04)(角色攻击范围)Champion Ranges
	--   1. 显示角色的普通攻击范围以及大招范围
	--   2. 我方，敌方视野可以分别开启/关闭
	--   3.	功能比较简单，若有更好版本，考虑后续修改；或者在游戏中关闭，启用其他插件显示功能
	
    -- (06)(完美插眼)Perfect Ward 1.7.2 	
	--   1.提示插眼的位置，且是最佳的位置
	--   2.提示如果最安全的插眼，隔墙，隔障碍
	--   3.眼的范围显示

	-- (07)(小地图计时)Minimap Timers	
	--   1.小地图，大地图上显示野怪的刷新时间
	--   2.可以设置预报野怪的刷新时间，通知方式

	-- (08)(他在哪里?)Where Is He???
	--   1.敌人消失时，会根据其移动速度估算其位置范围(一个红色光圈）
	--   2.光圈越靠近自身，线条会越来越粗
	--   3.有时会显得过于杂乱，需要后续修改
	
	-- (09)(敌人逃跑预测)Where did he go?	
	--   1.	使用闪现的敌人预计位置
	--   2.	一些自带类似闪现技能的逃跑方向，比如小丑，EZ……
	--   3.	预测嘛，不要迷信，只是估算角色距离，朝向做的预测

	-- (10)(本尊预测)Clone Revealer
	--   1.通过行为等判断哪个是分身；例如猴子，妖姬	
	--   2.真身会标记（另PS：使用Trackeee的，也可以通过UI来简单判定）
	
	-- (11)(自动点燃)--废弃
	--   1.自动点燃必定可杀死目标	
	
	-- (12 (自动虚弱/净化)--废弃
	--   1.自动虚弱逃跑目标
	--   2.自动净化自己
	
	-- (13)(自动升级加点)	
	--   1.	自动升级以后加点
	--   2.	加点设置按照主流加点方法，如果有不同想法，请在此文件内自行修改
	
	-- (14)(右键平滑移动)Right Click Smooth Move,
	--   1.	右键移动会平滑起来	
	--   2. 克制某些通过朝向判定东西的脚本
	
	-- (15)(自动惩戒)AutoSmite v2.9
	--   1.	预测野怪多少血量时最好惩戒
	--   2.	可自动帮你惩戒
	
	-- (16)(自动惩戒:扭曲丛林)Auto Smite Twisted Treeline, --废弃
	--   1.	自动惩戒  3V3扭曲丛林版本	
	
    -- (17)(无敌计时)Waiteee v0.2,	
	--   1.	一些无敌的计时，例如天使的无敌，钢铁大师的无敌……

	-- (18)(自动护盾)AutoShield, 
	--   1.自动给指定目标加护盾，治疗，加速等	
	--   2.英雄辅助技能，召唤师技能，道具都在此列

	-- (18)(自动护盾)AutoShield, 
	--   1.自动给指定目标加护盾，治疗，加速等	
	--   2.英雄辅助技能，召唤师技能，道具都在此列

	-- (19)(内置控制台)Console v1.0, 
	--   1.将绿字信息移动到此处，并增加一些调试脚本功能	
	--   2.	对中文ANSI码支持不是很好
	
	-- (20)(隐藏物件)Hidden Objects, 
	--   1.显示敌方插眼，蘑菇，陷阱
	--   2.需要资源文件支持

	-- (22)(自动吃药)Auto Potion 0.5	
	--   1.自动使用水晶瓶，饼干，红药，蓝药
	--   2.增加了吃药菜单
	
	-- (23)(绘制圆圈优化)Simple Draw	
	--   1.优化了绘制圆圈的函数，降低延迟
	--   2.适用于技能，眼等圆圈
	
	-- (24)(自动 吃药/中亚/巫师帽)Auto Potion/Zhonyas/Wooglets	
	--   1.自动使用水晶瓶，红药，蓝药
	--   2.自动使用中亚,巫师帽
	
	-- (25) (平A击杀计算器)aatokill
	--    1.计算出A多少下能够击杀
	--    2.包含暴击计算	
	
 	-- ##############################################  Features  ##################################################### --		
	
    -- ############################################# LOW AWARENESS ##############################################
     
    local alertActive = true
    local championTable = {}
    local playerTimer = {}
    local playerDrawer = {}
    local player = GetMyHero()
    --showErrorsInChat = false
    --showErrorTraceInChat = false
     
    nextTick = 0 function LowAwarenessOnTick()   if nextTick > GetTickCount() then return end   nextTick = GetTickCount() + 250 --(100 is the delay)        
        local tick = GetTickCount()
        if alertActive == true then
            for i = 1, heroManager.iCount, 1 do
            local object = heroManager:getHero(i)
                if object.team ~= player.team and object.dead == false then
                    if object.visible == true and player:GetDistance(object) < 2500 then
                        if playerTimer[i] == nil then
                            PrintChat(string.format("<font color='#FF0000'> >> !!!璀﹀憡: %s</font>", object.charName))
                            PingSignal(PING_FALLBACK,object.x,object.y,object.z,2)
                            PingSignal(PING_FALLBACK,object.x,object.y,object.z,2)
                            PingSignal(PING_FALLBACK,object.x,object.y,object.z,2)
                            table.insert(championTable, object )
                            playerDrawer[i] = tick
                        end
                        playerTimer[ i ] = tick
                        if (tick - playerDrawer[i]) > 5000 then
                            for ii, tableObject in ipairs(championTable) do
                                if tableObject.charName == object.charName then
                                    table.remove(championTable, ii)
                                end
                            end
                        end
                    else
                        if playerTimer[i] ~= nil and (tick - playerTimer[i]) > 10000 then
                            playerTimer[i] = nil
                            for ii, tableObject in ipairs(championTable) do
                                if tableObject.charName == object.charName then
                                    table.remove(championTable, ii)
                                end
                            end
                        end
                    end
                end
            end
        end
    end
     
     
     
    function LowAwarenessOnDraw()
        for i,tableObject in ipairs(championTable) do
           if tableObject.visible and tableObject.dead == false and tableObject.team ~= player.team then
                            for t = 0, 1 do
                                    DrawCircle(tableObject.x, tableObject.y, tableObject.z, 1250 + t*0.25, 0xFFFF0000)
                            end
           end
        end
    end
     
 -- ############################################# LOW AWARENESS ##############################################
 -- ############################################ CloneRevealer ############################################
--[[
    Displays the original enemy with a circle for enemies who can clone
    v1.1 BoL version
    Modified by Mistal, based on Zynox's AttackRange
]]

function CloneRevOnLoad()
--[[        Globals     ]]
    --if player == nil then player = GetMyHero() end

    --[[             Code            ]]
    do
        local cloneRevealer = {
            heros = {},
            charNameToShow = {"Shaco", "LeBlanc", "MonkeyKing", "Yorick"}
        }
        
        for index,charName in pairs(cloneRevealer.charNameToShow) do
            for i = 1, heroManager.iCount do
                local hero = heroManager:getHero(i)
                if hero ~= nil and hero.team ~= myHero.team and hero.charName == charName then
                    table.insert(cloneRevealer.heros, hero)
                end
            end
        end
        
        if #cloneRevealer.heros > 0 then
            --PrintChat(" >> Clones Revealer by Mistal Loaded!")
            function OnDraw()
                for index,hero in pairs(cloneRevealer.heros) do
                    if hero.dead == false and hero.visible then 
                        DrawCircle(hero.x, hero.y, hero.z, 100, 0x3232FF00) 
                    end
                end
            end
        else
            cloneRevealer = nil
        end
    end
end  
    -- ############################################# MINION MARKER ################################################
     
    function MinionMarkerOnLoad()
            minionTable = {}
            for i = 0, objManager.maxObjects do
                    local obj = objManager:GetObject(i)
                    if obj ~= nil and obj.type ~= nil and obj.type == "obj_AI_Minion" then
                            table.insert(minionTable, obj)
                    end
            end
    end
     
    function MinionMarkerOnDraw()
            for i,minionObject in ipairs(minionTable) do
                    if not ValidTarget(minionObject) then
                            table.remove(minionTable, i)
                            i = i - 1
                    elseif minionObject ~= nil and myHero:GetDistance(minionObject) ~= nil and myHero:GetDistance(minionObject) < 1500 and minionObject.health ~= nil and minionObject.health <= myHero:CalcDamage(minionObject, myHero.addDamage+myHero.damage) and minionObject.visible ~= nil and minionObject.visible == true then
                            for g = 0, 6 do
                                    DrawCircle(minionObject.x, minionObject.y, minionObject.z,80 + g,255255255)
                            end
            end
        end
    end
     
     
    function MinionMarkerOnCreateObj(object)
            if object ~= nil and object.type ~= nil and object.type == "obj_AI_Minion" then table.insert(minionTable, object) end
    end
     
    -- ############################################# MINION MARKER ################################################  
-- ############################################# ENEMY RANGE ################################################

-- Simple Player and Enemy Range Circles
-- by heist
-- v1.0
-- Initial release
-- v1.0.1
-- Adjusted AA range to be more accurate
-- Adopted to studio by Mistal

champAux = {}
champ = {
	Ahri = { 880, 975 },
	Akali = { 800, 0 },
	Alistar = { 650, 0 },
	Amumu = { 0, 600 },
	Anivia = { 650, 1100 },
	Annie = { 625, 0 },
	Ashe = { 1200, 0 },
	Blitzcrank = { 1000, 0 },
	Brand = { 900, 0 },
	Caitlyn = { 1300, 0 },
	Cassiopeia = { 700, 850 },
	Chogath = { 950, 700 },
	Corki = { 1200, 0 },
	Darius = {550, 475}, -- his E and R
	Diana = {830, 900}, -- Q and R
	Draven = {550, 0 }, -- his Ulti is global, his normal range is 550
	DrMundo = { 1000, 0 },
	Evelynn = { 325, 0 },
	Ezreal = { 1000, 0 },
	Fiddlesticks = { 475, 575 },
	Fiora = { 600, 400 },
	Fizz = { 550, 1275 },
	Galio = { 1000, 550 },
	Gangplank = { 625, 0 },
	Garen = { 400, 0 },
	Gragas = { 1150, 1050 },
	Graves = { 750, 0 },
	Hecarim = { 325, 0 }, -- Placeholder
	Heimerdinger = { 1000, 0 },
	Irelia = { 650, 425 },
	Janna = { 800, 1700 },
	JarvanIV = { 770, 650 },
	Jax = { 700, 0 },
	Jayce = { 500, 1050 }, -- normal range attack and ulti
	Karma = { 800, 0 },
	Karthus = { 850, 1000 },
	Kassadin = { 700, 650 },
	Katarina = { 700, 0 },
	Kayle = { 650, 0 },
	Kennen = { 1050, 0 },
	KogMaw = { 1000, 0 },
	Leblanc = { 1300, 600 },
	LeeSin = { 1100, 0 },
	Leona = { 1200, 0 },
	Lulu = { 925, 650 },
	Lux = { 1000, 0 },
	Malphite = { 1000, 0 },
	Malzahar = { 700, 0 },
	Maokai = { 650, 0 },
	MasterYi = { 600, 0 },
	MissFortune = { 625, 1400 },
	MonkeyKing = { 625, 325 },
	Mordekaiser = { 700, 1125 },
	Morgana = { 1300, 600 },
	Nasus = { 700, 0 },
	Nautilus = { 950, 850 },
	Nidalee = { 1500, 0 },
	Nocturne = { 1200, 465 },
	Nunu = { 550, 0 },
	Olaf = { 1000, 0 },
	Orianna = { 825, 0 },
	Pantheon = { 0, 600 },
	Poppy = { 525, 0 },
	Rammus = { 0, 325 },
	Renekton = { 550, 0 },
	Riven = { 0, 250 },
	Rumble = { 600, 1000 },
	Ryze = { 675, 625 },
	Sejuani = { 700, 1150 },
	Shaco = { 625, 0 },
	Shen = { 0, 600 },
	Shyvana = { 1000, 0 },
	Singed = { 0, 125 },
	Sion = { 550, 0 },
	Sivir = { 1100, 0 },
	Skarner = { 600, 350 },
	Sona = { 0, 1000 },
	Soraka = { 0, 725 },
	Swain = { 900, 0 },
	Syndra = { 550, 0 },
	Talon = { 700, 0 },
	Taric = { 0, 625 },
	Teemo = { 680, 0 },
	Tristana = { 900, 700 },
	Trundle = { 0, 1000 },
	Tryndamere = { 660, 0 },
	TwistedFate = { 1700, 0 },
	Twitch = { 1200, 0 },
	Udyr = { 625, 0 },
	Urgot = { 1000, 0 },
	Varus = { 925, 1075 },
	Vayne = { 0, 450 },
	Veigar = { 650, 0 },
	Viktor = { 600, 0 },
	Vladimir = { 600, 700 },
	Volibear = { 425, 0 },
	Warwick = { 0, 700 },
	Xerath = { 1300, 900 },
	XinZhao = { 650, 0 },
	Yorick = { 600, 0 },
	Ziggs = { 1000, 850 },
	Zilean = { 700, 0 },
	Zyra = { 825, 1100 },
}
champAux = champ
player = GetMyHero()
heroindex, c = {}, 0
for i = 1, heroManager.iCount do
local h = heroManager:getHero(i)
if h.name == player.name or h.team ~= player.team then
heroindex[c+1] = i
c = c+1
end
end

function ERangesOnDraw()
for _,v in ipairs(heroindex) do
local h = heroManager:getHero(v)
local t = champAux[h.charName]

if h.visible and not h.dead then
         if h.range > 400 and (t == nil or (h.range + 100 ~= t[1] and h.range + 100 ~= t[2])) then
         DrawCircle(h.x, h.y, h.z,h.range + 100, 0xFF640000)
         end
         if t ~= nil then
         if t[1] > 0 then
                 DrawCircle(h.x, h.y, h.z,t[1], 0xFF006400)
         end
         if t[2] > 0 then
                 DrawCircle(h.x, h.y, h.z,t[2], 0xFF006400)
         end
         end
end
end
end

function PRangesOnDraw()
local h = GetMyHero()
local t = champAux[h.charName]

if h.visible and not h.dead then
         if h.range > 400 and (t == nil or (h.range + 100 ~= t[1] and h.range + 100 ~= t[2])) then
         DrawCircle(h.x, h.y, h.z,h.range + 100, 0xFF640000)
         end
         if t ~= nil then
         if t[1] > 0 then
                 DrawCircle(h.x, h.y, h.z,t[1], 0xFF006400)
         end
         if t[2] > 0 then
                 DrawCircle(h.x, h.y, h.z,t[2], 0xFF006400)
         end
         end
end
end

-- ############################################# ENEMY RANGE ################################################

 -- ############################################# PERFECT WARD ################################################


--[[
    Perfect Ward 1.7.2 by Husky
    ========================================================================

    Makes warding a no-brainer by suggesting good ward places, that give you
    the maximum field of view for most of the important ward spots where
    ward positioning is crucial (in brushes + at dragon/nashor).

    Magnetic ward spots make it nearly impossible to misplace a ward - even
    in stressfull situations. This maximizes your vision and helps you gank
    and counter jungle.

    Changelog
    ~~~~~~~~~

    1.0    - Magnetic ward spots makes wards "snap" to the perfect position

    1.1    - Added "safe wards" that can be placed over walls
            (thx to Dottie for coordinates)
           - Smartcasting wards is now supported through packet modification
            (thx to TRUS for initial code snippet)
           - Added configurable hotkeys, for people having slot items bound
             to non-default keys
           - Added an intelligent anti-gank system, that automatically pings
             roaming enemies
           - Added Possibility, to easily dump ward coordinates for script
             customization (click on a placed ward and hover the ward with
             your mouse to see the coordinates)

    1.2    - Removed the anti-gank system, since it has to be improved to send
             reasonable pings. It will be part of a separate Script (Perfect
             Ping).
           - Simplified the placement of "safe wards" by automating the movement
             and moving the magnetic spots closer to the place from where the wards
             are put.
           - Improved the visuals to make it easier to distinguish warding spots
             between "safe wards" and "perfect wards".
           - Fixed FPS issues for users with not so powerfull computers.

    1.3   - Added new ward items of season 3 patch

    1.4   - Fixed a problem that occured when using other scripts that utilize
            the packet api as well

    1.5   - Removed AllClass since its included by default
          - Made it work with new Version of Bot of Legends

    1.6   - Added compatability for BoL Mods hotkeys
          - Added new ward spots to reflect season 3 map changes
          - Fixed a bug showing c stack overflows when casting inside a circle

    1.7   - Added full support for free users (except smart cast items)

    1.7.1 - Fixed a small bug with smartcasted wards not being magnetic after latest changes
          - Reduced the amount of ward spots drawn, by showing only the visible spots (increases fps)

    1.7.2 - script uses the Packet library now, to faster address packet changes in the future

    Hotkeys
    ~~~~~~~

    Just press the number associated to the inventory slot containing your
    ward, or define your own Hotkeys in the script
]]

-- Config ----------------------------------------------------------------------

local HK_1 = HK_1 and string.byte(HK_1) or 49 -- hotkey for first itemslot (default "1")
local HK_2 = HK_2 and string.byte(HK_2) or 50 -- hotkey for first itemslot (default "2")
local HK_3 = HK_3 and string.byte(HK_3) or 51 -- hotkey for first itemslot (default "3")
local HK_4 = HK_4 and string.byte(HK_4) or 52 -- hotkey for first itemslot (default "4")
local HK_5 = HK_5 and string.byte(HK_5) or 53 -- hotkey for first itemslot (default "5")
local HK_6 = HK_6 and string.byte(HK_6) or 54 -- hotkey for first itemslot (default "6")

local wardSpots = {
    -- Perfect Wards
    { x = 2572,    y = 45.84,  z = 7457},     -- BLUE GOLEM
    { x = 7422,    y = 46.53,  z = 3282},     -- BLUE LIZARD
    { x = 10148,   y = 44.41,  z = 2839},     -- BLUE TRI BUSH
    { x = 6269,    y = 42.51,  z = 4445},     -- BLUE PASS BUSH
    { x = 7406,    y = 43.31,  z = 4995},     -- BLUE RIVER ENTRANCE
    { x = 4325,    y = 44.38,  z = 7241.54},  -- BLUE ROUND BUSH
    { x = 4728,    y = -51.29, z = 8336},     -- BLUE RIVER ROUND BUSH
    { x = 6598,    y = 46.15,  z = 2799},     -- BLUE SPLIT PUSH BUSH
    { x = 7981.41, y = 17.29,  z = 5235.91},  -- BLUE RIVER RAMP

    { x = 11500,   y = 45.75,  z = 7095},     -- PURPLE GOLEM
    { x = 6661,    y = 44.46,  z = 11197},    -- PURPLE LIZARD
    { x = 3883,    y = 39.87,  z = 11577},    -- PURPLE TRI BUSH
    { x = 7775,    y = 43.14,  z = 10046.49}, -- PURPLE PASS BUSH
    { x = 6625.47, y = 47.66,  z = 9463},     -- PURPLE RIVER ENTRANCE
    { x = 9720,    y = 45.79,  z = 7210},     -- PURPLE ROUND BUSH
    { x = 9191,    y = -73.46, z = 6004},     -- PURPLE RIVER ROUND BUSH
    { x = 7490,    y = 41,     z = 11681},    -- PURPLE SPLIT PUSH BUSH
    { x = 5896,    y = -4.05,  z = 9264.81},  -- PURPLE RIVER RAMP

    { x = 3527.43, y = -74.95, z = 9534.51},  -- NASHOR
    { x = 10473,   y = -73,    z = 5059}      -- DRAGON
}

local safeWardSpots = {
    {    -- DRAGON -> TRI BUSH
        magneticSpot =  {x = 9695,    y = 43.02, z = 3465},
        clickPosition = {x = 9843.38, y = 43.02, z = 3125.16},
        wardPosition =  {x = 9946.10, y = 43.02, z = 3064.81},
        movePosition  = {x = 9595,    y = 43.02, z = 3665}
    },
    {    -- NASHOR -> TRI BUSH
        magneticSpot =  {x = 4346.10, y = 36.62, z = 10964.81},
        clickPosition = {x = 4214.93, y = 36.62, z = 11202.01},
        wardPosition =  {x = 4146.10, y = 36.62, z = 11314.81},
        movePosition  = {x = 4384.36, y = 36.62, z = 10680.41}
    },
    {    -- BLUE TOP -> SOLO BUSH
        magneticSpot  = {x = 2349,    y = 44.20, z = 10387},
        clickPosition = {x = 2267.97, y = 44.20, z = 10783.37},
        wardPosition  = {x = 2446.10, y = 44.20, z = 10914.81},
        movePosition  = {x = 2311,    y = 44.20, z = 10185}
    },
    { -- BLUE MID -> ROUND BUSH
        magneticSpot  = {x = 4929,    y = 45.29, z = 6274.23},
        clickPosition = {x = 4712.18, y = 45.29, z = 6473.71},
        wardPosition  = {x = 4496.10, y = 45.29, z = 6664.81},
        movePosition  = {x = 5041,    y = 45.29, z = 6105}
    },
    { -- BLUE MID -> RIVER ROUND BUSH
        magneticSpot  = {x = 5000,    y = 45.61, z = 7900},
        clickPosition = {x = 5068.58, y = 45.61, z = 8213.45},
        wardPosition  = {x = 5146.10, y = 45.61, z = 8264.81},
        movePosition  = {x = 4937,    y = 45.61, z = 7685}
    },
    { -- BLUE MID -> RIVER LANE BUSH
        magneticSpot  = {x = 5528.96, y = 45.64, z = 7615.20},
        clickPosition = {x = 5688.96, y = 45.64, z = 7825.20},
        wardPosition  = {x = 5796.10, y = 45.64, z = 7914.81},
        movePosition  = {x = 5460.13, y = 45.64, z = 7469.77}
    },
    { -- BLUE LIZARD -> DRAGON PASS BUSH
        magneticSpot  = {x = 7745,    y = 47.71, z = 4065},
        clickPosition = {x = 7927.65, y = 47.71, z = 4239.77},
        wardPosition  = {x = 8146.10, y = 47.71, z = 4414.81},
        movePosition  = {x = 7645,    y = 47.71, z = 4015}
    },
    { -- PURPLE MID -> ROUND BUSH
        magneticSpot  = {x = 9057,    y = 45.73, z = 8245},
        clickPosition = {x = 9313.63, y = 45.73, z = 8031.32},
        wardPosition  = {x = 9446.10, y = 45.73, z = 7864.81},
        movePosition  = {x = 8947,    y = 45.73, z = 8355}
    },
    { -- PURPLE MID -> RIVER ROUND BUSH
        magneticSpot  = {x = 9025.78, y = 46.27, z = 6591.64},
        clickPosition = {x = 9055.24, y = 46.27, z = 6340.57},
        wardPosition  = {x = 8846.10, y = 46.27, z = 6164.81},
        movePosition  = {x = 9125.78, y = 46.27, z = 6761.64}
    },
    { -- PURPLE MID -> RIVER LANE BUSH
        magneticSpot  = {x = 8669.17, y = 46.98, z = 6734.70},
        clickPosition = {x = 8539.27, y = 46.98, z = 6637.38},
        wardPosition  = {x = 8396.10, y = 46.98, z = 6464.81},
        movePosition  = {x = 8779.17, y = 46.98, z = 6804.70}
    },
    { -- PURPLE BOTTOM -> SOLO BUSH
        magneticSpot  = {x = 11889,    y = 42.84, z = 4205},
        clickPosition = {x = 11974.23, y = 42.84, z = 3807.21},
        wardPosition  = {x = 11646.10, y = 42.84, z = 3464.81},
        movePosition  = {x = 11939,    y = 42.84, z = 4255}
    },
    { -- PURPLE LIZARD -> NASHOR PASS BUSH
        magneticSpot  = {x = 6299,    y = 45.47, z = 10377.75},
        clickPosition = {x = 6256.72, y = 46.10, z = 10110.53},
        wardPosition  = {x = 6046.10, y = 45.47, z = 9914.81},
        movePosition  = {x = 6399,    y = 45.47, z = 10477.75}
    }
}

local wardItems = {
    { id = 2043, spellName = "VisionWard",     range = 1450, duration = 180000},
    { id = 2044, spellName = "SightWard",      range = 1450, duration = 180000},
    { id = 3154, spellName = "WriggleLantern", range = 1450, duration = 180000},
    { id = 2045, spellName = "ItemGhostWard",  range = 1450, duration = 180000},
    { id = 2049, spellName = "ItemGhostWard",  range = 1450, duration = 180000},
    { id = 2050, spellName = "ItemMiniWard",   range = 1450, duration = 60000}
}

-- Globals ---------------------------------------------------------------------

local drawWardSpots      = false
local wardSlot           = nil
local putSafeWard        = nil
local wardCorrected      = false
local packetApiAvailable = false

-- Code ------------------------------------------------------------------------


function PerfectWardOnTick()
    if putSafeWard ~= nil then
        if GetDistance(safeWardSpots[putSafeWard].clickPosition, myHero) <= 650 then
            CastSpell(wardSlot, safeWardSpots[putSafeWard].clickPosition.x, safeWardSpots[putSafeWard].clickPosition.z)
            putSafeWard = nil
        end
    end
end

function PerfectWardOnWndMsg(msg,key)
    if msg == KEY_DOWN then
        wardSlot = nil
        if key == HK_1 then
            wardSlot = ITEM_1
        elseif key == HK_2 then
            wardSlot = ITEM_2
        elseif key == HK_3 then
            wardSlot = ITEM_3
        elseif key == HK_4 then
            wardSlot = ITEM_4
        elseif key == HK_5 then
            wardSlot = ITEM_5
        elseif key == HK_6 then
            wardSlot = ITEM_6
        end

        if wardSlot ~= nil then
            local item = myHero:getInventorySlot(wardSlot)
            for i,wardItem in pairs(wardItems) do
                if item == wardItem.id and myHero:CanUseSpell(wardSlot) == READY then
                    drawWardSpots = true
                    return
                end
            end
        end
    elseif msg == WM_LBUTTONUP and drawWardSpots then
        drawWardSpots = false
    elseif msg == WM_LBUTTONDOWN and drawWardSpots and not packetApiAvailable then
        drawWardSpots = false
        for i,wardSpot in pairs(wardSpots) do
            if GetDistance(wardSpot, mousePos) <= 250 and not wardCorrected then
                CastSpell(wardSlot, wardSpot.x, wardSpot.z)
                return
            end
        end

        for i,wardSpot in pairs(safeWardSpots) do
            if GetDistance(wardSpot.magneticSpot, mousePos) <= 100 and not wardCorrected then
                CastSpell(wardSlot, wardSpot.clickPosition.x, wardSpot.clickPosition.z)
                myHero:MoveTo(wardSpot.movePosition.x, wardSpot.movePosition.z)
                putSafeWard = i
                return
            end
        end
    elseif msg == WM_RBUTTONDOWN and drawWardSpots then
        drawWardSpots = false
    elseif msg == WM_RBUTTONDOWN then
        putSafeWard = nil
    end
end

function OnSendPacket(p)
    packetApiAvailable = true

    local packet = Packet(p)
    if packet:get('name') == 'S_CAST' then
        if wardSlot == packet:get('spellId') then
            drawWardSpots = false
            for i,wardSpot in pairs(wardSpots) do
                if GetDistance(wardSpot, {x = packet:get('fromX'), y = myHero.y, z = packet:get('fromY')}) <= 250 and not wardCorrected then
                    packet:block()
                    wardCorrected = true
                    CastSpell(packet:get('spellId'), wardSpot.x, wardSpot.z)
                    wardCorrected = false
                    return
                end
            end

            for i,wardSpot in pairs(safeWardSpots) do
                if GetDistance(wardSpot.magneticSpot, {x = packet:get('fromX'), y = myHero.y, z = packet:get('fromY')}) <= 100 and not wardCorrected then
                    packet:block()
                    myHero:MoveTo(wardSpot.movePosition.x, wardSpot.movePosition.z)
                    putSafeWard = i
                    return
                end
            end
        end
    end
end

function PerfectWardOnDraw()
    if drawWardSpots then
        for i, wardSpot in pairs(wardSpots) do
            local wardColor = (GetDistance(wardSpot, mousePos) <= 250) and 0x00FF00 or 0xFFFFFF

            local x, y, onScreen = get2DFrom3D(wardSpot.x, wardSpot.y, wardSpot.z)
            if onScreen then
                DrawCircle(wardSpot.x, wardSpot.y, wardSpot.z, 31, wardColor)
                DrawCircle(wardSpot.x, wardSpot.y, wardSpot.z, 32, wardColor)
                DrawCircle(wardSpot.x, wardSpot.y, wardSpot.z, 250, wardColor)
            end
        end

        for i,wardSpot in pairs(safeWardSpots) do
            local wardColor  = (GetDistance(wardSpot.magneticSpot, mousePos) <= 100) and 0x00FF00 or 0xFFFFFF
            local arrowColor = (GetDistance(wardSpot.magneticSpot, mousePos) <= 100) and RGBA(0,255,0,0) or RGBA(255,255,255,0)

            local x, y, onScreen = get2DFrom3D(wardSpot.magneticSpot.x, wardSpot.magneticSpot.y, wardSpot.magneticSpot.z)
            if onScreen then
                DrawCircle(wardSpot.wardPosition.x, wardSpot.wardPosition.y, wardSpot.wardPosition.z, 31, wardColor)
                DrawCircle(wardSpot.wardPosition.x, wardSpot.wardPosition.y, wardSpot.wardPosition.z, 32, wardColor)

                DrawCircle(wardSpot.magneticSpot.x, wardSpot.magneticSpot.y, wardSpot.magneticSpot.z, 99, wardColor)
                DrawCircle(wardSpot.magneticSpot.x, wardSpot.magneticSpot.y, wardSpot.magneticSpot.z, 100, wardColor)

                local magneticWardSpotVector = Vector(wardSpot.magneticSpot.x, wardSpot.magneticSpot.y, wardSpot.magneticSpot.z)
                local wardPositionVector = Vector(wardSpot.wardPosition.x, wardSpot.wardPosition.y, wardSpot.wardPosition.z)
                local directionVector = (wardPositionVector-magneticWardSpotVector):normalized()
                local magneticWardSpotLineVector = magneticWardSpotVector + directionVector * 90

                DrawArrow(D3DXVECTOR3(magneticWardSpotLineVector.x, magneticWardSpotLineVector.y, magneticWardSpotLineVector.z), D3DXVECTOR3(directionVector.x, directionVector.y, directionVector.z), GetDistance(magneticWardSpotVector, wardPositionVector) + 85, 20, -10000000000000000000000, arrowColor)
            end
        end
    end

    local target = GetTarget()
    for i,wardItem in pairs(wardItems) do
        if target ~= nil and target.name == wardItem.spellName then
            DrawCircle(target.x, target.y, target.z, 68, 0x00FF00)
            DrawCircle(target.x, target.y, target.z, 69, 0x00FF00)
            DrawCircle(target.x, target.y, target.z, 70, 0x00FF00)
            if GetDistanceFromMouse(target) <= 70 then
                local cursor = GetCursorPos()
    
                DrawText("X = " .. string.format("%.2f", target.x),16, cursor.x, cursor.y + 50, 0xFF00FF00)
                DrawText("Y = " .. string.format("%.2f", target.y),16, cursor.x, cursor.y + 65, 0xFF00FF00)
                DrawText("Z = " .. string.format("%.2f", target.z),16, cursor.x, cursor.y + 80, 0xFF00FF00)
            end
        end
    end
end

-- ############################################# PERFECT WARD ################################################    
	 
-- ################################################## Minimap Timers v0.2c #######################################################
--[[
Script: minimapTimers v0.2
Author: SurfaceS

In replacement of my Jungle Display v0.2

UPDATES :
v0.1                    initial release
]]
do
    --[[      GLOBAL      ]]
    monsters = {
        summonerRift = {
            {   -- baron
                name = "baron",
                spawn = 900,
                respawn = 420,
                advise = true,
                camps = {
                    {
                        pos = { x = 4600, y = 60, z = 10250 },
                        name = "monsterCamp_12",
                        creeps = { { { name = "Worm12.1.1" }, }, },
                        team = TEAM_NEUTRAL,
                    },
                },
            },
            {   -- dragon
                name = "dragon",
                spawn = 150,
                respawn = 360,
                advise = true,
                camps = {
                    {
                        pos = { x = 9459, y = 60, z = 4193 },
                        name = "monsterCamp_6",
                        creeps = { { { name = "Dragon6.1.1" }, }, },
                        team = TEAM_NEUTRAL,
                    },
                },
            },
            {   -- blue
                name = "blue",
                spawn = 115,
                respawn = 300,
                advise = true,
                camps = {
                    {
                        pos = { x = 3632, y = 60, z = 7600 },
                        name = "monsterCamp_1",
                        creeps = { { { name = "AncientGolem1.1.1" }, { name = "YoungLizard1.1.2" }, { name = "YoungLizard1.1.3" }, }, },
                        team = TEAM_BLUE,
                    },
                    {
                        pos = { x = 10386, y = 60, z = 6811 },
                        name = "monsterCamp_7",
                        creeps = { { { name = "AncientGolem7.1.1" }, { name = "YoungLizard7.1.2" }, { name = "YoungLizard7.1.3" }, }, },
                        team = TEAM_RED,
                    },
                },
            },
            {   -- red
                name = "red",
                spawn = 115,
                respawn = 300,
                advise = true,
                camps = {
                    {
                        pos = { x = 7455, y = 60, z = 3890 },
                        name = "monsterCamp_4",
                        creeps = { { { name = "LizardElder4.1.1" }, { name = "YoungLizard4.1.2" }, { name = "YoungLizard4.1.3" }, }, },
                        team = TEAM_BLUE,
                    },
                    {
                        pos = { x = 6504, y = 60, z = 10584 },
                        name = "monsterCamp_10",
                        creeps = { { { name = "LizardElder10.1.1" }, { name = "YoungLizard10.1.2" }, { name = "YoungLizard10.1.3" }, }, },
                        team = TEAM_RED,
                    },
                },
            },
            {   -- wolves
                name = "wolves",
                spawn = 125,
                respawn = 50,
                advise = false,
                camps = {
                    {
                        name = "monsterCamp_2",
                        creeps = { { { name = "GiantWolf2.1.3" }, { name = "wolf2.1.1" }, { name = "wolf2.1.2" }, }, },
                        team = TEAM_BLUE,
                    },
                    {
                        name = "monsterCamp_8",
                        creeps = { { { name = "GiantWolf8.1.3" }, { name = "wolf8.1.1" }, { name = "wolf8.1.2" }, }, },
                        team = TEAM_RED,
                    },
                },
            },
            {   -- wraiths
                name = "wraiths",
                spawn = 125,
                respawn = 50,
                advise = false,
                camps = {
                    {
                        name = "monsterCamp_3",
                        creeps = { { { name = "Wraith3.1.3" }, { name = "LesserWraith3.1.1" }, { name = "LesserWraith3.1.2" }, { name = "LesserWraith3.1.4" }, }, },
                        team = TEAM_BLUE,
                    },
                    {
                        name = "monsterCamp_9",
                        creeps = { { { name = "Wraith9.1.3" }, { name = "LesserWraith9.1.1" }, { name = "LesserWraith9.1.2" }, { name = "LesserWraith9.1.4" }, }, },
                        team = TEAM_RED,
                    },
                },
            },
            {   -- Golems
                name = "Golems",
                spawn = 125,
                respawn = 50,
                advise = false,
                camps = {
                    {
                        name = "monsterCamp_5",
                        creeps = { { { name = "Golem5.1.2" }, { name = "SmallGolem5.1.1" }, }, },
                        team = TEAM_BLUE,
                    },
                    {
                        name = "monsterCamp_11",
                        creeps = { { { name = "Golem11.1.2" }, { name = "SmallGolem11.1.1" }, }, },
                        team = TEAM_RED,
                    },
                },
            },
        },
        twistedTreeline = {
            {   -- Wraith
                name = "Wraith",
                spawn = 100,
                respawn = 50,
                advise = false,
                camps = {
                    {
                        --pos = { x = 4414, y = 60, z = 5774 },
                        name = "monsterCamp_1",
                        creeps = {
                            { { name = "TT_NWraith1.1.1" }, { name = "TT_NWraith21.1.2" }, { name = "TT_NWraith21.1.3" }, },
                        },
                        team = TEAM_BLUE,
                    },
                    {
                        --pos = { x = 11008, y = 60, z = 5775 },
                        name = "monsterCamp_4",
                        creeps = {
                            { { name = "TT_NWraith4.1.1" }, { name = "TT_NWraith24.1.2" }, { name = "TT_NWraith24.1.3" }, },
                        },
                        team = TEAM_RED,
                    },
                },
            },
            {   -- Golems
                name = "Golems",
                respawn = 50,
                spawn = 100,
                advise = false,
                camps = {
                    {
                        --pos = { x = 5088, y = 60, z = 8065 },
                        name = "monsterCamp_2",
                        creeps = {
                            { { name = "TT_NGolem2.1.1" }, { name = "TT_NGolem22.1.2" } },
                        },
                        team = TEAM_BLUE,
                    },
                    {
                        --pos = { x = 10341, y = 60, z = 8084 },
                        name = "monsterCamp_5",
                        creeps = {
                            { { name = "TT_NGolem5.1.1" }, { name = "TT_NGolem25.1.2" } },
                        },
                        team = TEAM_RED,
                    },
                },
            },
            {   -- Wolves
                name = "Wolves",
                respawn = 50,
                spawn = 100,
                advise = false,
                camps = {
                    {
                        --pos = { x = 6148, y = 60, z = 5993 },
                        name = "monsterCamp_3",
                        creeps = { { { name = "TT_NWolf3.1.1" }, { name = "TT_NWolf23.1.2" }, { name = "TT_NWolf23.1.3" } }, },
                        team = TEAM_BLUE,
                    },
                    {
                        --pos = { x = 9239, y = 60, z = 6022 },
                        name = "monsterCamp_6",
                        creeps = { { { name = "TT_NWolf6.1.1" }, { name = "TT_NWolf26.1.2" }, { name = "TT_NWolf26.1.3" } }, },
                        team = TEAM_RED,
                    },
                },
            },
            {   -- Heal
                name = "Heal",
                spawn = 115,
                respawn = 90,
                advise = true,
                camps = {
                    {
                        pos = { x = 7711, y = 60, z = 6722 },
                        name = "monsterCamp_7",
                        creeps = { { { name = "TT_Relic7.1.1" }, }, },
                        team = TEAM_NEUTRAL,
                    },
                },
            },
            {   -- Vilemaw
                name = "Vilemaw",
                spawn = 600,
                respawn = 300,
                advise = true,
                camps = {
                    {
                        pos = { x = 7711, y = 60, z = 10080 },
                        name = "monsterCamp_8",
                        creeps = { { { name = "TT_Spiderboss8.1.1" }, }, },
                        team = TEAM_NEUTRAL,
                    },
                },
            },
        },
        crystalScar = {},
        provingGrounds = {
            {   -- Heal
                name = "Heal",
                spawn = 190,
                respawn = 40,
                advise = false,
                camps = {
                    {
                        pos = { x = 8922, y = 60, z = 7868 },
                        name = "monsterCamp_1",
                        creeps = { { { name = "OdinShieldRelic1.1.1" }, }, },
                        team = TEAM_NEUTRAL,
                    },
                    {
                        pos = { x = 7473, y = 60, z = 6617 },
                        name = "monsterCamp_2",
                        creeps = { { { name = "OdinShieldRelic2.1.1" }, }, },
                        team = TEAM_NEUTRAL,
                    },
                    {
                        pos = { x = 5929, y = 60, z = 5190 },
                        name = "monsterCamp_3",
                        creeps = { { { name = "OdinShieldRelic3.1.1" }, }, },
                        team = TEAM_NEUTRAL,
                    },
                    {
                        pos = { x = 4751, y = 60, z = 3901 },
                        name = "monsterCamp_4",
                        creeps = { { { name = "OdinShieldRelic4.1.1" }, }, },
                        team = TEAM_NEUTRAL,
                    },
                },
            },
        },
        howlingAbyss = {
            {   -- Heal
                name = "Heal",
                spawn = 190,
                respawn = 40,
                advise = false,
                camps = {
                    {
                        pos = { x = 8922, y = 60, z = 7868 },
                        name = "monsterCamp_1",
                        creeps = { { { name = "HA_AP_HealthRelic1.1.1" }, }, },
                        team = TEAM_NEUTRAL,
                    },
                    {
                        pos = { x = 7473, y = 60, z = 6617 },
                        name = "monsterCamp_2",
                        creeps = { { { name = "HA_AP_HealthRelic2.1.1" }, }, },
                        team = TEAM_NEUTRAL,
                    },
                    {
                        pos = { x = 5929, y = 60, z = 5190 },
                        name = "monsterCamp_3",
                        creeps = { { { name = "HA_AP_HealthRelic3.1.1" }, }, },
                        team = TEAM_NEUTRAL,
                    },
                    {
                        pos = { x = 4751, y = 60, z = 3901 },
                        name = "monsterCamp_4",
                        creeps = { { { name = "HA_AP_HealthRelic4.1.1" }, }, },
                        team = TEAM_NEUTRAL,
                    },
                },
            },
        },
    }

    altars = {
        summonerRift = {},
        twistedTreeline = {
            {
                name = "Left Altar",
                spawn = 180,
                respawn = 90,
                advise = true,
                objectName = "TT_Buffplat_L",
                locked = false,
                lockNames = {"TT_Lock_Blue_L.troy", "TT_Lock_Purple_L.troy", "TT_Lock_Neutral_L.troy", },
                unlockNames = {"TT_Unlock_Blue_L.troy", "TT_Unlock_purple_L.troy", "TT_Unlock_Neutral_L.troy", },
            },
            {
                name = "Right Altar",
                spawn = 180,
                respawn = 90,
                advise = true,
                objectName = "TT_Buffplat_R",
                locked = false,
                lockNames = {"TT_Lock_Blue_R.troy", "TT_Lock_Purple_R.troy", "TT_Lock_Neutral_R.troy", },
                unlockNames = {"TT_Unlock_Blue_R.troy", "TT_Unlock_purple_R.troy", "TT_Unlock_Neutral_R.troy", },
            },
        },
        crystalScar = {},
        provingGrounds = {},
        howlingAbyss = {},
    }

    relics = {
        summonerRift = {},
        twistedTreeline = {},
        crystalScar = {
            {
                pos = { x = 5500, y = 60, z = 6500 },
                name = "Relic",
                team = TEAM_BLUE,
                spawn = 180,
                respawn = 180,
                advise = true,
                locked = false,
                precenceObject = (player.team == TEAM_BLUE and "Odin_Prism_Green.troy" or "Odin_Prism_Red.troy"),
            },
            {
                pos = { x = 7550, y = 60, z = 6500 },
                name = "Relic",
                team = TEAM_RED,
                spawn = 180,
                respawn = 180,
                advise = true,
                locked = false,
                precenceObject = (player.team == TEAM_RED and "Odin_Prism_Green.troy" or "Odin_Prism_Red.troy"),
            },
        },
        provingGrounds = {},
        howlingAbyss = {},
    }

    heals = {
        summonerRift = {},
        twistedTreeline = {},
        provingGrounds = {},
        crystalScar = {
            {
                name = "Heal",
                objectName = "OdinShieldRelic",
                respawn = 30,
                objects = {},
            },
        },
        howlingAbyss = {},
    }

    inhibitors = {}

    function addCampCreepAltar(object)
        if object ~= nil and object.name ~= nil then
            if object.name == "Order_Inhibit_Gem.troy" or object.name == "Chaos_Inhibit_Gem.troy" then
                table.insert(inhibitors, { object = object, destroyed = false, lefttime = 0, x = object.x, y = object.y, z = object.z, minimap = GetMinimap(object), textTick = 0 })
                return
            elseif object.name == "Order_Inhibit_Crystal_Shatter.troy" or object.name == "Chaos_Inhibit_Crystal_Shatter.troy" then
                for i,inhibitor in pairs(inhibitors) do
                    if GetDistance(inhibitor, object) < 200 then
                        local tick = GetTickCount()
                        inhibitor.dtime = tick
                        inhibitor.rtime = tick + 300000
                        inhibitor.ltime = 300000
                        inhibitor.destroyed = true
                    end
                end
                return
            end
            for i,monster in pairs(monsters[mapName]) do
                for j,camp in pairs(monster.camps) do
                    if camp.name == object.name then
                        camp.object = object
                        return
                    end
                    if object.type == "obj_AI_Minion" then
                        for k,creepPack in ipairs(camp.creeps) do
                            for l,creep in ipairs(creepPack) do
                                if object.name == creep.name then
                                    creep.object = object
                                    return
                                end
                            end
                        end
                    end
                end
            end
            for i,altar in pairs(altars[mapName]) do
                if altar.objectName == object.name then
                    altar.object = object
                    altar.textTick = 0
                    altar.minimap = GetMinimap(object)
                end
                if altar.locked then
                    for j,lockName in pairs(altar.unlockNames) do
                        if lockName == object.name then
                            altar.locked = false
                            return
                        end
                    end
                else
                    for j,lockName in pairs(altar.lockNames) do
                        if lockName == object.name then
                            altar.drawColor = 0
                            altar.drawText = ""
                            altar.locked = true
                            altar.advised = false
                            altar.advisedBefore = false
                            return
                        end
                    end
                end
            end
            for i,relic in pairs(relics[mapName]) do
                if relic.precenceObject == object.name then
                    relic.object = object
                    relic.textTick = 0
                    relic.locked = false
                    return
                end
            end
            for i,heal in pairs(heals[mapName]) do
                if heal.objectName == object.name then
                    for j,healObject in pairs(heal.objects) do
                        if (GetDistance(healObject, object) < 50) then
                            healObject.object = object
                            healObject.found = true
                            healObject.locked = false
                            return
                        end
                    end
                    local k = #heal.objects + 1
                    heals[mapName][i].objects[k] = {found = true, locked = false, object = object, x = object.x, y = object.y, z = object.z, minimap = GetMinimap(object), textTick = 0,}
                    return
                end
            end
        end
    end

    function removeCreep(object)
        if object ~= nil and object.type == "obj_AI_Minion" and object.name ~= nil then
            for i,monster in pairs(monsters[mapName]) do
                for j,camp in pairs(monster.camps) do
                    for k,creepPack in ipairs(camp.creeps) do
                        for l,creep in ipairs(creepPack) do
                            if object.name == creep.name then
                                creep.object = nil
                                return
                            end
                        end
                    end
                end
            end
        end
    end

    function MiniMapTimersOnLoad()
        mapName = GetGame().map.shortName
        if monsters[mapName] == nil then
            mapName = nil
            monsters = nil
            addCampCreepAltar = nil
            removeCreep = nil
            addAltarObject = nil
            return
        else
            startTick = GetGame().tick
            -- CONFIG
            MMTConfig = scriptConfig("小地图计时 0.2", "minimapTimers")
            MMTConfig:addParam("pingOnRespawn", "复活时声音提示", SCRIPT_PARAM_ONOFF, false) -- ping location on respawn
            MMTConfig:addParam("pingOnRespawnBefore", "复活前声音预报", SCRIPT_PARAM_ONOFF, false) -- ping location before respawn
            MMTConfig:addParam("textOnRespawn", "复活时聊天提示", SCRIPT_PARAM_ONOFF, true) -- print chat text on respawn
            MMTConfig:addParam("textOnRespawnBefore", "复活前聊天预报", SCRIPT_PARAM_ONOFF, true) -- print chat text before respawn
            MMTConfig:addParam("adviceEnemyMonsters", "敌方野怪提醒", SCRIPT_PARAM_ONOFF, true) -- advice enemy monster, or just our monsters
            MMTConfig:addParam("adviceBefore", "预报野怪时间", SCRIPT_PARAM_SLICE, 20, 1, 40, 0) -- time in second to advice before monster respawn
            MMTConfig:addParam("textOnMap", "地图文本", SCRIPT_PARAM_ONOFF, true) -- time in second on map
            for i,monster in pairs(monsters[mapName]) do
                monster.isSeen = false
                for j,camp in pairs(monster.camps) do
                    camp.enemyTeam = (camp.team == TEAM_ENEMY)
                    camp.textTick = 0
                    camp.status = 0
                    camp.drawText = ""
                    camp.drawColor = 0xFF00FF00
                end
            end
            for i = 1, objManager.maxObjects do
                local object = objManager:getObject(i)
                if object ~= nil then
                    addCampCreepAltar(object)
                end
            end
            AddCreateObjCallback(addCampCreepAltar)
            AddDeleteObjCallback(removeCreep)
        end
    end
    function MiniMapTimersOnTick()
        if GetGame().isOver then return end
        local GameTime = (GetTickCount()-startTick) / 1000
        local monsterCount = 0
        for i,monster in pairs(monsters[mapName]) do
            for j,camp in pairs(monster.camps) do
                local campStatus = 0
                for k,creepPack in ipairs(camp.creeps) do
                    for l,creep in ipairs(creepPack) do
                        if creep.object ~= nil and creep.object.valid and creep.object.dead == false then
                            if l == 1 then
                                campStatus = 1
                            elseif campStatus ~= 1 then
                                campStatus = 2
                            end
                        end
                    end
                end
                --[[  Not used until camp.showOnMinimap work
                if (camp.object and camp.object.showOnMinimap == 1) then
                -- camp is here
                if campStatus == 0 then campStatus = 3 end
                elseif camp.status == 3 then                        -- empty not seen when killed
                campStatus = 5
                elseif campStatus == 0 and (camp.status == 1 or camp.status == 2) then
                campStatus = 4
                camp.deathTick = tick
                end
                ]]
                -- temp fix until camp.showOnMinimap work
                -- not so good
                if camp.object ~= nil and camp.object.valid then
                    camp.minimap = GetMinimap(camp.object)
                    if campStatus == 0 then
                        if (camp.status == 1 or camp.status == 2) then
                            campStatus = 4
                            camp.advisedBefore = false
                            camp.advised = false
                            camp.respawnTime = math.ceil(GameTime) + monster.respawn
                            camp.respawnText = (camp.enemyTeam and "敌人 " or "")..monster.name.." 将于 "..TimerText(camp.respawnTime).." 复活 "
                        elseif (camp.status == 4) then
                            campStatus = 4
                        else
                            campStatus = 3
                        end
                    end
                elseif camp.pos ~= nil then
                    camp.minimap = GetMinimap(camp.pos)
                    if (GameTime < monster.spawn) then
                        campStatus = 4
                        camp.advisedBefore = true
                        camp.advised = true
                        camp.respawnTime = monster.spawn
                        camp.respawnText = (camp.enemyTeam and "敌人 " or "")..monster.name.." 将于 "..TimerText(camp.respawnTime).." 复活 "
                    end
                end
                if camp.status ~= campStatus or campStatus == 4 then
                    if campStatus ~= 0 then
                        if monster.isSeen == false then monster.isSeen = true end
                        camp.status = campStatus
                    end
                    if camp.status == 1 then                -- ready
                        camp.drawText = "准备"
                        camp.drawColor = 0xFF00FF00
                    elseif camp.status == 2 then            -- ready, master creeps dead
                        camp.drawText = "偷掉"
                        camp.drawColor = 0xFFFF0000
                    elseif camp.status == 3 then            -- ready, not creeps shown
                        camp.drawText = "   ?"
                        camp.drawColor = 0xFF00FF00
                    elseif camp.status == 4 then            -- empty from creeps kill
                        local secondLeft = math.ceil(math.max(0, camp.respawnTime - GameTime))
                        if monster.advise == true and (MMTConfig.adviceEnemyMonsters == true or camp.enemyTeam == false) then
                            if secondLeft == 0 and camp.advised == false then
                                camp.advised = true
                                if MMTConfig.textOnRespawn then PrintChat("<font color='#00FFCC'>"..(camp.enemyTeam and "鏁屼汉 " or "")..monster.name.."</font><font color='#FFAA00'> 宸茬粡澶嶆椿</font>") end
                                if MMTConfig.pingOnRespawn then PingSignal(PING_FALLBACK,camp.object.x,camp.object.y,camp.object.z,2) end
                            elseif secondLeft <= MMTConfig.adviceBefore and camp.advisedBefore == false then
                                camp.advisedBefore = true
                                if MMTConfig.textOnRespawnBefore then PrintChat("<font color='#00FFCC'>"..(camp.enemyTeam and "鏁屼汉 " or "")..monster.name.."</font><font color='#FFAA00'> 灏嗕簬 </font><font color='#00FFCC'>"..secondLeft.." 绉掑唴澶嶆椿</font>") end
                                if MMTConfig.pingOnRespawnBefore then PingSignal(PING_FALLBACK,camp.object.x,camp.object.y,camp.object.z,2) end
                            end
                        end
                        -- temp fix until camp.showOnMinimap work
                        if secondLeft == 0 then
                            camp.status = 0
                        end
                        camp.drawText = " "..TimerText(secondLeft)
                        camp.drawColor = 0xFFFFFF00
                    elseif camp.status == 5 then            -- camp found empty (not using yet)
                        camp.drawText = "   -"
                        camp.drawColor = 0xFFFF0000
                    end
                end
                -- shift click
                if IsKeyDown(16) and camp.status == 4 then
                    camp.drawText = " "..(camp.respawnTime ~= nil and TimerText(camp.respawnTime) or "")
                    camp.textUnder = (CursorIsUnder(camp.minimap.x - 9, camp.minimap.y - 5, 20, 8))
                else
                    camp.textUnder = false
                end
                if MMTConfig.textOnMap and camp.status == 4 and camp.object and camp.object.valid and camp.textTick < GetTickCount() and camp.floatText ~= camp.drawText then
                    camp.floatText = camp.drawText
                    camp.textTick = GetTickCount() + 1000
                    PrintFloatText(camp.object,6,camp.floatText)
                end
            end
        end

        -- altars
        for i,altar in pairs(altars[mapName]) do
            if altar.object and altar.object.valid then
                if altar.locked then
                    if GameTime < altar.spawn then
                        altar.secondLeft = math.ceil(math.max(0, altar.spawn - GameTime))
                    else
                        local tmpTime = ((altar.object.mana > 39600) and (altar.object.mana - 39900) / 20100 or (39600 - altar.object.mana) / 20100)
                        altar.secondLeft = math.ceil(math.max(0, tmpTime * altar.respawn))
                    end
                    altar.unlockTime = math.ceil(GameTime + altar.secondLeft)
                    altar.unlockText = altar.name.." 将于 "..TimerText(altar.unlockTime).." 解锁 "
                    altar.drawColor = 0xFFFFFF00
                    if altar.advise == true then
                        if altar.secondLeft == 0 and altar.advised == false then
                            altar.advised = true
                            if MMTConfig.textOnRespawn then PrintChat("<font color='#00FFCC'>"..altar.name.."</font><font color='#FFAA00'> 宸茬粡瑙ｉ攣</font>") end
                            if MMTConfig.pingOnRespawn then PingSignal(PING_FALLBACK,altar.object.x,altar.object.y,altar.object.z,2) end
                        elseif altar.secondLeft <= MMTConfig.adviceBefore and altar.advisedBefore == false then
                            altar.advisedBefore = true
                            if MMTConfig.textOnRespawnBefore then PrintChat("<font color='#00FFCC'>"..altar.name.."</font><font color='#FFAA00'> 灏嗕簬 </font><font color='#00FFCC'>"..altar.secondLeft.." 绉掑唴瑙ｉ攣</font>") end
                            if MMTConfig.pingOnRespawnBefore then PingSignal(PING_FALLBACK,altar.object.x,altar.object.y,altar.object.z,2) end
                        end
                    end
                    -- shift click
                    if IsKeyDown(16) then
                        altar.drawText = " "..(altar.unlockTime ~= nil and TimerText(altar.unlockTime) or "")
                        altar.textUnder = (CursorIsUnder(altar.minimap.x - 9, altar.minimap.y - 5, 20, 8))
                    else
                        altar.drawText = " "..(altar.secondLeft ~= nil and TimerText(altar.secondLeft) or "")
                        altar.textUnder = false
                    end
                    if MMTConfig.textOnMap and altar.object and altar.object.valid and altar.textTick < GetTickCount() and altar.floatText ~= altar.drawText then
                        altar.floatText = altar.drawText
                        altar.textTick = GetTickCount() + 1000
                        PrintFloatText(altar.object,6,altar.floatText)
                    end
                end
            end
        end

        -- relics
        for i,relic in pairs(relics[mapName]) do
            if (not relic.locked and (not relic.object or not relic.object.valid or relic.dead)) then
                if GameTime < relic.spawn then
                    relic.unlockTime = relic.spawn - GameTime
                else
                    relic.unlockTime = math.ceil(GameTime + relic.respawn)
                end
                relic.advised = false
                relic.advisedBefore = false
                relic.drawText = ""
                relic.unlockText = relic.name.." 将于 "..TimerText(relic.unlockTime).." 复活 "
                relic.drawColor = 4288610048
                --FF9EFF00
                relic.minimap = GetMinimap(relic.pos)
                relic.locked = true
            end
            if relic.locked then
                relic.secondLeft = math.ceil(math.max(0, relic.unlockTime - GameTime))
                if relic.advise == true then
                    if relic.secondLeft == 0 and relic.advised == false then
                        relic.advised = true
                        if MMTConfig.textOnRespawn then PrintChat("<font color='#00FFCC'>"..relic.name.."</font><font color='#FFAA00'> 宸茬粡澶嶆椿</font>") end
                        if MMTConfig.pingOnRespawn then PingSignal(PING_FALLBACK,relic.pos.x,relic.pos.y,relic.pos.z,2) end
                    elseif relic.secondLeft <= MMTConfig.adviceBefore and relic.advisedBefore == false then
                        relic.advisedBefore = true
                        if MMTConfig.textOnRespawnBefore then PrintChat("<font color='#00FFCC'>"..relic.name.."</font><font color='#FFAA00'> 灏嗕簬 </font><font color='#00FFCC'>"..relic.secondLeft.." 绉掑唴澶嶆椿</font>") end
                        if MMTConfig.pingOnRespawnBefore then PingSignal(PING_FALLBACK,relic.pos.x,relic.pos.y,relic.pos.z,2) end
                    end
                end
                -- shift click
                if IsKeyDown(16) then
                    relic.drawText = " "..(relic.unlockTime ~= nil and TimerText(relic.unlockTime) or "")
                    relic.textUnder = (CursorIsUnder(relic.minimap.x - 9, relic.minimap.y - 5, 20, 8))
                else
                    relic.drawText = " "..(relic.secondLeft ~= nil and TimerText(relic.secondLeft) or "")
                    relic.textUnder = false
                end
            end
        end

        for i,heal in pairs(heals[mapName]) do
            for j,healObject in pairs(heal.objects) do
                if (not healObject.locked and healObject.found and (not healObject.object or not healObject.object.valid or healObject.object.dead)) then
                    healObject.drawColor = 0xFF00FF04
                    healObject.unlockTime = math.ceil(GameTime + heal.respawn)
                    healObject.drawText = ""
                    healObject.found = false
                    healObject.locked = true
                end
                if healObject.locked then
                    -- shift click
                    local secondLeft = math.ceil(math.max(0, healObject.unlockTime - GameTime))
                    if IsKeyDown(16) then
                        healObject.drawText = " "..(healObject.unlockTime ~= nil and TimerText(healObject.unlockTime) or "")
                        healObject.textUnder = (CursorIsUnder(healObject.minimap.x - 9, healObject.minimap.y - 5, 20, 8))
                    else
                        healObject.drawText = " "..(secondLeft ~= nil and TimerText(secondLeft) or "")
                        healObject.textUnder = false
                    end
                    if secondLeft == 0 then healObject.locked = false end
                end
            end
        end
        -- inhib
        for i,inhibitor in pairs(inhibitors) do
            if inhibitor.destroyed then
                local tick = GetTickCount()
                if inhibitor.rtime < tick then
                    inhibitor.destroyed = false
                else
                    inhibitor.ltime = (inhibitor.rtime - GetTickCount()) / 1000;
                    inhibitor.drawText = TimerText(inhibitor.ltime)
                    --inhibitor.drawText = (IsKeyDown(16) and TimerText(inhibitor.rtime) or TimerText(inhibitor.rtime))
                    if MMTConfig.textOnMap and inhibitor.textTick < tick then
                        inhibitor.textTick = tick + 1000
                        PrintFloatText(inhibitor.object,6,inhibitor.drawText)
                    end
                end
            end
        end
    end

    function MiniMapTimersOnDraw()
        if GetGame().isOver then return end
        for i,monster in pairs(monsters[mapName]) do
            if monster.isSeen == true then
                for j,camp in pairs(monster.camps) do
                    if camp.status == 2 then
                        DrawText("X",16,camp.minimap.x - 4, camp.minimap.y - 5, camp.drawColor)
                    elseif camp.status == 4 then
                        DrawText(camp.drawText,16,camp.minimap.x - 9, camp.minimap.y - 5, camp.drawColor)
                    end
                end
            end
        end
        for i,altar in pairs(altars[mapName]) do
            if altar.locked then
                DrawText(altar.drawText,16,altar.minimap.x - 9, altar.minimap.y - 5, altar.drawColor)
            end
        end
        for i,relic in pairs(relics[mapName]) do
            if relic.locked then
                DrawText(relic.drawText,16,relic.minimap.x - 9, relic.minimap.y - 5, relic.drawColor)
            end
        end
        for i,heal in pairs(heals[mapName]) do
            for j,healObject in pairs(heal.objects) do
                if healObject.locked then
                    DrawText(healObject.drawText,16,healObject.minimap.x - 9, healObject.minimap.y - 5, healObject.drawColor)
                end
            end
        end
        for i,inhibitor in pairs(inhibitors) do
            if inhibitor.destroyed == true then
                DrawText(inhibitor.drawText,16,inhibitor.minimap.x - 9, inhibitor.minimap.y - 5, 0xFFFFFF00)
            end
        end
    end

    function MiniMapTimersOnWndMsg(msg,key)
        if msg == WM_LBUTTONDOWN and IsKeyDown(16) then
            for i,monster in pairs(monsters[mapName]) do
                if monster.isSeen == true then
                    if monster.iconUnder then
                        monster.advise = not monster.advise
                        break
                    else
                        for j,camp in pairs(monster.camps) do
                            if camp.textUnder then
                                if camp.respawnText ~= nil then SendChat(""..camp.respawnText) end
                                break
                            end
                        end
                    end
                end
            end
            for i,altar in pairs(altars[mapName]) do
                if altar.locked and altar.textUnder then
                    if altar.unlockText ~= nil then SendChat(""..altar.unlockText) end
                    break
                end
            end
        end
    end
end	 
-- ################################################## Minimap Timers v0.2c #######################################################
-- ######################################  Where Is He ??? ###############################################
--[[
    WhereIsHe v1.2 by eXtragoZ
        
    -Simple script that shows where the enemy can be
    -Thinner is the line is means that the enemy can be closer
    -Calculates the moving speed multiplied by the time that the enemy is miss
    -Circles have a minimum radius and maximum radius
]]

--[[        Code        ]]
local player = GetMyHero()
local unittraveled = 0
local MissTimer = {}
local MissSec = {}
local tick_heros = {}

function WhereIsHeOnTick()
    for i=1, heroManager.iCount do
        local heros = heroManager:GetHero(i)
        if heros.visible == false and heros.dead == false and heros.team ~= player.team then
            if tick_heros[i] == nil then
                tick_heros[i] = GetTickCount()
            end
            MissTimer[i] = GetTickCount() - tick_heros[i]           
            MissSec[i] =  MissTimer[i]/1000
        else
            tick_heros[i] = nil
            MissTimer[i] = nil
            MissSec[i] = 0
        end
    end
end
function WhereIsHeOnDraw()
    for i = 1, heroManager.iCount, 1 do
        local enemy = heroManager:getHero(i)
        if enemy ~= nil and enemy.team ~= player.team and enemy.dead == false and enemy.visible == false then
                if MissSec[i] == nil then
                    MissSec[i] = 0
                end
            unittraveled = enemy.ms*MissSec[i]
            if unittraveled > 50 and unittraveled < 10000 then
                DrawCircle(enemy.x, enemy.y, enemy.z, unittraveled, 0xFF0000)
            end
        end
    end
end
--PrintChat(" >> WhereIsHe 1.2 loaded!")
-- #############################################################################
-- ############################################ Where did he go? #########################################
--[[#####   Where did he go? v0.4 by ViceVersa   #####]]--
--[[Draws a line to the location where enemys blink or flash to.]]

--Vars
local blink = {} --Blink Ability Array
local vayneUltEndTick = 0
local shacoIndex = 0


--Functions
function FindNearestNonWall(x0, y0, z0, maxRadius, precision) --returns the nearest non-wall-position of the given position(Credits to gReY)
    
    --Convert to vector
    local vec = D3DXVECTOR3(x0, y0, z0)
    
    --If the given position it a non-wall-position return it
    if not IsWall(vec) then return vec end
    
    --Optional arguments
    precision = precision or 50
    maxRadius = maxRadius and math.floor(maxRadius / precision) or math.huge
    
    --Round x, z
    x0, z0 = math.round(x0 / precision) * precision, math.round(z0 / precision) * precision

    --Init vars
    local radius = 1
    
    --Check if the given position is a non-wall position
    local function checkP(x, y) 
        vec.x, vec.z = x0 + x * precision, z0 + y * precision 
        return not IsWall(vec) 
    end
    
    --Loop through incremented radius until a non-wall-position is found or maxRadius is reached
    while radius <= maxRadius do
        --A lot of crazy math (ask gReY if you don't understand it. I don't)
        if checkP(0, radius) or checkP(radius, 0) or checkP(0, -radius) or checkP(-radius, 0) then 
            return vec 
        end
        local f, x, y = 1 - radius, 0, radius
        while x < y - 1 do
            x = x + 1
            if f < 0 then 
                f = f + 1 + 2 * x
            else 
                y, f = y - 1, f + 1 + 2 * (x - y)
            end
            if checkP(x, y) or checkP(-x, y) or checkP(x, -y) or checkP(-x, -y) or 
               checkP(y, x) or checkP(-y, x) or checkP(y, -x) or checkP(-y, -x) then 
                return vec 
            end
        end
        --Increment radius every iteration
        radius = radius + 1
    end
end
    
    
--Callbacks
function WDHGOnLoad() --Called one time on load
    
    --Fill the Blink Ability Array
    for i, heroObj in pairs(GetEnemyHeroes()) do
        
        --If the object exists and the player is in the enemy team
        if heroObj and heroObj.valid then
            
            --Summoner Flash
            if heroObj:GetSpellData(SUMMONER_1).name:find("Flash") or heroObj:GetSpellData(SUMMONER_2).name:find("Flash") then
                table.insert(blink,{name = "SummonerFlash"..heroObj.charName, maxRange = 400, delay = 0, casted = false, timeCasted = 0, startPos = {}, endPos = {}, castingHero = heroObj, shortName = "Flash"})
            end
            
            --Ezreal E
            if heroObj.charName == "Ezreal" then
                table.insert(blink,{name = "EzrealArcaneShift", maxRange = 475, delay = 0, casted = false, timeCasted = 0, startPos = {}, endPos = {}, castingHero = heroObj, shortName = "E"})
            
            --Fiora R
            elseif heroObj.charName == "Fiora" then
                table.insert(blink,{name = "FioraDance", maxRange = 700, delay = 1, casted = false, timeCasted = 0, startPos = {}, endPos = {}, castingHero = heroObj, shortName = "R", target = {}, targetDead = false})
            
            --Kassadin R
            elseif heroObj.charName == "Kassadin" then
                table.insert(blink,{name = "RiftWalk", maxRange = 700, delay = 0, casted = false, timeCasted = 0, startPos = {}, endPos = {}, castingHero = heroObj, shortName = "R"})
            
            --Katarina E
            elseif heroObj.charName == "Katarina" then
                table.insert(blink,{name = "KatarinaE", maxRange = 700, delay = 0, casted = false, timeCasted = 0, startPos = {}, endPos = {}, castingHero = heroObj, shortName = "E"})
            
            --Leblanc W
            elseif heroObj.charName == "Leblanc" then
                table.insert(blink,{name = "LeblancSlide", maxRange = 600, delay = 0.5, casted = false, timeCasted = 0, startPos = {}, endPos = {}, castingHero = heroObj, shortName = "W"})
                table.insert(blink,{name = "leblancslidereturn", delay = 0, casted = false, timeCasted = 0, startPos = {}, endPos = {}, castingHero = heroObj, shortName = "W"})
                table.insert(blink,{name = "LeblancSlideM", maxRange = 600, delay = 0.5, casted = false, timeCasted = 0, startPos = {}, endPos = {}, castingHero = heroObj, shortName = "W"})
                table.insert(blink,{name = "leblancslidereturnm", delay = 0, casted = false, timeCasted = 0, startPos = {}, endPos = {}, castingHero = heroObj, shortName = "W"})
            
            --[[Lissandra E (wip)(ToDo: Draw where she blinks to only when she does)
            elseif heroObj.charName == "Lissandra" then
                table.insert(blink,{name= "LissandraE", maxRange = 700, delay = 0, casted = false, timeCasted = 0, startPos = {}, endPos = {}, castingHero = heroObj, shortName = "E"})]]
            
            --Master Yi Q
            elseif heroObj.charName == "MasterYi" then
                table.insert(blink,{name = "AlphaStrike", maxRange = 600, delay = 0.9, casted = false, timeCasted = 0, startPos = {}, endPos = {}, castingHero = heroObj, shortName = "Q", target = {}, targetDead = false})
            
            --Shaco Q
            elseif heroObj.charName == "Shaco" then
                table.insert(blink,{name = "Deceive", maxRange = 400, delay = 0, casted = false, timeCasted = 0, startPos = {}, endPos = {}, castingHero = heroObj, shortName = "Q", outOfBush = false})
                shacoIndex = #blink --Save the position of shacos Q

            --Talon E
            elseif heroObj.charName == "Talon" then
                table.insert(blink,{name = "TalonCutthroat", maxRange = 700, delay = 0, casted = false, timeCasted = 0, startPos = {}, endPos = {}, castingHero = heroObj, shortName = "E"})
            
            --Vayne Q
            elseif heroObj.charName == "Vayne" then
                table.insert(blink,{name = "VayneTumble", maxRange = 250, delay = 0, casted = false, timeCasted = 0, startPos = {}, endPos = {}, castingHero = heroObj, shortName = "Q"})
                vayneUltEndTick = 1 --Start to check for Vayne's ult
            
            --[[Zed W (wip)(ToDo: Draw where he blinks when he swaps place with shadow)
            elseif heroObj.charName == "Zed" then
                table.insert(blink,{name= "ZedShadowDash", maxRange = 999, delay = 0, casted = false, timeCasted = 0, startPos = {}, endPos = {}, castingHero = heroObj, shortName = "W"})]]
            
            end
            
        end
    end
    
    --If something was added to the array
    if #blink > 0 then

        --Shift-Menu
        WDHGConfig = scriptConfig("敌人逃跑预测","whereDidHeGo")
        WDHGConfig:addParam("wallPrediction",     "使用警报墙",      SCRIPT_PARAM_ONOFF, false)
        WDHGConfig:addParam("displayTime",        "显示时间 (无视野)", SCRIPT_PARAM_SLICE, 3, 1, 5, 0)
        WDHGConfig:addParam("displayTimeVisible", "显示时间 (视野中)",    SCRIPT_PARAM_SLICE, 1, 0.5, 3, 1)
        WDHGConfig:addParam("lineColor",          "线条颜色",               SCRIPT_PARAM_COLOR, {255,255,255,0})
        WDHGConfig:addParam("lineWidth",          "线条宽度",               SCRIPT_PARAM_SLICE, 3, 1, 5, 0)
        WDHGConfig:addParam("circleColor",        "圆圈颜色",             SCRIPT_PARAM_COLOR, {255,255,25,0})
        WDHGConfig:addParam("circleSize",         "圆圈大小",              SCRIPT_PARAM_SLICE, 100, 50, 300, 0)
    
        --Print Load-Message
        --if #blink > 1 then
            --print('<font color="#A0FF00">Where did he go? >> v0.4 loaded! (Found '..#blink..' abilitys)</font>')
        --else
            --print('<font color="#A0FF00">Where did he go? >> v0.4 loaded! (Found 1 ability)</font>')
        --end
        
    --else
        --Print Notice
        --print('<font color="#FFFF55">Where did he go? >> No characters with blink abilitys or flash found!</font>')
    end
    
end

function WDHGOnProcessSpell(unit, spell)--When a spell is casted
    
    --If the casting unit is in the enemy team and if it is a champion-ability
    if unit and unit.valid and unit.team == TEAM_ENEMY and unit.type == "obj_AI_Hero" then
        
        --If the spell is Vayne's R
        if vayneUltEndTick > 0 and spell.name == "vayneinquisition" then
            vayneUltEndTick = os.clock() + 6 + 2*spell.level
            return --skip the array
        end
        
        --For each skillshot in the array
        for i=1, #blink, 1 do
            
            --If the casted spell is in the array
            if spell.name == blink[i].name or spell.name..unit.charName == blink[i].name then                
                
                --local function to set the normal end position
                local function SetNormalEndPosition(i, spell)
                    --If the position the enemy clicked is inside the range of the ability set the end position to that position
                    if GetDistance(spell.startPos, spell.endPos) <= blink[i].maxRange then
                        --Set the end position
                        blink[i].endPos = { x = spell.endPos.x, y = spell.endPos.y, z = spell.endPos.z }
                    
                    --Else Calculate the true position if the enemy clicked outside of the ability range
                    else
                        local vStartPos = Vector(spell.startPos.x, spell.startPos.y, spell.startPos.z)
                        local vEndPos = Vector(spell.endPos.x, spell.endPos.y, spell.endPos.z)
                        local tEndPos = vStartPos - (vStartPos - vEndPos):normalized() * blink[i].maxRange
                        
                        --If enabled, Check if the position is in a wall and return the position where the player was really flashed to
                        if WDHGConfig.wallPrediction then
                            tEndPos = FindNearestNonWall(tEndPos.x, tEndPos.y, tEndPos.z, 1000)
                        end
                        
                        --Set the end position
                        blink[i].endPos = { x = tEndPos.x, y = tEndPos.y, z = tEndPos.z }
                    
                    end
                end
                
                --##### Champion-Specific-Stuff #####--
                --#Vayne#
                --Exit if the spell is Vayne's Q and her ult isn't running
                if blink[i].name == "VayneTumble" and os.clock() >= vayneUltEndTick then return end
                
                --#Shaco#
                --Set outOfBush to false if the spell can be tracked
                if blink[i].name == "Deceive" then
                    blink[i].outOfBush = false
                end
                
                --#Leblanc#
                --If the spell is a mirrored W
                if blink[i].name == "LeblancSlideM" then
                    
                    --Cancel the normal W
                    blink[i-2].casted = false
                    
                    --Set the start position to the start position of the first W
                    blink[i].startPos = { x = blink[i-2].startPos.x, y = blink[i-2].startPos.y, z = blink[i-2].startPos.z }
                    
                    --Set the normal end position
                    SetNormalEndPosition(i, spell)
                
                --If the spell is one of Leblanc's returns
                elseif blink[i].name == "leblancslidereturn" or blink[i].name == "leblancslidereturnm" then
                    
                    --Cancel the other W-spells if she returns
                    if blink[i].name == "leblancslidereturn" then
                        blink[i-1].casted = false
                        blink[i+1].casted = false
                        blink[i+2].casted = false
                    else
                        blink[i-3].casted = false
                        blink[i-2].casted = false
                        blink[i-1].casted = false
                    end
                    
                    --Set the normal start position
                    blink[i].startPos = { x = spell.startPos.x, y = spell.startPos.y, z = spell.startPos.z }
                    
                    --Set the end position to the start position of her last slide
                    blink[i].endPos = { x = blink[i-1].startPos.x, y = blink[i-1].startPos.y, z = blink[i-1].startPos.z }
                
                --#Fiora# / #MasterYi#
                elseif blink[i].name == "FioraDance" or blink[i].name == "AlphaStrike" then
                    
                    --Set the target minion
                    blink[i].target = spell.target
                    
                    --Set targetDead to false
                    blink[i].targetDead = false
                    
                    --Set the normal start position
                    blink[i].startPos = { x = spell.startPos.x, y = spell.startPos.y, z = spell.startPos.z }
                    
                    --Set the end position to the position of the targeted unit
                    blink[i].endPos = { x = blink[i].target.x, y = blink[i].target.y, z = blink[i].target.z }
                    
                    
                --##### End of Champion-Specific-Stuff #####--
                                
                --Else set the normal positions
                else
                    
                    --Set the start position
                    blink[i].startPos = { x = spell.startPos.x, y = spell.startPos.y, z = spell.startPos.z }
                    
                    --Set the end position
                    SetNormalEndPosition(i, spell)

                end
                
                --Set casted to true
                blink[i].casted = true
                
                --Set the time when the ability is casted
                blink[i].timeCasted = os.clock()
                                
                --Exit loop
                break
                
            end
            
        end
    end
end

function WDHGOnCreateObj(obj)
    if shacoIndex ~= 0 and obj and obj.valid and obj.name == "JackintheboxPoof2.troy" and not blink[shacoIndex].casted then
        --Set the start and end position of shacos Q to the position of the obj
        blink[shacoIndex].startPos = { x = obj.x, y = obj.y, z = obj.z }
        blink[shacoIndex].endPos = { x = obj.x, y = obj.y, z = obj.z }
        
        --Set casted to true
        blink[shacoIndex].casted = true
        
        --Set the time when the ability is casted
        blink[shacoIndex].timeCasted = os.clock()
        
        --Set outOfBush to true to draw the circle instead the line
        blink[shacoIndex].outOfBush = true
    end
end

function WDHGOnTick()
    --Loop through all abilitys
    for i=1, #blink, 1 do
        
        --If the ability was casted
        if blink[i].casted then
            
            --If the enemy is Fiora or Master Yi and the target is not dead
            if blink[i].name == "FioraDance" or blink[i].name == "AlphaStrike" and not blink[i].targetDead then
                if os.clock() > (blink[i].timeCasted + blink[i].delay + 0.2) then
                    blink[i].casted = false
                elseif blink[i].target.dead then
                    --Save startPos in a temp var
                    local tempPos = { x = blink[i].endPos.x, y = blink[i].endPos.y, z = blink[i].endPos.z }
                    --Set the end position to the start position
                    blink[i].endPos = { x = blink[i].startPos.x, y = blink[i].startPos.y, z = blink[i].startPos.z }
                    --Set the start position to the enemy position
                    blink[i].startPos = { x = tempPos.x, y = tempPos.y, z = tempPos.z }
                    --Set targetDead to true
                    blink[i].targetDead = true
                else
                    --Set the end position the the target unit
                    blink[i].endPos = { x = blink[i].target.x, y = blink[i].target.y, z = blink[i].target.z }
                end
            
            --If the champ is dead or display time is over stop the drawing
            elseif blink[i].castingHero.dead or (not blink[i].castingHero.visible and os.clock() > (blink[i].timeCasted + WDHGConfig.displayTime + blink[i].delay)) or (blink[i].castingHero.visible and os.clock() > (blink[i].timeCasted + WDHGConfig.displayTimeVisible + blink[i].delay)) then
                blink[i].casted = false
                
            --If the enemy is visible after the delay set the target to his position
            elseif not blink[i].outOfBush and blink[i].castingHero.visible and os.clock() > blink[i].timeCasted + blink[i].delay then
                --Set the end position the the current position of the enemy
                blink[i].endPos = { x = blink[i].castingHero.x, y = blink[i].castingHero.y, z = blink[i].castingHero.z }
                
            end
        end
    end
end

function WDHGOnDraw()
    --For each ability in the array
    for i=1, #blink, 1 do
        
        --If the ability is casted
        if blink[i].casted then
            
            --Convert 3D-coordinates to 2D-coordinates for DrawLine and InfoText
            local lineStartPos = WorldToScreen(D3DXVECTOR3(blink[i].startPos.x, blink[i].startPos.y, blink[i].startPos.z))
            local lineEndPos = WorldToScreen(D3DXVECTOR3(blink[i].endPos.x, blink[i].endPos.y, blink[i].endPos.z))            
            
            --If the ability is shacos Q out of a bush draw only a circle
            if blink[i].outOfBush then
                --Draw a circle showing the possible target
                for j=0, 3, 1 do
                    DrawCircle(blink[i].endPos.x , blink[i].endPos.y , blink[i].endPos.z, blink[i].maxRange+j*2, ARGB(255, 255, 25, 0))
                end
            
            --Else draw the normal circle with a line
            else
                --Draw Circle at target position
                for j=0, 3, 1 do
                    DrawCircle(blink[i].endPos.x , blink[i].endPos.y , blink[i].endPos.z , WDHGConfig.circleSize+j*2, RGB(WDHGConfig.circleColor[2],WDHGConfig.circleColor[3],WDHGConfig.circleColor[4]))
                end
                                
                --Draw Line beetween the start and target position
                DrawLine(lineStartPos.x, lineStartPos.y, lineEndPos.x, lineEndPos.y, WDHGConfig.lineWidth, RGB(WDHGConfig.lineColor[2],WDHGConfig.lineColor[3],WDHGConfig.lineColor[4]))
            end
            
            --Draw the info text (Credits to Weee :3)
            local offset = 30
            local infoText = blink[i].castingHero.charName .. " " .. blink[i].shortName
            DrawLine(lineEndPos.x, lineEndPos.y, lineEndPos.x + offset, lineEndPos.y - offset, 1, ARGB(255,255,255,255))
            DrawLine(lineEndPos.x + offset, lineEndPos.y - offset, lineEndPos.x + offset + 6 * infoText:len(), lineEndPos.y - offset, 1, ARGB(255,255,255,255))
            DrawTextA(infoText, 12, lineEndPos.x + offset + 1, lineEndPos.y - offset, ARGB(255,255,255,255), "left", "bottom")

        end
    end
end
-- ############################################ Where did he go? #########################################
-- ############################################ AUTOSHIELD ###############################################
--[[
	Auto Shield 1.82
        by eXtragoZ
		LoL 3.12 Jinx fix by Kain
    Features:
        - Supports:
            - Shields: Lulu, Janna, Karma, LeeSin, Orianna, Lux, Thresh, JarvanIV, Nautilus, Rumble, Sion, Shen, Skarner, Urgot, Diana, Riven, Morgana, Sivir and Nocturne
            - Items: Locket of the Iron Solari, Seraph's Embrace
            - Summoner Spells: Heal, Barrier
            - Heals: Alistar E, Kayle W, Nami W, Nidalee E, Sona W, Soraka W, Taric Q ,Gangplank W
            - Ultimates: Zilean R, Tryndamere R, Kayle R, Shen R, Lulu R, Soraka R
        - If the skill has immediate damage, the script does not reached to activate the shield/heal/invulnerability
        - In the case that the enemy ability hits multiple allies the script looks for the highest percentage of damage unless it is a skillshot that only hits the first target in that case looks for the closest one from the enemy
        - Press shift to configure  
]]
local typeshield
local spellslot
local typeheal
local healslot
local typeult
local ultslot
if myHero.charName == "Lulu" then
    typeshield = 1
    spellslot = _E
    typeult = 1
    ultslot = _R
elseif myHero.charName == "Janna" then
    typeshield = 1
    spellslot = _E
elseif myHero.charName == "Karma" then
    typeshield = 1
    spellslot = _E
elseif myHero.charName == "LeeSin" then
    typeshield = 1
    spellslot = _W
elseif myHero.charName == "Orianna" then
    typeshield = 1
    spellslot = _E
elseif myHero.charName == "Lux" then
    typeshield = 2
    spellslot = _W
elseif myHero.charName == "Thresh" then
    typeshield = 2
    spellslot = _W
elseif myHero.charName == "JarvanIV" then
    typeshield = 3
    spellslot = _W
elseif myHero.charName == "Nautilus" then
    typeshield = 3
    spellslot = _W
elseif myHero.charName == "Rumble" then
    typeshield = 3
    spellslot = _W
elseif myHero.charName == "Sion" then
    typeshield = 3
    spellslot = _W
elseif myHero.charName == "Shen" then
    typeshield = 3
    spellslot = _W
    typeult = 3
    ultslot = _R
elseif myHero.charName == "Skarner" then
    typeshield = 3
    spellslot = _W
elseif myHero.charName == "Urgot" then
    typeshield = 3
    spellslot = _W
elseif myHero.charName == "Diana" then
    typeshield = 3
    spellslot = _W
-- elseif myHero.charName == "Udyr" then
    -- typeshield = 3
    -- spellslot = _W
elseif myHero.charName == "Riven" then
    typeshield = 4
    spellslot = _E
elseif myHero.charName == "Morgana" then
    typeshield = 5
    spellslot = _E
elseif myHero.charName == "Sivir" then
    typeshield = 6
    spellslot = _E
elseif myHero.charName == "Nocturne" then
    typeshield = 6
    spellslot = _W
elseif myHero.charName == "Alistar" then
    typeheal = 2
    healslot = _E
elseif myHero.charName == "Kayle" then
    typeheal = 1
    healslot = _W
    typeult = 1
    ultslot = _R
elseif myHero.charName == "Nami" then
    typeheal = 1
    healslot = _W
elseif myHero.charName == "Nidalee" then
    typeheal = 1
    healslot = _E
elseif myHero.charName == "Sona" then
    typeheal = 2
    healslot = _W
elseif myHero.charName == "Soraka" then
    typeheal = 1
    healslot = _W
    typeult = 2
    ultslot = _R
elseif myHero.charName == "Taric" then
    typeheal = 1
    healslot = _Q
elseif myHero.charName == "Gangplank" then
    typeheal = 3
    healslot = _W
elseif myHero.charName == "Zilean" then
    typeult = 1
    ultslot = _R
elseif myHero.charName == "Tryndamere" then
    typeult = 4
    ultslot = _R
end

local range = 0
local healrange = 0
local ultrange = 0
local shealrange = 300
local lisrange = 700
local sbarrier = nil
local sheal = nil
local useitems = true
local spelltype = nil
local casttype = nil
local BShield,SShield,Shield,CC = false,false,false,false
local shottype,radius,maxdistance = 0,0,0
local hitchampion = false
--[[        Code        ]]

function AutoShieldOnLoad()
    if myHero:GetSpellData(SUMMONER_1).name:find("SummonerBarrier") then sbarrier = SUMMONER_1
    elseif myHero:GetSpellData(SUMMONER_2).name:find("SummonerBarrier") then sbarrier = SUMMONER_2 end
    if myHero:GetSpellData(SUMMONER_1).name:find("SummonerHeal") then sheal = SUMMONER_1
    elseif myHero:GetSpellData(SUMMONER_2).name:find("SummonerHeal") then sheal = SUMMONER_2 end
    if typeshield ~= nil then
        ASConfig = scriptConfig("(AS) 自动护盾", "AutoShield")
        for i=1, heroManager.iCount do
            local teammate = heroManager:GetHero(i)
            if teammate.team == myHero.team then ASConfig:addParam("teammateshield"..i, "加护盾给 "..teammate.charName, SCRIPT_PARAM_ONOFF, true) end
        end
        ASConfig:addParam("maxhppercent", "生命值百分比", SCRIPT_PARAM_SLICE, 100, 0, 100, 0)  
        ASConfig:addParam("mindmgpercent", "最小攻击百分比", SCRIPT_PARAM_SLICE, 20, 0, 100, 0)
        ASConfig:addParam("mindmg", "最小攻击近似值", SCRIPT_PARAM_INFO, 0)
        ASConfig:addParam("skillshots", "自动给被集火目标加护盾", SCRIPT_PARAM_ONOFF, true)
        ASConfig:addParam("shieldcc", "自动给肉盾加护盾", SCRIPT_PARAM_ONOFF, true)
        ASConfig:addParam("shieldslow", "自动给减速者加护盾", SCRIPT_PARAM_ONOFF, true)
        ASConfig:addParam("drawcircles", "绘制范围", SCRIPT_PARAM_ONOFF, true)
        ASConfig:permaShow("mindmg")
    end
    if typeheal ~= nil then
        AHConfig = scriptConfig("(AS) 自动治疗", "AutoHeal")
        for i=1, heroManager.iCount do
            local teammate = heroManager:GetHero(i)
            if teammate.team == myHero.team then AHConfig:addParam("teammateheal"..i, "治疗 "..teammate.charName, SCRIPT_PARAM_ONOFF, true) end
        end
        AHConfig:addParam("maxhppercent", "生命值百分比", SCRIPT_PARAM_SLICE, 100, 0, 100, 0)  
        AHConfig:addParam("mindmgpercent", "最小攻击百分比", SCRIPT_PARAM_SLICE, 35, 0, 100, 0)
        AHConfig:addParam("mindmg", "最小攻击近似值", SCRIPT_PARAM_INFO, 0)
        AHConfig:addParam("skillshots", "治疗被集火目标", SCRIPT_PARAM_ONOFF, true)
        AHConfig:addParam("drawcircles", "绘制范围", SCRIPT_PARAM_ONOFF, true)
        AHConfig:permaShow("mindmg")
    end
    if typeult ~= nil then
        AUConfig = scriptConfig("(AS) 自动大招", "AutoUlt")
        for i=1, heroManager.iCount do
            local teammate = heroManager:GetHero(i)
            if teammate.team == myHero.team then AUConfig:addParam("teammateult"..i, "大招给 "..teammate.charName, SCRIPT_PARAM_ONOFF, false) end
        end
        AUConfig:addParam("maxhppercent", "生命值百分比", SCRIPT_PARAM_SLICE, 100, 0, 100, 0)  
        AUConfig:addParam("mindmgpercent", "最小攻击百分比", SCRIPT_PARAM_SLICE, 100, 0, 100, 0)
        AUConfig:addParam("mindmg", "最小攻击近似值", SCRIPT_PARAM_INFO, 0)
        AUConfig:addParam("skillshots", "大招给被集火目标", SCRIPT_PARAM_ONOFF, true)
        AUConfig:addParam("drawcircles", "绘制范围", SCRIPT_PARAM_ONOFF, true)
        AUConfig:permaShow("mindmg")
    end
    if sbarrier ~= nil then
        ASBConfig = scriptConfig("(AS) 自动召唤师技能:屏障", "AutoSummonerBarrier")
        ASBConfig:addParam("barrieron", "屏障", SCRIPT_PARAM_ONOFF, false)
        ASBConfig:addParam("maxhppercent", "生命值百分比", SCRIPT_PARAM_SLICE, 100, 0, 100, 0)
        ASBConfig:addParam("mindmgpercent", "最小攻击百分比", SCRIPT_PARAM_SLICE, 95, 0, 100, 0)
        ASBConfig:addParam("mindmg", "最小攻击近似值", SCRIPT_PARAM_INFO, 0)
        ASBConfig:addParam("skillshots", "被集火时释放屏障", SCRIPT_PARAM_ONOFF, true)
    end
    if sheal ~= nil then
        ASHConfig = scriptConfig("(AS) 自动召唤师技能:治疗", "AutoSummonerHeal")
        for i=1, heroManager.iCount do
            local teammate = heroManager:GetHero(i)
            if teammate.team == myHero.team then ASHConfig:addParam("teammatesheal"..i, "治疗 "..teammate.charName, SCRIPT_PARAM_ONOFF, false) end
        end
        ASHConfig:addParam("maxhppercent", "生命值百分比", SCRIPT_PARAM_SLICE, 100, 0, 100, 0)
        ASHConfig:addParam("mindmgpercent", "最小攻击百分比", SCRIPT_PARAM_SLICE, 95, 0, 100, 0)
        ASHConfig:addParam("mindmg", "最小攻击近似值", SCRIPT_PARAM_INFO, 0)
        ASHConfig:addParam("skillshots", "治疗被集火者", SCRIPT_PARAM_ONOFF, true)
    end
    if useitems then
        ASIConfig = scriptConfig("(AS) 自动使用护盾道具", "AutoShieldItems")
        for i=1, heroManager.iCount do
            local teammate = heroManager:GetHero(i)
            if teammate.team == myHero.team then ASIConfig:addParam("teammateshieldi"..i, "加护盾(道具)给 "..teammate.charName, SCRIPT_PARAM_ONOFF, false) end
        end
        ASIConfig:addParam("maxhppercent", "生命值百分比", SCRIPT_PARAM_SLICE, 100, 0, 100, 0)
        ASIConfig:addParam("mindmgpercent", "最小攻击百分比", SCRIPT_PARAM_SLICE, 50, 0, 100, 0)
        ASIConfig:addParam("mindmg", "最小攻击近似值", SCRIPT_PARAM_INFO, 0)
        ASIConfig:addParam("skillshots", "自动给被集火目标加护盾", SCRIPT_PARAM_ONOFF, true)
    end
    --PrintChat(" >> Auto Shield 1.8 loaded!")
end

function AutoShieldOnProcessSpell(object,spell)
    if object.team ~= myHero.team and not myHero.dead and not (object.name:find("Minion_") or object.name:find("Odin")) then
		if object.charName and object.charName == "Jinx" then return end
        if typeshield ~= nil then
            if myHero.charName == "Lux" then range = 1075
            else range = myHero:GetSpellData(spellslot).range end
        end
        if typeheal ~= nil then healrange = myHero:GetSpellData(healslot).range end
        if typeult ~= nil then ultrange = myHero:GetSpellData(ultslot).range end
        local leesinW = myHero.charName ~= "LeeSin" or myHero:GetSpellData(_W).name == "BlindMonkWOne"
        local nidaleeE = myHero.charName ~= "Nidalee" or myHero:GetSpellData(_E).name == "PrimalSurge"
        local shieldREADY = typeshield ~= nil and myHero:CanUseSpell(spellslot) == READY and leesinW
        local healREADY = typeheal ~= nil and myHero:CanUseSpell(healslot) == READY and nidaleeE
        local ultREADY = typeult ~= nil and myHero:CanUseSpell(ultslot) == READY
        local sbarrierREADY = sbarrier ~= nil and myHero:CanUseSpell(sbarrier) == READY
        local shealREADY = sheal ~= nil and myHero:CanUseSpell(sheal) == READY
        local lisslot = GetInventorySlotItem(3190)
        local seslot = GetInventorySlotItem(3040)
        local lisREADY = lisslot ~= nil and myHero:CanUseSpell(lisslot) == READY
        local seREADY = seslot ~= nil and myHero:CanUseSpell(seslot) == READY
        local HitFirst = false
        local shieldtarget,SLastDistance,SLastDmgPercent = nil,nil,nil
        local healtarget,HLastDistance,HLastDmgPercent = nil,nil,nil
        local ulttarget,ULastDistance,ULastDmgPercent = nil,nil,nil
        BShield,SShield,Shield,CC = false,false,false,false
        shottype,radius,maxdistance = 0,0,0
        if object.type == "obj_AI_Hero" then
            spelltype, casttype = getSpellType(object, spell.name)
            if casttype == 4 or casttype == 5 then return end
            if spelltype == "BAttack" or spelltype == "CAttack" or spell.name:find("SummonerDot") then
                Shield = true
            elseif spelltype == "Q" or spelltype == "W" or spelltype == "E" or spelltype == "R" or spelltype == "P" or spelltype == "QM" or spelltype == "WM" or spelltype == "EM" then
				if skillShield and spelltype and object and object.charName and not object.dead then
                HitFirst = skillShield[object.charName][spelltype]["HitFirst"]
                BShield = skillShield[object.charName][spelltype]["BShield"]
                SShield = skillShield[object.charName][spelltype]["SShield"]
                Shield = skillShield[object.charName][spelltype]["Shield"]
                CC = skillShield[object.charName][spelltype]["CC"]
                shottype = skillData[object.charName][spelltype]["type"]
                radius = skillData[object.charName][spelltype]["radius"]
                maxdistance = skillData[object.charName][spelltype]["maxdistance"]
				end
            end
        else
            Shield = true
        end
        for i=1, heroManager.iCount do
            local allytarget = heroManager:GetHero(i)
            if allytarget.team == myHero.team and not allytarget.dead and allytarget.health > 0 then
                hitchampion = false
                local allyHitBox = getHitBox(allytarget)
                if shottype == 0 then hitchampion = spell.target and spell.target.networkID == allytarget.networkID
                elseif shottype == 1 then hitchampion = checkhitlinepass(object, spell.endPos, radius, maxdistance, allytarget, allyHitBox)
                elseif shottype == 2 then hitchampion = checkhitlinepoint(object, spell.endPos, radius, maxdistance, allytarget, allyHitBox)
                elseif shottype == 3 then hitchampion = checkhitaoe(object, spell.endPos, radius, maxdistance, allytarget, allyHitBox)
                elseif shottype == 4 then hitchampion = checkhitcone(object, spell.endPos, radius, maxdistance, allytarget, allyHitBox)
                elseif shottype == 5 then hitchampion = checkhitwall(object, spell.endPos, radius, maxdistance, allytarget, allyHitBox)
                elseif shottype == 6 then hitchampion = checkhitlinepass(object, spell.endPos, radius, maxdistance, allytarget, allyHitBox) or checkhitlinepass(object, Vector(object)*2-spell.endPos, radius, maxdistance, allytarget, allyHitBox)
                elseif shottype == 7 then hitchampion = checkhitcone(spell.endPos, object, radius, maxdistance, allytarget, allyHitBox)
                end
                if hitchampion then
                    if shieldREADY and ASConfig["teammateshield"..i] and ((typeshield<=4 and Shield) or (typeshield==5 and BShield) or (typeshield==6 and SShield)) then
                        if (((typeshield==1 or typeshield==2 or typeshield==5) and GetDistance(allytarget)<=range) or allytarget.isMe) then
                            local shieldflag, dmgpercent = shieldCheck(object,spell,allytarget,"shields")
                            if shieldflag then
                                if HitFirst and (SLastDistance == nil or GetDistance(allytarget,object) <= SLastDistance) then
                                    shieldtarget,SLastDistance = allytarget,GetDistance(allytarget,object)
                                elseif not HitFirst and (SLastDmgPercent == nil or dmgpercent >= SLastDmgPercent) then
                                    shieldtarget,SLastDmgPercent = allytarget,dmgpercent
                                end
                            end
                        end
                    end
                    if healREADY and AHConfig["teammateheal"..i] and Shield then
                        if ((typeheal==1 or typeheal==2) and GetDistance(allytarget)<=healrange) or allytarget.isMe then
                            local healflag, dmgpercent = shieldCheck(object,spell,allytarget,"heals")
                            if healflag then
                                if HitFirst and (HLastDistance == nil or GetDistance(allytarget,object) <= HLastDistance) then
                                    healtarget,HLastDistance = allytarget,GetDistance(allytarget,object)
                                elseif not HitFirst and (HLastDmgPercent == nil or dmgpercent >= HLastDmgPercent) then
                                    healtarget,HLastDmgPercent = allytarget,dmgpercent
                                end
                            end     
                        end
                    end
                    if ultREADY and AUConfig["teammateult"..i] and Shield then
                        if typeult==2 or (typeult==1 and GetDistance(allytarget)<=ultrange) or (typeult==4 and allytarget.isMe) or (typeult==3 and not allytarget.isMe) then
                            local ultflag, dmgpercent = shieldCheck(object,spell,allytarget,"ult")
                            if ultflag then
                                if HitFirst and (ULastDistance == nil or GetDistance(allytarget,object) <= ULastDistance) then
                                    ulttarget,ULastDistance = allytarget,GetDistance(allytarget,object)
                                elseif not HitFirst and (ULastDmgPercent == nil or dmgpercent >= ULastDmgPercent) then
                                    ulttarget,ULastDmgPercent = allytarget,dmgpercent
                                end
                            end
                        end
                    end
                    if sbarrierREADY and ASBConfig.barrieron and allytarget.isMe and Shield then
                        local barrierflag, dmgpercent = shieldCheck(object,spell,allytarget,"barrier")
                        if barrierflag then
                            CastSpell(sbarrier)
                        end
                    end
                    if shealREADY and ASHConfig["teammatesheal"..i] and Shield then
                        if GetDistance(allytarget)<=shealrange then
                            local shealflag, dmgpercent = shieldCheck(object,spell,allytarget,"sheals")
                            if shealflag then
                                CastSpell(sheal)
                            end
                        end
                    end
                    if lisREADY and ASIConfig["teammateshieldi"..i] and Shield then
                        if GetDistance(allytarget)<=lisrange then
                            local lisflag, dmgpercent = shieldCheck(object,spell,allytarget,"items")
                            if lisflag then
                                CastSpell(lisslot)
                            end
                        end
                    end
                    if seREADY and ASIConfig["teammateshieldi"..i] and allytarget.isMe and Shield then
                        local seflag, dmgpercent = shieldCheck(object,spell,allytarget,"items")
                        if seflag then
                            CastSpell(seslot)
                        end
                    end
                end
            end
        end
        if shieldtarget ~= nil then
            if typeshield==1 or typeshield==5 then CastSpell(spellslot,shieldtarget)
            elseif typeshield==2 or typeshield==4 then CastSpell(spellslot,shieldtarget.x,shieldtarget.z)
            elseif typeshield==3 or typeshield==6 then CastSpell(spellslot) end
        end
        if healtarget ~= nil then
            if typeheal==1 then CastSpell(healslot,healtarget)
            elseif typeheal==2 or typeheal==3 then CastSpell(healslot) end
        end
        if ulttarget ~= nil then
            if typeult==1 or typeult==3 then CastSpell(ultslot,ulttarget)
            elseif typeult==2 or typeult==4 then CastSpell(ultslot) end     
        end
    end 
end

function shieldCheck(object,spell,target,typeused)
    local configused
    if typeused == "shields" then configused = ASConfig
    elseif typeused == "heals" then configused = AHConfig
    elseif typeused == "ult" then configused = AUConfig
    elseif typeused == "barrier" then configused = ASBConfig 
    elseif typeused == "sheals" then configused = ASHConfig
    elseif typeused == "items" then configused = ASIConfig end
    local shieldflag = false
    if (not configused.skillshots and shottype ~= 0) then return false, 0 end
    local adamage = object:CalcDamage(target,object.totalDamage)
    local InfinityEdge,onhitdmg,onhittdmg,onhitspelldmg,onhitspelltdmg,muramanadmg,skilldamage,skillTypeDmg = 0,0,0,0,0,0,0,0

    if object.type ~= "obj_AI_Hero" then
        if spell.name:find("BasicAttack") then skilldamage = adamage
        elseif spell.name:find("CritAttack") then skilldamage = adamage*2 end
    else
        if GetInventoryHaveItem(3186,object) then onhitdmg = getDmg("KITAES",target,object) end
        if GetInventoryHaveItem(3114,object) then onhitdmg = onhitdmg+getDmg("MALADY",target,object) end
        if GetInventoryHaveItem(3091,object) then onhitdmg = onhitdmg+getDmg("WITSEND",target,object) end
        if GetInventoryHaveItem(3057,object) then onhitdmg = onhitdmg+getDmg("SHEEN",target,object) end
        if GetInventoryHaveItem(3078,object) then onhitdmg = onhitdmg+getDmg("TRINITY",target,object) end
        if GetInventoryHaveItem(3100,object) then onhitdmg = onhitdmg+getDmg("LICHBANE",target,object) end
        if GetInventoryHaveItem(3025,object) then onhitdmg = onhitdmg+getDmg("ICEBORN",target,object) end
        if GetInventoryHaveItem(3087,object) then onhitdmg = onhitdmg+getDmg("STATIKK",target,object) end
        if GetInventoryHaveItem(3153,object) then onhitdmg = onhitdmg+getDmg("RUINEDKING",target,object) end
        if GetInventoryHaveItem(3209,object) then onhittdmg = getDmg("SPIRITLIZARD",target,object) end
        if GetInventoryHaveItem(3184,object) then onhittdmg = onhittdmg+80 end
        if GetInventoryHaveItem(3042,object) then muramanadmg = getDmg("MURAMANA",target,object) end
        if spelltype == "BAttack" then
            skilldamage = (adamage+onhitdmg+muramanadmg)*1.07+onhittdmg
        elseif spelltype == "CAttack" then
            if GetInventoryHaveItem(3031,object) then InfinityEdge = .5 end
            skilldamage = (adamage*(2.1+InfinityEdge)+onhitdmg+muramanadmg)*1.07+onhittdmg --fix Lethality
        elseif spelltype == "Q" or spelltype == "W" or spelltype == "E" or spelltype == "R" or spelltype == "P" or spelltype == "QM" or spelltype == "WM" or spelltype == "EM" then
            if GetInventoryHaveItem(3151,object) then onhitspelldmg = getDmg("LIANDRYS",target,object) end
            if GetInventoryHaveItem(3188,object) then onhitspelldmg = getDmg("BLACKFIRE",target,object) end
            if GetInventoryHaveItem(3209,object) then onhitspelltdmg = getDmg("SPIRITLIZARD",target,object) end
            muramanadmg = skillShield[object.charName][spelltype]["Muramana"] and muramanadmg or 0
            if casttype == 1 then
                skilldamage, skillTypeDmg = getDmg(spelltype,target,object,1,spell.level)
            elseif casttype == 2 then
                skilldamage, skillTypeDmg = getDmg(spelltype,target,object,2,spell.level)
            elseif casttype == 3 then
                skilldamage, skillTypeDmg = getDmg(spelltype,target,object,3,spell.level)
            end
            if skillTypeDmg == 2 then
                skilldamage = (skilldamage+adamage+onhitspelldmg+onhitdmg+muramanadmg)*1.07+onhittdmg+onhitspelltdmg
            else
                if skilldamage > 0 then skilldamage = (skilldamage+onhitspelldmg+muramanadmg)*1.07+onhitspelltdmg end
            end
        elseif spell.name:find("SummonerDot") then
            skilldamage = getDmg("IGNITE",target,object)
        end
    end
    local dmgpercent = skilldamage*100/target.health
    local dmgneeded = dmgpercent >= configused.mindmgpercent
    local hpneeded = configused.maxhppercent >= (target.health-skilldamage)*100/target.maxHealth
    
    if dmgneeded and hpneeded then
        shieldflag = true
    elseif typeused == "shields" and ((CC == 2 and configused.shieldcc) or (CC == 1 and configused.shieldslow)) then
        shieldflag = true
    end
    return shieldflag, dmgpercent
end
function getHitBox(hero)
    local hitboxTable = { ['HeimerTGreen'] = 50.0, ['Darius'] = 80.0, ['ZyraGraspingPlant'] = 20.0, ['HeimerTRed'] = 50.0, ['ZyraThornPlant'] = 20.0, ['Nasus'] = 80.0, ['HeimerTBlue'] = 50.0, ['SightWard'] = 1, ['HeimerTYellow'] = 50.0, ['Kennen'] = 55.0, ['VisionWard'] = 1, ['ShacoBox'] = 10, ['HA_AP_Poro'] = 0, ['TempMovableChar'] = 48.0, ['TeemoMushroom'] = 50.0, ['OlafAxe'] = 50.0, ['OdinCenterRelic'] = 48.0, ['Blue_Minion_Healer'] = 48.0, ['AncientGolem'] = 100.0, ['AnnieTibbers'] = 80.0, ['OdinMinionGraveyardPortal'] = 1.0, ['OriannaBall'] = 48.0, ['LizardElder'] = 65.0, ['YoungLizard'] = 50.0, ['OdinMinionSpawnPortal'] = 1.0, ['MaokaiSproutling'] = 48.0, ['FizzShark'] = 0, ['Sejuani'] = 80.0, ['Sion'] = 80.0, ['OdinQuestIndicator'] = 1.0, ['Zac'] = 80.0, ['Red_Minion_Wizard'] = 48.0, ['DrMundo'] = 80.0, ['Blue_Minion_Wizard'] = 48.0, ['ShyvanaDragon'] = 80.0, ['HA_AP_OrderShrineTurret'] = 88.4, ['Heimerdinger'] = 55.0, ['Rumble'] = 80.0, ['Ziggs'] = 55.0, ['HA_AP_OrderTurret3'] = 88.4, ['HA_AP_OrderTurret2'] = 88.4, ['TT_Relic'] = 0, ['Veigar'] = 55.0, ['HA_AP_HealthRelic'] = 0, ['Teemo'] = 55.0, ['Amumu'] = 55.0, ['HA_AP_ChaosTurretShrine'] = 88.4, ['HA_AP_ChaosTurret'] = 88.4, ['HA_AP_ChaosTurretRubble'] = 88.4, ['Poppy'] = 55.0, ['Tristana'] = 55.0, ['HA_AP_PoroSpawner'] = 50.0, ['TT_NGolem'] = 80.0, ['HA_AP_ChaosTurretTutorial'] = 88.4, ['Volibear'] = 80.0, ['HA_AP_OrderTurretTutorial'] = 88.4, ['TT_NGolem2'] = 80.0, ['HA_AP_ChaosTurret3'] = 88.4, ['HA_AP_ChaosTurret2'] = 88.4, ['Shyvana'] = 50.0, ['HA_AP_OrderTurret'] = 88.4, ['Nautilus'] = 80.0, ['ARAMOrderTurretNexus'] = 88.4, ['TT_ChaosTurret2'] = 88.4, ['TT_ChaosTurret3'] = 88.4, ['TT_ChaosTurret1'] = 88.4, ['ChaosTurretGiant'] = 88.4, ['ARAMOrderTurretFront'] = 88.4, ['ChaosTurretWorm'] = 88.4, ['OdinChaosTurretShrine'] = 88.4, ['ChaosTurretNormal'] = 88.4, ['OrderTurretNormal2'] = 88.4, ['OdinOrderTurretShrine'] = 88.4, ['OrderTurretDragon'] = 88.4, ['OrderTurretNormal'] = 88.4, ['ARAMChaosTurretFront'] = 88.4, ['ARAMOrderTurretInhib'] = 88.4, ['ChaosTurretWorm2'] = 88.4, ['TT_OrderTurret1'] = 88.4, ['TT_OrderTurret2'] = 88.4, ['ARAMChaosTurretInhib'] = 88.4, ['TT_OrderTurret3'] = 88.4, ['ARAMChaosTurretNexus'] = 88.4, ['OrderTurretAngel'] = 88.4, ['Mordekaiser'] = 80.0, ['TT_Buffplat_R'] = 0, ['Lizard'] = 50.0, ['GolemOdin'] = 80.0, ['Renekton'] = 80.0, ['Maokai'] = 80.0, ['LuluLadybug'] = 50.0, ['Alistar'] = 80.0, ['Urgot'] = 80.0, ['LuluCupcake'] = 50.0, ['Gragas'] = 80.0, ['Skarner'] = 80.0, ['Yorick'] = 80.0, ['MalzaharVoidling'] = 10.0, ['LuluPig'] = 50.0, ['Blitzcrank'] = 80.0, ['Chogath'] = 80.0, ['Vi'] = 50, ['FizzBait'] = 0, ['Malphite'] = 80.0, ['EliseSpiderling'] = 1.0, ['Dragon'] = 100.0, ['LuluSquill'] = 50.0, ['Worm'] = 100.0, ['redDragon'] = 100.0, ['LuluKitty'] = 50.0, ['Galio'] = 80.0, ['Annie'] = 55.0, ['EliseSpider'] = 50.0, ['SyndraSphere'] = 48.0, ['LuluDragon'] = 50.0, ['Hecarim'] = 80.0, ['TT_Spiderboss'] = 200.0, ['Thresh'] = 55.0, ['ARAMChaosTurretShrine'] = 88.4, ['ARAMOrderTurretShrine'] = 88.4, ['Blue_Minion_MechMelee'] = 65.0, ['TT_NWolf'] = 65.0, ['Tutorial_Red_Minion_Wizard'] = 48.0, ['YorickRavenousGhoul'] = 1.0, ['SmallGolem'] = 80.0, ['OdinRedSuperminion'] = 55.0, ['Wraith'] = 50.0, ['Red_Minion_MechCannon'] = 65.0, ['Red_Minion_Melee'] = 48.0, ['OdinBlueSuperminion'] = 55.0, ['TT_NWolf2'] = 50.0, ['Tutorial_Red_Minion_Basic'] = 48.0, ['YorickSpectralGhoul'] = 1.0, ['Wolf'] = 50.0, ['Blue_Minion_MechCannon'] = 65.0, ['Golem'] = 80.0, ['Blue_Minion_Basic'] = 48.0, ['Blue_Minion_Melee'] = 48.0, ['Odin_Blue_Minion_caster'] = 48.0, ['TT_NWraith2'] = 50.0, ['Tutorial_Blue_Minion_Wizard'] = 48.0, ['GiantWolf'] = 65.0, ['Odin_Red_Minion_Caster'] = 48.0, ['Red_Minion_MechMelee'] = 65.0, ['LesserWraith'] = 50.0, ['Red_Minion_Basic'] = 48.0, ['Tutorial_Blue_Minion_Basic'] = 48.0, ['GhostWard'] = 1, ['TT_NWraith'] = 50.0, ['Red_Minion_MechRange'] = 65.0, ['YorickDecayedGhoul'] = 1.0, ['TT_Buffplat_L'] = 0, ['TT_ChaosTurret4'] = 88.4, ['TT_Buffplat_Chain'] = 0, ['TT_OrderTurret4'] = 88.4, ['OrderTurretShrine'] = 88.4, ['ChaosTurretShrine'] = 88.4, ['WriggleLantern'] = 1, ['ChaosTurretTutorial'] = 88.4, ['TwistedLizardElder'] = 65.0, ['RabidWolf'] = 65.0, ['OrderTurretTutorial'] = 88.4, ['OdinShieldRelic'] = 0, ['TwistedGolem'] = 80.0, ['TwistedSmallWolf'] = 50.0, ['TwistedGiantWolf'] = 65.0, ['TwistedTinyWraith'] = 50.0, ['TwistedBlueWraith'] = 50.0, ['TwistedYoungLizard'] = 50.0, ['Summoner_Rider_Order'] = 65.0, ['Summoner_Rider_Chaos'] = 65.0, ['Ghast'] = 60.0, ['blueDragon'] = 100.0, }
    return (hitboxTable[hero.charName] ~= nil and hitboxTable[hero.charName] ~= 0) and hitboxTable[hero.charName] or 65
end
function AutoShieldOnDraw()
    if typeshield ~= nil then
        if ASConfig.drawcircles and not myHero.dead and (typeshield == 1 or typeshield == 2 or typeshield == 5) then
            DrawCircle(myHero.x, myHero.y, myHero.z, range, 0x19A712)
        end
        ASConfig.mindmg = math.floor(myHero.health*ASConfig.mindmgpercent/100)
    end
    if typeheal ~= nil then
        if AHConfig.drawcircles and not myHero.dead and (typeheal == 1 or typeheal == 2) then
            DrawCircle(myHero.x, myHero.y, myHero.z, healrange, 0x19A712)
        end
        AHConfig.mindmg = math.floor(myHero.health*AHConfig.mindmgpercent/100)
    end
    if typeult ~= nil then
        if AUConfig.drawcircles and not myHero.dead and typeult == 1 then
            DrawCircle(myHero.x, myHero.y, myHero.z, ultrange, 0x19A712)
        end
        AUConfig.mindmg = math.floor(myHero.health*AUConfig.mindmgpercent/100)
    end
    if sbarrier ~= nil then ASBConfig.mindmg = math.floor(myHero.health*ASBConfig.mindmgpercent/100) end
    if sheal ~= nil then ASHConfig.mindmg = math.floor(myHero.health*ASHConfig.mindmgpercent/100) end
    if useitems then ASIConfig.mindmg = math.floor(myHero.health*ASIConfig.mindmgpercent/100) end
end
-- ############################################ AUTOSHIELD ###############################################

-- ############################################# AutoLevel ##############################################

--[[ 		Globals		]]
local abilitySequence
local qOff, wOff, eOff, rOff = 0,0,0,0

--[[ 		Functions	]]
function AutoLevelOnTick()
    local qL, wL, eL, rL = player:GetSpellData(_Q).level + qOff, player:GetSpellData(_W).level + wOff, player:GetSpellData(_E).level + eOff, player:GetSpellData(_R).level + rOff
    if qL + wL + eL + rL < player.level then
        local spellSlot = { SPELL_1, SPELL_2, SPELL_3, SPELL_4, }
        local level = { 0, 0, 0, 0 }
        for i = 1, player.level, 1 do
            level[abilitySequence[i]] = level[abilitySequence[i]] + 1
        end
        for i, v in ipairs({ qL, wL, eL, rL }) do
            if v < level[i] then LevelSpell(spellSlot[i]) end
        end
    end
end

function AutoLevelOnLoad()
    local champ = player.charName
    --[[
     In this section you can adjust the ability sequence of champions.

     To turn off the script for a particular champion,
     you have to comment out this line with two dashes.
     ]]
    if champ == "Aatrox" then           abilitySequence = { 1, 2, 3, 2, 2, 4, 2, 3, 2, 3, 4, 3, 3, 1, 1, 4, 1, 1, }
    elseif champ == "Jinx" then         abilitySequence = { 1, 2, 3, 1, 1, 4, 1, 2, 1, 2, 4, 2, 2, 3, 3, 4, 2, 2, }
    elseif champ == "Ahri" then         abilitySequence = { 1, 3, 1, 2, 1, 4, 1, 2, 1, 2, 4, 2, 2, 3, 3, 4, 2, 2, }
    elseif champ == "Akali" then        abilitySequence = { 1, 2, 1, 3, 1, 4, 1, 3, 1, 3, 4, 3, 3, 2, 2, 4, 2, 2, }
    elseif champ == "Alistar" then      abilitySequence = { 1, 3, 2, 1, 3, 4, 1, 3, 1, 3, 4, 1, 3, 2, 2, 4, 2, 2, }
    elseif champ == "Amumu" then        abilitySequence = { 2, 3, 3, 1, 3, 4, 3, 1, 3, 1, 4, 1, 1, 2, 2, 4, 2, 2, }
    elseif champ == "Anivia" then       abilitySequence = { 1, 3, 1, 3, 3, 4, 3, 2, 3, 2, 4, 1, 1, 1, 2, 4, 2, 2, }
    elseif champ == "Annie" then        abilitySequence = { 2, 1, 1, 3, 1, 4, 1, 2, 1, 2, 4, 2, 2, 3, 3, 4, 3, 3, }
    elseif champ == "Ashe" then         abilitySequence = { 2, 3, 2, 1, 2, 4, 2, 1, 2, 1, 4, 1, 1, 3, 3, 4, 3, 3, }
    elseif champ == "Blitzcrank" then   abilitySequence = { 1, 3, 2, 3, 2, 4, 3, 2, 3, 2, 4, 3, 2, 1, 1, 4, 1, 1, }
    elseif champ == "Brand" then        abilitySequence = { 2, 3, 2, 1, 2, 4, 2, 3, 2, 3, 4, 3, 3, 1, 1, 4, 1, 1, }
    elseif champ == "Caitlyn" then      abilitySequence = { 2, 1, 1, 3, 1, 4, 1, 3, 1, 3, 4, 3, 3, 2, 2, 4, 2, 2, }
    elseif champ == "Cassiopeia" then   abilitySequence = { 1, 3, 1, 2, 1, 4, 1, 3, 1, 3, 4, 3, 3, 2, 2, 4, 2, 2, }
    elseif champ == "Chogath" then      abilitySequence = { 1, 3, 2, 2, 2, 4, 2, 3, 2, 3, 4, 3, 3, 1, 1, 4, 1, 1, }
    elseif champ == "Corki" then        abilitySequence = { 1, 2, 1, 3, 1, 4, 1, 3, 1, 3, 4, 3, 2, 3, 2, 4, 2, 2, }
    elseif champ == "Darius" then       abilitySequence = { 1, 3, 1, 2, 1, 4, 1, 2, 1, 2, 4, 2, 3, 2, 3, 4, 3, 3, }
    elseif champ == "Diana" then        abilitySequence = { 2, 1, 2, 3, 1, 4, 1, 1, 1, 2, 4, 2, 2, 3, 3, 4, 3, 3, }
    elseif champ == "DrMundo" then      abilitySequence = { 2, 1, 3, 2, 2, 4, 2, 3, 2, 3, 4, 3, 3, 1, 1, 4, 1, 1, }
    elseif champ == "Draven" then       abilitySequence = { 1, 3, 2, 1, 1, 4, 1, 2, 1, 2, 4, 2, 2, 3, 3, 4, 3, 3, }
    elseif champ == "Elise" then        abilitySequence = { 2, 1, 3, 1, 1, 4, 1, 2, 1, 2, 4, 2, 2, 3, 3, 4, 3, 3, } rOff = -1
    elseif champ == "Evelynn" then      abilitySequence = { 1, 3, 1, 2, 1, 4, 1, 3, 1, 3, 4, 3, 3, 2, 2, 4, 2, 2, }
    elseif champ == "Ezreal" then       abilitySequence = { 1, 3, 2, 1, 1, 4, 1, 3, 1, 2, 4, 3, 2, 3, 2, 4, 3, 2, }
    elseif champ == "FiddleSticks" then abilitySequence = { 3, 2, 2, 1, 2, 4, 2, 1, 2, 1, 4, 1, 1, 3, 3, 4, 3, 3, }
    elseif champ == "Fiora" then        abilitySequence = { 2, 1, 3, 2, 2, 4, 2, 3, 2, 3, 4, 3, 3, 1, 1, 4, 1, 1, }
    elseif champ == "Fizz" then         abilitySequence = { 3, 1, 2, 1, 2, 4, 1, 1, 1, 2, 4, 2, 2, 3, 3, 4, 3, 3, }
    elseif champ == "Galio" then        abilitySequence = { 1, 2, 1, 3, 1, 4, 1, 2, 1, 2, 4, 3, 3, 2, 2, 4, 3, 3, }
    elseif champ == "Gangplank" then    abilitySequence = { 1, 2, 1, 3, 1, 4, 1, 2, 1, 2, 4, 2, 2, 3, 3, 4, 3, 3, }
    elseif champ == "Garen" then        abilitySequence = { 1, 2, 3, 3, 3, 4, 3, 1, 3, 1, 4, 1, 1, 2, 2, 4, 2, 2, }
    elseif champ == "Gragas" then       abilitySequence = { 1, 3, 2, 1, 1, 4, 1, 2, 1, 2, 4, 2, 3, 2, 3, 4, 3, 3, }
    elseif champ == "Graves" then       abilitySequence = { 1, 3, 1, 2, 1, 4, 1, 2, 1, 3, 4, 3, 3, 3, 2, 4, 2, 2, }
    elseif champ == "Hecarim" then      abilitySequence = { 1, 2, 1, 3, 1, 4, 1, 2, 1, 2, 4, 2, 2, 3, 3, 4, 3, 3, }
    elseif champ == "Heimerdinger" then abilitySequence = { 1, 2, 2, 1, 1, 4, 3, 2, 2, 2, 4, 1, 1, 3, 3, 4, 1, 1, }
    elseif champ == "Irelia" then       abilitySequence = { 3, 1, 2, 2, 2, 4, 2, 3, 2, 3, 4, 1, 1, 3, 1, 4, 3, 1, }
    elseif champ == "Janna" then        abilitySequence = { 3, 1, 3, 2, 3, 4, 3, 2, 3, 2, 1, 2, 2, 1, 1, 1, 4, 4, }
    elseif champ == "JarvanIV" then     abilitySequence = { 1, 3, 1, 2, 1, 4, 1, 3, 2, 1, 4, 3, 3, 3, 2, 4, 2, 2, }
    elseif champ == "Jax" then          abilitySequence = { 3, 2, 1, 2, 2, 4, 2, 3, 2, 3, 4, 1, 3, 1, 1, 4, 3, 1, }
    elseif champ == "Jayce" then        abilitySequence = { 1, 3, 1, 2, 1, 4, 1, 3, 1, 3, 4, 3, 3, 2, 2, 4, 2, 2, } rOff = -1
    elseif champ == "Karma" then        abilitySequence = { 1, 3, 1, 2, 3, 1, 3, 1, 3, 1, 3, 1, 3, 2, 2, 2, 2, 2, }
    elseif champ == "Karthus" then      abilitySequence = { 1, 3, 2, 1, 1, 4, 1, 1, 3, 3, 4, 3, 3, 2, 2, 4, 2, 2, }
    elseif champ == "Kassadin" then     abilitySequence = { 1, 2, 1, 3, 1, 4, 1, 3, 1, 3, 4, 3, 3, 2, 2, 4, 2, 2, }
    elseif champ == "Katarina" then     abilitySequence = { 1, 3, 2, 2, 2, 4, 2, 3, 2, 1, 4, 1, 1, 1, 3, 4, 3, 3, }
    elseif champ == "Kayle" then        abilitySequence = { 3, 2, 3, 1, 3, 4, 3, 2, 3, 2, 4, 2, 2, 1, 1, 4, 1, 1, }
    elseif champ == "Kennen" then       abilitySequence = { 1, 3, 2, 2, 2, 4, 2, 1, 2, 1, 4, 1, 1, 3, 3, 4, 3, 3, }
    elseif champ == "Khazix" then       abilitySequence = { 1, 3, 1, 2 ,1, 4, 1, 2, 1, 2, 4, 2, 2, 3, 3, 4, 3, 3, }
    elseif champ == "KogMaw" then       abilitySequence = { 2, 3, 2, 1, 2, 4, 2, 1, 2, 1, 4, 1, 1, 3, 3, 4, 3, 3, }
    elseif champ == "Leblanc" then      abilitySequence = { 1, 2, 3, 1, 1, 4, 1, 2, 1, 2, 4, 2, 3, 2, 3, 4, 3, 3, }
    elseif champ == "LeeSin" then       abilitySequence = { 3, 1, 2, 1, 1, 4, 1, 2, 1, 2, 4, 2, 2, 3, 3, 4, 3, 3, }
    elseif champ == "Leona" then        abilitySequence = { 1, 3, 2, 2, 2, 4, 2, 3, 2, 3, 4, 3, 3, 1, 1, 4, 1, 1, }
    elseif champ == "Lissandra" then    abilitySequence = { 1, 3, 1, 2, 1, 4, 1, 2, 1, 2, 4, 2, 2, 3, 3, 4, 3, 3, }
    elseif champ == "Lucian" then       abilitySequence = { 1, 3, 2, 1, 1, 4, 1, 2, 1, 2, 4, 2, 2, 3, 3, 4, 3, 3, }
    elseif champ == "Lulu" then         abilitySequence = { 3, 2, 1, 3, 3, 4, 3, 2, 3, 2, 4, 2, 2, 1, 1, 4, 1, 1, }
    elseif champ == "Lux" then          abilitySequence = { 3, 1, 3, 2, 3, 4, 3, 1, 3, 1, 4, 1, 1, 2, 2, 4, 2, 2, }
    elseif champ == "Malphite" then     abilitySequence = { 1, 3, 1, 2, 1, 4, 1, 3, 1, 3, 4, 3, 2, 3, 2, 4, 2, 2, }
    elseif champ == "Malzahar" then     abilitySequence = { 1, 3, 3, 2, 3, 4, 1, 3, 1, 3, 4, 2, 1, 2, 1, 4, 2, 2, }
    elseif champ == "Maokai" then       abilitySequence = { 3, 1, 2, 3, 3, 4, 3, 2, 3, 2, 4, 2, 2, 1, 1, 4, 1, 1, }
    elseif champ == "MasterYi" then     abilitySequence = { 3, 1, 3, 1, 3, 4, 3, 1, 3, 1, 4, 1, 2, 2, 2, 4, 2, 2, }
    elseif champ == "MissFortune" then  abilitySequence = { 1, 3, 1, 2, 1, 4, 1, 2, 1, 2, 4, 2, 2, 3, 3, 4, 3, 3, }
    elseif champ == "MonkeyKing" then   abilitySequence = { 3, 1, 2, 1, 1, 4, 3, 1, 3, 1, 4, 3, 3, 2, 2, 4, 2, 2, }
    elseif champ == "Mordekaiser" then  abilitySequence = { 3, 1, 3, 2, 3, 4, 3, 1, 3, 1, 4, 1, 1, 2, 2, 4, 2, 2, }
    elseif champ == "Morgana" then      abilitySequence = { 1, 2, 2, 3, 2, 4, 2, 1, 2, 1, 4, 1, 1, 3, 3, 4, 3, 3, }
    elseif champ == "Nami" then         abilitySequence = { 1, 2, 3, 2, 2, 4, 2, 2, 3, 3, 4, 3, 3, 1, 1, 4, 1, 1, }
    elseif champ == "Nasus" then        abilitySequence = { 1, 2, 1, 3, 1, 4, 1, 2, 1, 2, 4, 2, 3, 2, 3, 4, 3, 3, }
    elseif champ == "Nautilus" then     abilitySequence = { 2, 3, 2, 1, 2, 4, 2, 3, 2, 3, 4, 3, 3, 1, 1, 4, 1, 1, }
    elseif champ == "Nidalee" then      abilitySequence = { 2, 3, 1, 3, 1, 4, 3, 2, 3, 1, 4, 3, 1, 1, 2, 4, 2, 2, }
    elseif champ == "Nocturne" then     abilitySequence = { 1, 2, 1, 3, 1, 4, 1, 3, 1, 3, 4, 3, 3, 2, 2, 4, 2, 2, }
    elseif champ == "Nunu" then         abilitySequence = { 1, 3, 1, 2, 1, 4, 3, 1, 3, 1, 4, 3, 3, 2, 2, 4, 2, 2, }
    elseif champ == "Olaf" then         abilitySequence = { 1, 3, 1, 2, 1, 4, 1, 3, 1, 3, 4, 3, 3, 2, 2, 4, 2, 2, }
    elseif champ == "Orianna" then      abilitySequence = { 1, 3, 2, 1, 1, 4, 1, 2, 1, 2, 4, 2, 2, 3, 3, 4, 3, 3, }
    elseif champ == "Pantheon" then     abilitySequence = { 1, 2, 3, 1, 1, 4, 1, 3, 1, 3, 4, 3, 2, 3, 2, 4, 2, 2, }
    elseif champ == "Poppy" then        abilitySequence = { 3, 2, 1, 1, 1, 4, 1, 2, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, }
    elseif champ == "Quinn" then        abilitySequence = { 3, 1, 1, 2, 1, 4, 1, 3, 1, 3, 4, 3, 3, 2, 2, 4, 2, 2, }
    elseif champ == "Rammus" then       abilitySequence = { 1, 2, 3, 3, 3, 4, 3, 2, 3, 2, 4, 2, 2, 1, 1, 4, 1, 1, }
    elseif champ == "Renekton" then     abilitySequence = { 2, 1, 3, 1, 1, 4, 1, 2, 1, 2, 4, 2, 2, 3, 3, 4, 3, 3, }
    elseif champ == "Rengar" then       abilitySequence = { 1, 3, 2, 1, 1, 4, 2, 1, 1, 2, 4, 2, 2, 3, 3, 4, 3, 3, }
    elseif champ == "Riven" then        abilitySequence = { 1, 2, 3, 2, 2, 4, 2, 3, 2, 3, 4, 3, 3, 1, 1, 4, 1, 1, }
    elseif champ == "Rumble" then       abilitySequence = { 3, 1, 1, 2, 1, 4, 1, 3, 1, 3, 4, 3, 3, 2, 2, 4, 2, 2, }
    elseif champ == "Ryze" then         abilitySequence = { 1, 2, 1, 3, 1, 4, 1, 2, 1, 2, 4, 2, 2, 3, 3, 4, 3, 3, }
    elseif champ == "Sejuani" then      abilitySequence = { 2, 1, 3, 3, 2, 4, 3, 2, 3, 3, 4, 2, 1, 2, 1, 4, 1, 1, }
    elseif champ == "Shaco" then        abilitySequence = { 2, 3, 1, 3, 3, 4, 3, 2, 3, 2, 4, 2, 2, 1, 1, 4, 1, 1, }
    elseif champ == "Shen" then         abilitySequence = { 1, 2, 3, 1, 1, 4, 1, 2, 1, 2, 4, 2, 2, 3, 3, 4, 3, 3, }
    elseif champ == "Shyvana" then      abilitySequence = { 2, 1, 2, 3, 2, 4, 2, 3, 2, 3, 4, 3, 1, 3, 1, 4, 1, 1, }
    elseif champ == "Singed" then       abilitySequence = { 1, 3, 1, 3, 1, 4, 1, 2, 1, 2, 4, 3, 2, 3, 2, 4, 2, 3, }
    elseif champ == "Sion" then         abilitySequence = { 1, 3, 3, 2, 3, 4, 3, 1, 3, 1, 4, 1, 1, 2, 2, 4, 2, 2, }
    elseif champ == "Sivir" then        abilitySequence = { 1, 3, 1, 2, 1, 4, 1, 2, 1, 2, 4, 2, 3, 2, 3, 4, 3, 3, }
    elseif champ == "Skarner" then      abilitySequence = { 1, 2, 1, 2, 1, 4, 1, 2, 1, 2, 4, 2, 3, 3, 3, 4, 3, 3, }
    elseif champ == "Sona" then         abilitySequence = { 1, 2, 3, 1, 1, 4, 1, 2, 1, 2, 4, 2, 2, 3, 3, 4, 3, 3, }
    elseif champ == "Soraka" then       abilitySequence = { 1, 3, 1, 2, 1, 4, 1, 2, 1, 3, 4, 2, 3, 2, 3, 4, 2, 3, }
    elseif champ == "Swain" then        abilitySequence = { 2, 3, 3, 1, 3, 4, 3, 1, 3, 1, 4, 1, 1, 2, 2, 4, 2, 2, }
    elseif champ == "Syndra" then       abilitySequence = { 1, 3, 1, 2, 1, 4, 1, 3, 1, 3, 4, 3, 3, 2, 2, 4, 2, 2, }
    elseif champ == "Talon" then        abilitySequence = { 2, 3, 1, 2, 2, 4, 2, 1, 2, 1, 4, 1, 1, 3, 3, 4, 3, 3, }
    elseif champ == "Taric" then        abilitySequence = { 3, 2, 1, 2, 2, 4, 1, 2, 2, 1, 4, 1, 1, 3, 3, 4, 3, 3, }
    elseif champ == "Teemo" then        abilitySequence = { 1, 3, 2, 3, 3, 4, 1, 3, 1, 3, 4, 1, 1, 2, 2, 4, 2, 2, }
    elseif champ == "Thresh" then       abilitySequence = { 1, 3, 2, 2, 2, 4, 2, 3, 2, 3, 4, 3, 3, 1, 1, 4, 1, 1, }
    elseif champ == "Tristana" then     abilitySequence = { 3, 2, 2, 3, 2, 4, 2, 1, 2, 1, 4, 1, 1, 1, 3, 4, 3, 3, }
    elseif champ == "Trundle" then      abilitySequence = { 1, 2, 1, 3, 1, 4, 1, 2, 1, 3, 4, 2, 3, 2, 3, 4, 2, 3, }
    elseif champ == "Tryndamere" then   abilitySequence = { 3, 1, 2, 1, 1, 4, 1, 2, 1, 2, 4, 2, 2, 3, 3, 4, 3, 3, }
    elseif champ == "TwistedFate" then  abilitySequence = { 2, 1, 1, 3, 1, 4, 1, 2, 1, 2, 4, 2, 2, 3, 3, 4, 3, 3, }
    elseif champ == "Twitch" then       abilitySequence = { 1, 3, 3, 2, 3, 4, 3, 1, 3, 1, 4, 1, 1, 2, 2, 1, 2, 2, }
    elseif champ == "Udyr" then         abilitySequence = { 4, 2, 3, 4, 4, 2, 4, 2, 4, 2, 2, 1, 3, 3, 3, 3, 1, 1, }
    elseif champ == "Urgot" then        abilitySequence = { 3, 1, 1, 2, 1, 4, 1, 2, 1, 3, 4, 2, 3, 2, 3, 4, 2, 3, }
    elseif champ == "Varus" then        abilitySequence = { 1, 2, 3, 1, 1, 4, 1, 2, 1, 2, 4, 2, 2, 3, 3, 4, 3, 3, }
    elseif champ == "Vayne" then        abilitySequence = { 1, 3, 2, 1, 1, 4, 1, 3, 1, 3, 4, 3, 3, 2, 2, 4, 2, 2, }
    elseif champ == "Veigar" then       abilitySequence = { 1, 3, 1, 2, 1, 4, 2, 2, 2, 2, 4, 3, 1, 1, 3, 4, 3, 3, }
    elseif champ == "Vi" then           abilitySequence = { 3, 1, 2, 3, 3, 4, 3, 1, 3, 1, 4, 1, 1, 2, 2, 4, 2, 2, }
    elseif champ == "Viktor" then       abilitySequence = { 3, 2, 3, 1, 3, 4, 3, 1, 3, 1, 4, 1, 2, 1, 2, 4, 2, 2, }
    elseif champ == "Vladimir" then     abilitySequence = { 1, 2, 1, 3, 1, 4, 1, 3, 1, 3, 4, 3, 3, 2, 2, 4, 2, 2, }
    elseif champ == "Volibear" then     abilitySequence = { 2, 3, 2, 1, 2, 4, 3, 2, 1, 2, 4, 3, 1, 3, 1, 4, 3, 1, }
    elseif champ == "Warwick" then      abilitySequence = { 2, 1, 1, 2, 1, 4, 1, 3, 1, 3, 4, 3, 3, 3, 2, 4, 2, 2, }
    elseif champ == "Xerath" then       abilitySequence = { 1, 3, 1, 2, 1, 4, 1, 2, 1, 2, 4, 2, 2, 3, 3, 4, 3, 3, }
    elseif champ == "XinZhao" then      abilitySequence = { 1, 3, 1, 2, 1, 4, 1, 2, 1, 2, 4, 2, 2, 3, 3, 4, 3, 3, }
    elseif champ == "Yorick" then       abilitySequence = { 2, 3, 1, 3, 3, 4, 3, 2, 3, 1, 4, 2, 1, 2, 1, 4, 2, 1, }
    elseif champ == "Zac" then          abilitySequence = { 1, 2, 3, 1, 1, 4, 1, 3, 1, 3, 4, 3, 3, 2, 2, 4, 2, 2, }
    elseif champ == "Zed" then          abilitySequence = { 1, 2, 3, 1, 1, 4, 1, 3, 1, 3, 4, 3, 3, 2, 2, 4, 2, 2, }
    elseif champ == "Ziggs" then        abilitySequence = { 1, 2, 3, 1, 1, 4, 1, 3, 1, 3, 4, 3, 3, 2, 2, 4, 2, 2, }
    elseif champ == "Zilean" then       abilitySequence = { 1, 2, 1, 3, 1, 4, 1, 2, 1, 2, 4, 2, 2, 3, 3, 4, 3, 3, }
    elseif champ == "Zyra" then         abilitySequence = { 3, 2, 1, 1, 1, 4, 1, 3, 1, 3, 4, 3, 3, 2, 2, 4, 2, 2, }
    else PrintChat(string.format(" >> 鑷姩鍗囩骇鍔犵偣鏃犳硶搴旂敤浜?  %s", champ))
    end
    if abilitySequence and #abilitySequence == 18 then
        --PrintChat(" >> AutoLevelSpell Script loaded!")
    else
        PrintChat(" >> 鑷姩鍗囩骇鍔犵偣鎶�鑳介『搴忛敊璇紝璇峰弽棣?")
        AutoLevelExhOnTick = function() end
        return
    end
end
-- ############################################# AUTO Exhuast/Cleanse and AutoLevel ##############################################

-- ################################################## My Move To Right Click #####################################################
--[[

Very simple script that smooths the movement when you hold right mouse butotn down.

    Modded by xkjtx, to fit his needs.. lol

]]


local del = os.clock()

local auto = false


function MyMoveToOnLoad()
    --PrintChat("SmoothMoves Loaded")

    co = scriptConfig("右键平滑移动", "smooth")

    co:addParam("smooth", "鼠标右键平滑移动", SCRIPT_PARAM_ONOFF, true)

    co:addParam("toggle", "切换延时: 3.0秒", SCRIPT_PARAM_ONOFF, false)

end

--function moveToCursor()
    
   -- myHero:MoveTo(mousePos.x, mousePos.z)

--end

function MyMoveToOnWndMsg(msg,key)

        if msg == 516 and key == 2 then

            del = os.clock()

            auto = false

        end

        if (co.smooth and key == 2 and msg == 512) or (auto and co.smooth and co.toggle) then

            if ((co.smooth and auto == false and ((os.clock() - del) > 0.2)) or auto) then

                --moveToCursor()
                myHero:MoveTo(mousePos.x, mousePos.z)

            end

            if co.toggle and auto == false and ((os.clock() - del) > 3) then

                auto = true

                PrintFloatText(myHero,0,"跟随鼠标")

            end

     end

end
-- ################################################## My Move To Right Click #####################################################

-- ######################################  AUTO SMITE  ###################################################
--[[
        AutoSmite 2.6-Hec style
            by eXtragoZ
            Credits: Zynox, Manciuszz, Mariopart, Husky
            
            Features:
            - Hotkey for switching AutoSmite On/Off (default: N)
            - Hold-Hotkey for using AutoSmite (default: ALT)
            - Hotkey (default: I) for moving to a nashor smite spot, that enables you to smite nashor behind the wall
            - Right click on the circle next to the wall of Nashor for moving to a nashor smite spot
            - Range indicator of smite
            - Says the percentage of current life you will do to the monster using smite
            - Circles in the camps in the range of smite
            - Supports Nunu's Q and Chogath's R and Hecarim's Q

    1.9 : nginx "isBusy" system
    2.0 : Smiteable targets will now be highlighted if you are in range
        : Different colors for the highlighting when smite is on cooldown
        : Added Nashor smite spot
    2.1 : Added Range indicator of smite
        : Added options on the menu Nashor spot, Draw Circles and Draw Text
        : Now says the percentage of life remaining will be left after use smite , Q and R in case of Nunu or Chogath
        : Added Vilemaw of Twisted Treeline
        : Removed Blue Dragon and Lizard Elder of Twisted Treeline
    2.5 : Smite damage increased to 490-1000
        : Added new method to go to Nashor spot, right click on the circle next to the wall of Nashor
        : Added option to always smite Nashor
        : Added option to draw smite range (separated from the option to draw circles)
        : Now the range of smite and the circles in the camps disappear when AutoSmite is off
        : Now the circles in the camps disappear when AutoSmite is on CD, if you are Nunu or Chogath the respective skills must be on CD
        : New function of the circles in the camps:
            The green circle indicates the percentage of current life
            The purple circle indicates the percentage of life necessary to use smite
            If you are Nunu or Chogath the respective skills have a circle ((Q + Smite) or Q for Nunu or (Smite + R) or R for ChoGath) depending if Smite is in CD
    2.6 : Now Says the percentage of current life you will do to the monster using smite
    ]]
    --[[            Config          ]]
    local range = 800         -- Range of smite (~800)
    local turnoff = false --true/false
    --[[            Globals         ]]
    local smiteSlot = nil
    local objminionTable = {}
local smiteDamage, qDamage, mixDamage, rDamage, mixdDamage = 0, 0, 0, 0, 0
local canuseQ,canuseR,canusesmite = false,false,false
    local Smiteison = false
--local nashorspotpos = {x=5381,z=10283}
--local nashorspot = 0

local Vilemaw,Nashor,Dragon,Golem1,Golem2,Lizard1,Lizard2 = nil,nil,nil,nil,nil,nil,nil

--[[            Code            ]]
    function AutoSmiteOnLoad()
	if myHero:GetSpellData(SUMMONER_1).name:find("Smite") then smiteSlot = SUMMONER_1
    elseif myHero:GetSpellData(SUMMONER_2).name:find("Smite") then smiteSlot = SUMMONER_2 end
	if myHero.charName == "Nunu" or myHero.charName == "Chogath" or smiteSlot or not turnoff then
		SmiteConfig = scriptConfig("自动惩戒 2.9", "autosmite")
            SmiteConfig:addParam("switcher", "切换热键", SCRIPT_PARAM_ONKEYTOGGLE, (smiteSlot ~= nil), 78) -- N
            SmiteConfig:addParam("hold", "正常状态", SCRIPT_PARAM_ONKEYDOWN, false, 17)
            SmiteConfig:addParam("active", "开启自动惩戒", SCRIPT_PARAM_INFO, false)
            SmiteConfig:addParam("smitenashor", "自动惩戒大龙", SCRIPT_PARAM_ONOFF, true)
            SmiteConfig:addParam("nashorspot", "大龙标记点", SCRIPT_PARAM_ONKEYDOWN, false, 67)
            SmiteConfig:addParam("drawrange", "绘制惩戒范围", SCRIPT_PARAM_ONOFF, true)
            SmiteConfig:addParam("drawcircles", "绘制圆圈", SCRIPT_PARAM_ONOFF, true)
            SmiteConfig:addParam("drawtext", "绘制文本", SCRIPT_PARAM_ONOFF, true)
            SmiteConfig:permaShow("active")
            
            ASLoadMinions()
            Smiteison = true
		--PrintChat(" >> AutoSmite 2.9 loaded!")
        end
    end

    function AutoSmiteOnTick()
        if not Smiteison then return end
        checkDeadMonsters()
        SmiteConfig.active = ((SmiteConfig.hold and not SmiteConfig.switcher) or (not SmiteConfig.hold and SmiteConfig.switcher))
        if not SmiteConfig.active and SmiteConfig.smitenashor and Nashor ~= nil and Nashor.x ~= nil then
            if GetDistance(Nashor)<=range+200 then SmiteConfig.active = true end
        end
        if not SmiteConfig.active and SmiteConfig.smitenashor and Vilemaw ~= nil then SmiteConfig.active = true end
        smiteDamage = 460+30*myHero.level
        qDamage = 375+125*myHero:GetSpellData(_Q).level
        mixDamage = qDamage+smiteDamage
        rDamage = 1000+.7*myHero.ap
        mixdDamage = rDamage+smiteDamage
        canuseQ = (myHero.charName == "Nunu" and myHero:CanUseSpell(_Q) == READY)
        canuseR = (myHero.charName == "Chogath" and myHero:CanUseSpell(_R) == READY)
        if smiteSlot ~= nil then canusesmite = (myHero:CanUseSpell(smiteSlot) == READY) end
	-- if GetTickCount()-nashorspot<=1000 or SmiteConfig.nashorspot then
		-- myHero:MoveTo(nashorspotpos.x, nashorspotpos.z)
	-- end
        if SmiteConfig.active and not myHero.dead and (canusesmite or canuseQ or canuseR) then
            if Vilemaw ~= nil then checkMonster(Vilemaw) end
            if Nashor ~= nil then checkMonster(Nashor) end
            if Dragon ~= nil then checkMonster(Dragon) end
            if Golem1 ~= nil then checkMonster(Golem1) end
            if Golem2 ~= nil then checkMonster(Golem2) end
            if Lizard1 ~= nil then checkMonster(Lizard1) end
            if Lizard2 ~= nil then checkMonster(Lizard2) end
        end
    end
    function checkMonster(object)
        if object ~= nil and not object.dead and object.visible and object.x ~= nil then
            local DistanceMonster = GetDistance(object)
            if canusesmite and DistanceMonster <= range and object.health <= smiteDamage then
                CastSpell(smiteSlot, object)
            elseif canuseQ and DistanceMonster <= 125+200 then
                if canusesmite and object.health <= mixDamage then
                    CastSpell(_Q, object)
                    --CastSpell(smiteSlot, object)
                elseif object.health <= qDamage then
                    CastSpell(_Q, object)
                end
            elseif canuseR and DistanceMonster <= 150+200 then
                if canusesmite and object.health <= mixdDamage then
                    CastSpell(_R, object)
                    --CastSpell(smiteSlot, object)
                elseif object.health <= rDamage then
                    CastSpell(_R, object)
                end
            end
        end
    end
--   function AutoSmiteOnWndMsg(msg,key)
--        if not Smiteison then return end
--        if msg == WM_RBUTTONDOWN and smiteSlot ~= nil then
--            nashorspot = ((GetDistance(nashorspotpos,mousePos)<=100) and GetTickCount() or 0)
--        end
--    end
    function AutoSmiteOnDraw()
        if not Smiteison then return end
        checkDeadMonsters()
        if smiteSlot ~= nil and SmiteConfig.active and SmiteConfig.drawrange and not myHero.dead then
            DrawCircle(myHero.x, myHero.y, myHero.z, range, 0x992D3D)
        end
	-- local colornashorspot = ((GetDistance(nashorspotpos,mousePos)<=100) and 0x00FF00 or 0x992D3D)
	-- if smiteSlot ~= nil then DrawCircle(nashorspotpos.x, 0, nashorspotpos.z, 100, colornashorspot) end
	-- if SmiteConfig.drawtext and (nashorspot ~= 0 or SmiteConfig.nashorspot) and GetDistance(nashorspotpos)>=100 then
        --    DrawText("朝大龙最佳惩戒点移动", 18, GetCursorPos().x-100, GetCursorPos().y-20, 0xFF00FF00)
	-- end
	if not myHero.dead and (SmiteConfig.drawtext or SmiteConfig.drawcircles) then
		if Vilemaw ~= nil then MonsterDraw(Vilemaw) end
		if Nashor ~= nil then MonsterDraw(Nashor) end
		if Dragon ~= nil then MonsterDraw(Dragon) end
		if Golem1 ~= nil then MonsterDraw(Golem1) end
		if Golem2 ~= nil then MonsterDraw(Golem2) end
		if Lizard1 ~= nil then MonsterDraw(Lizard1) end
		if Lizard2 ~= nil then MonsterDraw(Lizard2) end
	end
end
function MonsterDraw(object)
	if object ~= nil and not object.dead and object.visible and object.x ~= nil then
		local DistanceMonster = GetDistance(object)
		if SmiteConfig.active and SmiteConfig.drawcircles and (canusesmite or canuseQ or canuseR) and DistanceMonster <= range then
			local healthradius = object.health*100/object.maxHealth
			DrawCircle(object.x, object.y, object.z, healthradius+100, 0x00FF00)
			if canusesmite then
				local smitehealthradius = smiteDamage*100/object.maxHealth
				DrawCircle(object.x, object.y, object.z, smitehealthradius+100, 0x00FFFF)
			end
			if canuseQ and canusesmite then
				local Qsmitehealthradius = mixDamage*100/object.maxHealth
				DrawCircle(object.x, object.y, object.z, Qsmitehealthradius+100, 0x00FFFF)
			elseif canuseQ then
				local Qhealthradius = qDamage*100/object.maxHealth
				DrawCircle(object.x, object.y, object.z, Qhealthradius+100, 0x00FFFF)
			end
			if canuseR and canusesmite then
				local Rsmitehealthradius = mixdDamage*100/object.maxHealth
				DrawCircle(object.x, object.y, object.z, Rsmitehealthradius+100, 0x00FFFF)
			elseif canuseR then
				local Rhealthradius = rDamage*100/object.maxHealth
				DrawCircle(object.x, object.y, object.z, Rhealthradius+100, 0x00FFFF)
			end
		end
		if SmiteConfig.drawtext and DistanceMonster <= range*2 then
			local wtsobject = WorldToScreen(D3DXVECTOR3(object.x,object.y,object.z))
			local objectX, objectY = wtsobject.x, wtsobject.y
			local onScreen = OnScreen(wtsobject.x, wtsobject.y)
			if onScreen then
				local statusdmgS = smiteDamage*100/object.health
				local statuscolorS = (canusesmite and 0xFF00FF00 or 0xFFFF0000)
				local textsizeS = statusdmgS < 100 and math.floor((statusdmgS/100)^2*20+8) or 28
				textsizeS = textsizeS > 16 and textsizeS or 16
				DrawText(string.format("%.1f", statusdmgS).."% - 惩戒", textsizeS, objectX-40, objectY+38, statuscolorS)
                    if myHero.charName == "Nunu" and myHero:GetSpellData(_Q).level>0 then
					local statusdmgQ = qDamage*100/object.health
                        local statuscolorQ = (canuseQ and 0xFF00FF00 or 0xFFFF0000)
                        local textsizeQ = statusdmgQ < 100 and math.floor((statusdmgQ/100)^2*20+8) or 28
                        textsizeQ = textsizeQ > 16 and textsizeQ or 16
					DrawText(string.format("%.1f", statusdmgQ).."% - Q技能", textsizeQ, objectX-40, objectY+56, statuscolorQ)
                        if smiteSlot ~= nil then
						local statusdmgSQ = mixDamage*100/object.health
                            local statuscolorSQ = ((canusesmite and canuseQ) and 0xFF00FF00 or 0xFFFF0000)
                            local textsizeSQ = statusdmgSQ < 100 and math.floor((statusdmgSQ/100)^2*20+8) or 28
                            textsizeSQ = textsizeSQ > 16 and textsizeSQ or 16
						DrawText(string.format("%.1f", statusdmgSQ).."% - 惩戒+Q技能", textsizeSQ, objectX-40, objectY+74, statuscolorSQ)
                        end
                    end
                    if myHero.charName == "Chogath" and myHero:GetSpellData(_R).level>0 then
					local statusdmgR = rDamage*100/object.health
                        local statuscolorR = (canuseR and 0xFF00FF00 or 0xFFFF0000)
                        local textsizeR = statusdmgR < 100 and math.floor((statusdmgR/100)^2*20+8) or 28
                        textsizeR = textsizeR > 16 and textsizeR or 16
					DrawText(string.format("%.1f", statusdmgR).."% - R技能", textsizeR, objectX-40, objectY+56, statuscolorR)
                        if smiteSlot ~= nil then
						local statusdmgSR = mixdDamage*100/object.health
                            local statuscolorSR = ((canusesmite and canuseR) and 0xFF00FF00 or 0xFFFF0000)
                            local textsizeSR = statusdmgSR < 100 and math.floor((statusdmgSR/100)^2*20+8) or 28
                            textsizeSR = textsizeSR > 16 and textsizeSR or 16
						DrawText(string.format("%.1f", statusdmgSR).."% - 惩戒+R技能", textsizeSR, objectX-40, objectY+74, statuscolorSR)
                        end
                    end 
                end
            end
        end
    end
    function AutoSmiteOnCreateObj(obj)
        if not Smiteison then return end
        if obj ~= nil and obj.type == "obj_AI_Minion" and obj.name ~= nil then
            if obj.name == "TT_Spiderboss7.1.1" then Vilemaw = obj
            elseif obj.name == "Worm12.1.1" then Nashor = obj
            elseif obj.name == "Dragon6.1.1" then Dragon = obj
            elseif obj.name == "AncientGolem1.1.1" then Golem1 = obj
            elseif obj.name == "AncientGolem7.1.1" then Golem2 = obj
            elseif obj.name == "LizardElder4.1.1" then Lizard1 = obj
            elseif obj.name == "LizardElder10.1.1" then Lizard2 = obj end
        end
    end
    function AutoSmiteOnDeleteObj(obj)
        if not Smiteison then return end
        if obj ~= nil and obj.name ~= nil then
            if obj.name == "TT_Spiderboss7.1.1" then Vilemaw = nil
            elseif obj.name == "Worm12.1.1" then Nashor = nil
            elseif obj.name == "Dragon6.1.1" then Dragon = nil
            elseif obj.name == "AncientGolem1.1.1" then Golem1 = nil
            elseif obj.name == "AncientGolem7.1.1" then Golem2 = nil
            elseif obj.name == "LizardElder4.1.1" then Lizard1 = nil
            elseif obj.name == "LizardElder10.1.1" then Lizard2 = nil end
        end
    end
    function checkDeadMonsters()
	if Vilemaw ~= nil then if not Vilemaw.valid or Vilemaw.dead or Vilemaw.health <= 0 then Vilemaw = nil end end
	if Nashor ~= nil then if not Nashor.valid or Nashor.dead or Nashor.health <= 0 then Nashor = nil end end
	if Dragon ~= nil then if not Dragon.valid or Dragon.dead or Dragon.health <= 0 then Dragon = nil end end
	if Golem1 ~= nil then if not Golem1.valid or Golem1.dead or Golem1.health <= 0 then Golem1 = nil end end
	if Golem2 ~= nil then if not Golem2.valid or Golem2.dead or Golem2.health <= 0 then Golem2 = nil end end
	if Lizard1 ~= nil then if not Lizard1.valid or Lizard1.dead or Lizard1.health <= 0 then Lizard1 = nil end end
	if Lizard2 ~= nil then if not Lizard2.valid or Lizard2.dead or Lizard2.health <= 0 then Lizard2 = nil end end
    end
    function ASLoadMinions()
        for i = 1, objManager.maxObjects do
            local obj = objManager:getObject(i)
            if obj ~= nil and obj.type == "obj_AI_Minion" and obj.name ~= nil then
                if obj.name == "TT_Spiderboss7.1.1" then Vilemaw = obj
                elseif obj.name == "Worm12.1.1" then Nashor = obj
                elseif obj.name == "Dragon6.1.1" then Dragon = obj
                elseif obj.name == "AncientGolem1.1.1" then Golem1 = obj
                elseif obj.name == "AncientGolem7.1.1" then Golem2 = obj
                elseif obj.name == "LizardElder4.1.1" then Lizard1 = obj
                elseif obj.name == "LizardElder10.1.1" then Lizard2 = obj end
            end
        end
    end
-- ######################################  AUTO SMITE  ###################################################

-- ######################################  WAITEEE  ######################################################
--[[
    Waiteee aka Wait For It! v0.2
        by Weee

    Shows the countdowns of stuff like zilean's ultimate, tryndamere's ultimate, GA, zhonya, aatrox's passive and so on.
    Full list:
        - Zilean's Ultimate (before death)
        - Zilean's Ultimate (after death, same as GA)
        - Guardian Angel
        - Zhonya's Hourglass
        - Aatrox's Passive
        - Kayle's Ultimate
        - Tryndamere's Ultimate
        - Anivia's Egg
]]

immuneTable = {
    { "zhonyas_ring_activate.troy", 2.5 },              -- Zhonya
    { "Aatrox_Passive_Death_Activate.troy", 3 },        -- Aatrox Passive
    { "LifeAura.troy", 4 },                             -- GA and Zil Ulti after death
    { "nickoftime_tar.troy", 7 },                       -- Zil Ulti before death
    { "eyeforaneye_self.troy", 2 },                     -- Kayle Ulti (self?)
    { "UndyingRage_buf.troy", 5 },                      -- Tryn Ulti
    { "EggTimer.troy", 6 },                             -- Anivia Egg
}

function WaiteeeOnLoad()
    startTick = GetGame().tick
    
    WConfig = scriptConfig("无敌计时", "waiteee")
    WConfig:addParam("ShowAlly", "显示盟友无敌时间", SCRIPT_PARAM_ONOFF, true)
    WConfig:addParam("ShowEnemy", "显示敌方无敌时间", SCRIPT_PARAM_ONOFF, true)
    WConfig:addParam("UseTimerText", "计时显示方式: 0:02 (开启); 2 (关闭)", SCRIPT_PARAM_ONOFF, false)
    
    heroTable = {}
    for i = 1, heroManager.iCount do
        local hero = heroManager:GetHero(i)
        hero.immune = { endTime = 0, floatSec = 0, text = "" }
        table.insert(heroTable, hero)
    end
    --PrintChat("Waiteee aka Wait For It! v0.2 loaded.")
end

function WaiteeeOnCreateObj(object)
    for i, hero in pairs(heroTable) do
        for j, immune in pairs(immuneTable) do
            if object and object.valid and GetDistance(object,hero) <= 80 and object.name == immune[1] then
                hero.immune.endTime = GameTime + immune[2]
                hero.immune.floatSec = immune[2]
            end
        end
    end
end

function WaiteeeOnTick()
    GameTime = GetInGameTimer()

    for i, hero in pairs(heroTable) do
        if ((WConfig.ShowAlly and hero.team == player.team) or (WConfig.ShowEnemy and hero.team ~= player.team)) and hero.immune.endTime+0.1 >= GameTime then
            local secondsLeft = math.ceil(math.max(0, hero.immune.endTime - GameTime))
            if hero.immune.floatSec >= secondsLeft and hero.immune.text ~= TimerText(secondsLeft) then
                hero.immune.floatSec = secondsLeft - 1
                hero.immune.text = "" .. ((WConfig.UseTimerText and TimerText(secondsLeft)) or secondsLeft)
                PrintFloatText(hero, 10, hero.immune.text)
            end
        end
    end
end
-- ######################################  WAITEEE  ######################################################	

-- ###############################################   CONSOLE v1.0   ##############################################################

function ConsoleOnLoad()
    --[[
        Console Version 1.0
        
        @author frd
        @author Husky
        
        This script is to help seperate script errors from chat and provide
        a nice interface for viewing errors and script notifications.
        
        The other big benefit of this script is for in-game debugging with ability
        to execute Lua and view environment variables directly in the console.
        
        One of the features we wrote that we weren't sure were going to be used
        is the ability to create binds. This is something we want to get feedback
        on from the community and we can expand it from there.
        
        Examples:
        bind j say going b; recall
        bind k cast q; recall -- Stealth recall for Shaco?
        
        Please use your imagination and give us ideas and extra commands you
        might want to see.
        
        Current Commands:
            clear -- Clear console
            say -- Message team
            say_all -- Message all
            buy -- Buy an item, eg: buy 1001 (buy boots)
            cast -- Cast a spell eg: cast q (options: q, w, e, r, summoner1, summoner2, flash)
            flash -- Flash
            recall -- Recall
            bind -- Bind a key eg: bind k say hello
            unbind -- Unbind a key eg: unbind k
            unbindall -- Unbind all keys
            
        Binds are automatically saved and loaded.
        
        To open the console press the tilt. (`)
        German keyboards can use the (^) under the esc key.
        ========================================================================
        Current Console Commands:

        The following non-lua commands are available in the console:

        clear -- Clears console
        say -- Message team
        say_all -- Message all
        buy -- Buy an item, eg: buy 1001 (buy boots)
        cast -- Cast a spell eg: cast q (options: q, w, e, r, summoner1, summoner2, flash)
        flash -- Flash
        recall -- Recall
        bind -- Bind a key eg: bind k say hello
        unbind -- Unbind a key eg: unbind k
        unbindall -- Unbind all keys


        Binds are automatically saved and loaded.

        How to use it:

        To open the console press the tilt. (`)
        German keyboards can use the (^) under the esc key.

        The following examples show a few binds that can be done with the console:

        -- Stealth recall for shaco
        bind s cast q;cast recall

        -- Notifying teammates about recalling
        bind b cast recall;say brb - base
        
    ]]

    -- Console Configuration
    local console = {
        bgcolor = RGBA( 0, 0, 0, 170 ),
        padding = 10,
        textSize = 16,
        height = WINDOW_H / 2,
        width = WINDOW_W,
        linePadding = 2,
        brand = "Bot of Legends - 内置控制台 1.0",
        scrolling = {
            width   = 12
        },
        colors = {
            script = { R =   0, G = 255, B = 0 },
            console = { R = 255, G = 255, B = 0 },
            command = { R = 150, G = 255, B = 0 },
            prompt = { R =   0, G = 255, B = 0 },
            default = { R =  0, G = 255, B = 0 }
        },
        keys = {
            220 --, -- German Tilt = "|, \" key in english
            --192 -- English Tilt
        },
        selection = {
            content = "",
            startLine = 1,
            endLine = 1,
            startPosition = 1,
            endPosition = 1
        }
    }

    -- Notifications Configuration
    local notifications = {
        bgcolor = RGBA( 0, 0, 0, 80 ),
        max = 6,
        length = 5000,
        fadeTime = 500,
        slideTime = 200,
        perma = 0
    }

    -- Binds
    local binds = {}

    -- Command line structure
    local command = {
        bullet = ">",
        history = {},
        offset = 1,
        buffer = "",
        methods = {
            -- DEFINED at end of script to allow access to all methods
        }
    }

    -- Spell mapping (for cast/level command, etc)
    local spells = {
        q = _Q,
        w = _W,
        e = _E,
        r = _R,
        recall = RECALL,
        summoner1 = SUMMONER_1,
        summoner2 = SUMMONER_2,
        flash = function()
            if myHero:GetSpellData(SUMMONER_1).name:find("SummonerFlash") then return SUMMONER_1
            elseif myHero:GetSpellData(SUMMONER_2).name:find("SummonerFlash") then return SUMMONER_2
            else return nil end
        end
    }

    -- Cursor structure
    local cursor = {
        blinkSpeed = 1200,
        offset = 0,
    }

    -- Is the console active or not
    local active = false

    -- The stack of console messages
    local stack = {}
    local offset = 1

    -- Last notification time
    local closeTick = 0

    -- Unorganized variables
    local stayAtBottom = true

    -- Calculated max console messages to display on a single screen
    local maxMessages = math.floor((console.height - 2 * console.padding - 2 * console.textSize) / (console.textSize + console.linePadding)) + 1

    -- Code ------------------------------------------------
    local function LoadBinds()
        pcall(function() lines = io.lines(SCRIPT_PATH .. "binds.cfg") end)
        if lines ~= nil then
            for line in lines do
                local parts = string.split(line, " ", 3)
                binds[parts[2]] = parts[3]
            end
        end
    end

    local function SaveBinds()
        local file = assert(io.open(SCRIPT_PATH .. "binds.cfg", "w+"))
        if file then
            for key, cmd in pairs(binds) do
                file:write("bind " .. key .. " " .. cmd .. "\n")
            end
            file:close()
        end
    end

    local function IsConsoleKey(key)
        for i, k in ipairs(console.keys) do
            if k == key then
                return true
            end
        end

        return false
    end

    local function GetTextColor(type, opacity)
        local c = console.colors.default

        if console.colors[type] then
            c = console.colors[type]
        end

        return RGBA(c.R, c.G, c.B, (opacity or 1) * 255)
    end

    local function AddMessage(msg, type, insertionOffset)
        if insertionOffset then
            table.insert(stack, insertionOffset, {
                msg = tostring(msg),
                ticks = GetTickCount(),
                gameTime = GetInGameTimer(),
                type = type
            })
        else
            table.insert(stack, {
                msg = tostring(msg),
                ticks = GetTickCount(),
                gameTime = GetInGameTimer(),
                type = type
            })
        end

        if #stack - offset >= maxMessages and stayAtBottom then
            offset = offset + 1
        end

        if notifications.perma > 0 then
            for i = 1, notifications.perma do
                if #stack - i >= 1 then
                    local item = stack[#stack - i]

                    if item.ticks < GetTickCount() - notifications.length + notifications.fadeTime then
                        item.ticks = GetTickCount() - notifications.length + notifications.fadeTime
                        closeTick = GetTickCount() - notifications.length + notifications.fadeTime - 1
                    end
                end
            end
        end
    end

    local function LazyProcess(cmd)
        local preExecutionStackCount = #stack
        local successful, result = ExecuteLUA('return ' .. cmd)

        if successful then
            successfull, result = pcall(function() return cmd .. " = " .. tostring(result) end)
            if successfull then
                table.remove(stack, preExecutionStackCount)
                AddMessage(result, "command", preExecutionStackCount)
            else
                table.remove(stack, preExecutionStackCount)
                AddMessage(cmd .. " = <unknown>", "command", preExecutionStackCount)
            end     
        else
            AddMessage("Lua Error: " .. result:gsub("%[string \"\"%]:1: ", ""), "console")
        end
    end

    function ExecuteLUA(cmd)
        local func, err = load(cmd, "", "t", _ENV)

        if func then
            return pcall(func)
        else
            return false, err
        end
    end

    local function ProcessCommand(cmd)
        local parts = string.split(cmd, " ", 2)
        if command.methods[parts[1]] == nil then return end
        return command.methods[parts[1]](#parts == 2 and parts[2] or nil)
    end

    local function ExecuteCommand(cmd)
        if cmd ~= "" then
            AddMessage(cmd, "command")

            if string.len(cmd) == 0 then return end

            -- Display command in console, and add to history stack
            table.insert(command.history, cmd)

            -- Parse the command
            local process = ProcessCommand(cmd)

            -- If no command was found, we will attempt to execute the command as LUA code
            if not process then
                LazyProcess(cmd)
            end
        end
    end

    local function GetTextWidth(text, textSize)
        return GetTextArea("_" .. text .. "_", textSize or console.textSize).x - 2 * GetTextArea("_", textSize or console.textSize).x
    end

    local function DrawRectangle(x, y, width, height, color)
        DrawLine(x, y + (height/2), x + width, y + (height/2), height, color)
    end

    function Console__WriteConsole(msg)
        AddMessage(msg, "script")
    end

    function Console__OnLoad()
        AddMessage("Game started", "console")
        AddMessage("Champion: " .. myHero.charName, "console")
        LoadBinds()
    end

    function Console__OnDraw()
        local messageBoxHeight = 2 * console.padding + (maxMessages - 1) * (console.textSize + console.linePadding) + console.textSize
        local promptHeight       = 2 * console.padding + console.textSize
        local consoleHeight     = messageBoxHeight + promptHeight
        local scrollbarHeight   = math.ceil(messageBoxHeight / math.max(#stack / maxMessages, 1))

        if active == true then
            local showRatio = math.min((GetTickCount() - closeTick) / notifications.slideTime, 1)
            local slideOffset = (1 - showRatio) * consoleHeight

            -- Draw console background
            DrawRectangle(0, 0 - slideOffset, console.width, consoleHeight, RGBA(0, 0, 0, showRatio * 170))
            DrawLine(0, messageBoxHeight - slideOffset, console.width, messageBoxHeight - slideOffset, 1, GetTextColor("prompt", showRatio * 0.16))
            DrawLine(0, consoleHeight - slideOffset, console.width, consoleHeight - slideOffset, 1, GetTextColor("prompt", showRatio * 0.58))
            
            -- Display stack of messages
            console.selection.content = ""
            if #stack > 0 then
                for i = offset, offset + maxMessages - 1 do
                    if i > #stack then break end

                    local message = stack[i]

                    local selectionStartLine, selectionEndLine, selectionStartPosition, selectionEndPosition
                    if console.selection.startLine < console.selection.endLine or (console.selection.startLine == console.selection.endLine and console.selection.startPosition < console.selection.endPosition) then
                        selectionStartLine = console.selection.startLine
                        selectionEndLine = console.selection.endLine
                        selectionStartPosition = console.selection.startPosition
                        selectionEndPosition = console.selection.endPosition
                    else
                        selectionStartLine = console.selection.endLine
                        selectionEndLine = console.selection.startLine
                        selectionStartPosition = console.selection.endPosition
                        selectionEndPosition = console.selection.startPosition
                    end

                    if i >= selectionStartLine and i <= selectionEndLine then
                        local rightOffset
                        
                        local leftOffset = (i == selectionStartLine) and (GetTextArea("_" .. ("[" .. TimerText(message.gameTime) .. "] " .. message.msg):sub(1, selectionStartPosition - 1) .. "_", console.textSize).x - 2 * GetTextArea("_", console.textSize).x) or 0

                        if i == selectionEndLine then
                            local selectedText = ("[" .. TimerText(message.gameTime) .. "] " .. message.msg):sub(selectionStartLine == selectionEndLine and selectionStartPosition or 1, selectionEndPosition - 1)
                            rightOffset = GetTextWidth(selectedText)

                            console.selection.content = console.selection.content .. (console.selection.content ~= "" and "\r\n" or "") .. selectedText
                        else
                            local selectedText = ("[" .. TimerText(message.gameTime) .. "] " .. message.msg):sub(selectionStartLine == i and selectionStartPosition or 1)
                            rightOffset = WINDOW_W - 2 * console.padding - leftOffset - (scrollbarHeight == messageBoxHeight and 0 or console.scrolling.width)

                            console.selection.content = console.selection.content .. (console.selection.content ~= "" and "\r\n" or "") .. selectedText
                        end

                        DrawRectangle(console.padding + leftOffset, console.padding + (i - offset) * (console.textSize + console.linePadding) - slideOffset - console.linePadding / 2, rightOffset, console.textSize + console.linePadding, 1157627903)
                    end

                    if message ~= nil then
                        DrawText("[" .. TimerText(message.gameTime) .. "] " .. message.msg, console.textSize, console.padding, console.padding + (i - offset) * (console.textSize + console.linePadding) - slideOffset, GetTextColor(message.type, showRatio))
                    end
                end
            end

            -- Show what user is currently typing
            DrawText(command.bullet .. " " .. command.buffer, console.textSize, console.padding, messageBoxHeight + console.padding - slideOffset, GetTextColor("prompt", showRatio))
            if GetTickCount() % cursor.blinkSpeed > cursor.blinkSpeed / 2 then
                DrawText("_", console.textSize, console.padding + GetTextArea(command.bullet .. " " .. command.buffer:sub(1, cursor.offset) .. "_", console.textSize).x - GetTextArea("_", console.textSize).x, messageBoxHeight + console.padding - slideOffset, GetTextColor("prompt", showRatio))
            end

            DrawText(console.brand, console.textSize, console.width - GetTextArea(console.brand, console.textSize).x - console.padding, messageBoxHeight + console.padding - slideOffset, GetTextColor("prompt", showRatio * 0.58))

            if scrollbarHeight ~= messageBoxHeight then
                DrawRectangle(console.width - console.scrolling.width, 0 - slideOffset + (offset - 1) / (#stack - maxMessages) * (messageBoxHeight - scrollbarHeight), console.scrolling.width, scrollbarHeight, GetTextColor("prompt", showRatio * 0.4))
            end
        elseif #stack > 0 then
            local filteredStack = {}

            for i = #stack, math.max(#stack - notifications.max + 1, 1), - 1 do
                if (GetTickCount() - stack[i].ticks > notifications.length or stack[i].ticks < closeTick) and (#stack - i) >= notifications.perma then break end
                table.insert(filteredStack, stack[i])
            end

            if #filteredStack > 0 then
                local slideOffset = 0
                for i = 1, #filteredStack do
                    slideOffset = slideOffset - (console.textSize + (i == #filteredStack and console.padding * 2 or console.linePadding)) * ((#filteredStack - i < notifications.perma) and 0 or math.max((GetTickCount() - filteredStack[#filteredStack - i + 1].ticks - notifications.length + notifications.fadeTime) / notifications.fadeTime, 0))
                end

                DrawRectangle(0, 0, console.width, (console.textSize * #filteredStack) + (console.padding * 2) + (#filteredStack - 1) * console.linePadding + slideOffset, notifications.bgcolor)
                DrawLine(0, (console.textSize * #filteredStack) + (console.padding * 2) + slideOffset + (#filteredStack - 1) * console.linePadding, console.width, (console.textSize * #filteredStack) + (console.padding * 2) + slideOffset + (#filteredStack - 1) * console.linePadding, 1, GetTextColor("prompt", 0.27))

                for i = 1, #filteredStack do
                    local item = filteredStack[#filteredStack + 1 - i]

                    DrawText("[" .. TimerText(item.gameTime) .. "] " .. item.msg, console.textSize, console.padding, console.padding + (i - 1) * (console.linePadding + console.textSize) + slideOffset, GetTextColor(item.type, 1 - ((#filteredStack - i < notifications.perma) and 0 or math.max((GetTickCount() - item.ticks - notifications.length + notifications.fadeTime) / notifications.fadeTime, 0))) )
                end
            end
        end
    end

    function getLineCoordinates(referencePoint)
        local yValue = math.max(math.ceil((referencePoint.y - console.padding - console.textSize) / (console.textSize + console.linePadding)) + 1, 1) + offset - 1
        local xValue = referencePoint.x - console.padding

        if yValue > #stack then
            return #stack + 1, math.huge
        else
            local stringValue = "[" .. TimerText(stack[yValue].gameTime) .. "] " .. stack[yValue].msg
            local stringWidth = 0
            local charNumber = 0
            for i = 1, #stringValue do
                newStringWidth = stringWidth + GetTextArea("_" .. stringValue:sub(i,i) .. "_", console.textSize).x - 2 * GetTextArea("_", console.textSize).x
                if newStringWidth > xValue then break end
                stringWidth = newStringWidth
                charNumber = i
            end

            return yValue, charNumber + 1
        end
    end

    function Console__OnMsg(msg, key)
        local messageBoxHeight = 2 * console.padding + (maxMessages - 1) * (console.textSize + console.linePadding) + console.textSize
        local promptHeight       = 2 * console.padding + console.textSize
        local consoleHeight     = messageBoxHeight + promptHeight
        local scrollbarHeight   = math.ceil(messageBoxHeight / math.max(#stack / maxMessages, 1))

        if active and msg == WM_RBUTTONUP then
            SetClipboardText(console.selection.content)
            console.selection = {
                content = "",
                startLine = 1,
                endLine = 1,
                startPosition = 1,
                endPosition = 1
            }
        elseif active and msg == WM_LBUTTONDOWN then
            if GetCursorPos().x >= console.width - console.scrolling.width then
                dragConsole = true
                dragStart = {x = GetCursorPos().x, y = GetCursorPos().y}
                startOffset = offset
            else
                local line, char = getLineCoordinates(GetCursorPos())

                if line then
                    console.selection.startLine = line
                    console.selection.endLine = line
                    console.selection.startPosition = char
                    console.selection.endPosition = char

                    selecting = true
                end
            end
        elseif active and msg == WM_LBUTTONUP then
            if selecting then
                local line, char = getLineCoordinates(GetCursorPos())

                if line then
                    console.selection.endLine = line
                    console.selection.endPosition = char
                end
            end

            dragConsole = false
            selecting = false
        elseif active and msg == WM_MOUSEMOVE then
            if selecting then
                local line, char = getLineCoordinates(GetCursorPos())

                if line then
                    console.selection.endLine = line
                    console.selection.endPosition = char
                end
            end

            if dragConsole then
                if #stack > maxMessages then
                    stayAtBottom = false

                    offset = startOffset + math.round(((GetCursorPos().y - dragStart.y) * (#stack - maxMessages) / (messageBoxHeight - scrollbarHeight)) + 1)
                    if offset < 1 then
                        offset = 1
                    elseif offset >= #stack - maxMessages + 1 then
                        offset = #stack - maxMessages + 1
                        stayAtBottom = true
                    end
                end
            end
        end

        if active then
            BlockMsg()
        end

        if active and msg == KEY_DOWN then
            local oldCommandBuffer = command.buffer
        
            if key == 13 then --enter
                ExecuteCommand(command.buffer)
                if #stack > maxMessages then
                    offset = #stack - maxMessages + 1
                end
                command.buffer = ""
                cursor.offset = 0
                stayAtBottom = true
            elseif key == 8 then --backspace
                if cursor.offset > 0 then
                    command.buffer = command.buffer:sub(1, cursor.offset - 1) .. oldCommandBuffer:sub(cursor.offset + 1)
                    cursor.offset = cursor.offset - 1
                end
            elseif key == 46 then -- delete
                command.buffer = command.buffer:sub(1, cursor.offset) .. oldCommandBuffer:sub(cursor.offset + 2)
            elseif key == 33 then --pgup
                offset = math.max(offset - maxMessages, 1)
                stayAtBottom = false
            elseif key == 34 then --pgdn
                offset = math.max(math.min(offset + maxMessages, #stack - maxMessages + 1), 1)
                if offset == #stack - maxMessages + 1 then
                    stayAtBottom = true
                end
            elseif key == 38 and #command.history > 0 then --up arrow
                if command.offset < #command.history then
                    command.offset = command.offset + 1
                end
                command.buffer = command.history[command.offset]
                cursor.offset = #command.buffer
            elseif key == 40 and #command.history > 0 then --down arrow
                if command.offset > 1 then
                    command.offset = command.offset - 1
                end
                command.buffer = command.history[command.offset]
                cursor.offset = #command.buffer
            elseif key == 37 then --left arrow
                cursor.offset = math.max(cursor.offset - 1, 0)
            elseif key == 39 then --right arrow
                cursor.offset = math.min(cursor.offset + 1, #command.buffer)
            elseif key == 35 then
                cursor.offset = #command.buffer
            elseif key == 36 then
                cursor.offset = 0
            elseif ToAscii(key) == string.char(3) then
                SetClipboardText(console.selection.content)
                console.selection = {
                    content = "",
                    startLine = 1,
                    endLine = 1,
                    startPosition = 1,
                    endPosition = 1
                }
            elseif ToAscii(key) == string.char(22) then
                local textToAdd = GetClipboardText():gsub("\r", ""):gsub("\n", " ")
                command.buffer = oldCommandBuffer:sub(1, cursor.offset) .. textToAdd .. oldCommandBuffer:sub(cursor.offset + 1)
                cursor.offset = cursor.offset + #textToAdd
            elseif key == 9 then
                for k,v in pairs(_G) do
                    if k:sub(1, #command.buffer) == command.buffer then
                        command.buffer = k
                        cursor.offset = #k
                        break
                    end
                end
            else
                local asciiChar = ToAscii(key)
                if asciiChar ~= nil then
                    command.buffer = oldCommandBuffer:sub(1, cursor.offset) .. asciiChar .. oldCommandBuffer:sub(cursor.offset + 1)
                    cursor.offset = cursor.offset + 1
                end
            end
        end
        
        if msg == KEY_DOWN and IsConsoleKey(key) then
            active = not active
            command.buffer = ""
            closeTick = GetTickCount()

            if active then
                AllowKeyInput(false)
                AllowCameraInput(false)
            else
                AllowKeyInput(true)
                AllowCameraInput(true)
            end
        end
        
        if msg == KEY_DOWN and binds[ToAscii(key)] and not active then
            local parts = string.split(binds[ToAscii(key)], ";")
            for p, cmd in ipairs(parts) do
                ProcessCommand(cmd)
            end
        end
    end

    -- Console Commands ---------------------------------
    command.methods = {
        clear = function()
            stack = {}
            offset = 1
        end,
        
        say = function(query)
            SendChat(query)
            return true
        end,
        
        say_all = function(query)
            SendChat("/all " .. query)
            return true
        end,
        
        buy = function(query)
            BuyItem(tonumber(query))
            return true
        end,

        cast = function(query)      
            local s = type(spells[query]) == "function" and spells[query]() or spells[query]
            if s then
                local target = GetTarget()
                if target ~= nil then
                    CastSpell(s, target)
                else
                    CastSpell(s, mousePos.x, mousePos.z)
                end
            else
                AddMessage("Attempted to cast invalid spell: \"" .. query .. "\"", "console")
            end
            
            return true
        end,
        
        flash = function() return command.methods.cast("flash") end,
        recall = function() return command.methods.cast("recall") end,
        
        level = function(query)
            local s = type(spells[query]) == "function" and spells[query]() or spells[query]
            if s then
                LevelSpell(s)
            end
            
            return true
        end,
        
        bind = function(query)
            local parts = string.split(query, " ", 2)
            binds[parts[1]] = parts[2]
            SaveBinds()
            return true
        end,
        
        unbind = function(query)
            binds[query] = nil
            SaveBinds()
            return true
        end,
        
        unbindall = function()
            binds = {}
            SaveBinds()
            return true
        end,
        
        reload = function()
            LoadBinds()
            return true
        end
    }

    AddLoadCallback(Console__OnLoad)
    AddDrawCallback(Console__OnDraw)
    AddMsgCallback(Console__OnMsg)
    _G.WriteConsole = Console__WriteConsole
    _G.PrintChat = _G.WriteConsole
    _G.Console__IsOpen = active

end



-- ############################################# TOWER RANGE ################################################

--[[
	Script: Tower Range v0.4
	Author: SurfaceS

	v0.1 	initial release -- thanks Shoot for idea
	V0.1b	added mode 4 -- thanks hellspan
	v0.1c	added gameOver to stop script at end
	v0.2	BoL Studio version
	v0.3	use the scriptConfig
	v0.4    Tower ranges now show ally and enemy tower range when you are in range
    v0.4a   Tower range changed in S3
]]

local towerRange = {
	turrets = {},
	typeText = {"OFF", "ON (enemy close)", "ON (enemy)", "ON (all)", "ON (all close)"},
	--[[         Config         ]]
	turretRange = 975,	 				-- 950-S3 updates to 975
	fountainRange = 1050,	 			-- 1050
	allyTurretColor = 0x003300, 		-- Green color
	enemyTurretColor = 0xFF0000, 		-- Red color
--	activeType = 4,						-- 0 Off, 1 Close enemy towers, 2 All enemy towers, 3 Show all, 4 Show all close
	tickUpdate = 1000,
	nextUpdate = 0,
}

function	TowerRangeMenu()
		TRConfig = scriptConfig("塔攻击范围", "TowerRange")
		TRConfig:addParam("ActiveType", "显示方式", SCRIPT_PARAM_SLICE, 4 , 0 , 4 , 0)
		TRConfig:addParam("desc", "-----说明-------------", SCRIPT_PARAM_INFO,"" )			
		TRConfig:addParam("desc", "方式0：关闭显示", SCRIPT_PARAM_INFO,"" )		
		TRConfig:addParam("desc", "方式1：附近的敌方塔", SCRIPT_PARAM_INFO,"" )
		TRConfig:addParam("desc", "方式2：所有敌方塔", SCRIPT_PARAM_INFO,"" )
		TRConfig:addParam("desc", "方式3：所有的塔", SCRIPT_PARAM_INFO,"" )
		TRConfig:addParam("desc", "方式4：附近的塔", SCRIPT_PARAM_INFO,"" )
end

function towerRange.checkTurretState()
	if TRConfig.ActiveType > 0 then
		for name, turret in pairs(towerRange.turrets) do
			turret.active = false
		end
		for i = 1, objManager.maxObjects do
			local object = objManager:getObject(i)
			if object ~= nil and object.type == "obj_AI_Turret" then
				local name = object.name
				if towerRange.turrets[name] ~= nil then towerRange.turrets[name].active = true end
			end
		end
		for name, turret in pairs(towerRange.turrets) do
			if turret.active == false then towerRange.turrets[name] = nil end
		end
	end
end

function TowerRangeOnDraw()
	if GetGame().isOver then return end
	if TRConfig.ActiveType > 0 then
		for name, turret in pairs(towerRange.turrets) do
			if turret ~= nil then
				if (TRConfig.ActiveType == 1 and turret.team ~= player.team and player.dead == false and GetDistance(turret) < 2000)
				or (TRConfig.ActiveType == 2 and turret.team ~= player.team)
				or (TRConfig.ActiveType == 3)
				or (TRConfig.ActiveType == 4 and player.dead == false and GetDistance(turret) < 2000) then
					DrawCircle(turret.x, turret.y, turret.z, turret.range, turret.color)
				end
			end
		end
	end
end
function TowerRangeOnTick()
end
function TowerRangeOnDeleteObj(object)
	if object ~= nil and object.type == "obj_AI_Turret" then
		for name, turret in pairs(towerRange.turrets) do
			if name == object.name then
				towerRange.turrets[name] = nil
				return
			end
		end
	end
end
function TowerRangeOnLoad()
	gameState = GetGame()
	TowerRangeMenu()
	
	for i = 1, objManager.maxObjects do
		local object = objManager:getObject(i)
		if object ~= nil and object.type == "obj_AI_Turret" then
			local turretName = object.name
			towerRange.turrets[turretName] = {
				object = object,
				team = object.team,
				color = (object.team == player.team and towerRange.allyTurretColor or towerRange.enemyTurretColor),
				range = towerRange.turretRange,
				x = object.x,
				y = object.y,
				z = object.z,
				active = false,
			}
			if turretName == "Turret_OrderTurretShrine_A" or turretName == "Turret_ChaosTurretShrine_A" then
				towerRange.turrets[turretName].range = towerRange.fountainRange
				for j = 1, objManager.maxObjects do
					local object2 = objManager:getObject(j)
					if object2 ~= nil and object2.type == "obj_SpawnPoint" and GetDistance(object, object2) < 1000 then
						towerRange.turrets[turretName].x = object2.x
						towerRange.turrets[turretName].z = object2.z
					elseif object2 ~= nil and object2.type == "obj_HQ" and object2.team == object.team then
						towerRange.turrets[turretName].y = object2.y
					end
				end
			end
		end
	end
end

-- ############################################# TOWER RANGE ################################################


-- ############################################# Simple Draw ################################################

function DrawCircleNextLvl(x, y, z, radius, width, color, chordlength)
    radius = radius or 300
		quality = math.max(8,round(180/math.deg((math.asin((chordlength/(2*radius)))))))
		quality = 2 * math.pi / quality
		radius = radius*.92
    local points = {}
    for theta = 0, 2 * math.pi + quality, quality do
        local c = WorldToScreen(D3DXVECTOR3(x + radius * math.cos(theta), y, z - radius * math.sin(theta)))
        points[#points + 1] = D3DXVECTOR2(c.x, c.y)
    end
    DrawLines2(points, width or 1, color or 4294967295)
end

function round(num) 
	if num >= 0 then return math.floor(num+.5) else return math.ceil(num-.5) end
end

function DrawCircle2(x, y, z, radius, color)
    local vPos1 = Vector(x, y, z)
    local vPos2 = Vector(cameraPos.x, cameraPos.y, cameraPos.z)
    local tPos = vPos1 - (vPos1 - vPos2):normalized() * radius
    local sPos = WorldToScreen(D3DXVECTOR3(tPos.x, tPos.y, tPos.z))
    if OnScreen({ x = sPos.x, y = sPos.y }, { x = sPos.x, y = sPos.y }) then
        DrawCircleNextLvl(x, y, z, radius, 1, color, 75)	
    end
end

function DrawCircle2OnLoad()
	_G.DrawCircle = DrawCircle2
end


-- ############################################# Simple Draw ################################################
-- ############################################# Auto Potion/Zhonyas/Wooglets ################################################

function AutoHMZWOnLoad()
        AHMZWConfig = scriptConfig("自动 吃药/中亚/巫师帽", "AutoHMZW") 
		AHMZWConfig:addParam("ZWItems", "自动 中亚/巫师帽", SCRIPT_PARAM_ONOFF, true)
        AHMZWConfig:addParam("ZWHealth", "中亚/巫师帽 血量<%", SCRIPT_PARAM_SLICE, 25, 0, 100, 0)
        AHMZWConfig:addParam("ZWEnemys", "中亚/巫师帽 附近敌人>?", SCRIPT_PARAM_SLICE, 1, 0, 5, 0)
		AHMZWConfig:addParam("s1", "", SCRIPT_PARAM_INFO,"" )			
        AHMZWConfig:addParam("aHP", "自动生命药水", SCRIPT_PARAM_ONOFF, true)
        AHMZWConfig:addParam("aHPHealth", "生命药水 血量<%", SCRIPT_PARAM_SLICE, 50, 0, 100, 0)
		AHMZWConfig:addParam("s2", "", SCRIPT_PARAM_INFO,"" )		
        AHMZWConfig:addParam("aMP", "自动法力药水", SCRIPT_PARAM_ONOFF, true)
        AHMZWConfig:addParam("aMPMana", "法力药水 蓝量<%", SCRIPT_PARAM_SLICE, 50, 0, 100, 0)
		AHMZWConfig:addParam("s3", "提示:当红,蓝药不足时,自动水晶瓶", SCRIPT_PARAM_INFO,"" )		
end

function AutoZWOnTick()
	BuffCheck()
	if aRecall or InFountain() then return end
	znaSlot, wgtSlot = GetInventorySlotItem(3157),GetInventorySlotItem(3090)
    hpSlot, mpSlot, fskSlot = GetInventorySlotItem(2003),GetInventorySlotItem(2004),GetInventorySlotItem(2041)
	
    ZNAREADY = (znaSlot ~= nil and myHero:CanUseSpell(znaSlot) == READY)
    WGTREADY = (wgtSlot ~= nil and myHero:CanUseSpell(wgtSlot) == READY)
    HPREADY = (hpSlot ~= nil and myHero:CanUseSpell(hpSlot) == READY)
    MPREADY =(mpSlot ~= nil and myHero:CanUseSpell(mpSlot) == READY)
    FSKREADY = (fskSlot ~= nil and myHero:CanUseSpell(fskSlot) == READY)
		
    if AHMZWConfig.ZWItems and IsZWHealthLow() and (countNearbyAlies()>=AHMZWConfig.ZWEnemys) and (ZNAREADY or WGTREADY) then CastSpell((wgtSlot or znaSlot)) end

    if AHMZWConfig.aHP and IsHPHealthLow() and not (UsingHPot or UsingFlask) and (HPREADY or FSKREADY ) then CastSpell((hpSlot or fskSlot)) end
    if AHMZWConfig.aMP and IsMPManaLow() and not (UsingMPot or UsingFlask) and(MPREADY or FSKREADY ) then CastSpell((mpSlot or fskSlot)) end	
end

function BuffCheck()
	if 	TargetHaveBuff({"Recall","SummonerTeleport","RecallImproved"}) then 
	     aRecall = true
	else
	     aRecall = false	
	end
	
	if 	TargetHaveBuff({"ItemCrystalFlask"}) then 
	     UsingFlask = true
	else
	     UsingFlask = false	
	end
	
	if 	TargetHaveBuff({"RegenerationPotion"}) then 
	     UsingHPot = true
	else
	     UsingHPot = false	
	end	

	if 	TargetHaveBuff({"FlaskOfCrystalWater"}) then 
	     UsingMPot = true
	else
	     UsingMPot = false	
	end		
	
end

function countNearbyAlies()
        local nearbyHeroes = 0
        for i = 1, heroManager.iCount, 1 do
                local hero = heroManager:getHero(i)
                if hero ~= nil and hero.team ~= TEAM_ENEMY and hero.dead == false then
                        if math.sqrt((hero.x - player.x) ^ 2 + (hero.z - player.z) ^ 2) < 585 then
                                nearbyHeroes = nearbyHeroes + 1
                        end    
                end
        end
        return nearbyHeroes
end

function IsZWHealthLow()
        if myHero.health < (myHero.maxHealth * ( AHMZWConfig.ZWHealth / 100)) then
                return true
        else
                return false
        end
end

function IsHPHealthLow()
        if myHero.health < (myHero.maxHealth * ( AHMZWConfig.aHPHealth / 100)) then
                return true
        else
                return false
        end
end

function IsMPManaLow()
        if myHero.mana < (myHero.maxMana * ( AHMZWConfig.aMPMana / 100)) then
                return true
        else
                return false
        end
end
--[[
function AutoHMZWOnCreateObj(obj)
        if obj.name:find("TeleportHome.troy") or obj.name:find("TeleportHomeImproved.troy")  then
                if GetDistance(obj, myHero) <= 70 then
                        aRecall = true
                end
        end
        if obj.name:find("Regenerationpotion_itm.troy") then
                if GetDistance(obj, myHero) <= 70 then
                        UsingHPot = true
                end
        end
        if obj.name:find("ManaPotion_itm.troy") then
                if GetDistance(obj, myHero) <= 70 then
                        UsingMPot = true
                end
        end
        if obj.name:find("potion_manaheal") then
                if GetDistance(obj, myHero) <= 70 then
                        UsingFlask = true
                end
        end
end

function AutoHMZWOnDeleteObj(obj)
        if obj.name:find("TeleportHome.troy") or obj.name:find("TeleportHomeImproved.troy") then
                aRecall = false
        end
        if obj.name:find("Regenerationpotion_itm.troy") then
                UsingHPot = false
        end
        if obj.name:find("ManaPotion_itm.troy") then
                UsingMPot = false
        end
        if obj.name:find("potion_manaheal") then
                UsingFlask = false
        end
end
--]]

-- ############################################# Auto Potion/Zhonyas/Wooglets ################################################	

-- ############################################# aatokill ############################################## 
local Vayne = false

if myHero.charName == "Vayne" then
	Vayne=true
end

local IEid = 3031
local BOTkRid = 3153
local iebought = 1
local BOTkRdmg = 0
local Vaynedmg = 0
local Lethality = 1

function aatokillOnLoad()
	AAtoKill = scriptConfig("平A击杀计数器", "aatokill")
	
	AAtoKill:addParam("drawaa", "显示击杀所需时间", SCRIPT_PARAM_ONOFF, true)
	AAtoKill:addParam("usecrits", "计算时计入暴击伤害", SCRIPT_PARAM_ONOFF, true)
	AAtoKill:addParam("autos", "预测击杀所需平A次数", SCRIPT_PARAM_ONOFF, true)
	AAtoKill:addParam("lethality", "致命（天赋）点数", SCRIPT_PARAM_SLICE, 0, 0, 2, 0)
	--PrintChat(">> 平A击杀计数器载入成功")

end

function aatokillOnTick()

	if myHero.range < 350 then
		Lethality = 1 + 0.1*AAtoKill.lethality
	else
		Lethality = 1 + 0.05*AAtoKill.lethality
	end

end

function aatokillOnDraw()
	for _, Enemy in pairs(GetEnemyHeroes()) do
        if ValidTarget(Enemy) and Enemy.visible and AAtoKill.drawaa then
			local timetokill
			if AAtoKill.usecrits then
				timetokill = string.format("%4.1f", CRIT(Enemy)) .. "秒后击杀"
				DrawText3D(tostring(timetokill), Enemy.x, Enemy.y-75, Enemy.z, 18, RGB(255, 255, 255), true)
			else
				timetokill = string.format("%4.1f", DPS(Enemy)) .. "秒后击杀"
				DrawText3D(tostring(timetokill), Enemy.x, Enemy.y-75, Enemy.z, 18, RGB(255, 255, 255), true)
			end
			if AAtoKill.autos then
				autostokill = "致死平A数" .. string.format("%4.0f", 1+(Enemy.health/(myHero:CalcDamage(Enemy, myHero.totalDamage) + Vaynedmg + BOTkRdmg + (Lethality*iebought*myHero:CalcDamage(Enemy, myHero.totalDamage)*myHero.critChance))))
				DrawText3D(tostring(autostokill), Enemy.x, Enemy.y, Enemy.z, 18, RGB(255, 255, 255), true)
			end
        end
    end
end

function DPS(Enemy)
	return Enemy.health/ (myHero:CalcDamage(Enemy, myHero.totalDamage) * myHero.attackSpeed)
end

function CRIT(Enemy)
	if GetInventorySlotItem(IEid) ~=nil then
		iebought = 1.5
	else
		iebought = 1
	end
	
	if GetInventorySlotItem(BOTkRid) ~=nil then
		BOTkRdmg = myHero:CalcDamage(Enemy, (Enemy.health)*0.05)
	else
		BOTkRdmg = 0
	end
	
	if Vayne then
		WAble = (myHero:CanUseSpell(_W) == READY)
		Vaynedmg = (((3+(myHero:GetSpellData(_W).level))*0.01)*Enemy.maxHealth)/3
	else
		Vaynedmg = 0
	end
	
	return Enemy.health/ ((myHero:CalcDamage(Enemy, myHero.totalDamage) + (Lethality*iebought*myHero:CalcDamage(Enemy, myHero.totalDamage)*myHero.critChance) + BOTkRdmg + Vaynedmg) * myHero.attackSpeed)
end
-- ############################################# aatokill ############################################## 
	
-- ############################################# HP Helper ##############################################	
--[[HP Helper by BotHappy

	Features: It prints the life of your enemies
	
	GREEN --- LIFE>50%
	ORANGE --- 25%>LIFE>50%
	RED --- LIFE<25%
	GREY --- DEAD
	BLUE --- NOT VISIBLE (MISSING)
	
	Version: v0.1 - Release
]]

function HPHelperOnLoad()
	Variables()
	Menus()

end

function HPHelperOnDraw()
	if RoamerHelper.drawlife and GetNumberEnemies() > 0 then
		for i, hero in pairs(EnemyHeroes) do
--			DrawRectangleOutline(CalculateXBox(), 5, BoxWidth(), 35, ARGB(255,255,0,0), 1)
		--	DrawTextA(hero.charName, 13, CalculateXtext(i) , 6, ColorDraw(hero), "center")
	--		DrawTextA("等级"..tostring(hero.level), 13, CalculateXtext(i), 16, ColorDraw(hero), "center")
		--	DrawTextA(tostring(math.round(hero.health).."/"..tostring(math.round(hero.maxHealth))), 13, CalculateXtext(i), 26, ColorDraw(hero), "center")
			DrawText3D(tostring(math.round(hero.health).."/"..tostring(math.round(hero.maxHealth))), hero.x+4, hero.y+198, hero.z, 18, RGB(0, 0, 0), true)			
			DrawText3D(tostring(math.round(hero.health).."/"..tostring(math.round(hero.maxHealth))), hero.x, hero.y+200, hero.z, 18, RGB(255, 255, 255), true)			
		
			
		end
	end
end


function CalculateXBox()
	return (WINDOW_W/2)-(GetNumberEnemies()*(460/5))/2
end

function BoxWidth()
	return GetNumberEnemies()*(460/5)
end

function CalculateXtext(i)
	return CalculateXBox()+BoxWidth()-i*(BoxWidth())/GetNumberEnemies()+40
end

function GetNumberEnemies()
	local enemies = 0
	for i=1, heroManager.iCount, 1 do
		local hero = heroManager:getHero(i)
		if hero.team ~= myHero.team then
			enemies = enemies+1
		end
	end
	return enemies
end

function ColorDraw(hero)
	if not hero.visible then
		return ARGB(255,130,0,255)
	elseif hero.health >= hero.maxHealth*0.5 then
		return ARGB(255,0,255,55)
	elseif hero.health < hero.maxHealth*0.5 and hero.health >= hero.maxHealth*0.25 then
		return ARGB(255,255,125,0)
	elseif hero.health < hero.maxHealth*0.25 and not hero.dead then
		return ARGB(255,255,0,0)
	elseif hero.dead then
		return ARGB(255,128,128,128)
	end
end

function Variables()
	EnemyHeroes = GetEnemyHeroes()
end

function Menus()
	RoamerHelper = scriptConfig("HP显示助手", " Hp Helper")
	
	RoamerHelper:addParam("drawlife", "显示血条(敌方)", SCRIPT_PARAM_ONOFF, true)
	
end	
	
	
-- ############################################# HP Helper ##############################################	



	-- XT002 --
     
    function OnLoad()
        KCConfig = scriptConfig("多合一功能优化版V1.71A", "xt002hud")
		KCConfig:addParam("AutoHMZW", "自动 吃药/中亚/巫师帽", SCRIPT_PARAM_ONOFF, true)
        KCConfig:addParam("aatokill", "平A击杀计数器", SCRIPT_PARAM_ONOFF, true)
        KCConfig:addParam("HPHelper", "HP显示助手0.1", SCRIPT_PARAM_ONOFF, true)		
      --  KCConfig:addParam("MinionMarker", "补刀显示", SCRIPT_PARAM_ONOFF, true)
		KCConfig:addParam("TowerRange", "显示塔攻击范围", SCRIPT_PARAM_ONOFF, true)
		KCConfig:addParam("PerfectWard", "开启完美插眼", SCRIPT_PARAM_ONOFF, true)            
        KCConfig:addParam("LowAwareness", "敌方预警提示", SCRIPT_PARAM_ONOFF, true)        
		KCConfig:addParam("PlayerRange", "显示玩家范围", SCRIPT_PARAM_ONOFF, true)
		KCConfig:addParam("EnemyRanges", "显示敌方范围", SCRIPT_PARAM_ONOFF, true)			
		KCConfig:addParam("MiniMapTimers", "小地图计时 (如果报错,重新载入(F9))", SCRIPT_PARAM_ONOFF, true)           
		KCConfig:addParam("WhereIsHe", "他在哪里?? v1.2", SCRIPT_PARAM_ONOFF, false)
		KCConfig:addParam("CloneR", "本尊预测(如果报错,重新载入(F9))", SCRIPT_PARAM_ONOFF, true)	
		KCConfig:addParam("WhereHeGo", "敌人逃跑预测(如果报错,重新载入(F9))", SCRIPT_PARAM_ONOFF, true)			
		KCConfig:addParam("useLvl", "自动升级加点", SCRIPT_PARAM_ONOFF, false)
		KCConfig:addParam("MyMoveTo", "右键平滑移动(如果报错,重新载入(F9))", SCRIPT_PARAM_ONOFF, true)
		KCConfig:addParam("AutoSmite", "自动惩戒 v2.9+", SCRIPT_PARAM_ONOFF, false)
		KCConfig:addParam("WaiteEe", "无敌计时 v0.2", SCRIPT_PARAM_ONOFF, true)
		KCConfig:addParam("AutoShield", "自动护盾(如果报错,重新载入(F9))", SCRIPT_PARAM_ONOFF, true)
		KCConfig:addParam("Consolev1", "内置控制台 v1.0 - 按键 '\\'", SCRIPT_PARAM_ONOFF, false)
		KCConfig:addParam("DrawCircleFPS", "绘制圆圈优化(需重载)'", SCRIPT_PARAM_ONOFF, false)

		if KCConfig.aatokill then
			aatokillOnLoad()
		end		
		if KCConfig.Consolev1 then
			ConsoleOnLoad()
		end		
		if KCConfig.CloneR then
			CloneRevOnLoad()
		end		
		if KCConfig.AutoShield then
			AutoShieldOnLoad()
		end	
		if KCConfig.AutoSmite then
			AutoSmiteOnLoad()
		end

		if KCConfig.MyMoveTo then
			MyMoveToOnLoad()
		end
		if KCConfig.WaiteEe then
			WaiteeeOnLoad()
		end
		if KCConfig.MiniMapTimers then
			MiniMapTimersOnLoad()
		end
		if KCConfig.DrawCircleFPS then
			DrawCircle2OnLoad()
		end
		HPHelperOnLoad()
		WDHGOnLoad()   --WhereHeGo
		--MinionMarkerOnLoad()
		TowerRangeOnLoad()		
		AutoLevelOnLoad()
		AutoHMZWOnLoad()
    PrintChat(" >>澶氬悎涓�鍔熻兘 V1.71A宸茶浇鍏?")
    PrintChat(" >>姝よ剼鏈负鍏嶈垂姹夊寲鑴氭湰--寰风帥瑗夸簹鎴樹簤瀛﹂櫌 !")
    PrintChat(" >>鍙戝竷锛欱OL鍏嶈垂姹夊寲鑴氭湰鍙戝竷缇わ細168830534!")	

	end   
    function OnTick()
		if KCConfig.aatokill then
		    aatokillOnTick()
	    end
		if KCConfig.WhereHeGo then
			WDHGOnTick()
		end
		if KCConfig.PerfectWard then
			PerfectWardOnTick()
		end
		if KCConfig.WaiteEe then
			WaiteeeOnTick()
		end
		if KCConfig.AutoSmite then
			AutoSmiteOnTick()
		end 
		if KCConfig.useLvl then
			AutoLevelOnTick()
		end		
		if KCConfig.WhereIsHe then
			WhereIsHeOnTick()
		end		
		if KCConfig.MiniMapTimers then
			MiniMapTimersOnTick()
		end		
        if KCConfig.LowAwareness then
            LowAwarenessOnTick()
        end	
		if KCConfig.TowerRange then
			TowerRangeOnTick()
		end
		if KCConfig.AutoHMZW then
			AutoZWOnTick()
		end
    end
     
    function OnCreateObj(obj)

    --    if KCConfig.MinionMarker then
      --      MinionMarkerOnCreateObj(obj)
      --  end
		if KCConfig.WhereHeGo then
			WDHGOnCreateObj(obj)
		end	
		if KCConfig.WaiteEe then
			WaiteeeOnCreateObj(obj)
		end	
		if KCConfig.AutoSmite then
			AutoSmiteOnCreateObj(obj)
		end 
--		if KCConfig.AutoHMZW then
--			AutoHMZWOnCreateObj(obj)
--		end		
    end
     
    function OnDeleteObj(obj)
		if KCConfig.AutoSmite then
			AutoSmiteOnDeleteObj(obj)
		end 
		if KCConfig.TowerRange then
			TowerRangeOnDeleteObj(obj)
		end
		end
	function OnProcessSpell(obj,spell)
		if KCConfig.WhereHeGo then
			WDHGOnProcessSpell(obj,spell)
		end
		if KCConfig.AutoShield then
			AutoShieldOnProcessSpell(obj,spell)
		end	
			
	end  
	
    function OnDraw()
		if KCConfig.aatokill then
		    aatokillOnDraw()
	    end
	    if KCConfig.WhereHeGo then
			WDHGOnDraw()
		end
		if KCConfig.PerfectWard then
			PerfectWardOnDraw()
		end
		if KCConfig.WhereIsHe then
			WhereIsHeOnDraw()
		end
   --     if KCConfig.MinionMarker then
     --       MinionMarkerOnDraw()
      --  end
        if KCConfig.LowAwareness then
            LowAwarenessOnDraw()
        end
		if KCConfig.EnemyRanges then
			ERangesOnDraw()
		end
		if KCConfig.PlayerRange then
			PRangesOnDraw()
		end
	    if KCConfig.MiniMapTimers then
			MiniMapTimersOnDraw()
		end
		if KCConfig.AutoSmite then
			AutoSmiteOnDraw()
		end
		if KCConfig.AutoShield then
			AutoShieldOnDraw()
		end
		if KCConfig.TowerRange then
			TowerRangeOnDraw()
		end
		if KCConfig.HPHelper then
			HPHelperOnDraw()
		end		
		
    end
     
    function OnWndMsg(msg,key)
		if KCConfig.PerfectWard then
			PerfectWardOnWndMsg(msg, key)
		end
		if KCConfig.MiniMapTimers then
			MiniMapTimersOnWndMsg(msg, key)
		end
		if KCConfig.MyMoveTo then
			MyMoveToOnWndMsg(msg, key)
		end	
    end
     
class 'OnCC' -- {

local ccIndex= { 3, 5, 7, 8, 10, 11, 21, 22, 24, 25, 28, 29, 30 }
local cc = { 'Debuff', 'Stun', 'Silence', 'Taunt', 'Slow', 'Root', 'Fear', 'Charm', 'Supress', 'Blind', 'Flee', 'Knockup', 'Knockback'}  
local mode = false

function OnCC:__init()
	if _G.OnCCSimpleMode ~= nil then mode = _G.OnCCSimpleMode end
	
	-- simple mode
	if mode then
		-- let's make n callbacks
		for i,v in ipairs(cc) do
			cc[i] = 'On'..v
			_ENV = AdvancedCallback:register(cc[i])
		end
		
	-- advanced mode
	else
		-- let's make 3*n callbacks
		for i,v in ipairs(cc) do
			cbTable[i][1] = 'OnGain'..v
			cbTable[i][2] = 'OnLose'..v
			cbTable[i][3] = 'OnUpdate'..v
			_ENV = AdvancedCallback:register(cbTable[i][1], cbTable[i][2], cbTable[i][3])
		end
		cc = cbTable
	end

	AdvancedCallback:bind('OnGainBuff', function(unit, buff) self:buffHandler(unit, buff, 1) return end)
	AdvancedCallback:bind('OnLoseBuff', function(unit, buff) self:buffHandler(unit, buff, 2) return end)
	AdvancedCallback:bind('OnUpdateBuff', function(unit, buff) self:buffHandler(unit, buff, 3) return end)
end

function OnCC:buffHandler(unit, buff, gain)
	if unit and unit.valid and buff and buff.type ~= nil then
		inSet, index = self:checkType(buff.type)
		if inSet ~= nil and inSet then
			if mode ~= nil and mode then AdvancedCallback[cc[index]](AdvancedCallback, unit, buff, gain) return -- is cc and is simple mode
			else AdvancedCallback[cc[index]][gain](AdvancedCallback, unit, buff) return  -- is cc and is advanced mode
			end
		end
	end
end

function OnCC:checkType(btype)
	for i,v in ipairs(ccIndex) do
		if v == btype then return true, i end
	end
	return false
end

-- }
--[[ iZed by Apple
 
Spacebar for PewPew!
T for Poke!]]--
 
if myHero.charName ~= "Zed" then return end
 
require "AllClass"
 
--[[ Config ]]--
local HK1 = 32 -- PewPew! Hotkey
local HK2 = 84 -- Harass Hotkey
 
--[[ Constants ]]--
local QRange = 900
local WRange = 550
local ERange = 290
local RRange = 625
local DFGid, EXECid, YOGHid, RANOid, BWCid, HXGid = 3128, 3123, 3142, 3143, 3144, 3146
local DFGSlot, EXECSlot, YOGHSlot, RANOSlot, BWCSlot, HXGSlot = nil, nil, nil, nil, nil, nil
 
--[[ Script Variables ]]--
local ts = TargetSelector(TARGET_LOW_HP,QRange)
local tp = TargetPrediction(QRange, 1.17, 200, 50) -- Need proper variables
local prediction
local clone
 
function OnLoad()
        acConfig = scriptConfig("I系列-劫", "zedhelper")
        acConfig:addParam("scriptActive", "自动连招(空格)", SCRIPT_PARAM_ONKEYDOWN, false, HK1)
        acConfig:addParam("harass", "骚扰(T)", SCRIPT_PARAM_ONKEYDOWN, false, HK2)
        acConfig:addParam("drawcircles", "显示范围", SCRIPT_PARAM_ONOFF, true)
        acConfig:addParam("onlynuke", "开大时才使用装备", SCRIPT_PARAM_ONOFF, true)
        acConfig:permaShow("scriptActive")
        acConfig:permaShow("harass")
        ts.name = "Zed"
 
        acConfig:addTS(ts)
 
        clone = nil
end
 
function OnTick()
        ts:update()
        if acConfig.scriptActive then PewPew() end
        if acConfig.harass then Poke() end
        DFGSlot = GetInventorySlotItem(DFGid)
        EXECSlot = GetInventorySlotItem(EXECid)
        YOGHSlot = GetInventorySlotItem(YOGHid)
        RANOSlot = GetInventorySlotItem(RANOid)
        BWCSlot = GetInventorySlotItem(BWCid)
        HXGSlot = GetInventorySlotItem(HXGid)
end
 
function PewPew()
        ts:update()
        if TargetValid(ts.target) then
                if myHero:CanUseSpell(_R) == READY and GetDistance(ts.target) < RRange then
                        CastSpell(_R, ts.target)
                        if iReady(DFGSlot) then CastSpell(DFGSlot, ts.target) end
                        if iReady(EXECSlot) then CastSpell(EXECSlot, ts.target) end
                        if iReady(YOGHSlot) then CastSpell(YOGHSlot) end
                        if iReady(RANOSlot) then CastSpell(RANOSlot, ts.target) end
                        if iReady(BWCSlot) then CastSpell(BWCSlot, ts.target) end
                        if iReady(HXGSlot) then CastSpell(HXGSlot, ts.target) end
                elseif clone ~= nil then
                        if myHero:CanUseSpell(_W) == READY and ts.target:GetDistance(clone) < 125 and ts.target:GetDistance(clone) < GetDistance(ts.target) then
                                CastSpell(_W)
                        end
                end
        end
        if myHero:CanUseSpell(_E) == READY or myHero:CanUseSpell(_Q) == READY then
                if TargetValid(ts.target) and clone == nil and myHero:CanUseSpell(_W) == READY and (GetDistance(ts.target) < (WRange + QRange) or GetDistance(ts.target) < (WRange + ERange)) then
                        CastSpell(_W, ts.target)
                end
                for i=1, heroManager.iCount do
                        local enemy = heroManager:GetHero(i)
                        if enemy.team ~= player.team and enemy ~= nil then
                                if acConfig.onlynuke == false and GetDistance(ts.target) < ERange then
                                        if iReady(DFGSlot) then CastSpell(DFGSlot, ts.target) end
                                        if iReady(EXECSlot) then CastSpell(EXECSlot, ts.target) end
                                        if iReady(YOGHSlot) then CastSpell(YOGHSlot) end
                                        if iReady(RANOSlot) then CastSpell(RANOSlot, ts.target) end
                                        if iReady(BWCSlot) then CastSpell(BWCSlot, ts.target) end
                                        if iReady(HXGSlot) then CastSpell(HXGSlot, ts.target) end
                                end
                                if clone ~= nil then
                                        if enemy:GetDistance(clone) < ERange or GetDistance(enemy) < ERange then
                                                CastSpell(_E)
                                        end
                                elseif GetDistance(enemy) < ERange then
                                        CastSpell(_E)
                                end
                                if clone ~= nil then
                                        if enemy:GetDistance(clone) < QRange and GetDistance(enemy) < QRange then
                                                TargetPrediction__OnTick()
                                                prediction = tp:GetPrediction(enemy)
                                                if prediction ~= nil then
                                                        CastSpell(_Q,prediction.x, prediction.z)
                                                end
                                        elseif enemy:GetDistance(clone) < QRange or GetDistance(enemy) < QRange then
                                                TargetPrediction__OnTick()
                                                prediction = tp:GetPrediction(enemy)
                                                if prediction ~= nil then
                                                        CastSpell(_Q,prediction.x, prediction.z)
                                                end
                                        end
                                elseif GetDistance(enemy) < QRange then
                                        TargetPrediction__OnTick()
                                        prediction = tp:GetPrediction(enemy)
                                        if prediction ~= nil then
                                                CastSpell(_Q,prediction.x, prediction.z)
                                        end
                                end
                        end
                end
        end
end
 
function Poke()
        ts:update()
        if TargetValid(ts.target) and GetDistance(ts.target) < WRange + QRange then
                if clone == nil and myHero:CanUseSpell(_W) == READY then
                        EnemyPos = Vector(ts.target.x,ts.target.z)
                        HeroPos = Vector(myHero.x,myHero.z)
                        ClonePos = HeroPos + ( HeroPos - EnemyPos ):normalized() * (-0.5)
                        CastSpell(_W, ClonePos.x, ClonePos.z)
                        TargetPrediction__OnTick()
                        prediction = tp:GetPrediction(ts.target)
                        if prediction ~= nil then
                                CastSpell(_Q,prediction.x, prediction.z)
                        end
                else
                        TargetPrediction__OnTick()
                        prediction = tp:GetPrediction(ts.target)
                        if prediction ~= nil then
                                CastSpell(_Q,prediction.x, prediction.z)
                        end
                end
        end
end
 
function OnCreateObj(obj)
        if obj.name:find("Zed_Clone_idle.troy") then
                clone = obj
        end
end
 
function OnDeleteObj(obj)
        if obj.name:find("Zed_Clone_idle.troy") then
                clone = nil
        end            
end
 
function TargetValid(target)
        if target ~= nil and target.dead == false and target.team == TEAM_ENEMY and target.visible == true then
                return true
        else
                return false
        end
end
 
function iReady(itemslot)
        if itemslot ~= nil and myHero:CanUseSpell(itemslot) then
                return true
        else
                return false
        end
end
 
function OnDraw()
        if acConfig.drawcircles and not myHero.dead then
                DrawCircle(myHero.x,myHero.y,myHero.z, QRange, 0xFF80FF00)
                if ts.target ~= nil then
                        DrawText("Targetting: " .. ts.target.charName, 18, 100, 100, 0xFFFF0000)
                        DrawCircle(ts.target.x, ts.target.y, ts.target.z, 100, 0xFF80FF00)
                end
                if clone ~= nil then
                        DrawCircle(clone.x, clone.y, clone.z, ERange, 0xFFFF0000)
                elseif ClonePos ~= nil then
                        DrawCircle(ClonePos.x, ClonePos.y, ClonePos.z, 100, 0xFFFF0000)
                end
        end
        SC__OnDraw()
end
 
function OnWndMsg(msg,key)
        SC__OnWndMsg(msg,key)
end
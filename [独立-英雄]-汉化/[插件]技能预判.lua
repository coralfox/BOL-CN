--[[
QPredict = "Z"
WPredict = "X"
EPredict = "C"
RPredict = "V"
--]]
if FileExist(SCRIPT_PATH..'Common/Collision.lua') then print("Collision By HAckTool") require "Collision" end
--[[ Temporary Hardcoded Settings]]
local function ChampPredict(champ)
    return champ and ({
        Nidalee = {
            [_Q] = { speed = 1300, delay = 125, range = 1500, minionCollisionWidth = 80 },
        },
        Karthus = {
            [_Q] = { speed = math.huge, delay = 600, range = 875}
        },
        Zyra = {
            [_E] = { speed = 1150, delay = 250, range = 1200}
        },
        Blitzcrank = {
            [_Q] = { speed = 1800, delay = 250, range = 1050, minionCollisionWidth = 150 }
        },
        Cassiopeia = {
            [_Q] = { speed = math.huge, delay = 535, range = 850}
        },
        Gragas = {
            [_Q] = { speed = 1000, delay = 250, range = 1100}
        },
        Rumble = {
            [_E] = { speed = 2000, delay = 250, range = 950, minionCollisionWidth = 80 }
        },
        Kennen = {
            [_Q] = { speed = 1700, delay = 250, range = 1050, minionCollisionWidth = 80 }
        },
        Morgana = {
            [_Q] = { speed = 1200, delay = 250, range = 1300, minionCollisionWidth = 80 }
        },
        DrMundo = {
            [_Q] = { speed = 1500, delay = 250, range = 1050,minionCollisionWidth = 80 }
        },
        Lux = {
            [_Q] = { speed = 1200, delay = 245, range = 1175}
        },
        Ezreal = {
            [_Q] = { speed = 2000, delay = 251, range = 1200, minionCollisionWidth = 80 },
            [_W] = { speed = 1600, delay = 250, range = 1000}
        },
        Leona = {
            [_E] = { speed = 2000, delay = 250, range = 900}
        },
        LeeSin = {
            [_Q] = { speed = 1500, delay = 250, range = 1100, minionCollisionWidth = 80 }
        },
		Ahri = {
            [_Q] = { speed = 1700, delay = 250, range = 880},
			[_E] = { speed = 1600, delay = 100, range = 975, minionCollisionWidth = 80 }
        },
        Syndra = {
            [_Q] = { speed = math.huge, delay = 400, range = 800}
        },
        Elise = {
            [_E] = { speed = 1450, delay = 250, range = 975, minionCollisionWidth = 80 }
        },
        Amumu = {
            [_Q] = { speed = 2000, delay = 400, range = 1100, minionCollisionWidth = 100 }
        },
        TwistedFate = {
            [_Q] = { speed = 1450, delay = 200, range = 1450}
        },
        KogMaw = {
            [_R] = { speed = math.huge, delay = 700, range = GetSpellData(_R).range}
        },
        Thresh = {
            [_Q] = { speed = 1900, delay = 500, range = 1100, minionCollisionWidth = 100 }
        },
    })[champ]
end

local spellDB, ts, tp, config = ChampPredict(myHero.charName), {}, {}, nil
if not spellDB then return end
print(" <font color='#e066a3'> >>指向技能预�? " .. myHero.charName .. " 开�?</font>")
function OnLoad()
tpCol = {}
collised = {}
    config = scriptConfig("ָ����Ԥ��", "Predict")
    for spellKey, spell in pairs(spellDB) do
        local spellStr = ({ [_Q] = "Q", [_W] = "W", [_E] = "E", [_R] = "R" })[spellKey]
	
        config:addParam("scriptActive" .. spellStr , "������ʾ ".. spellStr , SCRIPT_PARAM_ONKEYDOWN, false , GetKey(_G[spellStr .. "Key"] or ({ [_Q] = QPredict, [_W] = WPredict, [_E] = EPredict, [_R] = RPredict })[spellKey]))
        ts[spellKey] = TargetSelector(TARGET_LOW_HP, spell.range, DAMAGE_MAGIC, true)
        tp[spellKey] = TargetPredictionNONEVIP(spell.range, spell.speed, spell.delay / 1000, spell.minionCollisionWidth)
		if Collision then
		tpCol[spellKey] = Collision(1050, 1820, 0.23, 70)
	
		end
		
    end
    config:addParam("hitchance", "��С��Χ", SCRIPT_PARAM_SLICE, 50, 0, 100, 0)
    config:addParam("minionCollision", "ʹ���赲Ԥ��", SCRIPT_PARAM_ONOFF, true)
    config:addParam("drawPrediction", "Ԥ���ʾ��ʾ", SCRIPT_PARAM_ONOFF, false)
    config:addParam("betaPrediction", "����Ԥ����ʾ", SCRIPT_PARAM_ONOFF, false)
end

function OnTick()
    for spellKey, cTS in pairs(ts) do
        if GetSpellData(spellKey).level > 0 then
            local spell = spellDB[spellKey]
			    cTS:update()      
            if cTS.target and cTS.target.type == "obj_AI_Hero" then
                local cTP = tp[spellKey]
                local position, t, enhPosition = cTP:GetPrediction(cTS.target)
                position = config.betaPrediction and enhPosition or position
				
			if spell.minionCollisionWidth and cTS.target then

						if not config.minionCollision then collised[spellKey] = true
						elseif config.minionCollision and not tpCol[spellKey] then collised[spellKey] = tp[spellKey]:GetCollision(cTS.target)
						elseif config.minionCollision and tpCol[spellKey] then collised[spellKey] = tpCol[spellKey]:GetMinionCollision(myHero,position) 
						end
				
			end	
				
				
                local hitChance = cTP:GetHitChance(cTS.target)
                if config["scriptActive" .. ({ [_Q] = "Q", [_W] = "W", [_E] = "E", [_R] = "R" })[spellKey]] and position and hitChance > config.hitchance / 100 and (collised[spellKey] == false or config.minionCollision == false or spell.minionCollisionWidth == nil) then
                    CastSpell(spellKey, position.x, position.z)
                end
            end
        end
    end
end

local function Transition(r, A1, R1, G1, B1, A2, R2, G2, B2)
    return ARGB(A1 + r * (A2 - A1), R1 + r * (R2 - R1), G1 + r * (G2 - G1), B1 + r * (B2 - B1))
end

function OnDraw()
    if config.drawPrediction then
        if not wayPointManager then wayPointManager = WayPointManager() end
        for spellKey, cTS in pairs(ts) do
            if GetSpellData(spellKey).level > 0 then
                local target = cTS.target
                if target then
                    local cTP = tp[spellKey]
                    local rate = wayPointManager:GetWayPointChangeRate(target)
                    local hitChance = cTP:GetHitChance(target)
                    if rate > 0 then DrawText3D(tostring(rate), target.x, target.y, target.z, 10, ARGB(255, 0, 255, 0), true) end
                    if hitChance > 0 then DrawText3D(tostring(math.round(hitChance * 100)) .. "%", target.x, target.y - 50, target.z, 10, ARGB(255, 0, 255, 255), true) end
                    wayPointManager:DrawWayPoints(target, ARGB(100, 255, 255, 255))
                    cTP:DrawPrediction(target, ARGB(100, 255, 0, 0))
                    if config.betaPrediction then cTP:DrawPredictionRectangle(target, ARGB(100,255,255,255)) end
                    local color = Transition(hitChance, 255, 255, 0, 0, 255, 0, 255, 0)
                    cTP:DrawAnimatedPrediction(target, color, color, 2, 2)
                end
            end
        end
    end
end

--[[
    I'M Leesin 0.2.8 by Klokje
    ================================================================================
 
    Change log:
    0.1:
        - Initial release
        - Added basic functions
       
    0.1.1
        - Small change insec combo
 
    0.1.2
        - Ward Drawings
        - Delay ward jump
 
    0.2
        - Added R Collision Killes
        - Delete ward jump <- added to other script
        - Rewrite some code
        - Added Minion and Jungle farm
        - Fixed a bug with passive use
        - Rewrote core
        - require "ImLib"
 
    0.2.1
        - fix some bugs
        - improve herras??
 
    0.2.2
        - Small change
        
    0.2.3 
        - bug on purple side
    0.2.3b
 
    0.2.4
        - With auto lib updater
        - Change buff stuff
        
    0.2.6
        - bug fix
        
    0.2.7
        - new prediction information. 
        
    0.2.8 
        - Fix baron bug
    0.2.8a
        - Lib update
]]
 
-- Library --------------------------------------------------------------------------

if myHero.charName ~= "LeeSin" then return end

function UpdateLib_imlib()
 local URL = "https://bitbucket.org/Klokje/public-klokjes-bol-scripts/raw/master/Common/ImLib.lua"
 local LIB_PATH = BOL_PATH.."Scripts\\Common\\ImLib.lua"
 DownloadFile(URL, LIB_PATH, function()
                if FileExist(LIB_PATH) then
                    PrintChat("<font color='#FF0000'> >> 支持库已下载，需要F9两次重新载入 <<</font>")
                end
                end)
end

function UpdateLib_TP()
 local URL = "http://ys-f.ys168.com/2.0/406354141/hwRTkVm45555S36KKMJK/AllClass.lua"
 local LIB_PATH = BOL_PATH.."Scripts\\Common\\TargetPredictionNONEVIP.lua"
 DownloadFile(URL, LIB_PATH, function()
                if FileExist(LIB_PATH) then
                    PrintChat("<font color='#FF0000'> >> 支持库已下载，需要F9两次重新载入 <<</font>")
                end
                end)
end
function UpdateLib_Col()
 local URL = "http://ys-f.ys168.com/2.0/406354156/j374715866HOH5jsSKjh/Collision.lua"
 local LIB_PATH = BOL_PATH.."Scripts\\Common\\Collision.lua"
 DownloadFile(URL, LIB_PATH, function()
                if FileExist(LIB_PATH) then
                    PrintChat("<font color='#FF0000'> >> 支持库已下载，需要F9两次重新载入 <<</font>")
                end
                end)
end

if FileExist(SCRIPT_PATH..'Common/ImLib.lua') then require "ImLib" else UpdateLib_imlib() end
if not VIP_USER then
 if FileExist(SCRIPT_PATH..'Common/TargetPredictionNONEVIP.lua') then require "TargetPredictionNONEVIP" else UpdateLib_TP() end
end
if FileExist(SCRIPT_PATH..'Common/Collision.lua') then require "Collision" else UpdateLib_Col() end


-- Globals --------------------------------------------------------------------------
enemyHeroes = {}
 
qRange = 1000
qRangeSecond = 1300
wRange = 700
eRange = 350
eRangeSecond = 600
rRange = 375
 
passive = {stack = 0, endT = 0}
 
--Dashes
dash = {
    [_Q] = nil,
    [_W] = nil
}
 
eStack = {}
extraDamage = {}
visionList = {}
 
qTarget = nil
ignite = nil
flash = nil
collition = Collision(qRange, myHero:GetSpellData(_Q).missileSpeed, cdelay, myHero:GetSpellData(_Q).lineWidth)
 
-- Code -----------------------------------------------------------------------------
function OnLoad()
    PrintChat(" >> I'M Leesin 0.2.8 by Klokje")  
 
    OrbWalking.Instance("I'M Leesin")
    Jumper.ability = _W
 
    ssLastHitting = LastHitting()
 
    ConfigBasic = scriptConfig("I'M Leesin: �����趨", "LeeSinBasic")    
    ConfigBasic:addParam("Harass", "ɧ��", SCRIPT_PARAM_ONKEYDOWN, false, string.byte("T"))
    ConfigBasic:addParam("Insec", "Insec����", SCRIPT_PARAM_ONKEYDOWN, false, string.byte("C"))
    ConfigBasic:addParam("Insecflash", "Insec������ʹ������", SCRIPT_PARAM_ONOFF, true)
    ConfigBasic:addParam("Prediction", "Q������", SCRIPT_PARAM_SLICE, 50, 0, 100, 0)
 
    ConfigCombo = scriptConfig("I'M Leesin: �����趨", "LeeSinCombo")
    ConfigCombo:addParam("combo", "�����ȼ�", SCRIPT_PARAM_ONKEYDOWN, false, 32)
    ConfigCombo:addParam("useQ", "ʹ��Q ", SCRIPT_PARAM_ONOFF, true)
    ConfigCombo:addParam("autoQ","�Զ�����Q", SCRIPT_PARAM_ONOFF, true)
    ConfigCombo:addParam("useW", "ʹ��W", SCRIPT_PARAM_ONOFF, false)
    ConfigCombo:addParam("autoW","�Զ�����W", SCRIPT_PARAM_ONOFF, true)
    ConfigCombo:addParam("useE", "ʹ��E", SCRIPT_PARAM_ONOFF, true)
    ConfigCombo:addParam("autoE","�Զ�����E", SCRIPT_PARAM_ONOFF, true)
    ConfigCombo:addParam("useAA", "�Զ��չ�", SCRIPT_PARAM_ONOFF, true)
 
    ConfigKillCombo = scriptConfig("I'M Leesin: ��ɱ�趨", "LeeSinKill")
    ConfigKillCombo:addParam("useR", "�Զ�R����ͷ", SCRIPT_PARAM_ONOFF, true)
    ConfigKillCombo:addParam("combokiller", "��������ͷ", SCRIPT_PARAM_ONOFF, true)
    ConfigKillCombo:addParam("combo", "����ͷ���谴�����ȼ�", SCRIPT_PARAM_ONOFF, false)
    ConfigKillCombo:addParam("rcollision", "�Զ���������ͷ", SCRIPT_PARAM_ONOFF, true)
    ConfigKillCombo:addParam("ward", "ʹ������", SCRIPT_PARAM_ONOFF, true)
 
    ConfigFarm = scriptConfig("I'M Leesin: �����趨", "farming")
    ConfigFarm:addParam("Farm", "����", SCRIPT_PARAM_ONKEYDOWN, false, GetKey("A"))
    ConfigFarm:addParam("jungle", "��Ұ", SCRIPT_PARAM_ONOFF, true)
    ConfigFarm:addParam("minions", "Ұ����ʱ", SCRIPT_PARAM_ONOFF, true)
 
    ConfigDraw = scriptConfig("I'M Leesin: ��Χ�趨", "LeeSinDraw")      
    ConfigDraw:addParam("DrawQ", "Q ��Χ", SCRIPT_PARAM_ONOFF, false)
    ConfigDraw:addParam("DrawQColor", "Q ��ɫ", SCRIPT_PARAM_COLOR, {40, 0, 0, 100})
    ConfigDraw:addParam("DrawW", "W ��Χ", SCRIPT_PARAM_ONOFF, false)
    ConfigDraw:addParam("DrawWColor", "W ��ɫ", SCRIPT_PARAM_COLOR, {40, 0, 20, 100})
    ConfigDraw:addParam("DrawE", "E ��Χ", SCRIPT_PARAM_ONOFF, false)
    ConfigDraw:addParam("DrawEColor", "E ��ɫ", SCRIPT_PARAM_COLOR, {0, 20, 0, 100})
    ConfigDraw:addParam("DrawR", "R ��Χ", SCRIPT_PARAM_ONOFF, false)
    ConfigDraw:addParam("DrawRColor", "R ��ɫ", SCRIPT_PARAM_COLOR, {0, 20, 40, 100})
    ConfigDraw:addParam("DrawPassive", "��ʾ������Χ", SCRIPT_PARAM_ONOFF, true)
    ConfigDraw:addParam("DrawPassiveColor", "���� ��ɫ", SCRIPT_PARAM_COLOR, ColorARGB.Linen:ToTable())
    ConfigDraw:addParam("DrawTarget", "��ʾĿ��", SCRIPT_PARAM_ONOFF, true)
    ConfigDraw:addParam("DrawTargetColor", "Ŀ�� ��ɫ", SCRIPT_PARAM_COLOR, ColorARGB.DarkRed:ToTable())
 
    sp = Spells()
 
    if myHero:GetSpellData(SUMMONER_1).name:find("SummonerDot") then ignite = SUMMONER_1
    elseif myHero:GetSpellData(SUMMONER_2).name:find("SummonerDot") then ignite = SUMMONER_2 end
    if myHero:GetSpellData(SUMMONER_1).name:find("SummonerFlash") then flash = SUMMONER_1
    elseif myHero:GetSpellData(SUMMONER_2).name:find("SummonerFlash") then flash = SUMMONER_2 end
end
 
function OnTick()
    enemyHeroes = Heroes({team = {TEAM_ENEMY}, range = 2000})
    table.sort(enemyHeroes, EnemySort)
 
     local currentTarget = GetTarget()
 
    if currentTarget ~= nil and currentTarget.type == "obj_AI_Hero" and ValidTarget(currentTarget, 2000, true) then
        selected = currentTarget
    else
        selected = nil
    end
 
    if KillCombo() then return
    elseif CollisionKiller() then return
    elseif InSecCombo() then return
    elseif Herras() then return
    elseif CollisionKiller() then return
    elseif Combo() then return
    elseif AutoAll() then return
    end
end
 
function OnDraw()
    if selected and ConfigDraw.DrawTarget then
        DrawCircle(selected.x, selected.y, selected.z, 100, ColorARGB.FromTable(ConfigDraw.DrawTargetColor))
        DrawCircle(selected.x, selected.y, selected.z, 99, ColorARGB.DarkBlue:ToARGB())
    end
 
    if ConfigDraw.DrawQ and not myHero.dead then
        if spell[_Q]:Ready()  == 1 then
            DrawCircle(myHero.x, myHero.y, myHero.z, qRange+50, ColorARGB.FromTable(ConfigDraw.DrawQColor))
        elseif spell[_Q]:Ready(2) == 1 then
            DrawCircle(myHero.x, myHero.y, myHero.z, qRangeSecond+50, ColorARGB.FromTable(ConfigDraw.DrawQColor))
        end
    end
 
    if ConfigDraw.DrawW and not myHero.dead and spell[_W]:Ready()  == 1 then
        DrawCircle(player.x, player.y, player.z, wRange, ColorARGB.FromTable(ConfigDraw.DrawWColor))
    end
 
    if ConfigDraw.DrawE and not myHero.dead then
        if spell[_E]:Ready()  == 1 then
            DrawCircle(myHero.x, myHero.y, myHero.z, eRange, ColorARGB.FromTable(ConfigDraw.DrawEColor))
        elseif spell[_E]:Ready(2)  == 1 then
            DrawCircle(myHero.x, myHero.y, myHero.z, eRangeSecond, ColorARGB.FromTable(ConfigDraw.DrawEColor))
        end
    end
 
    if ConfigDraw.DrawR and not myHero.dead and spell[_R]:Ready()  == 1  then
        DrawCircle(myHero.x, myHero.y, myHero.z, rRange, ColorARGB.FromTable(ConfigDraw.DrawRColor)) -- ColorARGB(0, 20, 40, 100):ToARGB() --ColorARGB.DarkRed:ToARGB()
    end
 
    if  ConfigDraw.DrawPassive and not myHero.dead and passive.stack ~= 0 then
        local pos = WorldToScreen(D3DXVECTOR3(myHero.x, myHero.y, myHero.z))
        Message.DrawTextWithBorder(tostring(passive.stack), 50, pos.x- 15, pos.y - 100, ColorARGB.FromTable(ConfigDraw.DrawPassiveColor), ColorARGB.Black:ToARGB())
    end
end
 
function OnGainBuff(unit, buff)
    if unit == nil or buff == nil or buff.name == nil then return end

    if unit.team ~= myHero.team and buff.name == "BlindMonkEOne"then
        eStack[unit.networkID] = unit
    end
 
    if unit.isMe and buff.name == "blindmonkpassive_cosmetic" then
        passive.stack = buff.stack
        passive.endT = buff.endT
    end
 
    if buff.source and buff.source.isMe and buff.name:lower():find("blindmonkqone") then
        qTarget = unit
        if p == true then
            spell[_Q]:Cast()
            p = false
        end
    end
 
    if unit.isMe and buff.name == "blindmonkqtwodash" then
        dash[_Q] = buff.source
    end
 
    if unit.isMe and buff.name == "blindmonkwonedash" then
        dash[_W] = buff.source
    end
end    
 
function OnUpdateBuff(unit, buff)
     if unit.isMe and buff.name == "blindmonkpassive_cosmetic" then
        passive.stack = buff.stack
        passive.endT = buff.endT
    end
end
 
function OnLoseBuff(unit, buff)
    if unit == nil or buff == nil or buff.name == nil then return end

    if unit.team ~= myHero.team and buff.name == "BlindMonkEOne" then
        eStack[unit.networkID] = nil
    end
 
    if unit.isMe and buff.name == "blindmonkpassive_cosmetic" then
        passive.stack = 0
        passive.endT = 0
    end
 
    if unit.team ~= myHero.team and buff.name:lower():find("blindmonkqone") then
        qTarget = nil
    end
 
    if unit.isMe and buff.name == "blindmonkqtwodash" then
       dash[_Q] = nil
    end
 
    if unit.isMe and buff.name == "blindmonkwonedash" then
        dash[_W] = nil
    end
end
 
function IsValid(target)
if visionList[target.networkID] == nil then 
    return true
elseif target.type == myHero.type and (GetGameTimer() - visionList[target.networkID] > 0.125) then
  return true
 else
  return false
 end
end
function OnLoseVision(unit)
 if unit and unit.type == myHero.type and unit.team ~= myHero.team then
  visionList[unit.networkID] = math.huge
 end
end
function OnGainVision(unit)
 if unit and unit.type == myHero.type and unit.team ~= myHero.team then
  visionList[unit.networkID] = GetGameTimer()
 end
end
 
 
-- Main Functions ----------------------------------------------------------------------
function KillCombo()
    if not ConfigKillCombo.combokiller then return false end
 
    OrbWalking.Enable(false)
    if ConfigKillCombo.combo or ConfigCombo.combo then
        if ComboHasQ() then OrbWalking.Enable(true) return true end
        if ComboHasNoQ() then OrbWalking.Enable(true) return true end
    end
 
    for i, target in ipairs(enemyHeroes) do
        if ConfigKillCombo.useR then AutoR(target) end
    end
    OrbWalking.Enable(true)
    return false
end
 
function ComboHasQ()
    if spell[_Q]:Ready(2) == 1 and qTarget~= nil and 30 <= myHero.mana then
        local rDmg = getDmg("R", qTarget, player)
        local qDmg = getDmg("Q", qTarget, player, 1)
        local aDmg = getDmg("AD",qTarget, player)
        local eDmg = getDmg("E", qTarget, player)
        local onhitDmg = aDmg
        local ignitedmg = 0
 
        if ignite ~= nil and myHero:CanUseSpell(ignite) == READY  then ignitedmg = getDmg("IGNITE", qTarget, player) end
 
        local reg = 0
        local extra = 0
            for i, damage in pairs(extraDamage) do
                if damage.time > GetGameTimer() then
                    if qTarget.networkID == damage.dtarget then
                    extra = extra + (math.floor((damage.dmg/5)*(damage.time - GetGameTimer())) - (damage.dmg/5))
                    extra = extra - ((qTarget.hpRegen/5) * (6 - (damage.time - GetGameTimer())))
                    end
                else
                    table.remove(extraDamage, i)
                    i = i - 1
                end
            end
 
        local health = qTarget.health + (((qTarget.hpRegen/5) * 3) - extra) + 50
        if health < 0 then health = math.huge end
 
        if GetDistance(qTarget) <= qRange then
            if health <= QSecondDamage(0, qTarget) then  spell[_Q]:Cast() return true
            elseif ignitedmg > 0 and health <= QSecondDamage(0, qTarget) + ignitedmg and GetDistance(qTarget) <= 600 then
                CastSpell(ignite, qTarget)
                table.insert(extraDamage, {dmg = ignitedmg, dtarget = qTarget, time = GetGameTimer() + 5})
                spell[_Q]:Cast()
                return true
            end
        end
 
        if spell[_E]:Ready() == 1 and 80 <= myHero.mana then
            if health<= eDmg +  QSecondDamage(eDmg, qTarget) and GetDistance(qTarget) <= eRange then
                spell[_E]:Cast()
                spell[_Q]:Cast()
                return true
            elseif health<= eDmg +  QSecondDamage(0, qTarget) and GetDistance(qTarget) > eRange then
                 spell[_Q]:Cast()
                return true
            elseif ignitedmg > 0 and health<= eDmg +  QSecondDamage(eDmg, qTarget) + ignitedmg and GetDistance(qTarget) <= eRange then
                CastSpell(ignite, qTarget)
                table.insert(extraDamage, {dmg = ignitedmg, dtarget = qTarget, time = GetGameTimer() + 5})
                spell[_E]:Cast()
                spell[_Q]:Cast()
                return true
            elseif ignitedmg > 0 and health<= eDmg +  QSecondDamage(0, qTarget) + ignitedmg and GetDistance(qTarget) > eRange then
                CastSpell(ignite, qTarget)
                table.insert(extraDamage, {dmg = ignitedmg, dtarget = qTarget, time = GetGameTimer() + 5})
                spell[_Q]:Cast()
                return true
            end
        end
 
        if spell[_R]:Ready() == 1 then
            if health <= rDmg + QSecondDamage(rDmg, qTarget) and GetDistance(qTarget) <= rRange then
                spell[_R]:Cast(qTarget)
                spell[_Q]:Cast()
                 return true
            elseif health<= rDmg + QSecondDamage(0, qTarget) and GetDistance(qTarget) > rRange then
                spell[_Q]:Cast()
                return true
            elseif ignitedmg > 0 and health<= rDmg + QSecondDamage(rDmg, qTarget) + ignitedmg and GetDistance(qTarget) <= rRange then
                CastSpell(ignite, qTarget)
                table.insert(extraDamage, {dmg = ignitedmg, dtarget = qTarget, time = GetGameTimer() + 5})
                spell[_R]:Cast(qTarget)
                spell[_Q]:Cast()
                return true
            elseif ignitedmg > 0 and health<= rDmg + QSecondDamage(0, qTarget) + ignitedmg and GetDistance(qTarget) > rRange and GetDistance(qTarget) <= 600 then
                CastSpell(ignite, qTarget)
                table.insert(extraDamage, {dmg = ignitedmg, dtarget = qTarget, time = GetGameTimer() + 5})
                spell[_Q]:Cast()
                return true
            end
        end
 
        if spell[_E]:Ready() == 1 and spell[_R]:Ready() == 1 and 80 <= myHero.mana then
            if health<= eDmg + rDmg + QSecondDamage(eDmg+rDmg, qTarget)  and GetDistance(qTarget) <= eRange then
                spell[_E]:Cast()
                spell[_R]:Cast(qTarget)
                spell[_Q]:Cast()
                return true
            elseif health<= eDmg +  QSecondDamage(rDmg, qTarget) + rDmg and GetDistance(qTarget) <= rRange then
                spell[_R]:Cast(qTarget)
                spell[_Q]:Cast()
                return true
            elseif health<= eDmg +  QSecondDamage(0, qTarget) + rDmg and GetDistance(qTarget) > rRange then
                spell[_Q]:Cast()
                return true
            elseif ignitedmg > 0 and health<= eDmg +  QSecondDamage(eDmg+rDmg, qTarget) + rDmg + ignitedmg and GetDistance(qTarget) <= eRange then
                CastSpell(ignite, qTarget)
                table.insert(extraDamage, {dmg = ignitedmg, dtarget = qTarget, time = GetGameTimer() + 5})
                spell[_E]:Cast()
                spell[_R]:Cast(qTarget)
                spell[_Q]:Cast()
                return true
            elseif ignitedmg > 0 and health<= eDmg +  QSecondDamage(rDmg, qTarget) + rDmg + ignitedmg and GetDistance(qTarget) <= rRange then
                CastSpell(ignite, qTarget)
                table.insert(extraDamage, {dmg = ignitedmg, dtarget = qTarget, time = GetGameTimer() + 5})
                spell[_R]:Cast(qTarget)
                spell[_Q]:Cast()
                return true
            elseif ignitedmg > 0 and health<= eDmg +  QSecondDamage(0, qTarget) + rDmg + ignitedmg and GetDistance(qTarget) > rRange and GetDistance(qTarget) <= 600 then
                CastSpell(ignite, qTarget)
                table.insert(extraDamage, {dmg = ignitedmg, dtarget = qTarget, time = GetGameTimer() + 5})
                spell[_Q]:Cast()
                return true
            end
        end
    end
end
 
function ComboHasNoQ()
    for i, target in ipairs(enemyHeroes) do
 
        if OrbWalking.CanAttack() then
            local rDmg = getDmg("R", target, player)
            local qDmg = getDmg("Q", target, player, 1)
            local aDmg = getDmg("AD",target, player)
            local eDmg = getDmg("E", target, player)
 
            local onhitDmg = aDmg
            local ignitedmg = 0
 
            if ignite ~= nil and  myHero:CanUseSpell(ignite) == READY then ignitedmg = getDmg("IGNITE", target, player) end
 
            local reg = 0
            local extra = 0
            for i, damage in pairs(extraDamage) do
                if damage.time > GetGameTimer() then
                    if target.networkID == damage.dtarget then
                    extra = extra + (math.floor((damage.dmg/5)*(damage.time - GetGameTimer())) - (damage.dmg/5))
                    extra = extra - ((target.hpRegen/5) * (1 - (damage.time - GetGameTimer())))
                    end
                else
                    table.remove(extraDamage, i)
                    i = i - 1
                end
            end
 
            local health = target.health + (((target.hpRegen/5) * 3) - extra) + 50
            if health < 0 then health = math.huge end
 
            if spell[_E]:Ready() == 1 then
                if GetDistance(target) <= eRange then
                    if health<= eDmg then spell[_E]:Cast() return true
                    elseif ignitedmg > 0 and health <= eDmg + ignitedmg then spell[_E]:Cast() CastSpell(ignite, target) table.insert(extraDamage, {dmg = ignitedmg, dtarget = target, time = GetGameTimer() + 5}) return true end
                elseif spell[_W]:Ready() == 1 and GetDistance(target) <= 700 + eRange - 100 and (health<= eDmg or (ignitedmg > 0 and health <= eDmg + ignitedmg)) then
                    local JCheck, obj = ComboJump(target,  eRange - 100)
                    if JCheck and GetDistance(obj) < 700 then
                        spell[_W]:Cast(obj)
                        return
                    end
                end
            end
 
            if spell[_R]:Ready() == 1 then
                if GetDistance(target) < rRange then
                    if health<= rDmg then spell[_R]:Cast(target) return true end
                    if ignitedmg > 0 and health<= rDmg + ignitedmg then CastSpell(ignite, target) spell[_R]:Cast(target) table.insert(extraDamage, {dmg = ignitedmg, dtarget = target, time = GetGameTimer() + 5}) return true end
                elseif spell[_W]:Ready() == 1 and GetDistance(target) <= 700 + rRange - 100 and (health<= rDmg or (ignitedmg > 0 and health<= rDmg + ignitedmg)) then
                    local JCheck, obj = ComboJump(target, rRange - 100)
                    if JCheck and GetDistance(obj) < 700 then
                        spell[_W]:Cast(obj)
                        return
                    end
                end
            end
 
            if spell[_E]:Ready() == 1 and spell[_R]:Ready() == 1 then
                if GetDistance(target) <= eRange then
                    if health<= eDmg + rDmg then spell[_E]:Cast() spell[_R]:Cast(target) return true
                    elseif ignitedmg > 0 and health <= eDmg + rDmg + ignitedmg then CastSpell(ignite, target) spell[_E]:Cast() spell[_R]:Cast(target) table.insert(extraDamage, {dmg = ignitedmg, dtarget = target, time = GetGameTimer() + 5}) return true end
                elseif spell[_W]:Ready() == 1 and GetDistance(target) <= 700 + eRange - 100  and (health<= eDmg + rDmg or (ignitedmg > 0 and health <= eDmg + rDmg + ignitedmg)) then
                    local JCheck, obj = ComboJump(target, eRange - 100)
                    if JCheck and GetDistance(obj) < 700 then
                        spell[_W]:Cast(obj)
                        return
                    end
                end
            end
 
 
            if spell[_Q]:Ready() == 1 then
                if health <= qDmg + ignitedmg and 50 <= myHero.mana then CastQ(myHero, target) return true end
                if health <= qDmg + QSecondDamage(qDmg, target) + ignitedmg and 80 <= myHero.mana then CastQ(myHero, target) return true end
 
                if spell[_E]:Ready() == 1 then
                    if health<= qDmg + eDmg + ignitedmg and 100 <= myHero.mana and GetDistance(target) < eRange then spell[_E]:Cast() CastQ(myHero, target) return true end
                    if health<= qDmg + eDmg + QSecondDamage(qDmg + eDmg, target) + ignitedmg and 130 <= myHero.mana and GetDistance(target) < eRange then spell[_E]:Cast() CastQ(myHero, target) return true end
                    if health<= qDmg + eDmg + QSecondDamage(qDmg, target) + ignitedmg and 130 <= myHero.mana and GetDistance(target) >= eRange then CastQ(myHero, target) return true end
                end
                if spell[_R]:Ready() == 1 then
                    if health<= qDmg + rDmg + ignitedmg and 50 <= myHero.mana and GetDistance(target) < rRange then CastQ(myHero, target) return true end
                    if health<= qDmg + rDmg + QSecondDamage(qDmg + rDmg, target) + ignitedmg and 80 <= myHero.mana and GetDistance(target) < rRange then CastQ(myHero, target) return true end
                    if health<= qDmg + rDmg + QSecondDamage(qDmg, target) + ignitedmg and 80 <= myHero.mana and GetDistance(target) < rRange then CastQ(myHero, target) return true end
                end
                if  spell[_E]:Ready() == 1 and spell[_R]:Ready() == 1 then
                    if health<= qDmg+eDmg+rDmg + QSecondDamage(qDmg+eDmg, target) + ignitedmg then  CastQ(myHero, target) return true end
                    if health<= qDmg+eDmg+rDmg + QSecondDamage(qDmg+rDmg, target) + ignitedmg then CastQ(myHero, target) return true end
                    if health<= qDmg+eDmg+rDmg + QSecondDamage(qDmg+eDmg+rDmg, target) + ignitedmg then  CastQ(myHero, target) return true end
                end
            end
        end
    end
    return false
end
 
function Combo()
    AutoPassive()
end
 
function InSecCombo()
    if not ConfigBasic.Insec then return false end
 
    if myHero:GetSpellData(_R).level < 1 then Message.AddMassage("����: R�ȼ�̫��.", ColorARGB.Red) return false end
 
    champs = Heroes({team = {TEAM_ENEMY}, range = 2000})
    table.sort(champs, InsecSort)
    local target
    local flashtarget = nil
 
    if selected and selected.bTargetable and not selected.dead then
        target = selected
        flashtarget = selected
    elseif #champs > 0 then
        target = champs[1]
    elseif target == nil then
        Message.AddMassage("����: �����޵���.", ColorARGB.Red)
        return false
    else
        Message.AddMassage("����: �����޵���.", ColorARGB.Red)
        return false
    end
 
    if spell[_R]:Ready() ~= 1 then Message.AddMassage("����: R��ȴ��", ColorARGB.Red)  return false end
    local wardPos = GetWardPosition(target)
    if wardPos == nil then return false end
 
    if GetDistance(wardPos) < 200 then spell[_R]:Cast(target) return true end
 
    if spell[_Q]:Ready(2) == 1 and spell[_W]:Ready() == 1 and 80 <= myHero.mana and qTarget ~= nil and GetDistance(qTarget, wardPos) <= 600 and GetDistance(wardPos) > 600 then
        if Jumper.GetWardSlot() ~= nil or (InsecFlash() and GetDistance(qTarget, wardPos) <= 400) then
            spell[_Q]:Cast(nil,nil, 2)
            return true
        end
    end
 
    if spell[_Q]:Ready() == 1 and spell[_W]:Ready() == 1 and 130 <= myHero.mana and qTarget == nil and GetDistance(wardPos) > 600 then
        local bCheck, pos = CanCastQ(myHero, target)
        if bCheck then spell[_Q]:Cast(pos.x,pos.z) return true end
    end
 
    if spell[_W]:Ready() == 1 and GetDistance(wardPos) <= 600 and 130 <= myHero.mana then
        local jCheck, obj = CheckObject(wardPos, 200)
        if jCheck then
            if GetDistance(obj) <= 600 then
                spell[_W]:Cast(obj)
                return true
            end
        else
            if Jumper.GetWardSlot() ~= nil then
                Jumper.PlaceWard(wardPos.x, wardPos.z)
                return true
            end
        end  
    elseif InsecFlash() and GetDistance(wardPos) <= 400 and dash[_W] ~= nil then  
        CastSpell(flash, wardPos.x, wardPos.z)
        spell[_R]:Cast(target)
    end
 
end
 
function InsecFlash()
    return ConfigBasic.Insecflash and flash and myHero:CanUseSpell(flash) == READY
end
 
 
function CollisionKiller()
    if not ConfigKillCombo.rcollision then return true end
    for i, target in ipairs(enemyHeroes) do
        if GetDistance(target) < 375 then
            for i, unit in ipairs(enemyHeroes) do
                local rDmg = getDmg("R", target, player)
                if  unit.networkID ~= target.networkID and unit.health < rDmg and GetDistance(target,unit) < 700 then
                    local QPrediction = VIP_USER and TargetPredictionVIP(1000, 1250, 0.25, getHitBox(target), target) or TargetPredictionNONEVIP(1000, 1250, 0.25, getHitBox(target), target)
                    local pos, t, vec = QPrediction:GetPrediction(unit)
                    if pos then
                        local V = Vector(target) - Vector(myHero)
                        local k = V:normalized()
                        local P = V:perpendicular2():normalized()
 
                        local t,i,u = k:unpack()
                        local x,y,z = P:unpack()
                       
                        local point1 = target.x + (x * (getHitBox(target))) + (t * 1000)
                        local point2 = target.y + (y * (getHitBox(target))) + (i * 1000)
                        local point3 = target.z + (z * (getHitBox(target))) + (u * 1000)
 
                        local point4 = target.x + (x *(getHitBox(target))) + (t * getHitBox(target))
                        local point5 = target.y + (y *(getHitBox(target))) + (i * getHitBox(target))
                        local point6 = target.z + (z * (getHitBox(target)))  + (u *getHitBox(target))
 
                        local lpoint1 = target.x - (x * (getHitBox(target))) + (t * 1000)
                        local lpoint2 = target.y - (y * (getHitBox(target)))+ (i * 1000)
                        local lpoint3 = target.z - (z * (getHitBox(target)))+ (u * 1000)
 
                        local lpoint4 = target.x - (x * (getHitBox(target))) + (t * getHitBox(target))
                        local lpoint5 = target.y - (y * (getHitBox(target))) + (i * getHitBox(target))
                        local lpoint6 = target.z - (z * (getHitBox(target))) + (u * getHitBox(target))
 
                        local c = WorldToScreen(D3DXVECTOR3(point1, point2, point3))
                        local l = WorldToScreen(D3DXVECTOR3(point4, point5, point6))
                        local cl = WorldToScreen(D3DXVECTOR3(lpoint1, lpoint2, lpoint3))
                        local ll = WorldToScreen(D3DXVECTOR3(lpoint4, lpoint5, lpoint6))
 
                        local poly = Polygon(Point(c.x, c.y),  Point(l.x, l.y), Point(cl.x, cl.y),   Point(ll.x, ll.y))
 
               
                        local b = WorldToScreen(D3DXVECTOR3(pos.x, unit.y, pos.z))
                        local po = Point(b.x, b.y)
                        if poly:contains(po) and GetDistance(target) < 550 then
                            CastSpell(_R, target)
                            return true
                        end
                    end
                end
            end
        end
    end
    return false
end
 
function Herras()
    if not ConfigBasic.Harass then return false end
 
    if spell[_Q]:Ready(2) == 1 and 80 <= myHero.mana and qTarget ~= nil then
        if qTarget.type == myHero.type then
            local jCheck, objs = CanJump(target)
            if jCheck then
                spell[_Q]:Cast()
                return
            end
        end
    end
 
    for i, target in ipairs(enemyHeroes) do
        if spell[_Q]:Ready() == 1 and spell[_W]:Ready() == 1 and 120 <= myHero.mana and qTarget == nil then
            local bCheck, pos = CanCastQ(myHero, target)
            if bCheck then
                local jCheck, obj = CanJump(target)
                if jCheck then
                    spell[_Q]:Cast(pos.x, pos.z)
                    return
                end
            end
        end
 
        if spell[_Q]:Ready(1) == 0 and spell[_Q]:IsCasted(2) and spell[_W]:Ready() == 1 and not dash[_Q] and GetDistance(target) < 150 then
            local jCheck, objs = CanJump(target)
            if jCheck then
                 spell[_W]:Cast(objs)
                 return
            end
        end
    end
end
 
function Farm()
    if not ConfigFarm.Farm then ssLastHitting:Enable(false) return end
 
    if ConfigFarm.minions then  ssLastHitting:Enable(true)
    else ssLastHitting:Enable(false) end
 
    if ConfigFarm.jungle then
        jungle = Minions({team = {TEAM_NEUTRAL}, range = 450})
        table.sort(jungle, function(x, y)
            local aDmg = x:CalcDamage(myHero,x.totalDamage)
            local bDmg = y:CalcDamage(myHero,y.totalDamage)
            if aDmg == bDmg then return x.health < y.health end
            return aDmg > bDmg
         end)
        for i, target in ipairs(jungle) do
    --PrintChat(tostring(target.parType))
            if GetDistance(target) <= myHero.range + getHitBox(target) + 150 then
                if OrbWalking.Attack(target) then
                    return true
                end
 
                if spell[_E]:Ready() == 2 or spell[_Q]:Ready() == 2 or spell[_W]:Ready() == 2 then return false end
 
                if spell[_E]:Ready() == 1 and HaveNoStack() and GetDistance(target) < 350 + getHitBoxRadius(target) then
                    spell[_E]:Cast()
                    return true
                end
 
                if spell[_E]:Ready() == 2 or spell[_Q]:Ready() == 2 or spell[_W]:Ready() == 2 then return false end
                if AutoPassiveE() then return true end
 
 
                if spell[_E]:Ready(2) == 2 or spell[_Q]:Ready() == 2 then return false end
                if spell[_Q]:Ready() == 1 and passive.stack == 0 and GetDistance(target) > 100 then
                    spell[_Q]:Cast(target.x, target.z)
                    return true
                end
 
                if spell[_Q]:Ready(2) == 1 and ((passive.stack == 0 or GetGameTimer() - spell[_Q].starttime > 3 - Delay()) or (qTarget and qTarget.health < QSecondDamage(0, qTarget))) then
                    spell[_Q]:Cast()
                    return true
                end  
 
                if spell[_E]:Ready() == 2 or spell[_Q]:Ready() == 2 or spell[_W]:Ready() == 2 then return false end
 
                if spell[_W]:Ready() == 1 and passive.stack == 0 then
                    spell[_W]:Cast(myHero)
                    return true
                end
 
                if spell[_W]:Ready(2) == 1 and (passive.stack == 0 or GetGameTimer() - spell[_W].starttime > 3 - Delay()) then
                    spell[_W]:Cast()
                    return true
                end
            end
        end
    end
end
 
-- Functions -------------------------------------------------------------------------
function AutoAll()
    if not ConfigCombo.combo and not ConfigFarm.Farm then OrbWalking.Enable(false) ssLastHitting:Enable(false) return end
    OrbWalking.Enable(true)
 
    if ConfigCombo.combo then
        ssLastHitting:Enable(false)
        if selected and selected.bTargetable and not selected.dead then
            if not IsValid(selected) then return false end
            if ConfigCombo.useAA and GetDistance(selected) <= myHero.range + getHitBox(selected) + 50  and OrbWalking.Attack(selected) then
                return
            end
            if spell[_W]:Ready() == 2 or spell[_Q]:Ready() == 2 or spell[_E]:Ready() == 2 then return end
            if ConfigCombo.useE then AutoE(selected) end
            if ConfigCombo.useW then AutoW(selected) end
            if ConfigCombo.useQ then AutoQ(selected) end
        else    
            for i, target in ipairs(enemyHeroes) do
                if IsValid(target) then
                    if ConfigCombo.useAA and GetDistance(target) <= myHero.range + getHitBox(target)+ 50  and OrbWalking.Attack(target) then
                        return
                    end
                    if spell[_W]:Ready() == 2 or spell[_Q]:Ready() == 2 or spell[_E]:Ready() == 2 then return end
                    if ConfigCombo.useE then AutoE(target) end
                    if ConfigCombo.useQ then AutoQ(target) end  
                    if ConfigCombo.useW then AutoW(target) end
                end
            end
        end
    else
        Farm()
    end
end
 
 
function AutoPassive()
    if not ConfigCombo.combo then return end
 
    if spell[_W]:Ready(2) == 2 or spell[_Q]:Ready(2) == 2 or spell[_E]:Ready(2) == 2 then return end
 
    if ConfigCombo.autoE then AutoPassiveE() end
    if ConfigCombo.autoW then AutoPassiveW() end
    if ConfigCombo.autoQ then AutoPassiveQ() end
end
 
function AutoPassiveQ()
    if qTarget == nil or qTarget.type ~= "obj_AI_Hero" then return end
 
    if spell[_Q]:Ready(2) == 1 then
        if HaveNoStack() or GetGameTimer() - spell[_Q].starttime > 3 - Delay() then
            spell[_Q]:Cast()
        elseif GetDistance(qTarget) > myHero.range + 250 + getHitBox(qTarget) then
            spell[_Q]:Cast()
        end
    end      
end
 
function AutoPassiveW()
    if spell[_W]:Ready(2) == 1 and (HaveNoStack() or GetGameTimer() - spell[_W].starttime > 3 - Delay()) then
        spell[_W]:Cast()
    end        
end
 
function AutoPassiveE()
    if spell[_E]:Ready(2) == 1 and (HaveNoStack() or GetGameTimer() - spell[_E].starttime > 3 - Delay()) then
        spell[_E]:Cast()
    end        
 
    for i, unit in pairs(eStack) do
        if unit and spell[_E]:Ready(2) == 1 and unit.valid and not unit.dead and unit.type == myHero.type and GetDistance(unit) > myHero.range + 100 + getHitBox(unit) then
            spell[_E]:Cast()
        end
    end
 
    if spell[_E]:Ready() == 1 then
        for i, unit in pairs(eStack) do
            unit = nil
        end
    end
end
 
function AutoE(target)
    if spell[_E]:Ready() == 1 and GetDistance(target) < 300 + getHitBox(target) and GetDistance(target) > myHero.range + 100 + getHitBox(target) then
        spell[_E]:Cast()
    elseif spell[_E]:Ready() == 1 and HaveNoStack() and GetDistance(target) < 300 + getHitBoxRadius(target) then
        spell[_E]:Cast()
    end
end
 
function AutoW(target)
    if spell[_W]:Ready() == 1 and HaveNoStack() and GetDistance(target) < 250 + getHitBox(target) then
        spell[_W]:Cast()
    end
end
 
function AutoQ(target)
    if spell[_Q]:Ready() == 1 and (HaveNoStack() or GetDistance(target) >  myHero.range +  getHitBox(target)) then
        CastQ(myHero, target)
    end
end
 
function AutoR(target)
    local ultDmg = getDmg("R",target,myHero)
    if ultDmg>=target.health then
        if myHero:CanUseSpell(_R) == READY and GetDistance(target)<=rRange then
            spell[_R]:Cast(target)
        end
    end
end
 
function HaveNoStack()
    local canAttack = OrbWalking.CanAttack()
    return passive.stack == 0 or (canAttack and GetGameTimer() > passive.endT - Delay()) or (canAttack and (passive.endT - Delay()) - GetGameTimer() < OrbWalking.WindUpTime()) or (not canAttack and (passive.endT-Delay()) < (OrbWalking.NextAttack() + OrbWalking.WindUpTime()))
end
 
function Delay()
    return GetLatency()/2000 + 0.08    
end
 
function GetWardPosition(target)
    local tempLocation = nil
 
    local Prediction = VIP_USER and TargetPredictionVIP(2000, math.huge, 0.250) or TargetPredictionNONEVIP(2000, math.huge, 0.250)
    local pos, t, vec = Prediction:GetPrediction(target)
    if pos ~= nil and Prediction:GetHitChance(target) > 0.6 then
 
    else
        pos = target
    end
        local allies = Heroes({team = {myHero.team}, range = 1200}) + Turrets({team = {myHero.team}})          --Turrets.GetObjects(ALLY, 1500, pos)
        table.sort(allies, function(x,y) return GetDistance(x, pos) < GetDistance(y, pos) end)
 
        if #allies > 0 then
            local unit = allies[1]
            if allies[1].isMe then
                if #allies > 1 then
                    unit = allies[2]
                else
                    return nil
                end
            end
 
            local x,y,z = (Vector(pos) - Vector(unit)):normalized():unpack()
            local posX = pos.x + (x * 250)
            local posY = pos.y + (y * 250)
            local posZ = pos.z + (z * 250)
            tempLocation = Vector(posX, posY, posZ)
            return tempLocation
        end
    return nil
end
 
function CheckObject(target, range)
    local tempRange = range or 700
    allys = Heroes({team = {myHero.team}, range = tempRange, source = target}) + Minions({team = {myHero.team}, range = tempRange, source = target}) --Heroes.GetObjects(ALLY, tempRange, target)
    table.sort(allys, function(x,y) return GetDistance(x) < GetDistance(y) end)
    for i, ally in ipairs(allys) do
        if not ally.isMe then
            return true, ally
        end
    end
    return false, nil
end
 
function CanJump(target, range)
    local tempRange = range or 700
    allys = Heroes({team = {myHero.team}, range = tempRange, source = target}) + Minions({team = {myHero.team}, range = tempRange, source = target})
    table.sort(allys, function(x,y) return GetDistance(x) > GetDistance(y) end)
    for i, ally in ipairs(allys) do
        if not ally.isMe and GetDistance(ally, target) > 300 then
            return true, ally
        end
    end
    return false, nil
end
 
function ComboJump(target, range)
    local tempRange = 700
    allys = Heroes({team = {myHero.team}, range = tempRange}) + Minions({team = {myHero.team}, range = tempRange})
    table.sort(allys, function(x,y) return GetDistance(x, target) < GetDistance(y, target) end)
    for i, ally in pairs(allys) do
        if not ally.isMe and GetDistance(ally, target) < range then
            return true, ally
        end
    end
    return false, nil
end
 
function BestDmgCombo(target, mana, dmg, tables, added)
    local endtable = {}
    for i, spells in ipairs(tables) do
        local tempadded = table.copy(added)
        local damage = dmg + spells.damage(target, target.health-dmg)
        local m = mana - spells.mana
 
        if GetDistance(target, tables.range) <= spells.range then
            local temptable = table.copy(tables)
            if #temptable > 0 and target.health-1300 > damage and myHero.mana + m >= 0 then
                if temptable[i].afterRange ~= 0 then
                    temptable.range = target
                end
                table.remove(temptable, i)
                table.insert(tempadded, spells)
                endtable = joinTables(endtable, BestDmgCombo(target, m, damage, temptable, tempadded))
            end
 
            if target.health-1300 < damage and myHero.mana + m >= 0 then table.insert(endtable, {dmg = damage, table = tempadded}) end
        end
    end
    return endtable
end
 
function joinTables(t1, t2)
        for k,v in ipairs(t2) do table.insert(t1, v) end return t1
end
 
function QSecondDamage(dmg, target)
    if dmg == nil then dmg = 0 end
    local predHealth = target.health - dmg
    local Qdmg = ((myHero:GetSpellData(_Q).level*30) + 20) + (myHero.addDamage*0.9) + (0.08*(target.maxHealth-predHealth))
    return myHero:CalcDamage(target, Qdmg)
end
 
function CastQ(from, to)
    if  myHero:GetSpellData(_Q).mana < myHero.mana then
        local QPrediction =  VIP_USER and TargetPredictionVIP(qRange, myHero:GetSpellData(_Q).missileSpeed, 0.250, myHero:GetSpellData(_Q).lineWidth, from) or TargetPredictionNONEVIP(qRange, myHero:GetSpellData(_Q).missileSpeed, 0.250, myHero:GetSpellData(_Q).lineWidth, from)
        
        local pos, t, vec = QPrediction:GetPrediction(to)
        if pos == nil then pos = to end
 
        if pos ~= nil and GetDistance(pos, from)<=qRange and not collition:GetMinionCollision(pos, from) and QPrediction:GetHitChance(to) > ConfigBasic.Prediction/100 then
            spell[_Q]:Cast(pos.x, pos.z)
            return true
        end
    end
    return false
end
 
function CanCastQ(from, to)
    local QPrediction = VIP_USER and TargetPredictionVIP(qRange, myHero:GetSpellData(_Q).missileSpeed, 0.250, myHero:GetSpellData(_Q).lineWidth, from) or TargetPredictionNONEVIP(qRange, myHero:GetSpellData(_Q).missileSpeed, 0.250, myHero:GetSpellData(_Q).lineWidth, from)
    local pos, t, vec = QPrediction:GetPrediction(to)
    if pos == nil then pos = to end
 
 
    if pos ~= nil and GetDistance(pos, from)<=qRange and not collition:GetMinionCollision(pos, from) then
        return true, pos
    end
    return false, nil
end
if myHero.charName ~= "Taric" then return end
 
--[[
        Taric: the Family Jewels
        by Tux
--]]
 
local kill = {}
local wDmg
local eDmg
local rDmg
local ts = TargetSelector(TARGET_LOW_HP,625,DAMAGE_MAGIC,true)
local items =
{
DFG = {id=3128, range = 750, reqTarget = true, slot = nil },
}
 
function OnLoad()
        TaricConfig = scriptConfig("��ʯ��ʿ", "TaricCombo")
        TaricConfig:addParam("Active", "����", SCRIPT_PARAM_ONKEYDOWN, false, 32)
        TaricConfig:addParam("Harass", "ɧ��", SCRIPT_PARAM_ONKEYDOWN, false, string.byte("T"))
        TaricConfig:addParam("KS", "�Զ�����ͷ", SCRIPT_PARAM_ONOFF, true)
        TaricConfig:addParam("Movement", "��������ƶ�", SCRIPT_PARAM_ONOFF, true)
        TaricConfig:addParam("DrawCircles", "��ʾ��Χ", SCRIPT_PARAM_ONOFF, true)
        TaricConfig:permaShow("Active")
        TaricConfig:permaShow("Harass")
        ts.name = "Taric"
        TaricConfig:addTS(ts)
        PrintChat(">> Taric - the Family Jewels v1.0 loaded.")
end
 
function CanCast(Spell)
        return (player:CanUseSpell(Spell) == READY)
end
 
function getHitBoxRadius(target)
 return GetDistance(target.minBBox, target.maxBBox)/2
end
 
function UseItems(target)
        if target == nil then return end
                for _,item in pairs(items) do
                        item.slot = GetInventorySlotItem(item.id)
                        if item.slot ~= nil then
                                if item.reqTarget and GetDistance(target) < item.range then
                                        CastSpell(item.slot, target)
                                elseif not item.reqTarget then
                                        if (GetDistance(target) - getHitBoxRadius(myHero) - getHitBoxRadius(target)) < 50 then
                                                CastSpell(item.slot)
                                        end
                                end
                        end
        end
end
 
function AutoKS()
    for i=1, heroManager.iCount do
    target = heroManager:GetHero(i)
        wDmg = getDmg("W", target, player)
        eDmg = getDmg("E", target, player)
        rDmg = getDmg("R", target, player)
                if target ~= nil and not target.dead and target.team ~= player.team and target.visible and GetDistance(target) < 625 then
                        if target.health < eDmg and CanCast(_E) then
                                CastSpell(_E, target)
                        end
                end
                                if target ~= nil and not target.dead and target.team ~= player.team and target.visible and GetDistance(target) < 300 then
                                                if target.health < wDmg and CanCast(_W) then
                                                        CastSpell(_W, target)
                                                end
                                                        if target.health < rDmg  and CanCast(_R)then
                                                                CastSpell(_R, target)
                                                        end
                                end
                if target ~= nil and not target.dead and target.team ~= player.team and target.visible and GetDistance(target) < 625 then
                        if target.health < eDmg + rDmg + wDmg and CanCast(_E) and CanCast(_R) and CanCast(_W) then
                                CastSpell(_E, target)
                                myHero:Attack(target)
                                if target ~= nil and not target.dead and target.team ~= player.team and target.visible and GetDistance(target) < 300 then
                                        CastSpell(_R, target)
                                        CastSpell(_W, target)
                                end
                        end
                end
                if target ~= nil and not target.dead and target.team ~= player.team and target.visible and GetDistance(target) < 625 then
                        if target.health < eDmg + rDmg and CanCast(_E) and CanCast(_R) then
                                CastSpell(_E, target)
                                myHero:Attack(target)
                                if target ~= nil and not target.dead and target.team ~= player.team and target.visible and GetDistance(target) < 300 then
                                        CastSpell(_R, target)
                                end
                        end
                end
                if target ~= nil and not target.dead and target.team ~= player.team and target.visible and GetDistance(target) < 625 then
                        if target.health < eDmg + wDmg and CanCast(_E) and CanCast(_W) then
                                CastSpell(_E, target)
                                myHero:Attack(target)
                                if target ~= nil and not target.dead and target.team ~= player.team and target.visible and GetDistance(target) < 300 then
                                        CastSpell(_W, target)
                                end
                        end
                end
                if target ~= nil and not target.dead and target.team ~= player.team and target.visible and GetDistance(target) < 300 then
                        if target.health < wDmg + rDmg and CanCast(_W) and CanCast(_R) then
                                CastSpell(_W, target)
                                CastSpell(_R, target)
                        end
                end
        end
end
 
function OnTick()
    ts:update()
        Damage()
        if TaricConfig.KS then AutoKS() end
                if TaricConfig.Active then
                        if ValidTarget(ts.target, 625) and CanCast(_E) then
                                CastSpell(_E, ts.target)
                                myHero:Attack(ts.target)
                        end
                                if ValidTarget(ts.target, 300) and CanCast(_R) then
                                        UseItems(ts.target)
                                        CastSpell(_R, ts.target)
                                end
                                        if ValidTarget(ts.target, 300) and CanCast(_W) then
                                                CastSpell(_W, ts.target)
                                        end
                end
        if TaricConfig.Harass then
        if ValidTarget(ts.target, 625) and CanCast(_E) then
            CastSpell(_E, ts.target)
            myHero:Attack(ts.target)
        end
            if ValidTarget(ts.target, 300) and CanCast(_W) then
                CastSpell(_W, ts.target)
            end
    end
    if TaricConfig.Movement and (TaricConfig.Active or TaricConfig.Harass) and ts.target == nil then myHero:MoveTo(mousePos.x, mousePos.z)
    end
end
 
function Damage()
        ts:update()
                for i=1, heroManager.iCount do
                        local dfgdamage = 0
                        local enemy = heroManager:GetHero(i)
                        local wdmg = getDmg("W",enemy,myHero)
                        local edmg = getDmg("E",enemy,myHero)
                        local rdmg = getDmg("R",enemy,myHero)
                        local dfgdamage = (DFG and getDmg("DFG",enemy,myHero) or 0)
                        local possible = edmg + wdmg + rdmg + dfgdamage
                        local gemkill = 0
                        if myHero:CanUseSpell(_E) then
                                gemkill = gemkill + edmg
                        end
                        if myHero:CanUseSpell(_W) then
                                gemkill = gemkill + wdmg
                        end
                        if myHero:CanUseSpell(_R) then
                                gemkill = gemkill + rdmg
                        end
                        if UseItems then      
                                gemkill = gemkill + dfgdamage
                        end
                        if gemkill >= enemy.health then
                                kill[i] = 2
                        elseif possible >= enemy.health then
                                kill[i] = 1
                        else
                                kill[i] = 0
                        end
                end
end
 
function OnDraw()
        if TaricConfig.DrawCircles then
                DrawCircle(myHero.x,myHero.y,myHero.z,400,0xFFFF0000)
                DrawCircle(myHero.x,myHero.y,myHero.z,625,0xFFFF0000)
                for i=1, heroManager.iCount do
                        local enemydraw = heroManager:GetHero(i)
                        if ValidTarget(enemydraw) then
                                if kill[i] == 1 then
                                        PrintFloatText(enemydraw,0,"Wait on Cooldowns")
                                        DrawCircle(enemydraw.x,enemydraw.y,enemydraw.z, 100, 0xFF80FF00)
                                        DrawCircle(enemydraw.x,enemydraw.y,enemydraw.z, 150, 0xFF80FF00)
                                elseif kill[i] == 2 then
                                        PrintFloatText(enemydraw,0,"Give'em the Jewels!")
                                        DrawCircle(enemydraw.x,enemydraw.y,enemydraw.z, 100, 0xFF80FF00)
                                        DrawCircle(enemydraw.x,enemydraw.y,enemydraw.z, 150, 0xFF80FF00)
                                        DrawCircle(enemydraw.x,enemydraw.y,enemydraw.z, 200, 0xFF80FF00)      
                                end
                        end
                end
        end
end